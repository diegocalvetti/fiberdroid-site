<?php


namespace App\View\Components;


use Illuminate\Support\Str;
use Illuminate\View\Component;

abstract class BaseLink extends Component
{
    protected function to($to, $params = []) {
        if( Str::startsWith($to, ["http", "https", "www", "tel", "mailto"]) ){

            return $to;

        } else if( Str::startsWith($to, "#") ) {

            return url()->current().$to;

        } else if( Str::contains($to, "/") ) {

            return url($to);

        } else {

            return route($to, $params);

        }
    }
}
