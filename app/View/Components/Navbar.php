<?php

namespace App\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Navbar extends Component
{
    public $paddingX;
    public $bg;

    public $link_servizi;
    public $link_prodotti;
    public $link_soluzioni;

    /**
     * Create a new component instance.
     *
     * @param string $paddingX
     * @param string $bg
     * @param array $link_servizi
     * @param array $link_prodotti
     * @param array $link_soluzioni
     */
    public function __construct($paddingX = "5", $bg = "fd-black", $link_servizi = [], $link_prodotti = [], $link_soluzioni = [])
    {
        $this->paddingX = $paddingX;
        $this->bg = $bg;
        $this->link_prodotti = $link_prodotti;
        $this->link_soluzioni = $link_soluzioni;
        $this->link_servizi = $link_servizi;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.nav.navbar');
    }
}
