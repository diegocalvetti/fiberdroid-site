<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Button extends Component
{
    public $bg;
    public $color;
    public $uppercase;
    public $full;
    public $disabled;
    public $wireLoading;

    /**
     * Create a new component instance.
     *
     * @param string $bg
     * @param string $color
     * @param string $uppercase
     * @param string $full
     * @param string $disabled
     * @param string $wire_loading
     */
    public function __construct($bg = "fd-primary", $color = "white", $uppercase = "false", $full = "false", $disabled = "false", $wireLoading = "false")
    {
        $this->bg = $bg;
        $this->color = $color;
        $this->uppercase = filter_var($uppercase, FILTER_VALIDATE_BOOL);
        $this->full = filter_var($full, FILTER_VALIDATE_BOOL);
        $this->disabled = filter_var($disabled, FILTER_VALIDATE_BOOL);
        $this->wireLoading = filter_var($wireLoading, FILTER_VALIDATE_BOOL);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.button.button');
    }
}
