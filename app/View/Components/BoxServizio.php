<?php

namespace App\View\Components;

use Illuminate\View\Component;

class BoxServizio extends Component
{

    public $title;
    public $price;
    public $features;
    public $icon;
    public $size;
    public $id;
    public $sellable;
    public $description;

    /**
     * Create a new component instance.
     *
     * @param $title
     * @param $features
     * @param string $icon
     * @param float $price
     * @param bool $id
     * @param string $size
     * @param bool $sellable
     */
    public function __construct($title, $features, $icon = "servizi/adsl.svg", $price = 0.0, $id = false, $size = "96x96", $sellable = true, $description = "")
    {
        $this->title = $title;
        $this->price = $price;
        $this->features = $features;
        $this->icon = asset("images/$icon");
        $this->id = $id;
        $this->size = $size;
        $this->sellable = $sellable;
        $this->description = $description;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.box-servizio');
    }
}
