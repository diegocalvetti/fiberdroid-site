<?php

namespace App\View\Components;

use Illuminate\View\Component;

class NavbarLink extends BaseLink
{

    public $color;
    public $size;
    public $float;
    public $class;
    public $to;
    public $target;

    /**
     * Create a new component instance.
     *
     * @param string $color
     * @param string $size
     * @param string $class
     * @param string $to
     * @param string $target
     * @param string $float
     */
    public function __construct($color = "white", $size = "is-size-5", $class = "", $to = "", $target = "_self", $float = "false")
    {
        $this->color = $color;
        $this->size = $size;
        $this->float = $float ? $float : filter_var($float,FILTER_VALIDATE_BOOL);
        $this->to = $to != "" ? $this->to($to) : "";
        $this->target = $target;
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.nav.navbar-link');
    }
}
