<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModalResponse extends Component
{

    public $title;
    public $active;
    public $content;

    /**
     * Create a new component instance.
     *
     * @param array $response
     */
    public function __construct($response = [])
    {
        if( is_null($response) ){
            $response = [];
        }

        if( $response != [] ){
            $this->active = true;
            $this->title = $response['title'];
            $this->content = $response['content'];
        } else {
            $this->active = false;
            $this->title = "response vuota";
            $this->content = "response vuota";
        }

        session()->remove('response');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.modal-response');
    }
}
