<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Icon extends Component
{

    public $icon;
    public $isImage = false;
    public $float;
    public $size;
    public $color;
    public $disabled;
    public $class;

    /**
     * Create a new component instance.
     *
     * @param $icon
     * @param bool $float
     * @param string $size
     * @param string $color
     * @param string $disabled
     * @param string $class
     */
    public function __construct($icon, $float = false, $size = "24x24", $color = "white", $disabled = "false", $class = "")
    {
        $this->icon = $icon;
        $this->isImage = strpos($icon, ".") != false;
        $this->float = $float;
        $this->size = filter_var($size, FILTER_VALIDATE_BOOL) ? $size : false;
        $this->color = $color;
        $this->disabled = filter_var($disabled, FILTER_VALIDATE_BOOL);
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.figure.icon');
    }
}
