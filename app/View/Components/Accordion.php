<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Accordion extends Component
{

    public $title;
    public $active;
    public $id;
    public $notIcon;

    /**
     * Create a new component instance.
     *
     * @param $title
     * @param bool $id
     * @param string $active
     */
    public function __construct($title, $id = false, $active = "false")
    {
        $this->title = str_replace("&#039;", "'", $title);
        $this->id = $id;
        $this->active = filter_var($active, FILTER_VALIDATE_BOOL);
        $this->notIcon = false;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.accordion.accordion');
    }
}
