<?php

namespace App\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Row extends Component
{

    public $bg;
    public $margin;
    public $class;
    public $id;
    public $style;
    public $thin;

    /**
     * Create a new component instance.
     *
     * @param string $bg
     * @param string $margin
     * @param string $style
     * @param string $class
     * @param string $id
     * @param string $thin
     */
    public function __construct($bg = "light", $margin = "true", $style = "", $class = "", $id = "", $thin = "false")
    {
        $this->class = $class;
        $this->id = $id;
        $this->bg = $bg;
        $this->style = $style != "" ? $style : false;
        $this->margin = filter_var($margin, FILTER_VALIDATE_BOOL);
        $this->thin = filter_var($thin, FILTER_VALIDATE_BOOL);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.flex.row');
    }
}
