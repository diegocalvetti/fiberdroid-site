<?php

namespace App\View\Components;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class Link extends BaseLink
{
    public $to;
    public $target;
    public $color;
    public $dataAction;
    public $ariaLabel;

    /**
     * Create a new component instance.
     *
     * @param $to
     * @param string $target
     * @param string $color
     * @param array $params
     * @param bool $dataAction
     */
    public function __construct($to, $target = "_self", $color = "white", $params = [], $dataAction = false, $ariaLabel=null)
    {
        $this->to = $this->to($to, $params);
        $this->target = $target;
        $this->color = $color;
        $this->dataAction = $dataAction;
        $this->ariaLabel = $ariaLabel;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.link.link');
    }
}
