<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    public $label;
    public $type;
    public $wireModel;
    public $error;
    public $list;
    public $uname;

    /**
     * Create a new component instance.
     *
     * @param $label
     * @param string $wireModel
     * @param string $type
     * @param bool $error
     * @param bool $list
     * @param string $uname
     */
    public function __construct($label, $wireModel = "", $type = "text", $error = false, $list = false, $uname = "")
    {
        $this->label = $label;
        $this->type = $type;
        $this->wireModel = $wireModel;
        $this->error = $error;
        $this->list = $list;
        $this->uname = $uname;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.input');
    }
}
