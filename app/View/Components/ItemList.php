<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ItemList extends Component
{
    public $color;
    public $gap;

    public function __construct($color = "primary", $gap = "3")
    {
        $this->color = $color;
        $this->gap = $gap;
    }

    public function render()
    {
        return view('components.item-list');
    }
}
