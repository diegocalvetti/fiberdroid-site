<?php

namespace App\View\Components;

use Illuminate\View\Component;

abstract class BaseText extends Component
{
    public $color;
    public $class;
    public $style;
    public $uppercase;
    public $role;

    /**
     * Create a new component instance.
     *
     * @param string $color
     * @param string $class
     * @param string $style
     * @param string $uppercase
     * @param string $role
     */
    public function __construct($color = "black", $class = "", $style = "", $uppercase = "true", $role = "p")
    {
        $this->color = $color;
        $this->class = $class;
        $this->style = $style != "" ? $style : false;
        $this->uppercase = filter_var($uppercase, FILTER_VALIDATE_BOOL);
        $this->role = $role;
    }
}
