<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CheckedList extends Component
{

    public $list;

    /**
     * Create a new component instance.
     *
     * @param $list
     */
    public function __construct($list)
    {
        $this->list = $list;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.list.checked-list');
    }
}
