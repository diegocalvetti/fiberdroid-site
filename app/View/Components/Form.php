<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Form extends Component
{

    public string $uname = "";

    /**
     * Create a new component instance.
     *
     * @param string $uname
     */
    public function __construct($uname = "")
    {
        $this->uname = $uname;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.form');
    }
}
