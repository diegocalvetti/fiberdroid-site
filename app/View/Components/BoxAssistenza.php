<?php

namespace App\View\Components;

use Illuminate\View\Component;

class BoxAssistenza extends Component
{

    public $title;
    public $icon;
    public $mail;
    public $phone;
    public $contact;

    public function __construct($title, $mail = "", $phone = "", $icon = "", $contact = "true")
    {
        $this->mail = $mail;
        $this->phone = $phone;
        $this->contact = filter_var($contact, FILTER_VALIDATE_BOOL);
        $this->title = $title;
        $this->icon = $icon;
    }

    public function render()
    {
        return view('components.box-assistenza');
    }
}
