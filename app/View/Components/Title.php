<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Title extends BaseText
{
    /**
     * Create a new component instance.
     *
     * @param string $color
     * @param string $class
     * @param string $style
     * @param bool $uppercase
     * @param string $role
     */
    public function __construct($color = "black", $class = "", $style = "", $uppercase = true, $role = "h2")
    {
        parent::__construct($color, $class, $style, $uppercase, $role);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.text.title');
    }
}
