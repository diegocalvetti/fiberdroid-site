<?php

namespace App\View\Components;

use Illuminate\View\Component;

class BoxEquipaggio extends Component
{

    public $name;
    public $role;
    public $photo;
    public $alt;
    public $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $role, $photo = "", $alt = "", $title = "")
    {
        $this->name = $name;
        $this->role = $role;
        $this->title = $title;
        $this->alt = $alt;
        $this->photo = $photo != "" ? $photo : "https://bulma.io/images/placeholders/480x640.png";
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.box-equipaggio');
    }
}
