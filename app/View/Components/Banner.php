<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Banner extends Component
{

    public $image;
    public $uniqueId;

    /**
     * Create a new component instance.
     *
     * @param $image
     */
    public function __construct($image)
    {
        $this->image = $image;
        $this->uniqueId = uniqid();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.figure.banner');
    }
}
