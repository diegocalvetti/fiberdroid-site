<?php

namespace App\View\Components;

use Illuminate\View\Component;

class NavbarLinkDropdown extends Component
{

    public $text;
    public $links;
    public $base;
    public $style;

    /**
     * Create a new component instance.
     *
     * @param string $text
     * @param string $base
     * @param string $style
     * @param array $links
     */
    public function __construct($text = "", $base = "servizi", $style = "false", $links = [])
    {

        $this->text = $text;
        $this->links = $links;
        $this->base = $base;
        $this->style = $style ? $style : false;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.nav.navbar-link-dropdown');
    }
}
