<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ItemCircle extends Component
{
    public $text;
    public $desc;

    public function __construct($text, $desc = "Esmempio")
    {
        $this->text = $text;
        $this->desc = $desc;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.item-circle');
    }
}
