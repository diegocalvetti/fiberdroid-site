<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Separator extends Component
{

    public $color;
    public $width;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($color, $width = "100%")
    {
        $this->color = $color;
        $this->width = $width;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.separator.separator');
    }
}
