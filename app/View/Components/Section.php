<?php

namespace App\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Section extends Component
{

    public $section;
    public $isTitle;
    public $titleRole;

    /**
     * Create a new component instance.
     *
     * @param \App\Models\Section $section
     * @param string $isTitle
     */
    public function __construct(\App\Models\Section $section, $isTitle = "false")
    {
        $this->section = $section;
        $this->titleRole = filter_var($isTitle, FILTER_VALIDATE_BOOL) ? "h1" : "h2";
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.flex.section');
    }
}
