<?php

namespace App\View\Components;

use Illuminate\Contracts\View\View;

class Subtitle extends BaseText
{

    public $mb;

    /**
     * Create a new component instance.
     *
     * @param string $color
     * @param string $class
     * @param string $mb
     * @param string $style
     * @param string $uppercase
     * @param string $role
     */
    public function __construct($color = "black", $class = "is-h2", $mb = "3", $style = "", $uppercase = "true", $role = "h3")
    {
        parent::__construct($color, $class, $style, $uppercase, $role);
        $this->mb = $mb;
    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.text.subtitle');
    }
}
