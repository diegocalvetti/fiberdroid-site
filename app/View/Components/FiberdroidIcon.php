<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FiberdroidIcon extends Component
{

    public $colored;

    /**
     * Create a new component instance.
     *
     * @param bool $colored
     */
    public function __construct($colored = false)
    {
        $this->colored = $colored;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.fiberdroid-icon');
    }
}
