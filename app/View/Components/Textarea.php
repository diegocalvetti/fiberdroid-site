<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Textarea extends Component
{

    public $label;
    public $wireModel;
    public $error;
    public $uname;

    /**
     * Create a new component instance.
     *
     * @param string $label
     * @param string $wireModel
     * @param bool $error
     * @param string $uname
     */
    public function __construct($label = "", $wireModel = "", $error = false, $uname = "")
    {
        $this->label = $label;
        $this->wireModel = $wireModel;
        $this->error = $error;
        $this->uname = $uname;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.textarea');
    }
}
