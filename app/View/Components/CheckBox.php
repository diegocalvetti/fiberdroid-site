<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CheckBox extends Component
{

    public $wireModel;
    public $error;

    /**
     * Create a new component instance.
     *
     * @param $error
     * @param mixed $wireModel
     */
    public function __construct($error, $wireModel = false)
    {
        $this->error = $error;
        $this->wireModel = $wireModel;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.check-box');
    }
}
