<?php

namespace App\View\Components;

use Faker\Provider\Base;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Text extends BaseText
{
    /**
     * Create a new component instance.
     *
     * @param string $color
     * @param string $class
     * @param string $style
     * @param string $uppercase
     */
    public function __construct($color = "black-ter", $class = "", $style = "", $uppercase = "false")
    {
        parent::__construct($color, $class, $style, $uppercase);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('old.components.text.text');
    }
}
