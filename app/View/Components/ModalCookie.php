<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModalCookie extends Component
{

    public $accepted;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($accepted = "false")
    {
        $this->accepted = filter_var($accepted, FILTER_VALIDATE_BOOL);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.modal-cookie');
    }
}
