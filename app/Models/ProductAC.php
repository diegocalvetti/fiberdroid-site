<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAC extends Model
{
    protected $connection = "mysql_ac";
    protected $table = "products";
}
