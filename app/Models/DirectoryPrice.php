<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DirectoryPrice extends Model
{
    use HasFactory;

    protected $connection = "mysql_ac";
    protected $fillable = [
        'directory_id',
        'customer_id',
        'answer_charge',
        'rate',
        'first_charge',
        'sub_charge',
        'peek_offpeek',
        'duration',
    ];

    public static function default($directory_id) {
        return DirectoryPrice::where('customer_id', null)->where('directory_id', $directory_id)->first();
    }
}
