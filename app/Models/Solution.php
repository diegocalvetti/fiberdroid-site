<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solution extends Model
{
    public function sections()
    {
        return $this->morphMany(Section::class, "model");
    }
}
