<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Image extends Model
{

    protected $appends = ["full_url"];

    public function getFullUrlAttribute(): string
    {
        return config('images.path').$this->url;
    }

    public static function url($alias): string
    {
        return config('images.path').Image::query()->where('alias', $alias)->first()->url ?? "";
    }

    public static function attr($alias): string
    {
        $image = Image::query()->where('alias', $alias)->first();
        return "alt=\"{$image->alt}\" title=\"{$image->title}\"";
    }

    public static function get($alias) {
        return Image::query()->where('alias', $alias)->first();
    }

    public static function getId($alias) {
        return Image::query()->where('alias', $alias)->first()->id ?? null;
    }
}
