<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Document extends Model
{
    use HasFactory;
    protected $connection = "mysql_ac";
    protected $table = "documents";

    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
