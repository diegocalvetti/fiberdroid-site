<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $connection = "mysql_ac";

    protected $table = "products";

    protected $casts = ['portability' => "boolean", 'iva' => 'float'];
    protected $appends = ['prices'];
    protected $fillable = ['bollino'];

    public function getPricesAttribute() {

        $idListino = config('site.BASE_PRICE_LIST_ID');
        $listino = ListiniHasProdotto::where(['price_list_id' => $idListino, 'product_id' => $this->id])->first();

        return $listino;
    }

    public function features() {
        return $this->hasMany("App\Models\ItemFeature", "item_id");
    }
}
