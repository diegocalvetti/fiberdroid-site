<?php /** @noinspection PhpUndefinedFieldInspection */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class FiberdroidService extends Model
{
    use HasFactory;

    protected $appends = ['identifier', 'plans', 'slider', 'views', 'hasMainView'];

    private function parseJson($json, $fallback = null) {
        if (!$json) return $fallback;

        return json_decode($json, true);
    }

    public function getPlansAttribute() {
        return $this->parseJson($this->json_plans, []);
    }

    public function getSliderAttribute() {
        return $this->parseJson($this->json_slider);
    }

    public function getViewsAttribute() {
        return $this->parseJson($this->json_views);
    }

    public function getHasMainViewAttribute() {
        return $this->getViewsAttribute()['main'] ?? null;
    }

    public function getIdentifierAttribute() {
        return Str::slug($this->name);
    }
}
