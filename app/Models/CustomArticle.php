<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Parsedown;

class CustomArticle extends Model
{
    protected $connection = "mysql_ac";
    protected $table = "articles";

    protected $appends = ['url', 'html', 'img'];

    public function getImgAttribute()
    {
        return FdImage::where([
            'model_id' => $this->id,
            'model_type' => Article::class,
        ])->first();
    }

    public function getUrlAttribute(): string
    {
        return Str::slug($this->title);
    }


    /**
     * @param $html
     * @return string
     * @deprecated
     */
    private function _getIndex($html): string
    {
        $regExp = "/(<h(.)>(.*?)<\/h(.)>)/m";
        preg_match_all($regExp, $html, $matched);

        $data = !$matched ? [] :
            collect($matched[3])->map(function ($value, $index) use($matched) {
                return ['level' => $matched[2][$index] - 1, 'value' => $value];
            })->reject(function ($value){
                return $value['level'] == 0;
            })->values()->toArray();

        $html = "<ul>";

        for($i=0; $i<sizeof($data); $i++) {

            // Create new sub ul when before level is greater
            if($i > 0 && $data[$i-1]['level'] < $data[$i]['level']) {
                $html .= "<ul>";
            }

            $anchor = Str::slug($data[$i]['value']);
            $html .= "<li><a href='#$anchor'>".$data[$i]['value']."</a></li>";

            dump($anchor, $data[$i]);

            // Close the sub ul when next level is less
            if($i<sizeof($data)-1 && $data[$i+1]['level'] < $data[$i]['level']) {
                $html .= "</ul>";
            }
        }

        return "<div class='content'>".$html . "</ul></div>";
    }

    private function getIndex($html): string
    {
        $regExp = "/(<h(.)>(.*?)<\/h(.)>)/m";
        preg_match_all($regExp, $html, $matched);

        $data = !$matched ? [] :
            collect($matched[3])->map(function ($value, $index) use($matched) {
                return ['level' => $matched[2][$index] - 1, 'value' => $value];
            })->reject(function ($value){
                return $value['level'] == 0;
            })->values()->toArray();

        $html = "<ul class='m-0'>";
        $indent = 0;

        for($i=0; $i<sizeof($data); $i++) {

            // Create new sub ul when before level is greater
            if($i > 0 && $data[$i-1]['level'] < $data[$i]['level']) {
                $html .= "";
                $indent++;
            }

            $anchor = Str::slug($data[$i]['value']);
            $html .= "<a class='panel-block index-block' href='#$anchor'><li class='indent-$indent'>".$data[$i]['value']."</li></a>";

            //dump($anchor, $data[$i]);

            // Close the sub ul when next level is less
            if($i<sizeof($data)-1 && $data[$i+1]['level'] < $data[$i]['level']) {
                $html .= "";
                $indent--;
            }
        }

        return $html;
    }

    private function parsePlaceholder($raw): string
    {
        //Accordions
        preg_match_all("/((\/accordion)\{(.*?)\}(((\n*)(.*?)(\n*))*)(\/endaccordion))/", $raw, $accordionMatch);
        if(isset($accordionMatch[0]) && !empty($accordionMatch[0])) {
            for($i=0; $i<sizeof($accordionMatch[0]); $i++) {
                $accordion['full'] = $accordionMatch[0][$i];
                $accordion['title'] = trim($accordionMatch[3][$i]);
                $accordion['content'] = trim($accordionMatch[4][$i]);

                $accordionHtml = view('old.components.accordion.accordion', [
                    'active' => true,
                    'id' => null,
                    'title' => $accordion['title'],
                    'slot' => $accordion['content'],
                    'notIcon' => $this->type_id == 26
                ])->render();

                $raw = Str::replace($accordion['full'], $accordionHtml, $raw);
            }
        }

        //Quote
        preg_match_all("/(\/quote\{(.*?)\})/", $raw, $quoteMatch);
        if(isset($quoteMatch[0]) && !empty($quoteMatch[0])) {
            for($i=0; $i<sizeof($quoteMatch[0]); $i++) {
                $quote['full'] = $quoteMatch[0][$i];
                $quote['content'] = trim($quoteMatch[2][$i]);

                $quoteHtml = view('pages.components.quote', [
                    'slot' => $quote['content'],
                ])->render();

                $raw = Str::replace($quote['full'], $quoteHtml, $raw);
            }
        }

        //Table
        preg_match_all("/\/table([^\n]*\n+)+?\/endtable/", $raw, $tableMatch);
        for($i=0; $i<sizeof($tableMatch[0]); $i++) {
            $tableContent = $tableMatch[0][$i] ?? null;

            if($tableContent) {
                $tableContent = Str::replace("/table", "", $tableContent);
                $tableContent = Str::replace("/endtable", "", $tableContent);
                $tableContent = trim($tableContent);

                preg_match_all("/\[([^\n]*\n*?)\]/", $tableContent, $columnMatch);

                for($j=0; $j<sizeof($columnMatch[0]); $j++) {
                    $columns = explode("|", $columnMatch[1][$j]);

                    $htmlColumn = "<tr>";
                    foreach ($columns as $column) {
                        $htmlColumn .= "<td>$column</td>";
                    }
                    $htmlColumn .= "</tr>";

                    $raw = Str::replace($columnMatch[0][$j], $htmlColumn, $raw);
                }

                $raw = Str::replace("/table", "<table class='table' style='font-weight: 300'>", $raw);
                $raw = Str::replace("/endtable", "</table>", $raw);
            }

        }

        //Ln
        $raw = Str::replace("/ln", "<br>", $raw);
        return $raw;
    }

    public function getHtmlAttribute(): Collection
    {
        if ($this->content) {
            $html = $this->content;
        } else {
            $raw = $this->text;
            $raw = $this->parsePlaceholder($raw);

            $html = Parsedown::instance()->parse($raw);
        }

        $index = $this->getIndex($html);

        $regExp = "/(<h(.)>(.*?)<\/h(.)>)/m";
        preg_match_all($regExp, $html, $matched);
        foreach ($matched[0] as $key => $match) {
            $fullTag = $match;
            $value = $matched[3][$key];
            $closingTag = "</h" . $matched[2][$key] . ">";
            $anchor = Str::slug($value);

            $new = "<h" . $matched[2][$key] . " id='$anchor'>" . $value . $closingTag;
            $html = Str::replace($fullTag, $new, $html);
        }

        // hidden h1
        //$html = Str::replace("<h1", "<span class='is-h1 my-3 is-size-4-desktop is-size-6-mobile is-size-4-tablet is-uppercase has-text-fd-primary is-hidden'", $html);
        //$html = Str::replace("/h1>", "/span>", $html);
        $h1Start = strpos($html, "<h1");
        $h1End = strpos($html, "/h1>");

        if ($h1Start)
            $html = substr($html, 0, $h1Start) . substr($html, $h1End+4);

        $html = Str::replace("<h2", "<h2 class='article-h2 mt-6 mb-4'", $html);
        $html = Str::replace("<h3", "<h3 class='article-h3 mt-5 mb-4'", $html);
        $html = Str::replace("<h4", "<h4 class='is-p mt-4'", $html);
        $html = Str::replace("<a", "<a target='_blank' rel='noopener'", $html);

        // removed index from html
        //$html = Str::replace("/index", $index, $html);
        //$html = Str::replace("/index", "", $html);

        // Format intro
        $start = strpos($html, "<h2");
        $intro = substr($html, 0, $start);
        $html = substr($html, $start, -1);

        return collect([
            'html' => $html,
            'intro' => $intro,
            'index' => $index,
        ]);
    }

    function check($string) {
        $start =strpos($string, '<');
        $end  =strrpos($string, '>',$start);

        $len=strlen($string);

        if ($end !== false) {
            $string = substr($string, $start);
        } else {
            $string = substr($string, $start, $len-$start);
        }
        libxml_use_internal_errors(true);
        libxml_clear_errors();
        $xml = simplexml_load_string($string);
        return count(libxml_get_errors())==0;
    }

    public function image(): MorphOne
    {
        return $this->morphOne(FdImage::class, 'model');
    }
}
