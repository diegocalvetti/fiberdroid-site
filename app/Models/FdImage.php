<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Post Model
 */
class FdImage extends Model
{
    use HasFactory;

    protected $connection = "mysql_ac";
    protected $table = "images";
    protected $primaryKey = 'id';

    /**
     * @var array The attributes that are mass assignable.
     */
    protected $fillable = [
        'user_id',
        'model_id',
        'model_type',
        'image',
        'is_avatar',
    ];

    protected $casts = [
        'model_id' => 'integer',
        'user_id' => 'integer',
        'is_avatar' => 'boolean',
    ];

    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
