<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Parsedown;

class Directories extends Model
{
    protected $connection = "mysql_ac";
    protected $table = "directories";

    public function price(): HasOne
    {
        $request = request();

        $hasOne = $this->hasOne(DirectoryPrice::class, 'directory_id')->where('customer_id', $request->customer_id);

        if($hasOne->count() == 0) {
            return $this->hasOne(DirectoryPrice::class, 'directory_id')->whereNull('customer_id');
        }

        return $hasOne;
    }
}
