<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Section extends Model
{
    use HasFactory;

    protected $appends = ['slug', 'accordions', 'cards', 'img', "isVoid"];

    public function getIsVoidAttribute() {
        return (
            strlen($this->title) == 0 &&
            strlen($this->content) == 0 &&
            sizeof($this->getCardsAttribute()) == 0 &&
            sizeof($this->getAccordionsAttribute()) == 0
        );
    }

    public function getSlugAttribute() {
        return Str::slug($this->title);
    }

    private function jsonAttribute($value, $option = true) {
        $data = json_decode($value, $option);
        return !is_null($data) ? collect($data)->toArray() : [];
    }

    public function getImgAttribute()
    {
        return $this->jsonAttribute($this->image);
    }

    public function getAccordionsAttribute(): array
    {
        return $this->jsonAttribute($this->json_accordions);
    }

    public function getCardsAttribute(): array
    {
        $cards = $this->jsonAttribute($this->json_cards);
        foreach ($cards as $key => $card) {
            if(isset($card['image'])) {
                $cards[$key]['image'] = Image::get($card['image']);
            }
        }

        return $cards;
    }

    public function subsections(): HasMany
    {
        return $this->hasMany(self::class, "section_id");
    }

    public function anchorableSubsections(): HasMany
    {
        return $this->hasMany(self::class, "section_id")
            ->where('is_anchor', 1);
    }

    public function photo(): BelongsTo
    {
        return $this->belongsTo(Image::class, "image_id");
    }
}
