<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $appends = ['published_at', 'photo', 'img'];

    public function getPublishedAtAttribute() {
        Carbon::setLocale('it');
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function sections()
    {
        return $this->morphMany(Section::class, "model");
    }

    public function image() {
        return $this->belongsTo(Image::class);
    }

    public function getImgAttribute() {
        $image = FdImage::query()
            ->where('model_id', $this->id)
            ->where('model_type', "App\Models\Article")
            ->first();

        $image = $image->toArray();
        $image['image'] = json_decode($image['image']);

        return $image ? $image : null;
    }

}
