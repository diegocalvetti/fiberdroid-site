<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListiniHasProdotto extends Model
{
    protected $connection = "mysql_ac";
    protected $table = "price_lists_products";
}
