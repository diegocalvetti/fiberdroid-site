<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;


    public function image() {
        return $this->belongsTo(Image::class);
    }

    public function items() {
            $db = config('database.connections.mysql.database');
           return $this->belongsToMany('App\Models\Item', "$db.service_has_items", 'service_id')
               ->whereNull('hidden_at')
               ->orderBy('sort_number');
    }

    public function most_selled() {
        $db = config('database.connections.mysql.database');
        return $this->belongsToMany('App\Models\Item', "$db.service_has_items", 'service_id')->where('most_purchased', true)->orderBy('order');
    }

    public function sections()
    {
        return $this->morphMany(Section::class, "model");
    }

    public function type() {
        return $this->belongsTo("App\Models\ServiceType", "service_type_id");
    }

}
