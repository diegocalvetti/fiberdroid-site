<?php


namespace App\Mail;


class OrderSentMail extends BaseMail
{
    public function build()
    {
        $this->subject = "Fiberdroid - Ordine inviato con successo";

        return $this->view('emails.order_sent', ['data' => $this->data]);
    }

}
