<?php


namespace App\Mail;


class OrderReceivedMail extends BaseMail
{
    public function build()
    {
        $this->subject = "Fiberdroid - Nuovo ordine ricevuto";

        return $this->view('emails.order_received', ['data' => $this->data]);
    }

}
