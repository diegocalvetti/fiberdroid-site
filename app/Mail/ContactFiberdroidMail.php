<?php


namespace App\Mail;


class ContactFiberdroidMail extends BaseMail
{

    public function build()
    {
        $this->subject = "Nuovo contatto da fiberdroid";

        return $this->view('emails.contactFiberdroid', ['data' => $this->data]);
    }

}
