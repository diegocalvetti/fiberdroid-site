<?php


namespace App\Mail;


class ContactMail extends BaseMail
{
    public function build()
    {
        return $this->view('emails.contact', ['data' => $this->data]);
    }

}
