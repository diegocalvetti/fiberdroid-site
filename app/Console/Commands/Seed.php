<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Seed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed {seeders*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the specified seeders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $seeders = $this->argument('seeders');

        foreach ($seeders as $seeder) {

            if (!class_exists("Database\Seeders\\{$seeder}")) {
                $this->warn("Seeder Database\Seeders\\{$seeder} not found!");
            } else {
                $this->call("db:seed", ['class' => $seeder]);
            }
        }

        return 0;
    }
}
