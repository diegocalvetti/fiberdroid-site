<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AvoidDuplicatedUrlWithCanonical
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $uri = $request->getRequestUri();
        $url = config('app.url').$uri;
        $response->header('Link', " <{$url}>; rel=\"canonical\"");

        return $response;
    }
}
