<?php

namespace App\Http\Controllers;

class CoperturaController
{
    private function get($url)
    {

        $url = str_replace(" ", "+", $url);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        curl_close($ch);

        return json_decode($response);
    }


    public function login()
    {
        $response = $this->get("https://customers.clouditalia.com/public/ws_json_whs_logon?ws_login_os=1012911-0699344&ws_password_os=6QnQrg42");

        return $response->Response[0]->Token;
    }

    public function province($chiave)
    {
        $response = $this->get("https://customers.clouditalia.com/public/ws_json_whs_copertura_province?token=$chiave");


        return json_decode(json_encode($response->Response[0]->Details), true);

    }

    public function cities($chiave, $prov)
    {
        $response = $this->get("https://customers.clouditalia.com/public/ws_json_whs_copertura_comuni?token=$chiave&desc_provincia=$prov");

        return json_decode(json_encode($response->Response[0]->Details), true);
    }

    public function streets($chiave, $prov, $comune)
    {
        $response = $this->get("https://customers.clouditalia.com/public/ws_json_whs_copertura_indirizzi?token=$chiave&desc_provincia=$prov&desc_comune=$comune");

        return json_decode(json_encode($response->Response[0]->Details), true);
    }

    public function street_numbers($chiave, $prov, $comune, $prefixVia, $via)
    {

        $response = $this->get("https://customers.clouditalia.com/public/ws_json_whs_copertura_civici?token=$chiave&desc_provincia=$prov&desc_comune=$comune&desc_part_toponomastica=$prefixVia&via=$via");

        return json_decode(json_encode($response->Response[0]->Details), true);

    }

    public function services($chiave, $prov, $comune, $prefixVia, $via, $civico)
    {

        /*
        $civico = str_replace("/", "-", $civico);
        $prov = str_replace(" ", "-", $prov);
        $comune = str_replace(" ", "-", $comune);
        $prefixVia = str_replace(" ", "-", $prefixVia);
        $via = str_replace(" ", "-", $via);
        */

        $response = $this->get("https://customers.clouditalia.com/public/ws_json_whs_copertura_verifica?token=$chiave&desc_provincia=$prov&desc_comune=$comune&desc_part_toponomastica=$prefixVia&via=$via&civico=$civico");

        return json_decode(json_encode($response->Response[0]->Details[0]), true);

    }

}
