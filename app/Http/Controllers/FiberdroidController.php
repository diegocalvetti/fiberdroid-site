<?php

namespace App\Http\Controllers;

use App\Models\CustomArticle;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

use App\Models\Article;
use App\Models\Crew;
use App\Models\Service;
use App\Models\Solution;
use Illuminate\Support\Str;

class FiberdroidController extends Controller
{

    public function __construct(Request $request = null, $noCookie = false)
    {
        //$services = Service::all();
        //$solutions = Solution::all();

        $cookie = (
            Cookie::has('fiberdroid_cookie') &&
            Cookie::get('fiberdroid_cookie') == 'true'
        ) || $noCookie;

        $config = [
            'link_youtube' => "https://www.youtube.com/channel/UCWxxXdSBmerg3hu26yaioVA/featured",
            'link_linkedin' => "https://www.linkedin.com/company/fiberdroid",
            'link_twitter' => "https://twitter.com/fiberdroid",
            'link_areaclienti' => "https://area-clienti.fiberdroid.it",
            'link_speedtest' => "http://fiberdroid.speedtestcustom.com",
            'download' => [
                'supremo' => url('/download/supremo.exe')
            ],
            'contact' => [
                'mail_amministrazione' => "amministrazione@fiberdroid.it",
                'tel_amministrazione' => "02 83595676",
                'mail_assistenza' => "speed@fiberdroid.it",
                'tel_assistenza' => "02 83595676",
            ],
        ];


        $uri = $request ? $request->getRequestUri() : "";
        $canonical = config('app.url').$uri;

        View::share([
            //'services' => $services,
            //'solutions' => $solutions,
            'cookie' => $cookie,
            'config' => $config,
            'canonical' => $canonical,
            'bubbles' => false,
        ]);
    }

    public function sendMail($to, $mailableClassName, $data = null)
    {
        try {
            $mailableClassName = '\App\Mail\\' . $mailableClassName;
            $data = json_decode(json_encode($data), true);


            Mail::to($to)
                ->send(new $mailableClassName($data));

            return true;
        }
        catch (\Swift_TransportException $e) {
            dd($e->getMessage());
            return $e->getMessage();
        }
        catch (\Exception $exception) {
            dd($exception->getMessage());
            return $exception->getMessage();
        }
    }

    function showHome() {
        return view('home');
    }

    function showChiSiamo() {

        $crews = Crew::select('*')->orderBy('order')->get();

        return view('chi_siamo', ['crews' => $crews]);
    }

    function showAssistenza() {
        return view('assistenza');
    }

    function showCopertura() {
        return view('copertura');
    }

    function showService($service) {

        $service = Service::with([
            'items',
            'sections'
        ])->where('url', $service)->first();

        //dd($service->sections->toArray());

        if( is_null($service) ) {
            abort(404);
        }

        $sectionCount = $service->sections()->where(['is_intro' => 0, 'section_id' => null])->count();

        if( $sectionCount > 0 ) {
            $mainSections = $service->sections()->where(['is_intro' => 0, 'section_id' => null])->skip(1)->take($sectionCount-1)->get();
        } else {
            $mainSections = new Collection();
        }

        if( $sectionCount > 1 ) {
            $anchorableSections = $service->sections()->where(['is_anchor' => 1, 'section_id' => null])->get();
        } else {
            $anchorableSections = new Collection();
        }

        $firstMainSection = $service->sections()->where(['is_intro' => 0, 'section_id' => null])->first();
        $introSections = $service->sections()->where(['is_intro' => 1, 'section_id' => null])->get();

        $totAnchorableSections = $anchorableSections->sum(function ($sec) {
            return $sec->subsections->count();
        }) + $anchorableSections->count();
        $displayAnchor = $totAnchorableSections > 2;

        return view('service', ['service' => $service,'displayAnchor' => $displayAnchor, 'firstMainSection' => $firstMainSection, 'mainSections' => $mainSections, 'introSections' => $introSections, 'anchorableSections' => $anchorableSections]);
    }

    function showSolution($solution) {
        $solution = Solution::where('url', $solution)->first();

        if( is_null($solution) ) {
            abort(404);
        }

        $mainSections = $solution->sections()->where(['is_intro' => 0, 'section_id' => null]);

        if( $mainSections->count() > 0 ) {
            $anchorableSections = $solution->sections()->where(['is_anchor' => 1, 'section_id' => null])->get();
        } else {
            $anchorableSections = new Collection();
        }

        if( $mainSections->count() > 0 ) {
            $mainSections = $mainSections->get()->skip(1)->take($mainSections->count()-1);
        } else {
            $mainSections = new Collection();
        }

        $firstMainSection = $solution->sections()->where(['is_intro' => 0, 'section_id' => null])->first();
        $introSections = $solution->sections()->where(['is_intro' => 1, 'section_id' => null])->get();

        if( is_null($solution) ) {
            abort(404);
        }

        return view('solution', ['solution' => $solution,'firstMainSection' => $firstMainSection, 'anchorableSections' => $anchorableSections, 'mainSections' => $mainSections, 'introSections' => $introSections]);
    }

    function showBlog() {
        $articles = Article::all()->reject(function ($article) {
            return in_array($article->id, [1]);
        });
        $articlesFd = CustomArticle::with(['image'])->where('type_id', 24)->where('published', 1)->orderByDesc('published_at')->get();

        //dd($articlesFd[0]->image);

        return view('blog', [
            'articles' => $articlesFd->concat($articles),
        ]);
    }

    function showGuides() {
        $articlesFd = CustomArticle::with(['image'])
            ->where('type_id', 25)
            ->where('published', 1)
            ->orderByDesc('published_at')
            ->get();

        return view('guides', [
            'articles' => $articlesFd,
        ]);
    }

    function showArticle($url) {


        $article = Article::query()->where('url', $url)->where('id', '!=', 1)->first();

        if( !is_null($article) ) {
            $mainSections = $article->sections()->where(['is_intro' => 0, 'section_id' => null])->get();
            $introSections = $article->sections()->where(['is_intro' => 1, 'section_id' => null])->get();
            $anchorableSections = $article->sections()->where(['is_anchor' => 1, 'section_id' => null])->get();
            $articles = Article::where('id', '<>', $article->id)->get();

            return view('article', ['article' => $article, 'articles' => $articles, 'mainSections' => $mainSections, 'anchorableSections' => $anchorableSections, 'introSections' => $introSections]);
        }

        $article = CustomArticle::query()->where('type_id', 24)->where('published', 1)->where('url', $url)->first();

        if( !is_null($article) ) {
            return view('custom_article', ['article' => $article]);
        }

        return abort(404);
    }

    function showGuide($url) {
        $article = CustomArticle::query()->where('type_id', 25)->where('published', 1)->where('url', $url)->first();

        if( !is_null($article) ) {
            return view('article', ['article' => $article]);
        }

        return abort(404);
    }

    function showCustomArticle($article) {

        $article = CustomArticle::where('url', $article)->first();

        dd($article);

        if( is_null($article) ) {
            abort(404);
        }


        //return view('article', ['article' => $article, 'articles' => $articles, 'mainSections' => $mainSections, 'anchorableSections' => $anchorableSections, 'introSections' => $introSections]);
    }

    function showSpeed() {
        return view('speed');
    }

    function showThankYou() {
        $articles = CustomArticle::query()->whereIn('id', [
            23, 22, 14
        ])->orderBy('id', 'DESC')->get();

        return view('thanks-you', ['articles' => $articles]);
    }

    function showPrivacyPolicy() {
        return view('policy_privacy');
    }

    function showCookiePolicy() {
        return view('policy_cookie');
    }

    function articleApi($url) {
        $article = CustomArticle::query()->where('url', $url)->first();

        if( !is_null($article) ) {
            $view = view('custom_article_no_template', ['article' => $article]);
            $html = $view->render();

            return ['content' => $html];
        }

        return abort(404);
    }

    private function validateMeta($meta, $value): array {
        $errors = [];

        $len = strlen($value);

        switch ($meta) {
            case 'title':
                if($len < 50) {
                    $errors[]= "meta-title is too short <b>($len/50)</b>";
                }

                if($len > 70) {
                    $errors[]= "meta-title is too long <b>($len/70)</b>";
                }
                break;

            case 'description':
                if($len < 70) {
                    $errors[]= "meta-description is too short <b>($len/70)</b>";
                }

                if($len > 155) {
                    $errors[]= "meta-description is too long <b>($len/155)</b>";
                }
                break;
        }

        return $errors;
    }

    function sitemapApi(): Collection {
        $sitemap_raw = simplexml_load_string(
            file_get_contents(public_path('sitemap.xml'))
        );

        $sitemap = collect(json_decode(json_encode($sitemap_raw))->url);

        $sitemap = $sitemap->map(function ($url) {
           $html = file_get_contents($url->loc);

           $meta_regex = "/<meta name=\"(.*)\" content=\"(.*)\">/";

           preg_match_all($meta_regex, $html, $meta_matched);

           $meta = [];

           for($i=0; $i<sizeof($meta_matched[1]); $i++) {
               $meta_name = $meta_matched[1][$i];
               $meta_value = $meta_matched[2][$i];

               $errors = $this->validateMeta($meta_name, $meta_value);

               $meta[$meta_name] = [
                   'value' => $meta_value,
                   'errors' => $errors,
               ];
           }

           $title_regex = "/<title>(.*)<\/title>/";
           preg_match($title_regex, $html, $title_matched);

           $meta['title'] = [
               'value' => $title_matched[1],
               'errors' => [],
           ];

           return [
               'loc' => $url->loc,
               'meta' => $meta
           ];
        });

        return $sitemap;
    }

}
