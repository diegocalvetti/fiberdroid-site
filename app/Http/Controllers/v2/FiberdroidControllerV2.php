<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\Crew;
use App\Models\CustomArticle;
use App\Models\Directories;
use App\Models\FdImage;
use App\Models\FiberdroidService;
use App\Models\ListiniHasProdotto;
use App\Models\ProductAC;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\View;

class FiberdroidControllerV2 extends Controller
{
    public function __construct(Request $request = null, $noCookie = false)
    {
        $cookie = (
                Cookie::has('fiberdroid_cookie') &&
                Cookie::get('fiberdroid_cookie') == 'true'
            ) || $noCookie;

        $config = [
            'link_youtube' => "https://www.youtube.com/channel/UCWxxXdSBmerg3hu26yaioVA/featured",
            'link_instagram' => "https://www.instagram.com/fiberdroid_real?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw==",
            'link_linkedin' => "https://www.linkedin.com/company/fiberdroid",
            'link_tiktok' => "https://www.tiktok.com/@fiberdroid",
            'link_twitter' => "https://twitter.com/fiberdroid",
            'link_areaclienti' => "https://area-clienti.fiberdroid.it",
            'link_speedtest' => "http://fiberdroid.speedtestcustom.com",
            'download' => [
                'supremo' => url('/download/supremo.exe')
            ],
            'contact' => [
                'mail_amministrazione' => "amministrazione@fiberdroid.it",
                'tel_amministrazione' => "02 83595676",
                'mail_assistenza' => "speed@fiberdroid.it",
                'tel_assistenza' => "02 83595676",
            ],
        ];
        $menu = [
            ['label' => 'Home', 'link' => '/'],
            ['label' => 'Chi Siamo', 'link' => '/chi-siamo'],
            ['label' => 'Servizi', 'dataTarget' => 'servizi', 'dropdown' => true],
            ['label' => 'Prodotti', 'dataTarget' => 'prodotti', 'dropdown' => true],
            ['label' => 'Soluzioni', 'dataTarget' => 'soluzioni', 'dropdown' => true],
            ['label' => 'Blog', 'link' => '/blog'],
            ['label' => 'Verifica Copertura', 'link' => '/verifica-copertura'],
            ['label' => 'Assistenza', 'link' => '/assistenza'],
            ['label' => 'Accedi', 'link' => 'https://area-clienti.fiberdroid.it/', 'onlyMobile' => true]
        ];

        $uri = $request ? $request->getRequestUri() : "";
        $canonical = config('app.url').strtolower($uri);

        $services = FiberdroidService::query()->whereNotIn('type', ['Prodotto', 'Soluzione'])->get();
        $service_products = FiberdroidService::query()->where('type', 'Prodotto')->get();
        $solutions = FiberdroidService::query()->where('type', 'Soluzione')->get();

        View::share([
            'cookie' => $cookie,
            'config' => $config,
            'canonical' => $canonical,
            'services' => $services,
            'service_products' => $service_products,
            'solutions' => $solutions,
            'menu' => $menu,
            'bubbles' => false,
            'useWebcomponent' => true,
        ]);
    }

    function showHome() {

        $homeServices = $this->withPrices(
            ProductAC::query()
                ->where('name', 'like',  '%10 Giga%')
                ->get()
                ->merge(
                    ProductAC::query()
                        ->where('name', 'like',  '%GNR 10 Num%')
                        ->get()
                )
                ->merge(
                    ProductAC::query()
                        ->where('name', 'like',  '%Singolo Interno%')
                        ->get()
                )
        );

        return view('pages/home', ['noNavbar' => true, 'homeServices' => $homeServices]);
    }

    function showChiSiamo() {
        $crews = Crew::query()->select('*')->orderBy('order')->get();

        return view('pages/chi-siamo', ['crews' => $crews, 'useWebcomponent' => false]);
    }

    function showBlog(Request $request) {
        $articlesFd = CustomArticle::with(['image'])
            ->where('type_id', 24)
            ->where('published', 1)
            ->orderByDesc('published_at')
            ->get();

        $featured = $articlesFd->first();

        $articlesFd = $articlesFd->filter(function ($article) use($featured) {
            return $article->id != $featured->id;
        });

        $search = $request->search ?? null;

        if ($search) {
            $articlesFd = $articlesFd->filter(function ($item) use ($search) {
                return false !== stristr($item->title, $search);
            });
        }

        return view('pages/blog', [
            'featured' => $featured,
            'articles' => CustomArticle::with(['image'])->whereIn('id', $articlesFd->pluck('id'))->orderByDesc('published_at')->paginate()->toArray(),
            'useWebcomponent' => false,
        ]);
    }

    function showArticle($url) {
        $article = CustomArticle::query()->where('type_id', 24)->where('published', 1)->where('url', $url)->first();

        if (!$article) {
            return $this->showGuide($url);
        }

        $articles = CustomArticle::with(['image'])
            ->where('type_id', 24)
            ->where('published', 1)
            ->where('id', '<>', $article->id)
            ->whereDate('published_at', '<', $article->published_at)
            ->orderByDesc('published_at')
            ->get()->take(3);

        if( !is_null($article) ) {
            return view('pages/article', ['article' => $article, 'articles' => $articles, 'useWebcomponent' => false]);
        }

        return abort(404);
    }

    function showGuide($url) {
        $article = CustomArticle::query()->where('type_id', 25)->where('published', 1)->where('url', $url)->first();

        if( !is_null($article) ) {
            return view('pages/article', ['article' => $article, 'articles' => []]);
        }

        return abort(404);
    }

    function showListini()
    {
        $directories = Directories::with(['price']);

        $search = request()->search ?? null;

        $directories = $directories->where('name', 'not like', "%Italy%");
        $directories = $directories->where('name', 'not like', "%Italia%");

        if ($search) {
            $directories = $directories->where('name', 'like', "%$search%");
        }

        $directories = $directories->orderBy('name');

        return view('pages/listini', ['directories' => $directories->paginate(10)->toArray()]);
    }
    function redirectToGuide($url) {
        return redirect($url, 301);
    }

    private function withPrices($collection) {
        return $collection->map(function ($element) {
            $element['prices'] = ListiniHasProdotto::query()->where([
                'price_list_id' => config('site.BASE_PRICE_LIST_ID'),
                'product_id' => $element->id,
            ])->first()->toArray();

            $element['image'] = FdImage::query()
                ->where([
                    'model_id' => $element->id,
                    'model_type' => 'App\Models\Product',
                    'is_avatar' => true
                ])->first();

            if($element['image']) {
                $element['image'] = $element['image']->toArray();
            }

            return $element;
        });
    }
    private function withoutPrices($collection) {
        return $collection->map(function ($element) {
            $element['prices'] = [
                'fee_amount' => 0,
                'activation_amount' => 0
            ];

            $element['image'] = FdImage::query()
                ->where([
                    'model_id' => $element->id,
                    'model_type' => 'App\Models\Product',
                    'is_avatar' => true
                ])->first();

            if($element['image']) {
                $element['image'] = $element['image']->toArray();
            }

            return $element;
        });
    }

    private function products(): array
    {
        return [
            'fibra' => collect([
                'ftth' => collect([
                    'products' => $this->withPrices(
                        ProductAC::query()
                            ->where('category_id', 1)
                            ->where('name', 'like',  '%FTTH%')
                            ->whereNull('hidden_at')
                            ->orderBy('sort_number')
                            ->get()
                            ->merge(
                                ProductAC::query()
                                    ->where('category_id', 1)
                                    ->where('name', 'like',  '%FTTC%')
                                    ->whereNull('hidden_at')
                                    ->orderBy('sort_number')
                                    ->get()
                            )
                    ),
                    'items' => [
                        "FTTH <br> 1 Giga",
                        "FTTH <br>2,5 Giga",
                        "FTTH <br> 10 Giga",
                    ],
                    'type' => 'triple',
                    'bg' =>  gradient('ffth'),
                ]),
                'fttc' => collect([
                    'products' => [],
                    'items' => [
                        "FTTC <br> 30 Mega",
                        "FTTC <br> 50 Mega",
                        "FTTC <br> 100 Mega",
                        "FTTC <br> 200 Mega",
                    ],
                    'type' => 'double',
                    'bg' => gradient('fftc'),
                ]),
            ]),
            'fibra-dedicata' => collect([
                'fttc' => collect([
                    'products' => [],
                    'type' => 'double',
                    'bg' => gradient('ffth'),
                ]),
            ]),
            'reti-wan' => collect([
                'wan' => collect([
                    'products' => [],
                    'type' => 'double',
                    'bg' => 'linear-gradient(135deg, #FBDA61 0%, #F76B1C 100%)',
                ]),
            ]),
            'adsl' => collect([
                'adsl' => collect([
                    'products' => $this->withPrices(
                        ProductAC::query()
                            ->where('category_id', 1)
                            ->where('name', 'like',  '%ADSL%')
                            ->whereNull('hidden_at')
                            ->orderBy('sort_number')
                            ->get()
                    ),
                    'type' => 'double',
                    'bg' => 'linear-gradient(135deg, #FBDA61 0%, #F76B1C 100%)',
                ]),
            ]),
            'hdsl' => collect([
                'hdsl' => collect([
                    'products' => $this->withoutPrices(
                        ProductAC::query()
                            ->where('category_id', 1)
                            ->where('name', 'like',  '%HDSL%')
                            ->orderBy('sort_number')
                            ->get()
                    ),
                    'items' => null,
                    'type' => 'triple',
                    'bg' => 'linear-gradient(135deg, #FBDA61 2.88%, #F76B1C 98.13%)',
                    'bg_box' => 'linear-gradient(135deg, #A1FF8B 0%, #3F93FF 96.83%)',
                ]),
            ]),
            'voip' => collect([
                'voip' => collect([
                    'products' => $this->withPrices(
                        ProductAC::query()
                            ->where('category_id', 3)
                            ->orderBy('sort_number')
                            ->whereNull('hidden_at')
                            ->get()
                    ),
                    'items' => [
                        "Numero <br> Voip <br> Singolo",
                        "GNR 20 <br> Numeri",
                        "GNR 30 <br> Numeri",
                        "GNR 100 <br> Numeri",
                    ],
                    'bg' => 'url("https://fiberdroid.it/pages/gradients/voip.png")',
                    'type' => 'triple',
                ]),
                'gnr' => collect([
                    'products' => [],
                    'items' => [
                        "Centrale <br> Voip <br> Cloud 4",
                        "Centrale <br> Voip <br> Cloud 8",
                        "Centrale <br> Voip <br> Cloud 16",
                        "Centrale <br> Voip <br> Cloud 24",
                        "Centrale <br> Voip <br> Cloud 32",
                        "Centrale <br> Voip <br> Cloud 48",
                        "Centrale <br> Voip <br> Cloud 64",
                    ],
                    'bg' => 'url("https://fiberdroid.it/pages/gradients/voip.png")',
                    'type' => 'none',
                ])
            ]),
            'wireless' => collect([
                'wireless' => collect([
                    'products' => $this->withPrices(
                        ProductAC::query()
                            ->where('category_id', 2)
                            ->where('name', '<>', 'Hybrid Fibra + 5G')
                            ->where(function ($q) {
                                $q
                                    ->where('name', 'like',  '%Wireless%')
                                    ->orWhere('name', 'like',  '%4G%')
                                    ->orWhere('name', 'like',  '%5G%');
                            })
                            ->whereNull('hidden_at')
                            ->orderBy('sort_number')
                            ->get()
                    ),
                    'type' => 'double',
                    'bg' => 'linear-gradient(135deg, #FBDA61 2.88%, #F76B1C 98.13%)',
                    'bg_box' => 'linear-gradient(135deg, #A1FF8B 0%, #3F93FF 96.83%)',
                ]),
            ]),
            'satellitare' => collect([
                'satellitare' => collect([
                    'products' => $this->withoutPrices(
                        ProductAC::query()
                            ->where('category_id', 2)
                            ->where('name', 'like',  '%satellite%')
                            ->orderBy('sort_number')
                            ->get()
                    ),
                    'type' => 'triple',
                    'bg' => 'linear-gradient(135deg, #FBDA61 2.88%, #F76B1C 98.13%)',
                    'bg_box' => 'linear-gradient(135deg, #A1FF8B 0%, #3F93FF 96.83%)',
                ]),
            ]),
            'centralino-cloud' => collect([
                'centralino_cloud' => collect([
                    'products' => $this->withPrices(
                        ProductAC::query()
                            ->where('category_id', 4)
                            ->where('name', 'like',  '%centrale%')
                            ->whereNull('hidden_at')
                            ->orderBy('sort_number')
                            ->get()
                    ),
                    'type' => 'triple',
                    'bg' => 'url("https://fiberdroid.it/pages/gradients/voip.png")',
                ]),
            ]),
            /*'fiber-ai' => collect([
                'fiber-ai' => collect([
                    'products' => $this->withPrices(
                        ProductAC::query()
                            ->where('category_id', 10)
                            ->whereNull('hidden_at')
                            ->orderBy('sort_number')
                            ->get()
                    ),
                    'type' => 'triple',
                    'bg' => 'url("https://fiberdroid.it/pages/gradients/voip.png")',
                ]),
            ]),*/
            'centralino-voip' => collect([
                'fiber-ai' => collect([
                    'products' => [],
                    'type' => 'triple',
                    'bg' => 'url("https://fiberdroid.it/pages/gradients/voip.png")',
                ]),
            ]),
            'cloud-wifi' => collect([
                'cloud' => collect([
                    'products' => collect([
                        collect([
                            'id' => 0,
                            'name' => 'DA 1 A 5 ANTENNE CONNESSE',
                            'prices' => [
                                'fee_amount' => 14.99,
                            ],
                            'items' => json_encode(["App su smartphone per il controllo in tempo reale", "Assistenza tecnica inclusa", "Aggiornamenti firmware inclusi", "Supporto specialistisco per progettazione copertura"])
                        ]),
                        collect([
                            'id' => 0,
                            'name' => 'DA 6 A 15 ANTENNE CONNESSE',
                            'prices' => [
                                'fee_amount' => 19.99,
                            ],
                            'items' => json_encode(["App su smartphone per il controllo in tempo reale", "Assistenza tecnica inclusa", "Aggiornamenti firmware inclusi", "Supporto specialistisco per progettazione copertura"])
                        ]),
                        collect([
                            'id' => 0,
                            'name' => 'DA 16 A 30 ANTENNE CONNESSE',
                            'prices' => [
                                'fee_amount' => 24.99,
                            ],
                            'items' => json_encode(["App su smartphone per il controllo in tempo reale", "Assistenza tecnica inclusa", "Aggiornamenti firmware inclusi", "Supporto specialistisco per progettazione copertura"])
                        ]),
                        collect([
                            'id' => 0,
                            'name' => 'OLTRE 30 ANTENNE CONNESSE',
                            'prices' => [
                                'fee_amount' => 0,
                            ],
                            'items' => json_encode(["App su smartphone per il controllo in tempo reale", "Assistenza tecnica inclusa", "Aggiornamenti firmware inclusi", "Supporto specialistisco per progettazione copertura"])
                        ])
                    ]),
                    'type' => 'double',
                    'bg' => 'linear-gradient(135deg, #FBDA61 2.88%, #F76B1C 98.13%)',
                    'bg_box' => 'linear-gradient(135deg, #A1FF8B 0%, #3F93FF 96.83%)',
                ]),
            ]),

            'router' => collect([
                'router' => collect([
                    'products' => $this->withPrices(
                        ProductAC::query()
                            ->where('category_id', 6)
                            ->orderBy('sort_number')
                            ->whereNull('hidden_at')
                            ->get()
                    ),
                    'type' => 'triple',
                    'bg' => 'linear-gradient(135deg, #FBDA61 2.88%, #F76B1C 98.13%)',
                    'bg_box' => 'linear-gradient(135deg, #A1FF8B 0%, #3F93FF 96.83%)',
                    'items' => [
                        [
                            'name' => 'DRAYTEK<br> 2765 AC',
                            'bg' => gradient('fftc'),
                            'image' => [
                                'image' => json_encode(['path' => ac_url("/storage/images/62c6f26125f8e8aaf2649262bd85280f_65095f0608597.png")])
                            ]
                        ],
                        [
                            'name' => 'DRAYTEK<br> 2865 AC',
                            'bg' => gradient('fftc'),
                            'image' => [
                                'image' => json_encode(['path' => ac_url("/storage/images/62c6f26125f8e8aaf2649262bd85280f_65095f0608597.png")])
                            ]
                        ],
                        [
                            'name' => 'DRAYTEK<br> 2865 AX',
                            'bg' => gradient('fftc'),
                            'image' => [
                                'image' => json_encode(['path' => ac_url("/storage/images/62c6f26125f8e8aaf2649262bd85280f_65095f0608597.png")])
                            ]
                        ],
                        [
                            'name' => 'FRITZ!BOX<br> AVM 7590',
                            'bg' => gradient('fftc&ffth'),
                            'image' => [
                                'image' => json_encode(['path' => ac_url("/storage/images/62c6f26125f8e8aaf2649262bd85280f_65095f0608597.png")])
                            ]
                        ],
                    ],
                ]),
                'black' => [
                    'products' => collect([]),
                    'tags' => [],
                    'items' => [
                        [
                            'name' => 'DRAYTEK<br> 3910',
                            'bg' => gradient('fftc'),
                            'image' => [
                                'image' => json_encode(['path' => ac_url("/storage/images/b0846eb5a1fd17b838f0433b77ea528a_65095f3aa4ed7.png")])
                            ]
                        ],
                        [
                            'name' => 'DRAYTEK<br> 2962',
                            'bg' => gradient('ffth'),
                            'image' => [
                                'image' => json_encode(['path' => ac_url("/storage/images/b0846eb5a1fd17b838f0433b77ea528a_65095f3aa4ed7.png")])
                            ]
                        ],
                        [
                            'name' => 'DRAYTEK<br> 2915 AC',
                            'bg' => gradient('ffth'),
                            'image' => [
                                'image' => json_encode(['path' => ac_url("/storage/images/b0846eb5a1fd17b838f0433b77ea528a_65095f3aa4ed7.png")])
                            ]
                        ],
                        [
                            'name' => 'DRAYTEK<br> 2927AC',
                            'bg' => gradient('ffth'),
                            'image' => [
                                'image' => json_encode(['path' => ac_url("/storage/images/b0846eb5a1fd17b838f0433b77ea528a_65095f3aa4ed7.png")])
                            ]
                        ],
                    ],
                ]
            ]),
            'access-point' => collect([
                'access-point' => collect([
                    'products' => $this->withPrices(
                        ProductAC::query()
                            ->where('name', 'like',  '%wifi 7%')
                            ->whereNull('hidden_at')
                            ->orderBy('sort_number')
                            ->get()
                    ),
                    'type' => 'double',
                    'bg' => 'url("/pages/gradients/antenna.png")',
                    'bg_box' => 'url("/pages/gradients/antenna.png")',
                ]),
            ]),
            'telefoni-voip' => collect([
                'access-point' => collect([
                    'products' => collect([
                        collect([
                            'id' => 0,
                            'name' => 'Telefono Desktop',
                            'prices' => [
                                'fee_amount' => 2.90,
                            ],
                            'items' => json_encode([
                                "2 porte gigabit",
                                "Display grafico retroilluminato",
                                "Due pulsanti programmabili",
                                "Alimentazione PoE o tramite alimentatore esterno"
                            ]),
                            'tags' => json_encode(["Desktop"]),
                            'image' => [
                                'image' => json_encode(['path' => url("pages/products/desktop-phone.png")])
                            ]
                        ]),
                        collect([
                            'id' => 0,
                            'name' => 'Telefono Direzionale',
                            'prices' => [
                                'fee_amount' => 6.90,
                            ],
                            'items' => json_encode([
                                "2 porte gigabit",
                                "Display a colori da 7 pollici, touch",
                                "29 pulsanti programmabili",
                                "Alimentazione PoE o tramite alimentatore esterno"
                            ]),
                            'tags' => json_encode(["Desktop"]),
                            'image' => [
                                'image' => json_encode(['path' => url("pages/products/directional-phone.png")])
                            ]
                        ]),
                        collect([
                            'id' => 0,
                            'name' => 'Telefono <br>Posto<br> Operatore',
                            'prices' => [
                                'fee_amount' => 9.90,
                            ],
                            'items' => json_encode([
                                "2 porte gigabit",
                                "Display a colori da 7 pollici, touch",
                                "50+ pulsanti DSS programmabili",
                                "Alimentazione PoE o tramite alimentatore esterno"
                            ]),
                            'tags' => json_encode(["Desktop", "Posto Operatore"]),
                            'image' => [
                                'image' => json_encode(['path' => url("pages/products/directional-phone.png")])
                            ]
                        ]),
                        collect([
                            'id' => 0,
                            'name' => 'Telefono <br>Wi-Fi<br>Cordless',
                            'prices' => [
                                'fee_amount' => 5.90,
                            ],
                            'tags' => json_encode(["Desktop", "WiFi"]),
                            'items' => json_encode([
                                "WiFi dual band",
                                "Display a colori",
                                "Base di ricarica",
                                "7.5 ore di conversazione"
                            ]),
                            'image' => [
                                'image' => json_encode(['path' => url("pages/products/cordless-phone.png")])
                            ]
                        ]),
                    ]),
                    'items' => [
                        [
                            'name' => 'Telefono Desktop',
                            'bg' => 'url("/pages/gradients/telefoni-voip.png")',
                            'image' => [
                                'image' => json_encode(['path' => url("pages/products/desktop-phone.png")])
                            ]
                        ],
                        [
                            'name' => 'Telefono Direzionale',
                            'bg' => 'url("/pages/gradients/telefoni-voip.png")',
                            'image' => [
                                'image' => json_encode(['path' => url("pages/products/directional-phone.png")])
                            ]
                        ],
                        [
                            'name' => 'Cordless WiFi',
                            'bg' => 'url("/pages/gradients/telefoni-voip.png")',
                            'image' => [
                                'image' => json_encode(['path' => url("pages/products/cordless-phone.png")])
                            ]
                        ],
                        [
                            'name' => 'Telefono Operatore',
                            'bg' => 'url("/pages/gradients/telefoni-voip.png")',
                            'image' => [
                                'image' => json_encode(['path' => url("pages/products/directional-phone.png")])
                            ]
                        ],
                    ],
                    'type' => 'double',
                    'bg' => 'url("/pages/gradients/telefoni-voip.png")',
                    'bg_box' => 'url("/pages/gradients/telefoni-voip.png")',
                ]),
            ]),

            // Only for coverage
            '4g5g' => collect([
                '4g5g' => collect([
                    'products' => $this->withPrices(
                        ProductAC::query()
                            ->where('category_id', 2)
                            ->where(function ($query) {
                                $query->where('name', 'like',  '%4G%')
                                    ->orWhere('name', 'like',  '%5G%');
                            })
                            ->whereNull('hidden_at')
                            ->orderBy('sort_number')
                            ->get()
                    ),
                    'type' => 'double',
                    'bg' => 'linear-gradient(135deg, #FBDA61 2.88%, #F76B1C 98.13%)',
                    'bg_box' => 'linear-gradient(135deg, #A1FF8B 0%, #3F93FF 96.83%)',
                ]),
            ]),
        ];
    }

    private function getAllServiceProducts(): array
    {
        $products = $this->products();

        $products['wireless']['wireless']['products'] = $this->withPrices(
            ProductAC::query()
                ->where('category_id', 2)
                ->where(function ($q) {
                    $q->where('name', 'like',  '%Wireless%')
                        ->orWhere('name', 'like',  '%4G%')
                        ->orWhere('name', 'like',  '%5G%');
                })
                ->whereNull('hidden_at')
                ->orderBy('sort_number')
                ->where('name', 'like', '%wireless%')
                ->where('name', 'not like', '%4g%')
                ->where('name', 'not like', '%5g%')
                ->get()
        );

        return $products;
    }

    private function getServiceProducts($service)
    {
        return $this->products()[$service] ?? null;
    }

    function redirectToRestaurant() {
        return redirect('/soluzioni/ristorazione');
    }

    function showServizio($url) {
        $url = strtolower($url);
        $servizio = FiberdroidService::query()->where('url', $url)->first();
        $products = $this->getServiceProducts($url);

        if (!$servizio) {
            abort(404);
        }

        return view('pages.servizio', ['service' => $servizio, 'products' => $products, 'bubbles' => true]);
    }

    function showSoluzione($url) {
        $url = strtolower($url);
        $soluzione = FiberdroidService::query()->where('url', $url)->first();

        if (!$soluzione) {
            abort(404);
        }

        return view('pages.soluzione', ['solution' => $soluzione, 'bubbles' => true]);
    }

    function showFiberJet() {
        return view('pages.jetfiber');
    }

    function showVerificaCopertura() {
        $services = $this->getAllServiceProducts();

        return view('pages.verifica-copertura', ['servicesData' => $services]);
    }

    function showProdotto($url) {
        $url = strtolower($url);
        $product = FiberdroidService::query()->where('url', $url)->first();
        $products = $this->getServiceProducts($url);

        if (!$product) {
            abort(404);
        }

        if ($url === 'router') {
            return view('pages.routers', ['service_product' => $product, 'products' => $products, 'bubbles' => false]);
        }

        return view('pages.prodotto', ['service_product' => $product, 'products' => $products, 'bubbles' => true]);
    }

    function showAssistenza() {
        return view('pages/assistenza');
    }

    function showPrivacyPolicy() {
        return view('pages/policy-privacy', ['noNavbar' => true]);
    }

    function showCookiePolicy() {
        return view('pages/policy-cookie');
    }

    function showCartaServizi() {
        return view('pages/carta-servizi');
    }

    function showComingSoon() {
        return view('pages/coming-soon');
    }

    function showThankYou() {
        $articles = CustomArticle::query()->whereIn('id', [
            23, 22, 14
        ])->orderBy('id', 'DESC')->get();

        return view('thanks-you', ['articles' => $articles]);
    }

    function articleApi($url) {
        $article = CustomArticle::query()->where('url', $url)->first();

        if( !is_null($article) ) {
            $view = view('custom_article_no_template', ['article' => $article]);
            $html = $view->render();

            return ['content' => $html];
        }

        return abort(404);
    }

    private function validateMeta($meta, $value): array {
        $errors = [];

        $len = strlen($value);

        switch ($meta) {
            case 'title':
                if($len < 50) {
                    $errors[]= "meta-title is too short <b>($len/50)</b>";
                }

                if($len > 70) {
                    $errors[]= "meta-title is too long <b>($len/70)</b>";
                }
                break;

            case 'description':
                if($len < 70) {
                    $errors[]= "meta-description is too short <b>($len/70)</b>";
                }

                if($len > 155) {
                    $errors[]= "meta-description is too long <b>($len/155)</b>";
                }
                break;
        }

        return $errors;
    }

    function sitemapApi(): Collection {
        $sitemap_raw = simplexml_load_string(
            file_get_contents(public_path('sitemap.xml'))
        );

        $sitemap = collect(json_decode(json_encode($sitemap_raw))->url);

        $sitemap = $sitemap->map(function ($url) {
            $html = file_get_contents($url->loc);

            $meta_regex = "/<meta name=\"(.*)\" content=\"(.*)\">/";

            preg_match_all($meta_regex, $html, $meta_matched);

            $meta = [];

            for($i=0; $i<sizeof($meta_matched[1]); $i++) {
                $meta_name = $meta_matched[1][$i];
                $meta_value = $meta_matched[2][$i];

                $errors = $this->validateMeta($meta_name, $meta_value);

                $meta[$meta_name] = [
                    'value' => $meta_value,
                    'errors' => $errors,
                ];
            }

            $title_regex = "/<title>(.*)<\/title>/";
            preg_match($title_regex, $html, $title_matched);

            $meta['title'] = [
                'value' => $title_matched[1],
                'errors' => [],
            ];

            return [
                'loc' => $url->loc,
                'meta' => $meta
            ];
        });

        return $sitemap;
    }

    function redirectToBlog() {
        return redirect('/blog', 301);
    }

}
