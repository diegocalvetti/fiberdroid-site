<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FiberdroidController;
use App\Mail\OrderSentMail;
use App\Models\City;
use App\Models\CustomArticle;
use App\Models\ListiniHasProdotto;
use App\Models\Newsletter;
use App\Models\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class FiberdroidApiControllerV2 extends Controller
{
    private string $token = "Bearer 924|F9UfKcEhxHHon9DsBQElbeRwdwd4NpaVn4DjlaPu";
    private string $apiUrl = "https://coverage.mywic.it/api/v1";

    private function toolCall($path, array $params) {
        $query = Arr::query($params);

        return Http::withHeaders([
            'Authorization' => $this->token
        ])->get($this->apiUrl . "/tools/{$path}?{$query}");
    }

    private function geoCall($path, array $params) {
        $query = Arr::query($params);

        return Http::withHeaders([
            'Authorization' => $this->token
        ])->get($this->apiUrl . "/geo/{$path}?{$query}");
    }

    private function coverageCall($geoId) {
        $query = Arr::query(['geoid' => $geoId]);

        return Http::withHeaders([
            'Authorization' => $this->token
        ])->get($this->apiUrl . "/coverage/check?{$query}");
    }

    private function linkCall($path, array $params = []) {
        $query = Arr::query($params);

        return Http::withHeaders([
            'Authorization' => $this->token
        ])->get($this->apiUrl . "/coverage/{$path}?{$query}");
    }

    private function getGeoID($city, $street, $civic, $stair = null) {
        $geoData = $this->geoCall("find", [
            'city' => $city,
            'street' => $street,
            'number' => $civic,
        ]);

        return $geoData['id'];
    }

    public function cities()
    {
        return City::all();
    }

    public function streets($city, $street) {
        return $this->geoCall("search", [
            'city' => $city,
            'q' => $street,
        ]);
    }

    public function civics($city, $street, $civic) {
        return $this->geoCall("search", [
            'city' => $city,
            'street' => $street,
            'q' => $civic,
        ]);
    }

    public function stairs($city, $street, $civic)
    {
        return $this->geoCall("search", [
            'city' => $city,
            'street' => $street,
            'number' => $civic,
        ]);
    }

    public function coverage($city, $street, $civic, $stair = null)
    {
        $coverage = $this->coverageCall(
            $this->getGeoID($city, $street, $civic, $stair)
        )->json();


        //el.technology.family.name === 'FTTX'

        $fttx = collect($coverage['coverage'])->filter(function ($el) {
            return $el['technology']['family']['name'] == 'FTTX';
        });

        $fttc = $fttx->filter(function ($el) {
            return $el['technology']['name'] == 'FTTC';
        });

        $ftth = $fttx->filter(function ($el) {
            return $el['technology']['name'] == 'FTTH';
        });

        $fttc_max_speed = $fttc->max('data.max_speed');
        $ftth_max_speed = $ftth->max('data.max_speed');

        return $coverage;
    }

    public function boundaries($lat, $lng, $radius)
    {
        return $this->toolCall('bounds', [
            'lat' => $lat,
            'lng' => $lng,
            'radius' => $radius,
        ]);
    }

    public function cabinets($provider, $north, $south, $east, $west)
    {
        return $this->linkCall(
            "cabinets/{$provider}", [
                "north" => $north,
                "south" => $south,
                "east" => $east,
                "west" => $west,
            ]
        );
    }

    public function centrals($provider, $north, $south, $east, $west)
    {
        return $this->linkCall(
            "centrals/{$provider}", [
                "north" => $north,
                "south" => $south,
                "east" => $east,
                "west" => $west,
            ]
        );
    }

    public function bts()
    {
        return $this->linkCall("bts");
    }

    public function elevation($from_lat, $from_lng, $to_lat, $to_lng)
    {
        return $this->toolCall("elevation-profile", [
            "lat1" => $from_lat,
            "lng1" => $from_lng,
            "lat2" => $to_lat,
            "lng2" => $to_lng,
        ]);
    }

    private function evaluateQuiz($data, $increasePower = 0): array {
        $services = [

            /**
             * AVAILABLE CONNECTIVITY
             * Ordered by power
             */
            'connectivity' => [
                //"FTTH 10/2 Gbps"
                1 => [
                    'name' => "FTTH 10/2 Gbps",
                    'items' => [
                        ["name" => "10 Giga in download + 2 Giga in upload", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2915",
                        'withBackup' => "3910",
                    ],
                    'item_id' => 185,
                ],
                //"FTTH 2.5/1 Gbps"
                2 => [
                    'name' => 'FTTH 2.5/1 Gbps',
                    'items' => [
                        ["name" => "2.5 Giga in download + 1 Giga in upload", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2915",
                        'withBackup' => "3910",
                    ],
                    'item_id' => 163,
                ],
                // "FTTH 2.5/500 Gbps/Mbps"
                3 => [
                    'name' => 'FTTH 2.5/500 Gbps/Mbps',
                    'items' => [
                        ["name" => "2.5 Giga in download + 500 Mega in upload", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2915",
                        'withBackup' => "3910",
                    ],
                    'item_id' => 162,
                ],
                // "FTTH 1/500 Gbps/Mbps"
                4 => [
                    'name' => 'FTTH 1/500 Gbps/Mbps',
                    'items' => [
                        ["name" => "1 Giga in download + 500 Mega in upload", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2915",
                        'withBackup' => "3910",
                    ],
                    'item_id' => 9,
                ],
                // "FTTH 1/300 Gbps/Mbps"
                5 => [
                    'name' => 'FTTH 1/300 Gbps/Mbps',
                    'items' => [
                        ["name" => "1 Giga in download + 300 Mega in upload", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2915",
                        'withBackup' => "3910",
                    ],
                    'item_id' => 9,
                ],
                // "FTTC 200/20 Mbps"
                6 => [
                    'name' => 'FTTC 200/20 Mbps',
                    'items' => [
                        ["name" => "200 Mega in download + 20 Mega in upload", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2765",
                        'withBackup' => "2915",
                    ],
                    'item_id' => 4,
                ],
                // "FTTC 100/20 Mbps"
                7 => [
                    'name' => 'FTTC 100/20 Mbps',
                    'items' => [
                        ["name" => "100 Mega in download + 20 Mega in upload", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2765",
                        'withBackup' => "2915",
                    ],
                    'item_id' => 3,
                ],
                // "FTTC 50/10 Mbps"
                8 => [
                    'name' => 'FTTC 50/10 Mbps',
                    'items' => [
                        ["name" => "50 Mega in download + 10 Mega in upload", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2765",
                        'withBackup' => "2915",
                    ],
                    'item_id' => 2,
                ],
                // "FTTC 30/3 Mbps"
                9 => [
                    'name' => 'FTTC 30/3 Mbps',
                    'items' => [
                        ["name" => "30 Mega in download + 3 Mega in upload", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2765",
                        'withBackup' => "2915",
                    ],
                    'item_id' => 1,
                ],
                // "Wireless 100"
                10 => [
                    'name' => 'Wireless 100',
                    'items' => [
                        ["name" => "Wireless", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2765",
                        'withBackup' => "2915",
                    ],
                    'item_id' => 71,
                ],
                // "5G"
                11 => [
                    'name' => '5G',
                    'items' => [
                        ["name" => "5G", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2765",
                        'withBackup' => "2915",
                    ],
                    'item_id' => 165,
                ],
                // "Wireless 30"
                12 => [
                    'name' => 'Wireless 30',
                    'items' => [
                        ["name" => "Wireless", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2765",
                        'withBackup' => "2915",
                    ],
                    'item_id' => 41,
                ],
                // "4G"
                13 => [
                    'name' => '4G',
                    'items' => [
                        ["name" => "Wireless", "icon" => "download.svg"],
                        ["name" => "IP Statico pubblico", "icon" => "ip.svg"],

                        ["name" => "Monitoring linea", "icon" => "line.svg"],
                    ],
                    'routers' => [
                        'base' => "2765",
                        'withBackup' => "2915",
                    ],
                    'item_id' => 108,
                ],
            ]
        ];
        $numbers = [
            [
                'name' => 'NUMERO VOIP SINGOLO',
                'items' => [
                    ["name" => "Numero singolo fino a 128 chiamate in contemporanea", "icon" => "phone.svg"],
                ],
                'item_id' => 24,
            ],
            [
                'name' => 'GNR 10 NUMERI',
                'items' => [
                    ["name" => "10 Numerazioni fino a 128 chiamate in contemporanea", "icon" => "phone.svg"],
                ],
                'item_id' => 25,
            ],
            [
                'name' => 'GNR 30 NUMERI',
                'items' => [
                    ["name" => "30 Numerazioni fino a 128 chiamate in contemporanea", "icon" => "phone.svg"],
                ],
                'item_id' => 27,
            ],
            [
                'name' => 'GNR 50 NUMERI',
                'items' => [
                    ["name" => "50 Numerazioni fino a 128 chiamate in contemporanea", "icon" => "phone.svg"],
                ],
                'item_id' => 28,
            ],
            [
                'name' => 'GNR 100 NUMERI',
                'items' => [
                    ["name" => "100 Numerazioni fino a 128 chiamate in contemporanea", "icon" => "phone.svg"],
                ],
                'item_id' => 29,
            ],
        ];
        $centrales = [
            [
                'name' => 'CENTRALE VOIP CLOUD 4',
                'items' => [
                    ["name" => "Centrale Cloud 4 Contemporaneità", "icon" => "centrale.svg"],
                    ["name" => "Interni illimitati", "icon" => "infinite.svg"],
                ],
                'item_id' => 63,
            ],
            [
                'name' => 'CENTRALE VOIP CLOUD 16',
                'items' => [
                    ["name" => "Centrale Cloud 16 Contemporaneità", "icon" => "download.svg"],
                    ["name" => "Interni illimitati", "icon" => "infinite.svg"],
                ],
                'item_id' => 57,
            ],
            [
                'name' => 'CENTRALE VOIP CLOUD 24',
                'items' => [
                    ["name" => "Centrale Cloud 24 Contemporaneità", "icon" => "download.svg"],
                    ["name" => "Interni illimitati", "icon" => "infinite.svg"],
                ],
                'item_id' => 57,
            ],
        ];
        $backup4G = [
            'name' => '4G Backup',
            'items' => [
                ["name" => "4G Out CAT 6 Vodafone", "icon" => "centrale.svg"],
            ],
            'item_id' => 171,
        ];
        $telephone = [
            'name' => '',
            'items' => [
                ["name" => "Telefono Yealink T31G", "icon" => "telephone.svg"],
            ],
            'item_id' => 66,
        ];

        $routers = [
            "2765" => [
                'name' => "2765",
                'items' => [
                    ["name" => "Router Professionale Draytek 2765", "icon" => "router.svg"],
                ],
                'item_id' => 64,
            ],
            "2915" => [
                'name' => "2915",
                'items' => [
                    ["name" => "Router Professionale Draytek 2915", "icon" => "router.svg"],
                ],
                'item_id' => 65,
            ],
            "2962" => [
                'name' => "2962",
                'items' => [
                    ["name" => "Router Professionale Draytek 2962", "icon" => "router.svg"],
                ],
                'item_id' => 183,
            ],
            "3910" => [
                'name' => "3910",
                'items' => [
                    ["name" => "Router Professionale Draytek 3910", "icon" => "router.svg"],
                ],
                'item_id' => 186,
            ],
        ];

        $weight = [
            'email' => [
                7, 5, 5,
            ],
            'cloud' => [
                5, 3, 3
            ],
            'video' => [
                4, 3, 2,
            ],
            'file' => [
                6, 5, 2,
            ],
            'guest-wifi' => [
                3, 3, 2,
            ],
        ];

        $hasInternet = $data->where('id', 'internet')->isNotEmpty();
        $hasCentrale = $data->where('step', 2)->where('id', 'voip-phone')->isNotEmpty();
        $hasVoip = $hasCentrale || $data->where('id', 'phone')->isNotEmpty();

        $routerChoose = $data->where('id', 'router')->first();
        $hasRouter = $routerChoose ? boolval($routerChoose['value']) : false;

        $phoneChoose = $data->where('id', 'desk-phone')->first();
        $hasDeskPhone = $phoneChoose ? boolval($phoneChoose['value']) : false;

        $backupChoose = $data->where('id', 'desk-phone')->first();
        $hasBackup = $backupChoose ? boolval($backupChoose['value']) : false;

        $people = $data->where('step', 3)->first()['power'] ?? 0;


        $options = $data->where('step', 2)->map(function ($el) {return $el['id'];});

        $service = null;
        $phone = null;
        $centrale = null;
        $backup = null;
        $deskPhone = null;
        $router = null;

        $increasePhone = false;

        if ($hasInternet) {

            $suggested_power = $options
                ->filter(function ($el) {
                    return $el != "voip-phone";
                })
                ->map(function ($el) use($weight, $people){
                    return $weight[$el][$people];
                })
                ->sort()->first();

            $power = $suggested_power + $increasePower;
            if ($power < 1) {
                $power = 1;
                $increasePhone = true;
            }

            $service = $services['connectivity'][$power];

        } else {
            $increasePhone = true;
        }

        if ($hasInternet && $hasRouter) {
            $routerId = $service['routers'][$hasBackup ? 'withBackup' : 'base'];
            $router = $routers[$routerId];
        }

        if ($hasCentrale) {
            $centrale = $centrales[$people];

            if (!$increasePhone) {
                $phone = $numbers[0];
            } else {
                $phone = $numbers[-$increasePower];
            }

        } else if ($hasVoip) {
            $phone = $numbers[0];
        }

        if ($hasBackup) {
            $backup = $backup4G;
        }

        if ($hasDeskPhone) {
            $deskPhone = $telephone;
        }


        if ($service) {
            $prices = $this->getPrices($service['item_id']);

            $service['prices'] = [
                'fee_amount' => $prices['fee_amount'] ?? 0,
                'activation_amount' => $prices['activation_amount'] ?? 0,
            ];
        }

        if ($centrale) {
            $prices = $this->getPrices($centrale['item_id']);

            $centrale['prices'] = [
                'fee_amount' => $prices['fee_amount'] ?? 0,
                'activation_amount' => $prices['activation_amount'] ?? 0,
            ];
        }

        if ($backup) {
            $prices = $this->getPrices($backup['item_id']);

            $backup['prices'] = [
                'fee_amount' => $prices['fee_amount'] ?? 0,
                'activation_amount' => $prices['activation_amount'] ?? 0,
            ];
        }

        if ($phone) {
            $prices = $this->getPrices($phone['item_id']);

            $phone['prices'] = [
                'fee_amount' => $prices['fee_amount'] ?? 0,
                'activation_amount' => $prices['activation_amount'] ?? 0,
            ];
        }

        if ($deskPhone) {
            $prices = $this->getPrices($deskPhone['item_id']);

            $deskPhone['prices'] = [
                'fee_amount' => $prices['fee_amount'] ?? 0,
                'activation_amount' => $prices['activation_amount'] ?? 0,
            ];
        }

        if ($router) {
            $prices = $this->getPrices($router['item_id']);

            $router['prices'] = [
                'fee_amount' => $prices['fee_amount'] ?? 0,
                'activation_amount' => $prices['activation_amount'] ?? 0,
            ];
        }

        return [
            [
                'service' => $service,
                'centrale' => $centrale,
                'phone' => $phone,
                'backup' => $backup,
                'deskPhone' => $deskPhone,
                'router' => $router,
                'totalAmount' =>
                    (isset($service['prices']) ? $service['prices']['fee_amount'] : 0) +
                    (isset($centrale['prices']) ? $centrale['prices']['fee_amount'] : 0) +
                    (isset($backup['prices']) ? $backup['prices']['fee_amount'] : 0) +
                    (isset($phone['prices']) ? $phone['prices']['fee_amount'] : 0) +
                    (isset($deskPhone['prices']) ? $deskPhone['prices']['fee_amount'] : 0) +
                    (isset($router['prices']) ? $router['prices']['fee_amount'] : 0)
            ]
        ];
    }

    private function getPrices($id) {
        return ListiniHasProdotto::query()->where([
            'price_list_id' => config('site.BASE_PRICE_LIST_ID'),
            'product_id' => $id
        ])->first();
    }

    public function quiz(Request $request): array
    {

        $data = collect($request->input('data'));

        $suggested = $this->evaluateQuiz($data);
        $second = $this->evaluateQuiz($data, -1);
        $third = $this->evaluateQuiz($data, -2);

        return [
            $suggested,
            $second,
            $third,
        ];
    }

    public function newsletter(Request $request)
    {
        $data = $request->only(['email']);

        $validator = Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:newsletters,email'],
        ], [
            'email.unique' => 'Sei già iscritto alla newsletter',
            'email.email' => 'Formato email non valido',
        ]);

        if ($validator->fails()) {
            return response([
                'error' => $validator->getMessageBag()
            ], 400);
        }

        $newsletter = Newsletter::query()->create($data);

        return response([
            'data' => $newsletter
        ]);
    }

    public function order(Request $request)
    {
        $data = $request->only(['name', 'email', 'phone', 'address', 'cart']);

        $validator = Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'cart' => ['required', 'json'],
        ], [
            'email.email' => 'Formato email non valido',
            'required' => 'Campo obbligatorio'
        ]);

        if ($validator->fails()) {
            return response([
                'error' => $validator->getMessageBag()
            ], 400);
        }

        $order = Order::query()->create($data);

        $api = new FiberdroidController();

        try {
            $api->sendMail($data['email'], 'OrderSentMail', $data);
            $api->sendMail("speed@fiberdroid.it", 'OrderReceivedMail', $data);
        } catch (\Exception $e){
            return response([
                'data' => $e->getMessage()
            ]);
        }

        return response([
            'data' => $order
        ]);
    }

    function articleApi($url): array
    {
        $article = CustomArticle::query()->where('url', $url)->first();

        if( !is_null($article) ) {
            $view = view('old.custom_article_no_template', ['article' => $article]);
            $html = $view->render();

            return ['content' => $html];
        }

        abort(404);
    }
}
