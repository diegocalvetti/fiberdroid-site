<?php

namespace App\Http\Livewire;

use App\Http\Controllers\CoperturaController;
use Illuminate\Support\Arr;
use Livewire\Component;

class VerificaCopertura extends Component
{

    private $apiCopertura;

    public $isOk;

    public $token;
    public $provinces;
    public $cities;
    public $streets;
    public $street_numbers;

    public $selectedProvince;
    public $selectedCity;
    public $selectedStreet;
    public $selectedStreetNumber;
    public $services;
    public $showForm;


    public function mount() {
        $this->apiCopertura = new CoperturaController();
        $this->token = $this->apiCopertura->login();
        $this->provinces = json_decode(json_encode($this->apiCopertura->province($this->token)), true);
        $this->cities = [];
        $this->streets = [];
        $this->street_numbers = [];
        $this->services = [];

        $this->selectedProvince = "";
        $this->selectedCity = "";
        $this->selectedProvince = "";
        $this->selectedStreetNumber = "";

        $this->isOk = false;
    }

    public function loadCities($province) {

        $this->apiCopertura = new CoperturaController();
        $this->token = $this->apiCopertura->login();
        return $this->apiCopertura->cities($this->token, $province);
    }

    public function loadStreets($province, $city) {
        $this->apiCopertura = new CoperturaController();
        $this->token = $this->apiCopertura->login();
        return $this->apiCopertura->streets($this->token, $province, $city);
    }

    public function loadStreetNumbers($province, $city, $toponomastica, $street) {
        $this->apiCopertura = new CoperturaController();
        $this->token = $this->apiCopertura->login();
        return $this->apiCopertura->street_numbers($this->token, $province, $city, $toponomastica, $street);
    }

    public function loadServices($province, $city, $toponomastica, $street, $street_number){
        $this->apiCopertura = new CoperturaController();
        $this->token = $this->apiCopertura->login();
        return $this->apiCopertura->services($this->token, $province, $city, $toponomastica, $street, $street_number);
    }

    public function updatedSelectedProvince($value) {
        $this->selectedCity = "";
        $this->selectedStreet = "";
        $this->selectedStreetNumber = "";
        $this->cities = $this->selectedProvince != "" ? $this->loadCities($this->selectedProvince) : [];
        $this->streets = [];
        $this->street_numbers = [];
        $this->services = [];
        $this->showForm = false;
    }

    public function updatedSelectedCity() {
        $this->streets = $this->selectedCity != "" ? $this->loadStreets($this->selectedProvince, $this->selectedCity) : [];
        $this->street_numbers = [];
        $this->selectedStreetNumber = "";
        $this->services = [];
        $this->showForm = false;
    }

    public function updatedSelectedStreet($value) {
        $street = preg_split("/\|/", $value);
        $this->street_numbers = $this->selectedStreet != "" ? $this->loadStreetNumbers($this->selectedProvince, $this->selectedCity, $street[0], $street[1]) : [];
        $this->selectedStreetNumber = "";
        $this->services = [];
        $this->showForm = false;
    }

    public function updatedSelectedStreetNumber(){
        $street = preg_split("/\|/", $this->selectedStreet);
        $this->services = $this->selectedStreetNumber != "" ? $this->loadServices($this->selectedProvince, $this->selectedCity, $street[0], $street[1], $this->selectedStreetNumber) : [];
        $this->showForm = true;
        if( sizeof($this->services) > 0 ) {

            $this->services = Arr::except($this->services, [
               'desc_provincia',
               'desc_comune',
               'desc_part_toponomastica',
               'via',
               'civico'
            ]);

            $this->services = array_map(function ($value) {
                $value = str_replace("OKIndiretta", "Indiretta", $value);
                $value = str_replace("richiedere", "Richiedere", $value);
                return $value;
            }, $this->services);

            $this->isOk = true;
        }
    }

    public function submit() {
        dd($this->getDataForValidation([]));
    }

    public function render()
    {
        return view('livewire.verifica-copertura');
    }
}
