<?php

namespace App\Http\Livewire;

use App\Http\Controllers\FiberdroidController;
use App\Models\Lead;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;

class ContactForm extends Component
{

    public $name;
    public $uname;
    public $surname;
    public $email;
    public $telephone;
    public $message;
    public $privacy;
    public $verifica = null;

    protected function validator(array $data, $type)
    {
        $validate = [];

        switch ($type) {
            case 'submit':
                $validate = [
                    'name' => ['required', 'regex:/^[a-zA-Z][a-zA-Z\s]*$/', 'max:60'],
                    'surname' => ['required', 'regex:/^[a-zA-Z][a-zA-Z\s]*$/', 'max:60'],
                    'email' => ['required','email:rfc,dns','max:60'],
                    'telephone' => ['required','regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/','max:60'],
                    'message' => ['nullable','max:500'],
                    'privacy' => ['required'],
                ];
                break;
        }

        $messages = [
            'required' => 'Campo obbligatorio!',
            'max' => 'Inserisci al massimo :value caratteri',
            'email' => 'Inserisci un indirizzo valido',
            'privacy.required' => 'Campo obbligatorio!',
            'alpha' => "Puoi inserire solo lettere",
            'name.regex' => "Puoi inserire solo caratteri e spazi!",
            'surname.regex' => "Puoi inserire solo caratteri e spazi!",
            'telephone.regex' => "Inserisci un numero di telefono valido"
        ];

        return Validator::make($data, $validate, $messages);
    }

    public function submit()
    {
        $data = $this->getDataForValidation([]);

        $validator = $this->validator($data, "submit");

        if( $validator->fails() ) {
            return redirect()->back()->with(['errors' => $validator->getMessageBag()]);
        }

        $lead = new Lead();
        $lead->name = $data['name'];
        $lead->surname = $data['surname'];
        $lead->email = $data['email'];
        $lead->telephone = $data['telephone'];
        $lead->message = $data['message'];
        $lead->copertura = $this->verifica;
        $lead->save();

        $response = [
            'title' => "Grazie di aver scelto Fiberdroid!",
            'content' => "Grazie per averci contattato! I nostri esperti riceveranno il tuo messaggio e ti risponderanno con una mail."
        ];

        $api = new FiberdroidController();

        try {
            $api->sendMail($data['email'], "ContactMail", [
                'name' => $data['name'] . " " . $data['surname'],
                'message' => $data['message'],
                'verifica' => $this->verifica
            ]);
        } catch (\Exception $e){}

        try {
            $api->sendMail("speed@fiberdroid.it", "ContactFiberdroidMail", [
                'name' => $data['name'] . " " . $data['surname'],
                'message' => $data['message'],
                'email' => $data['email'],
                'telephone' => $data['telephone'],
                'verifica' => $this->verifica
            ]);
        } catch (\Exception $e) {}


        session()->flash('response', $response);
        $this->reset();

        return redirect()->to('/thank-you');
    }

    public function render()
    {
        return view('livewire.contact-form');
    }
}
