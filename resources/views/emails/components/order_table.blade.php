<table class="table price-table">
    <tr style="background-color: white">
        <th class="th">
            Prodotto
        </th>
        <th class="th">
            Prezzo
        </th>
    </tr>
    @foreach(json_decode($products) as $key => $product)
        <tr>
            <td class="pc-sm-fs-18 pc-xs-fs-16 pc-fb-font" style="{{ $key % 2 == 0 ? 'background-color: #dddddd' : '' }}">
                {{ $product->name }}
            </td>
            <td class="pc-sm-fs-18 pc-xs-fs-16 pc-fb-font pl-6" style="{{ $key % 2 == 0 ? 'background-color: #dddddd' : '' }}">
                {{ number_format($product->price) }}&euro;
            </td>
        </tr>
    @endforeach

</table>

<style>
    .price-table th, .price-table td {
        padding: 5px 20px;
    }
</style>
