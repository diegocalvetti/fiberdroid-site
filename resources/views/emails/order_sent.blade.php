@extends('old.emails._layout_mail')

@section('content')

    <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
        <tbody>
        <tr>
            <td height="8" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
        </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
        <tbody>
        <tr>
            <td class="pc-sm-p-30-20 pc-xs-p-25-10" style="padding: 40px 30px; background: #ffffff; border-radius: 8px;" bgcolor="#ffffff" valign="top">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                    <tbody>
                    <tr>
                        <td class="pc-sm-fs-30 pc-fb-font" style="font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 46px; letter-spacing: -0.6px; color: #151515; padding: 0 10px;" valign="top">Benvenuto su Fiberdroid.it</td>
                    </tr>
                    <tr>
                        <td height="15" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                    </tr>
                    </tbody>
                    <tbody>
                    <tr>
                        <td class="pc-sm-fs-18 pc-xs-fs-16 pc-fb-font" style="font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 20px; line-height: 30px; letter-spacing: -0.2px; color: #9B9B9B; padding: 0 10px" valign="top">
                            Carissimo/a {{ $data['name'] }}, grazie per averci contattato, i tecnici di Fiberdroid analizzeranno la tua richiesta
                            e ti ricontatteranno il prima possibile.
                            <br><br>
                            Richiesta effettuata per l'indirizzo: <br>
                            <i>{{ $data['address'] }}</i>
                            <br><br>
                            Riepilogo dei prodotti selezionati:
                        </td>
                    </tr>
                    <tr>
                        <td height="25" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="pc-sm-fs-18 pc-xs-fs-16 pc-fb-font" style="font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 20px; line-height: 30px; letter-spacing: -0.2px; color: #9B9B9B; padding: 0 10px" valign="top">
                            @component('emails.components.order_table', ['products' => $data['cart']])@endcomponent
                        </td>
                    </tr>
                    </tbody>
                    <tbody>
                    <tr>
                        <td height="25" style="line-height: 1px; font-size: 1px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="padding: 0 10px;" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                <tbody>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

@endsection
