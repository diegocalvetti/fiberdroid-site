@extends('layouts._layout_v2')

@section('title', ($service_product->meta_title ?: "$service_product->name") . " | Fiberdroid")
@section('description', $service_product->meta_description)

@section('content')
    <div class="columns pt-6-mobile">
        <div class="column is-12 px-6-mobile" style="padding-top: 59px">
            <div class="columns my-6 is-vcentered">
                <div class="column is-5 is-offset-1">
                    <h1 class="service-text-title mt-6-tablet mt-6-desktop mt-2-mobile">
                        {{ $service_product->name }}
                    </h1>
                    <p class="service-text-description mt-6-tablet mt-6-desktop mt-3-mobile">
                        {!! $service_product->description !!}
                    </p>
                </div>
                <div class="column is-6 hidden-mobile is-flex">
                    <div style="display: flex; flex-direction: column" class="product-main-image-wrapper-{{ \Illuminate\Support\Str::slug($service_product->name) }}">
                        <img src="{{ asset('pages/' . ($service_product->bg ?? "services/fiber.png")) }}" class="product-main-image-{{ \Illuminate\Support\Str::slug($service_product->name) }}" style="width: 356px; border-radius: 20px; position: relative; z-index: 999" alt="Immagine prodotto {{ strtolower($service_product->name) }}">

                        @if($service_product->name == "Telefoni VoIp")
                            <div class="mx-auto" style="display: flex">
                            <div class="columns is-vcentered is-unselectable">
                                <div class="column">
                                    <div class="figure" style="width: 52px; height: 52px">
                                        <img alt="icona-voip-upload" src="{{ asset('pages/products/voip/icon_1.svg') }}">
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="figure" style="width: 52px; height: 52px">
                                        <img alt="icona-videocall" src="{{ asset('pages/products/voip/icon_2.svg') }}">
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="figure" style="width: 80px; height: 60px">
                                        <img alt="icona-chiama" src="{{ asset('pages/products/voip/icon_main.svg') }}">
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="figure" style="width: 52px; height: 52px">
                                        <img alt="icona-videocall" src="{{ asset('pages/products/voip/icon_2.svg') }}">
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="figure" style="width: 52px; height: 52px">
                                        <img alt="icona-voip-upload" src="{{ asset('pages/products/voip/icon_1.svg') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($products)
        @foreach($products->values() as $key => $productInfo)
            <div class="columns hidden-mobile ml-4 mt-4 pt-6" @if($key % 2 == 1) style="display: none; position: relative; left: -14%" @else style="display: none" @endif>
                @foreach($productInfo['items'] ?? [] as $item)
                    <div class="column is-carousel-column">
                        @component('pages.components.products.carousel-product-box', $item)@endcomponent
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif

    @php
        function get_product_tags($products) {
            $tags = collect([]);

            foreach ($products as $productInfo) {
                $productTags = $productInfo['products']->pluck('tags')->map(function ($el) {return json_decode($el);})->toArray();

                foreach ($productTags as $productTagArray) {
                    if($productTagArray) {
                        $tags->push(...$productTagArray);
                    }
                }
            }

            return $tags->unique();
        }
        $tags = get_product_tags($products);
    @endphp
    @if($products && $tags->isNotEmpty())
        <div class="columns is-mobile my-6-desktop my-6-tablet pt-6 pb-2" style="position: relative">
            <div class="column is-12 has-text-centered is-flex">
                <div class="field is-grouped is-grouped-multiline mx-auto">
                    @foreach($tags as $tag)
                        <div class="control">
                            <div class="tags is-clickable filter-tags is-unselectable" data-tag-content="{{ $tag }}" style="border: 1.638px solid #FFF; position:relative; z-index: 1000">
                                <span class="tag filter-tag is-white" style="margin: 0">{{ $tag }}</span>
                                <span class="tag filter-tag is-delete is-hidden"></span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @else
        <div class="mt-6 pt-6"></div>
    @endif

    <div class="columns">
        <div class="column is-8 mx-auto">
            @if($products)
                @foreach($products->where('type', 'double') as $type => $productInfo)
                    <div class="columns is-multiline mx-3-mobile" data-section data-{{$type}}>
                        @foreach($productInfo['products'] as $key => $product)
                            <div class="column is-6 px-5 py-5 is-flex" @if($key == intval(sizeof($productInfo['products']) / 2)) id="all" @endif>
                                @component('pages.components.service-box', [
                                    'title' => $product['website_name'] ?? $product['name'],
                                    'type' => "Connettività",
                                    'price' => $product['prices']['fee_amount'],
                                    'image' => $product['image'] ?? null,
                                    'bg' => $product['gradient'] ?? ($productInfo['bg_box'] ?? $productInfo['bg']),
                                    'mark' => $product['mark'] ?? null,
                                    'tags' => $product['tags'] ?? null,
                                    'classes' => $key % 2 == 0 ? "ml-auto" : "mr-auto",
                                    'items' => $product['items'] ? json_decode($product['items']) : ["empty", "empty", "empty"]
                                ])@endcomponent
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    @if($products)
        @foreach($products->where('type', 'triple') as $type => $productInfo)
            <div class="columns is-multiline mx-6-tablet mx-6-desktop mx-3-mobile" data-section data-{{ $type }}>
                @foreach($productInfo['products']->values() as $key => $product)
                    <div class="column is-4 px-4 py-5 is-flex" @if($key == intval(sizeof($productInfo['products']) / 2)) id="all" @endif>
                        @component('pages.components.service-box', [
                            'title' => $product['website_name'] ?? $product['name'],
                            'type' => "Connettività",
                            'price' => $product->prices['fee_amount'],
                            'image' => $product['image'] ?? null,
                            'bg' => $product['gradient'] ?? ($productInfo['bg_box'] ?? $productInfo['bg']),
                            'mark' => $product->mark ?? null,
                            'tags' => $product['tags'] ?? null,
                            'classes' => $key % 3 == 0 ? "ml-auto" : ($key % 3 == 1 ? "mx-auto" : "mr-auto") ,
                            'items' => $product->items ? json_decode($product->items) : ["empty", "empty", "empty"]
                        ])@endcomponent
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif

    @if($service_product->views['termini'] ?? false)
        <div class="columns mt-6 px-4-mobile">
            <div class="column is-12">
                <div class="box service-termini-box py-6 mx-auto">
                    {!! $service_product->views['termini'] !!}
                </div>
            </div>
        </div>
    @endif

    <div class="columns">
        <div class="column is-12">
            <web-component-fiberdroid-quiz
                id="quiz-component"
                icon="{{ $service_product->quiz_icon ?? "quiz/digital.png" }}"
                gradient="{{ $service_product->quiz_gradient ?? url("pages/home/quiz-mask.png") }}"
            ></web-component-fiberdroid-quiz>
        </div>
    </div>

    @if($service_product->hasMainView)
        <div class="columns mt-6 pt-6">
            <div class="column is-11 px-6 mx-auto has-text-white service-content">
                {!! $service_product->views['main'] !!}
            </div>
        </div>
    @endif
@endsection
