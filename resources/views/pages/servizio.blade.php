@extends('layouts._layout_v2')

@section('title', ($service->meta_title ?: "$service->name") . " | Fiberdroid")
@section('description', $service->meta_description)

@php
$hasGrid = false;

if($products->values()[0]['items'] ?? false) {
    $hasGrid = true;
}

$tags = array_merge(...$products->map(function($el) { return ($el['items'] ?? []); })->values());
$hasTag = sizeof($tags) > 0
@endphp

@section('content')
    <div class="columns pt-6-mobile">
        <div class="column is-8 mx-auto px-6-mobile" style="padding-top: 59px">
            <div class="columns my-6 is-vcentered">
                <div class="column is-7">
                    <div class="columns is-vcentered is-mobile">
                        <div class="column is-1-tablet is-2-mobile" style="max-width: 50px">
                            <div class="box" style="width: 40px; height: 40px; padding: 0; display: flex">
                                <img
                                    style="display: inline; margin: auto; width: 32px; filter: brightness(0) saturate(100%)"
                                    src="{{ asset("/pages/menu/" . $service->icon) }}"
                                    alt="Icona globo dei servizi Fiberdroid"
                                >
                            </div>
                        </div>
                        <div class="column is-10">
                            <p class="service-text-type">{{ $service->type }}</p>
                        </div>
                    </div>

                    <h1 class="service-text-title mt-6-tablet mt-6-desktop mt-2-mobile">
                        {{ $service->name }}
                    </h1>
                    <p class="service-text-description mt-6-tablet mt-6-desktop mt-3-mobile">
                        {!! $service->description !!}
                    </p>

                    <!--
                    <div class="control w-75 has-icons-right mt-6">
                        <input class="cover-input input is-medium" style="width: 100%" type="email"
                               placeholder="Enter your address to check coverage">
                        <span class="icon is-small is-right">
                                <img class="image mx-auto" src="https://fiberdroid.it/pages/home/input-icon.svg"/>
                            </span>
                    </div>
                    -->
                    <div class="control w-50-desktop w-75-mobile mt-6" style="position: relative; z-index: 1000">
                        @if($service->name == "Fibra dedicata")
                            <a aria-label="Organizza una call con Fiberdroid" rel="noopener noreferrer" href="https://calendly.com/fiberdroid/richiesta-chiamata" target="_blank" class="button is-fd-primary" style="width: 100%">
                                Organizza una call
                            </a>
                        @elseif($service->name == "Voip")
                            <a aria-label="Consulta il listino prezzi" href="{{ url('/listini') }}" target="_blank" class="button is-dark" style="width: 100%">
                                Consulta il listino prezzi
                            </a>
                        @else
                            <a aria-label="Verifica la tua copertura di rete" href="{{ url('/verifica-copertura') }}" target="_blank" class="button is-fd-primary" style="width: 100%">
                                Verifica la tua copertura
                            </a>
                        @endif
                    </div>
                </div>
                <div class="column is-5 hidden-mobile is-flex">
                    <img
                        src="{{ asset('images/' . ($service->bg ?? "services/fiber.webp")) }}"
                        class="ml-auto"
                        style="width: 356px; height: 500px;border-radius: 20px; position: relative; z-index: 999"
                        alt="Immagine {{ $service->name }}"
                    >
                </div>
            </div>
        </div>
    </div>

    @if($service->title_plans)
        <p class="service-title-plans mt-6 pt-6 mb-0">
            {{ $service->title_plans }}
        </p>
    @endif

    @if($hasTag)
        @foreach($products->values() as $key => $productInfo)
            <div class="hidden-mobile" style="max-width: 1800px; height: 330px; margin: auto; overflow: hidden">
                <div class="columns hidden-mobile ml-4 carousel @if($key % 2 == 1) is-carousel-left @else is-carousel-right @endif" style="position: relative">
                    @foreach([1,2,3,4,5,6] as $repeat)
                        @foreach($productInfo['items'] ?? [] as $item)
                            <div class="column is-carousel-column">
                                @component('pages.components.services.carousel-box', ['bg' => $productInfo['bg']])
                                    {!! $item !!}
                                @endcomponent
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        @endforeach
        <style>
            .is-carousel-left {
                animation: slide-carousel-left 1s linear reverse;
                animation-play-state: paused;
                animation-delay: calc(var(--scroll-left) * -0.00085s);
            }
            .is-carousel-right {
                animation: slide-carousel-right 1s linear reverse;
                animation-play-state: paused;
                animation-delay: calc(var(--scroll) * -0.00085s);
            }

            @keyframes slide-carousel-left {
                from {
                    left: 25px
                }
                to {
                    left: -2500px
                }
            }
            @keyframes slide-carousel-right {
                from {
                    left: -2500px
                }
                to {
                    left: 25px
                }
            }

        </style>
    @endif

    @if($service->name == "Fibra")
        <div class="columns mt-6 mx-3-mobile mb-0">
            <div class="column is-8 mx-auto">
                <div class="box box-shadow p-0 m-0" style="background: transparent">
                    <div class="columns m-0 p-0">
                        <div class="column is-8 is-flex">
                        <span class="service-text-description p-6 px-4-touch" style="margin: auto">
                            <span class="is-size-3"><b>Fibra Business su misura</b></span><br><br>
                            Dì addio alle reti condivise e ai rallentamenti dei grandi operatori. Fiberdroid è diverso: una fibra <b>ultra-veloce</b>, <b>stabile</b> e <b>senza congestioni</b>.
                            Affidati a una rete progettata solo per le Aziende. <br><br>
                            Massima continuità, massime prestazioni.
                        </span>
                        </div>
                        <div class="column is-4 p-0 m-0 is-flex">
                            <img class="mt-auto" src="{{ url('images/robot.png') }}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!--
    <p class="service-title-plans scroll-text mt-6 pt-6 mb-0"></p>
    <p class="service-title-plans scroll-text-left mt-6 pt-6 mb-0"></p>
    -->


    <!--<div class="columns mt-6 pt-6" style="position: relative; left: -14%">

    </div>-->

    <div class="columns">
        <div class="column is-8 mx-auto">
            @if($hasGrid && false)
                <div class="columns is-mobile mt-6 py-6">
                    <div class="column is-12 has-text-centered">
                        <a href="#all" class="button is-fd-primary has-text-white">
                            VEDI TUTTI
                        </a>
                    </div>
                </div>
            @endif

                @php
                    function get_product_tags($products) {
                            $tags = collect([]);

                            foreach ($products as $productInfo) {
                                $productTags = collect($productInfo['products'])->pluck('tags')->map(function ($el) {return json_decode($el);})->toArray();

                                foreach ($productTags as $productTagArray) {
                                    if($productTagArray) {
                                        $tags->push(...$productTagArray);
                                    }
                                }
                            }

                            return $tags->unique();
                    }

                    $tags = get_product_tags($products);
                @endphp
                @if($products && $tags->isNotEmpty())
                    <div class="columns is-mobile my-6-desktop my-6-tablet pt-6 pb-2" style="position: relative">
                        <div class="column is-12 has-text-centered is-flex">
                            <div class="field is-grouped is-grouped-multiline mx-auto">
                                @foreach($tags as $tag)
                                    <div class="control">
                                        <div class="tags is-clickable filter-tags is-unselectable" data-tag-content="{{ $tag }}" style="border: 1.638px solid #FFF; position:relative; z-index: 1000">
                                            <span class="tag filter-tag is-white" style="margin: 0">{{ $tag }}</span>
                                            <span class="tag filter-tag is-delete is-hidden"></span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @else
                    <div class="mt-6 pt-6"></div>
                @endif

            @if($products)
                @foreach($products->where('type', 'double') as $type => $productInfo)
                        <div class="columns is-multiline mx-3-mobile" data-section data-{{$type}}>
                            @foreach($productInfo['products'] as $key => $product)
                                <div class="column is-6 px-5 py-5 is-flex" @if($key == intval(sizeof($productInfo['products']) / 2)) id="all" @endif>
                                    @component('pages.components.service-box', [
                                        'title' => $product['website_name'] ?? $product['name'],
                                        'slug' => \Illuminate\Support\Str::slug($product['name']),
                                        'type' => "Connettività",
                                        'service' => $service,
                                        'price' => $product['prices']['fee_amount'],
                                        'activation_price' => $product['prices']['activation_amount'] ?? "0",
                                        'bg' => $product['gradient'] ?? ($productInfo['bg_box'] ?? $productInfo['bg']),
                                        'mark' => $product['mark'] ?? null,
                                        'tags' => $product['tags'] ?? null,
                                        'classes' => $key % 2 == 0 ? "ml-auto" : "mr-auto",
                                        'items' => $product['items'] ? json_decode($product['items']) : ["empty", "empty", "empty"]
                                    ])@endcomponent
                                </div>
                            @endforeach
                        </div>
                    @endforeach
            @endif
        </div>
    </div>

    @php
        function get_product_type($product): string
        {
            if ($product['category_id'] === 3) {
                return "VoIp";
            }

            return "Connettività";
        }
    @endphp

    @if($products)
        @foreach($products->where('type', 'triple') as $type => $productInfo)
            <div class="columns is-multiline mx-6-tablet mx-6-desktop mx-3-mobile" data-section data-{{ $type }}>
                @foreach($productInfo['products'] as $key => $product)
                    <div class="column is-4 px-4 py-5 is-flex" @if($key == intval(sizeof($productInfo['products']) / 2)) id="all" @endif>



                        @component('pages.components.service-box', [
                            'title' => $product['website_name'] ?? $product['name'],
                            'type' => get_product_type($product),
                            'service' => $service,
                            'price' => $product->prices['fee_amount'],
                            'activation_price' => $product['prices']['activation_amount'] ?? "0",
                            'bg' => $product['gradient'] ?? ($productInfo['bg_box'] ?? $productInfo['bg']),
                            'mark' => $product->mark,
                            'tags' => $product['tags'] ?? null,
                            'slug' => \Illuminate\Support\Str::slug($product['name']),
                            'classes' => $key % 3 == 0 ? "ml-auto" : ($key % 3 == 1 ? "mx-auto" : "mr-auto") ,
                            'items' => $product->items ? json_decode($product->items) : ["empty", "empty", "empty"]
                        ])@endcomponent
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif


    @if($service->views['termini'] ?? false)
        <div class="columns mt-6 px-4-mobile">
            <div class="column is-12">
                <div class="box service-termini-box py-6 mx-auto">
                    {!! $service->views['termini'] !!}
                </div>
            </div>
        </div>
    @endif

    <div class="columns">
        <div class="column is-12">
            <web-component-fiberdroid-quiz
                id="quiz-component"
                icon="{{ $service->quiz_icon ?? "quiz/digital.png" }}"
                gradient="{{ $service->quiz_gradient ?? "home/quiz-mask.png" }}"
            ></web-component-fiberdroid-quiz>
        </div>
    </div>

    @if($service->hasMainView)
        <div class="columns mt-6 pt-6">
            <div class="column is-11 px-6 mx-auto has-text-white service-content">
                {!! $service->views['main'] !!}
            </div>
        </div>
    @endif

    <!--
    <div class="columns is-multiline mx-6 mt-6 pt-6">
        <div class="column is-11 mx-auto">
            <p class="service-fit-text">
                This solution is the best fit <br> for your if:
            </p>
            <div class="columns mt-4">
                <div class="column is-4">
                    @component('pages.components.services.service-grid', ['icon' => 'rounded-eye.svg'])
                        La Fibra Ottica è la connessione per eccellenza per la banda ultra larga.
                    @endcomponent
                </div>
                <div class="column is-4">
                    @component('pages.components.services.service-grid', ['icon' => 'connection.svg'])
                        La rete punto-punto consente una velocità, continuità ed espandibilità della connessione senza
                        precedenti.
                    @endcomponent
                </div>
                <div class="column is-4">
                    @component('pages.components.services.service-grid', ['icon' => 'compass.svg'])
                        Puoi estendere il servizio sulle diverse sedi aziendali per poter garantire la condivisione di
                        risorse ed accedere ad internet
                    @endcomponent
                </div>
            </div>
        </div>
        -->

    </div>

@endsection
