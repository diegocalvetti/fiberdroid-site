@extends('layouts._layout_v2')

@section('title', "Verifica di Copertura Fibra, Wireless e ADSL | Fiberdroid")
@section('description', "Verifica la copertura per le varie connettività nella tua zona, in tempo reale e in tutta Italia")

@section('content')
    <div class="columns is-mobile is-multiline has-background-black" style="padding-top: 59px">
        <div class="column is-12 mt-3-mobile mb-0 pb-0">
            <h1 class="coverage-title">
                Verifica la tua copertura
            </h1>
        </div>
        <div class="column is-12 mt-6-desktop">
            <web-component-fiberdroid-coverage id="verifica-copertura" url="{{ url('/') }}"></web-component-fiberdroid-coverage>
        </div>
    </div>

    <div class="coverage-clones" style="display: none">
        @foreach($servicesData as $type => $service)
            @foreach($service as $productInfo)
                <div>
                    <div class="columns is-multiline mx-6-tablet mx-6-desktop mx-3-mobile" data-section data-{{ $type }} data-coverage-section-{{ $type }}>
                        @foreach($productInfo['products'] as $key => $product)
                            <div class="column is-4 px-4 py-5" @if($key == intval(sizeof($productInfo['products']) / 2)) id="all" @endif style="display: flex">
                                @component('pages.components.service-box', [
                                    'title' => $product['website_name'] ?? $product['name'],
                                    'type' => "Connettività",
                                    'price' => $product['prices']['fee_amount'] ?? "0",
                                    'activation_price' => $product['prices']['activation_amount'] ?? "0",
                                    'bg' => $product['gradient'] ?? ($productInfo['bg_box'] ?? $productInfo['bg']),
                                    'mark' => $product['mark'] ?? '',
                                    'max_speed' => $product['max_speed'] ?? '',
                                    'slug' => \Illuminate\Support\Str::slug($product['name']),
                                    'classes' => $key % 3 == 0 ? "ml-auto" : ($key % 3 == 1 ? "mx-auto" : "mr-auto") ,
                                    'items' => $product['items'] ? json_decode($product['items']) : ["empty", "empty", "empty"],
                                ])@endcomponent
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        @endforeach
    </div>
@endsection

