@extends('layouts._layout_v2')

@section('title', "Soluzioni Internet e VOIP aziendali: wifi, centralino cloud, fibra e connettivita’| Fiberdroid")
@section('description', "Siamo il provider internet per il tuo Business. Le nostre soluzioni Fibra e Voip sono personalizzate. La nostra assistenza è veloce,sicura e tutta italiana. Vantiamo un'area clienti dedicata e un uptime del 99,99%.")

@section('content')
    <img class="landing-child" src="{{ url('pages/home/home-gradient.svg') }}" alt="Sfondo Homepage Fiberdroid, gradiente leggero colorato"/>

    <!-- HEADER INTRO -->
    <div class="columns is-mobile is-multiline has-background-black" style="padding-top: 89px">
        <div class="column is-12 mt-4-touch">
            <div>
                <h1 class="has-text-centered">
                    <span class="landing-text-title pt-4" data-invisible data-fade-in-text>Il tuo business sempre connesso,</span>
                    <br>
                    <span class="landing-text-title pt-4" data-invisible data-fade-in-text>ovunque tu sia.</span>
                </h1>
            </div>
        </div>
        <div class="column is-10 mx-auto">
            <div class="landing-text-description has-text-centered" data-invisible data-fade-in-description>
                Dal VoIP e WiFi al cloud e alla fibra ottica, siamo in grado <br class="hidden-touch">
                di connettere la tua attività a internet. Sblocca il tuo pieno potenziale <br class="hidden-touch">
                con soluzioni cloud e IT pensate studiate su misura per le tue esigenze.
            </div>
        </div>
    </div>
    <div class="columns is-multiline has-background-black mt-4 px-6-mobile">
        <div class="column is-6-desktop is-12-touch px-0-mobile has-text-right-desktop has-text-centered-touch">
            <a href="#quiz" class="button menu-link is-fd-primary full-mobile" data-invisible data-fade-in-button style="border-radius: 8px">
                <b>CREA IL TUO PIANO</b>
            </a>
        </div>
        <div class="column is-6-desktop is-12-touch px-0-mobile has-text-left-desktop has-text-centered-touch">
            <a rel="noopener noreferrer" target="_blank" href="https://calendly.com/fiberdroid/richiesta-chiamata" class="button is-dark full-mobile" data-invisible data-fade-in-button style="border-radius: 8px">
                <b class="has-text-white">ORGANIZZA UNA CALL</b>
            </a>
        </div>
    </div>

    <div class="columns is-mobile is-multiline has-background-black">
        <div class="column is-10 mt-6 pt-6 mx-auto">
            <div class="landing-text-big-description has-text-centered" data-invisible data-fade-in-faster>
                <span>Scelta fidata di più di 1500 piccole e grandi aziende</span>
                <br />
                <span>italiane ed europee</span>
            </div>
        </div>
    </div>

    <!-- CUSTOMER LOGOS -->
    <div class="columns is-mobile is-multiline has-background-black">
        <div class="column is-hidden-mobile is-12 my-6 px-6 ml-3 mr-6 mb-5">
            <div class="figure">
                <img class="image mx-auto" src="{{ url('images/logos/logos-full.webp') }}" alt="Loghi partner Fiberdroid" />
            </div>
        </div>
        <div class="column is-12 is-hidden-tablet mt-6 pl-5 pr-6 ml-3 mr-6 mb-0">
            <img class="image mx-auto" src="{{ url('images/logos/logos_pt1.webp') }}" alt="Loghi partner Fiberdroid pt.2" />
        </div>
        <div class="column is-12 is-hidden-tablet my-0 pl-5 pr-6 ml-3 mr-6 mb-0">
            <img class="image mx-auto" src="{{ url('images/logos/logos_pt2.webp') }}" alt="Loghi partner Fiberdroid pt.3"  />
        </div>
    </div>

    <div class="columns mb-4 mb-3">
        <div class="column is-12 px-6-mobile">
            <p class="industries-title is-size-1-desktop is-size-3-touch">Cosa Dicono di Noi</p>
        </div>
    </div>

    <div class="columns">
        <div class="column is-11 mx-auto px-6 mt-6">
            <div class="columns is-variable is-7 industries-plans">
                <div class="column desktop-6 tablet-6 mobile-12 is-clickable">
                    <p class="text mb-3">Luca Cantoni, CEO di Horizon dice di noi:</p>
                    @component('pages.components.home.box-home', [
                        'image' => '../horizon.jpg',
                        'title' => 'Horizon Automotive',
                        'description' => '
                        Oggi ci troviamo nei nuovi uffici di HORIZON AUTOMOTIVE ad Assago, insieme all\'Amministratore Delegato Luca Cantoni, per esplorare come Horizon abbia rivoluzionato il settore del noleggio di auto in Italia.
                        ',
                        'link' => 'https://www.youtube.com/watch?v=wlOcAGx-rlE',
                        'cit_old' => "<b style=\"color: #1e2b57!important\"><span>La vostra connettività è alla base di tutto il nostro ecosistema. <br></span> <span>— <b style=\"color: #1e2b57!important\">Luca Cantoni, CEO di Horizon Automotive</b></span></b>",
                        'cit' => '<span class="pl-6">
                                La vostra connettività è alla base di tutto il nostro ecosistema. <br>
                            </span>
                            <span class="pl-6 mt-1" style="line-height: 30px">
                                — <b>Luca Cantoni, CEO di Horizon Automotive</b>
                            </span>',
                        'citMobile' => '"La vostra connettività è alla base di tutto <br> il nostro ecosistema."'
                    ])@endcomponent
                </div>
                <div class="column desktop-6 tablet-6 mobile-12 is-clickable pt-6-mobile">
                    <p class="text mb-3">Mons. Francesco Braschi, Dottore della Biblioteca Ambrosiana dice di noi:</p>
                    @component('pages.components.home.box-home', [
                        'image' => '../biblioteca.png',
                        'title' => 'Biblioteca Ambrosiana',
                        'description' => 'Abbiamo avuto l\'onore di parlare con Monsignor Francesco Braschi, il quale ha condiviso il notevole percorso di adattamento della Biblioteca Ambrosiana all\'era digitale.',
                        'link' => 'https://www.youtube.com/watch?v=3DzuCeYBwV4',
                        'cit_old' => "<b style=\"color: gold!important\"><span>Disponibilità e confronto a cercare soluzioni modellate sulle necessità.<br></span> <span>— <b style=\"color: gold!important\">- Mons. Francesco Braschi, dottore della Biblioteca Ambrosiana.</b></span></b>",
                        'cit' => '<span class="pl-6">
                                Disponibilità e confronto a cercare soluzioni modellate sulle necessità. <br>
                            </span>
                            <span class="pl-6 mt-1" style="line-height: 30px">
                               <b>- Mons. Francesco Braschi, Dottore della Biblioteca Ambrosiana.</b>
                            </span>',
                        'citMobile' => '"Disponibilità e confronto a cercare soluzioni modellate sulle necessità."',
                    ])@endcomponent
                </div>
            </div>
            <!--<div class="columns is-mobile is-vcentered hidden-desktop">
                <div class="column is-6">
                    <img src="{{ url('pages/home/icons/arrow-left.png') }}" class="image is-32x32" style="margin-left: auto; margin-right: 20px" data-solution-previous/>
                </div>
                <div class="column is-6">
                    <img src="{{ url('pages/home/icons/arrow-right.png') }}" class="image is-32x32" style="margin-left: 20px; margin-right: auto" data-solution-next/>
                </div>
            </div>-->
        </div>
    </div>

    <div class="columns is-multiline mt-6 mt-3-mobile">
        <!-- BOX GRID -->
        <div class="column desktop-4 touch-12 offset-1-desktop px-6-touch" style="display: flex">
            <div style="display:inline; margin: auto">
                <h2 class="title-text mb-0">
                    Rendiamo le connessioni <br class="hidden-tablet-only">
                    un argomento facile <br class="hidden-tablet-only">
                    per te
                </h2>

                @component('pages.components.home.icon-list', ['data' => [
                    [
                        'title' => 'Assistenza Personalizzata',
                        'description' => "Sfrutta un'assistenza rapida ed efficiente con un contatto diretto. Non è più necessario ripartire da zero ad ogni ticket di supporto; conosciamo a fondo la tua configurazione.",
                    ],
                    [
                        'title' => 'Soluzioni Innovative',
                        'description' => "Comprendiamo quanto sia prezioso e cruciale il tuo lavoro. Con le nostre tecnologie più recenti e affidabili, garantiamo disponibilità 24 ore su 24, 7 giorni su 7, consentendoti di concentrarti su ciò che conta di più.",
                    ],
                    [
                        'title' => 'Un unico portale per tutti i tuoi <br> uffici. Fattura singola.',
                        'description' => "Semplifica le tue operazioni con un'unica piattaforma unificata che gestisce facilmente tutte le tue sedi. Dì addio a fatture multiple e rendi il tuo lavoro efficente come mai prima d'ora."
                    ],
                ]])@endcomponent
            </div>
        </div>
        <div class="column desktop-7 touch-12 px-6-touch mt-6-tablet hidden-touch">
            <div style="display: inline; width: 100%" class="hidden-mobile">
                @component('pages.components.home.grid-box', ['grids' => [
                    ['img' => 'fiber.webp', 'label' => 'Each time you will speak with the same operator'],
                    ['img' => 'work.webp', 'label' => 'Each time you will speak with the same operator'],
                    ['img' => 'operator.webp', 'label' => 'Each time you will speak with the same operator'],
                    ['img' => 'office.webp', 'label' => 'Each time you will speak with the same operator'],
                    ['img' => 'night-office.webp', 'label' => 'Each time you will speak with the same operator'],
                ]])@endcomponent
            </div>
        </div>

        <!-- NETWORK COVERAGE -->
        <div class="column is-12 my-6 px-6">
            <div class="copertura mx-auto" id="copertura-box">
                <div class="copertura-container mx-auto" style="position:relative;">
                    <div>
                        <img loading="lazy" class="copertura-icon" src="https://fiberdroid.it/pages/home/group-20630.svg" alt="Icona Neon Fiberdroid" data-invisible data-fade-coverage-icon>
                        <p class="copertura-text mb-3 pt-6-mobile" data-invisible data-fade-coverage-text>
                            Copriamo il 99% del
                            territorio italiano.<br>
                            Scopri quali servizi sono disponibili per te
                        </p>
                        <!--
                        <div class="control w-50">
                            <input data-invisible data-fade-coverage-input class="cover-input input is-block mx-auto is-medium w-50-desktop w-85-mobile mt-6" type="email" placeholder="Comune">

                            <input data-invisible data-fade-coverage-input class="cover-input input is-block mx-auto is-medium w-50-desktop mt-4 w-85-mobile" type="email" placeholder="Strada">
                        </div>
                        -->

                        <a class="button is-fd-primary mt-4" target="_blank" href="{{ url('/verifica-copertura') }}" data-animable>Verifica Copertura</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SOLUTIONS -->
    <!--<div class="columns is-multiline hidden-touch">
        <div class="column is-12 my-6">
            <p class="title-text has-text-centered">
                Build your ideal solution <br>
                to keep your business running
            </p>
        </div>
        <div class="column is-10 my-6 mx-auto">
            <div class="columns">
                <div class="column is-4 px-6">
                    @component('pages.components.home.solution-box', [
                        'image' => "bluebox.svg",
                        'title' => "Connettività",
                        'description' => "FTTC; FTTH; Wireless; <br>4G/5G; Satellitare",
                        'icon' => 'system--wifi-high.svg',
                    ])@endcomponent
                </div>
                <div class="column is-4 px-6">
                    @component('pages.components.home.solution-box', [
                        'image' => "orangebox.svg",
                        'title' => "Centralino<br>VOIP",
                        'description' => "",
                        'icon' => 'communication--phone.svg',
                    ])@endcomponent
                </div>
                <div class="column is-4 px-6">
                    @component('pages.components.home.solution-box', [
                        'image' => "purplebox.svg",
                        'title' => "Linea di Backup",
                        'description' => "4G services to make your <br>business available 24/7",
                        'icon' => 'shape--shield.svg',
                        'iconTop' => '-115px'
                    ])@endcomponent
                </div>
            </div>
        </div>
    </div>-->

    <!-- MOST POPULAR PLANS -->
    <div class="columns is-multiline mb-6 pb-6 mt-4">
        <!-- SOLUTIONS -->
        <div class="column is-12 my-6-tablet mt-5-mobile mb-3-mobile">
            <p class="title-text has-text-centered">
                Servizi più richiesti
            </p>
        </div>
        <div class="column is-11 px-6 pb-0 mx-auto best-plans-container">
            <div class="columns is-mobile is-variable is-6" id="best-plans">
                @foreach($homeServices as $key => $product)
                    <div class="column desktop-4 touch-12 is-flex" id="best-plans-{{ $key }}" >
                        @component('pages.components.service-box', [
                                         'title' => $product['website_name'] ?? $product['name'],
                                         'slug' => \Illuminate\Support\Str::slug($product['name']),
                                         'type' => "Connettività",
                                         'service' => "",
                                         'price' => $product['prices']['fee_amount'],
                                         'activation_price' => $product['prices']['activation_amount'] ?? "0",
                                         'bg' => $key == 0 ? $product['gradient'] : ($key == 1 ? "url(\"https://fiberdroid.it/pages/gradients/voip.png\")" : "url(\"/pages/gradients/telefoni-voip.png\")"),
                                         'mark' => $product['mark'] ?? null,
                                         'tags' => $product['tags'] ?? null,
                                         'classes' => "",
                                         'items' => $product['items'] ? json_decode($product['items']) : ["empty", "empty", "empty"]
                                     ])@endcomponent
                    </div>
                @endforeach

            </div>
            <!--<div class="columns is-mobile is-vcentered hidden-desktop">
                <div class="column is-6">
                    <img src="{{ url('pages/home/icons/arrow-left.png') }}" class="image is-32x32" style="margin-left: auto; margin-right: 20px" data-best-plans-previous/>
                </div>
                <div class="column is-6">
                    <img src="{{ url('pages/home/icons/arrow-right.png') }}" class="image is-32x32" style="margin-left: 20px; margin-right: auto" data-best-plans-next/>
                </div>
            </div>-->
        </div>

        <!--<div class="column is-12 px-6 pb-0 mx-auto hidden-desktop">
            <div class="columns is-mobile is-multiline">
                <div class="column is-12">
                    [...box here]
                </div>
                <div class="column is-6">
                    <img src="https://fiberdroid.it/pages/home/icons/arrow-left.png" class="image is-32x32" style="margin-left: auto; margin-right: 20px"/>
                </div>
                <div class="column is-6">
                    <img src="https://fiberdroid.it/pages/home/icons/arrow-right.png" class="image is-32x32" style="margin-left: 20px; margin-right: auto"/>
                </div>
            </div>
        </div>-->
    </div>

    <!-- QUIZ -->
    <div id="quiz">
        <web-component-fiberdroid-quiz id="quiz-component" url="{{ url('/') }}"></web-component-fiberdroid-quiz>
    </div>

    <!-- STATS -->
    <div class="columns is-multiline mb-0 mt-6 pt-4 pb-0">
        <div class="column is-4 mt-3-mobile has-text-centered">
            @component('pages.components.home.stats', ['value' => '98%', 'description' => 'Clienti soddisfatti'])@endcomponent
        </div>
        <div class="column is-4 mt-3-mobile has-text-centered">
            @component('pages.components.home.stats', ['value' => '4 Ore', 'description' => 'Tempo medio di risposta'])@endcomponent
        </div>
        <div class="column is-4 mt-3-mobile has-text-centered">
            @component('pages.components.home.stats', ['value' => '99,99%', 'description' => 'Uptime'])@endcomponent
        </div>

        <div class="column is-10 mx-auto my-6 px-6-mobile has-text-centered-desktop" style="padding: 4rem 0">
            <cite class="quote mt-6">
                “L'unico provider che rende la connettività un piacere e non un mal di testa”
            </cite>
        </div>
    </div>

    <!-- INDUSTRIES -->
    <div class="columns mb-6 mb-3">
        <div class="column is-12 px-6-mobile">
            <p class="industries-title">Soluzioni studiate su misura</p>
        </div>
    </div>
    <div class="columns">
        <div class="column is-11 mx-auto px-6 mt-6">
            <div class="columns is-mobile is-variable is-7 industries-plans">
                <div class="column desktop-3 tablet-6 mobile-12 is-clickable" id="solution-0">
                    @component('pages.components.home.industry-box', [
                        'image' => 'manufactory.webp',
                        'title' => 'Aziende',
                        'description' => 'Per costruire soluzioni personalizzate, affidabili e sicure, al minor prezzo, il nostro equipaggio è il partner unico per condurre la tua azienda verso il futuro della connettività!',
                        'link' => 'soluzioni/aziende',
                    ])@endcomponent
                </div>
                <div class="column desktop-3 tablet-6 mobile-12 is-clickable" id="solution-1">
                    @component('pages.components.home.industry-box', [
                        'image' => 'retail-and-food.webp',
                        'title' => 'Retail',
                        'description' => 'Gestione delle prenotazioni in remoto, connessione veloce e sicura, disponibile per i clienti, sistemi IT avanzati per negozi e ristoranti, per ripartire la tua attività e riaccendere la città!',
                        'link' => 'soluzioni/retail',
                    ])@endcomponent
                </div>
                <div class="column desktop-3 tablet-6 mobile-12 is-clickable" id="solution-2">
                    @component('pages.components.home.industry-box', [
                        'image' => 'restaurants.webp',
                        'title' => 'Ristorazione',
                        'description' => 'Gestione delle prenotazioni in remoto, connessione veloce e sicura, disponibile per i clienti, sistemi IT avanzati per negozi e ristoranti, per ripartire la tua attività e riaccendere la città!',
                        'link' => 'soluzioni/ristorazione',
                    ])@endcomponent
                </div>
                <div class="column desktop-3 tablet-6 mobile-12 is-clickable" id="solution-3">
                    @component('pages.components.home.industry-box', [
                        'image' => 'hospitality.webp',
                        'title' => 'Strutture Ricettive',
                        'description' => 'Assicuriamo la massima affidabilità per il tuo hotel o b&b, perché tu possa accogliere i tuoi ospiti con la migliore tecnologia, e far vivere loro un’esperienza indimenticabile.',
                        'link' => 'soluzioni/strutture-ricettive',
                    ])@endcomponent
                </div>
            </div>
            <!--<div class="columns is-mobile is-vcentered hidden-desktop">
                <div class="column is-6">
                    <img src="{{ url('pages/home/icons/arrow-left.png') }}" class="image is-32x32" style="margin-left: auto; margin-right: 20px" data-solution-previous/>
                </div>
                <div class="column is-6">
                    <img src="{{ url('pages/home/icons/arrow-right.png') }}" class="image is-32x32" style="margin-left: 20px; margin-right: auto" data-solution-next/>
                </div>
            </div>-->
        </div>
    </div>


@endsection

