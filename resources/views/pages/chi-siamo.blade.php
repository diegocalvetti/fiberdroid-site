@extends('layouts._layout_v2')

@section('title', "Chi siamo, il nostro manifesto | Fiberdroid")
@section('description', "Siamo l’azienda che ti permette di avere un’unica assistenza per Internet, wifi, centralino cloud, fibra e connettivita’, piattaforma tecnologica. Il nostro team è in servizio da oltre 10 anni nel settore IT & TLC per garantire alle aziende, in Italia e all’estero, connettività veloce, assistenza affidabile, tecnologie innovative.")

@section('content')
    <div class="columns">
        <div class="column is-6 tablet-12 mx-auto px-6-mobile" style="padding-top: 129px">
            <h1 class="title-text has-text-centered mb-6" data-invisible data-fade-in-text>
                Tutto In Un Solo <br>
                Partner, e questo è il nostro <br>
                manifesto
            </h1>
            <p class="text-chi-siamo has-text-centered" data-invisible data-fade-in-description>
                Siamo l’azienda che ti permette di avere un’unica assistenza per Internet, wifi, centralino cloud, fibra e connettivita’, piattaforma tecnologica.
                <br><br>
                Il nostro team è in servizio da oltre 10 anni nel settore IT & TLC per garantire alle aziende, in Italia e all’estero, connettività veloce, assistenza affidabile, tecnologie innovative.
                <br><br>
                Offriamo la customizzazione dell’intero progetto di messa in rete e di telecomunicazione, nella totale trasparenza e garanzia di puntualità, fornendo ai nostri Clienti Business un servizio di consulenza attento e completo, a supporto della crescita aziendale.
                I Valori che guidano la nostra squadra sono la liberta’, l’Innovazione, l’Esplorazione e l’attenzione al cliente.
                <br><br>
                Ci occupiamo dell’installazione, presso i nostri Clienti Business, esclusivamente di apparati certificati e testati. Grazie ai test effettuati preventivamente dal nostro team di supporto, vantiamo una media del 90% di chiusura ticket entro 4 ore dall’apertura.
                La nostra piattaforma è distribuita su due data-center: il primo presso il MIX, a Milano.
                <br><br>
                Il secondo tra le montagne della Valle d’Aosta, e sono operanti totalmente con hardware di proprietà.
                <br><br>
                Solo così possiamo garantirti la Qualità, la Sicurezza e il Controllo gestionale di cui necessità la tua Rete aziendale: implementando e aggiornando continuamente la nostra infrastruttura.
                <br><br>
                I nostri sistemi sono sotto monitoraggio continuo, consentendo alla nostra equipe di anticipare eventuali disservizi o problematiche.
                <br><br>
                Fiberdroid s.r.l., con sede a Milano, operante da oltre 10 anni nel settore delle telecomunicazioni a livello internazionale.
            </p>
        </div>
    </div>

    <div class="columns is-multiline px-6 mx-6 mx-0-mobile px-0-mobile" style="margin-bottom: 100px">
        @foreach($crews->values() as $key => $crew)
            <div class="column desktop-4 tablet-6 mobile-12 mt-6 is-flex is-flex-direction-column" data-crew-card>
                <div
                    class="box crew-box-image {{ $key % 2 == 0 ? "ml-auto-tablet" : "" }} {{ $key % 2 == 1 ? "mr-auto-tablet" : "" }} {{ $key % 3 == 0 ? "ml-auto-desktop" : "" }} {{ $key % 3 == 1 ? "mx-auto-desktop" : "" }} {{ $key % 3 == 2 ? "mr-auto-desktop" : "" }}"
                >
                    <img alt="Foto del team Fiberdroid: {{ $crew->name }}" src="{{ $crew->image->full_url ?? '' }}" />
                </div>
                <div class="crew-box-container {{ $key % 2 == 0 ? "mr-3-tablet" : "" }} {{ $key % 2 == 1 ? "ml-3-tablet" : "" }} {{ $key % 3 == 0 ? "mr-3-desktop" : "" }} {{ $key % 3 == 2 ? "ml-3-desktop" : "" }} mx-auto-mobile">
                    <div class="box mt-0 crew-box-header">
                        <div class="crew-box-header-content p-5">
                            <p class="crew-title my-5">{{ $crew->name }}</p>
                            <p class="crew-role">{{ $crew->role }}</p>
                        </div>
                    </div>
                    <div class="px-5">
                        <div class="crew-description pt-5" style="margin-top: -50px">
                            {!! $crew->content !!}
                        </div>
                    </div>

                    <div class="p-5 mt-auto">
                        @component('pages.components.icons-list', ['icons' => ['linkedin-full'], 'links' => ['linkedin-full' => $crew->url_linkedin], 'color' => 'white'])@endcomponent
                    </div>
                </div>

            </div>
        @endforeach
    </div>
@endsection
