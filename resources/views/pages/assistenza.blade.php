@extends('layouts._layout_v2')

@section('title', "Assistenza, siamo qui per aiutare | Fiberdroid")
@section('description', "Qui trovi i nostri contatti e i nostri software di monitoraggio. Siamo qui per aiutare, sappiamo quanto l’assistenza sia fondamentale nel rapporto tra provider e cliente.")

@section('content')
    <img class="landing-child" alt="" src="{{ url('/pages/gradients/bg-assistenza.png') }}" />
    <canvas class="particles"></canvas>

    <!-- HEADER INTRO -->
    <div class="columns is-mobile is-multiline has-background-black assistance-main">
        <div class="column is-12">
            <p class="landing-text-title" data-invisible data-fade-in-text>
                Nelle telecomunicazioni l’assistenza <br>
                ai clienti fa la differenza!
            </p>
        </div>
        <div class="column desktop-6 tablet-6 mobile-10 mt-6 mx-auto">
            <div class="landing-text-description has-text-centered" data-invisible data-fade-in-description>
                Se sei stanco di buttare ore/lavoro preziose per il tuo business, se vuoi avere un’assistenza veloce, precisa e attenta alle tue problematiche ma soprattutto un’assistenza personalizzata, dedicata a te, con un tecnico di riferimento che sa esattamente di cosa stai parlando, in Fiberdroid l’hai trovata.
            </div>
        </div>
    </div>

    <div class="columns is-multiline" style="position: relative; z-index: 999">
        <div class="column desktop-4 mt-4-touch tablet-4 mobile-12">
            @component('pages.components.assistance.assistance-box', ['bg' => url('/pages/assistance/technical-support-gradient.png'), 'class' => 'ml-auto-desktop ml-auto-tablet mx-auto-mobile'])
                <img src="{{ url('/images/assistance/technical-support.webp') }}" alt="Icona area supporto tecnico di Fiberdroid">
                <p class="has-text-white">Area supporto tecnico</p>

                <div class="control">
                    <button aria-label="Crea un ticket di supporto dal portale di Fiberdroid" class="button assistance-button is-dark" onclick="window.open('https://area-clienti.fiberdroid.it/tickets')">
                        Crea Ticket
                    </button>
                    <button aria-label="Scarica software di assistenza remota" class="button assistance-button mt-4 is-fd-primary" onclick="window.open('https://fiberdroid.it/download/supremo.exe')">
                        Scarica Software
                    </button>
                </div>
            @endcomponent
        </div>
        <div class="column desktop-4 mt-4-touch tablet-4 mobile-12">
            @component('pages.components.assistance.assistance-box', ['bg' => url('/pages/assistance/amministrazione-gradient.png')])
                <img src="{{ url('/images/assistance/amministrazione.webp') }}" alt="Icona Area Assistenza Fiberdroid">
                <p class="has-text-white">Amministrazione</p>

                <div class="control" style="width: 100%">
                    <button aria-label="Mail amministrazione Fiberdroid" class="button assistance-button" onclick="window.open('mailto:amministrazione@fiberdroid.it')">
                        amministrazione@fiberdroid.it
                    </button>
                    <button aria-label="Telefono amministrazione Fiberdroid" class="button assistance-button mt-4 is-fd-primary" onclick="window.open('tel:+39 02 83595676')">
                        02 83595676
                    </button>
                </div>
            @endcomponent
        </div>
        <div class="column desktop-4 mt-4-touch tablet-4 mobile-12">
            @component('pages.components.assistance.assistance-box', ['bg' => url('/pages/assistance/assistenza-gradient.jpg'), 'class' => 'mr-auto-desktop mr-auto-tablet mx-auto-mobile'])
                <img src="{{ url('/images/assistance/assistenza.webp') }}" alt="Icona assistenza Fiberdroid">
                <p class="has-text-white">Assistenza</p>

                <div class="control">
                    <button aria-label="Mail assistenza Fiberdroid" class="button assistance-button" onclick="window.open('mailto:speed@fiberdroid.it')">
                        speed@fiberdroid.it
                    </button>
                    <button aria-label="Telefono assistenza Fiberdroid" class="button assistance-button mt-4 is-fd-primary" onclick="window.open('tel:+39 02 83595676')">
                        02 83595676
                    </button>
                </div>
            @endcomponent
        </div>
    </div>

    <div class="columns is-multiline my-6 pt-6">
        <div class="column is-12 mt-2-mobile">
            <div>
                <h1 class="landing-text-title pt-4" data-invisible data-fade-in-text>Siamo qui per aiutare</h1>
            </div>
        </div>
        <div class="column desktop-6 tablet-6 mobile-10 mt-6 mx-auto">
            <div class="landing-text-description has-text-centered" data-invisible data-fade-in-description>
                Ci rendiamo perfettamente conto di quanto sia difficile per te scegliere la miglior assistenza per la connessione internet e soprattutto il giusto partner tra le decine che si propongono sul WEB, ma se sei qui è perché sai che l’assistenza è fondamentale nel rapporto tra provider e cliente.
                <br><br>
                Forse sei tra quelli che hanno scelto in base a un’offerta molto competitiva e poi nel momento del bisogno hai avuto a che fare con call center che ingoiano il tuo problema come un buco nero, dove tu sei solo un numero di chiamata e ogni volta che cade la linea passi allo step successivo e devi rispiegare da capo il tuo problema.
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/particlesjs/2.2.2/particles.min.js"></script>

    <style>
        .particles {
            position: absolute;
            display: block;
            top: 0;
            left: 0;
        }
    </style>
    <script>
        window.onload = function() {
            Particles.init({
                selector: '.particles',
                maxParticles: 450,
                color: ["#ffffff", "#9db8bf"],
            });

        };
    </script>
@endpush
