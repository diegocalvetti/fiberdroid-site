@extends('layouts._layout_v2')

@section('title', 'Carta dei Servizi | Fiberdroid')
@section('description', "")

@section('content')
    <div class="columns" style="margin-top: 59px">
        <div class="column is-10 mx-auto px-6-touch">
            <h1 class="title-text mb-4">Carta dei Servizi</h1>
            <div class="content has-text-white">
                <p class="subtitle-text">
                    PREMESSA
                </p>
                <p class="text">
                    La Carta dei Servizi è il documento che descrive i principi e le modalità operative adottate
                    dall’operatore nella relazione con i propri clienti (“Clienti”) e intende sintetizzare non solo i
                    diritti dell’utenza finale ma anche le responsabilità che l’operatore assume nei loro confronti.
                    Per tale ragione la Carta dei Servizi è richiamata nelle Condizioni Generali di Contratto e va
                    letta congiuntamente alla documentazione contrattuale specifica del singolo servizio fruito
                    dal Cliente. <br><br>
                    In particolare, la Carta dei Servizi costituisce base di riferimento nei rapporti tra l’operatore
                    gli utenti che utilizzano o chiedono di utilizzare servizi di comunicazione elettronica
                    accessibili al pubblico (il/i “Servizio/i”), indicando una serie di parametri di qualità dei servizi
                    offerti, quali continuità, regolarità e tempi di ripristino, che l’operatore si impegna a garantire,
                    permettendo agli utenti di verificarne l’effettiva realizzazione e fornendo informazioni utili per
                    l’inoltro di segnalazioni, proposte, richieste di chiarimenti e segnalazioni di disservizio,
                    modalità e costi della prestazione erogata.
                    <br><br>
                    Il presente documento potrà essere aggiornato periodicamente al fine di tener conto
                    dell’evoluzione tecnologica e regolamentare, nonché dell’organizzazione dell’azienda e
                    viene reso disponibile, nella versione più aggiornata, sul sito web, o fornito ai Clienti che ne
                    facciano richiesta al Servizio Clienti.
                    <br><br>
                    <strong><strong>Fiberdroid</strong> s.r.l.</strong> (di seguito brevemente "Fiberdroid") adotta e pubblica la presente Carta dei
                    Servizi in ottemperanza alle delibere dell'Autorità per le Garanzie nelle Comunicazioni
                    ("Autorità") elencate di seguito:
                </p>

                <ul class="text">
                    <li>Direttiva del Consiglio dei Ministri del 27/1/1994, "Principi sull'erogazione dei servizi pubblici";</li>
                    <li>Delibera n. 179/03/CSP, "Approvazione della direttiva generale in materia di qualità e carte dei servizi di telecomunicazioni ai sensi dell'art. 1, comma 6, lettera b), numero 2 della legge 31 luglio 1997, n. 249";</li>
                    <li>Allegato A alla Delibera n. 156/23/CONS “Regolamento in materia di qualità e carte dei servizi di comunicazione da postazione fissa”.</li>
                </ul>

                <p class="text">La Carta dei Servizi è disponibile sul sito web www.fiberdroid.it e può essere inviata ai Clienti su esplicita richiesta attraverso i seguenti canali:</p>

                <ul class="text">
                    <li>Recapito telefonico: <a href="tel:+390283595676">02 835 95676</a> </li>
                    <li>Indirizzo e-mail: <a href="mailto:speed@fiberdroid.it">speed@fiberdroid.it</a></li>
                </ul>

                <p class="subtitle-text">
                    LA SOCIETA'
                </p>

                <p class="text">
                    <b>Fiberdroid</b> è un Interner Service Provider (“ISP”) e reseller fonia italiano che vende i propri
                    servizi principalmente alla clientela business, con sede legale a Milano, Via Enrico Cosenz
                    n. 54, c.f./p.iva 07057060969, e sede operativa a Milano (MI), Via Enrico Cosenz n. 54.
                    Grazie agli accordi stipulati con primari Operatori autorizzati alla fornitura di servizi telefonici
                    pubblici e assegnatari delle relative risorse di numerazione, come con gli operatori rivenditori
                    a livello wholesale di connettività, FiberDroid è in grado di offrire ai propri Clienti i Servizi
                    oggetto del contratto.
                </p>

                <p class="subtitle-text">
                    SERVIZI OFFERTI
                </p>

                <p class="text">Nel corso degli anni, <strong>Fiberdroid</strong> ha ampliato la gamma di servizi offerti, arricchendo il proprio
                    listino con soluzioni di voce, connettività e servizi a valore aggiunto.</p>

                <p class="text">La descrizione dettagliata dei servizi offerti, unitamente alle caratteristiche tecniche, è
                    pubblicata e consultabile sul sito www.fiberdroid.it, seguendo il percorso “Servizi”:</p>

                <ul class="text">
                    <li>a) “Fibra”</li>
                    <li>b) “Fibra Dedicata”</li>
                    <li>c) “Voip”</li>
                    <li>d) “Wireless”</li>
                    <li>e) “HDSL”</li>
                    <li>f) “Cloud WiFi”</li>
                    <li>g) “Centralino Voip”</li>
                    <li>h) “Satellitare”</li>
                    <li>i) “ADSL”</li>
                    <li>l) “Reti Wan”</li>
                    <li>m) “Centralino Cloud”</li>
                </ul>

                <p class="text">
                    I Servizi sono riportati a titolo non esaustivo e sono soggetti ad aggiornamenti e mutamenti a
                    discrezione di Fiberdroid. Per ogni novità e aggiornamento, si consiglia di consultare il sito
                    web.
                </p>

                <p class="subtitle-text"><strong>PRINCIPI FONDAMENTALI</strong></p>

                <p class="text"><strong>Fiberdroid</strong> opera nel mercato delle comunicazioni elettroniche promuovendo principi di uguaglianza, trasparenza, correttezza, cortesia e disponibilità nella fornitura dei propri servizi.</p>

                <p class="text">Oltre ad aver adottato un proprio Codice Etico, che regola i rapporti con dipendenti, collaboratori e partner, <strong>Fiberdroid</strong> definisce con il presente documento i principi fondamentali che regolano il rapporto con i propri Clienti.</p>

                <p class="text"><strong>1) Uguaglianza e Imparzialità</strong></p>
                <p class="text"><strong>Fiberdroid</strong> fornisce servizi nel settore delle comunicazioni elettroniche rispettando i principi di uguaglianza, pari dignità e imparzialità nel trattamento dei Clienti, senza distinzioni basate su genere, razza, lingua, religione o opinioni politiche. È vietata ogni forma di discriminazione ingiustificata.</p>
                <p class="text"><strong>Fiberdroid</strong> si impegna ad adottare tutte le misure tecniche necessarie per agevolare l’accesso e l’utilizzo dei servizi da parte di persone con disabilità e soggetti socialmente più vulnerabili.</p>

                <p class="text"><strong>2) Continuità del Servizio</strong></p>
                <p class="text"><strong>Fiberdroid</strong> offre servizi di comunicazione elettronica tramite reti e servizi di altri Operatori. Di conseguenza, potrà essere ritenuta responsabile solo per la parte di propria competenza. Inoltre, <strong>Fiberdroid</strong> declina ogni responsabilità per interventi di manutenzione ordinaria e straordinaria, riparazioni o casi di forza maggiore.</p>
                <p class="text">Tuttavia, <strong>Fiberdroid</strong> si impegna a ridurre al minimo i disagi per i Clienti, comunicando per tempo eventuali interventi di manutenzione programmata che potrebbero causare interruzioni, tramite e-mail o i propri canali ufficiali, e contattando direttamente il Cliente se si rendono necessari interventi presso la sede del Cliente.</p>

                <p class="text"><strong>3) Partecipazione e Confronto</strong></p>
                <p class="text"><strong>Fiberdroid</strong> promuove il dialogo attivo con i Clienti, singoli o tramite associazioni di categoria, incoraggiando la presentazione di memorie, documenti, proposte e suggerimenti per migliorare la qualità dei servizi offerti.</p>
                <p class="text">Le indicazioni possono essere inviate telefonicamente al numero 0283595676 o via e-mail all'indirizzo:<a href="mailto:speed@fiberdroid.it">speed@fiberdroid.it</a>.</p>

                <p class="text"><strong>4) Trasparenza, Correttezza, Disponibilità e Cortesia</strong></p>
                <p class="text">Per garantire trasparenza e chiarezza, <strong>Fiberdroid</strong> utilizza un linguaggio semplice e comprensibile nella stesura delle condizioni contrattuali. Le informazioni sui servizi, attivazioni, fatturazione, recesso e reclami sono chiaramente riportate nel contratto e pubblicate sul sito www.fiberdroid.it.</p>
                <p class="text">I Clienti possono richiedere ulteriori chiarimenti contattando il numero 0283595676 o inviando una e-mail a:<a href="mailto:speed@fiberdroid.it">speed@fiberdroid.it</a>. <strong>Fiberdroid</strong> si affida alla consulenza di installatori e system integrator su tutto il territorio nazionale per valutare e consigliare le migliori soluzioni tecniche per i Clienti.</p>
                <p class="text">Il servizio di Customer Care, erogato da personale interno, garantisce assistenza professionale, cortesia e disponibilità, assicurando una gestione rapida e competente delle richieste dei Clienti.</p>

                <p class="text"><strong>5) Efficacia ed Efficienza</strong></p>
                <p class="text">La mission di <strong>Fiberdroid</strong> è promuovere la digitalizzazione del Paese e ridurre il divario tecnologico tra le diverse aree geografiche. Per raggiungere tali obiettivi, <strong>Fiberdroid</strong> amplia costantemente la propria offerta di servizi con soluzioni innovative volte a migliorare le performance e l’operatività dei propri Clienti.</p>

                <p class="text"><strong>6) Diritto di Scelta</strong></p>
                <p class="text"><strong>Fiberdroid</strong> rispetta e tutela la libertà di scelta dei Clienti in tutte le fasi della relazione contrattuale, garantendo chiarezza nelle informazioni riguardanti le offerte, le condizioni economiche e le caratteristiche tecniche dei servizi.</p>
                <p class="text">Ad eccezione di alcune tipologie di servizi che richiedono investimenti infrastrutturali, <strong>Fiberdroid</strong> non prevede vincoli contrattuali. Il Cliente può disdire o risolvere il contratto in qualsiasi momento inviando una comunicazione PEC a: info@pec.fiberdroid.it o una raccomandata a/r a Fiberdroid, Via Enrico Cosenz n. 54, 20158 Milano (MI). In alternativa, attraverso il portale Fiberdroid, è anche possibile inviare disdetta accedendo alla propria area cliente.</p>
                <p class="text"><strong>Fiberdroid</strong> garantisce, inoltre, il diritto del Cliente di utilizzare il proprio codice di migrazione, sempre disponibile in fattura o tramite comunicazione all'indirizzo e-mail:<a href="mailto:speed@fiberdroid.it">speed@fiberdroid.it</a>.</p>
                <p class="text">In conformità alla delibera 348/18/CONS, <strong>Fiberdroid</strong> promuove il “modem libero”, consentendo ai Clienti di scegliere liberamente gli apparati da utilizzare, indipendentemente dalle soluzioni proposte dall’azienda. In caso di disservizio dovuto ad un guasto o ad un problema tecnico dell’apparato di proprietà del cliente, <strong>Fiberdroid</strong> non si riterrà responsabile per la mancata fruibilità del servizio e potrebbero essere addebitati al cliente eventuali costi di uscita a vuoto da parte del tecnico.</p>

                <p class="text"><strong>7) Sicurezza</strong></p>
                <p class="text"><strong>Fiberdroid</strong> informa i propri Clienti che, in ottemperanza alle normative vigenti, i Server sono ubicati all'interno dell'Unione Europea e protetti da sistemi di sicurezza antincendio, sorveglianza, monitoraggio e protezione contro agenti atmosferici, con verifiche e manutenzioni programmate.</p>
                <p class="text">I sistemi di posta elettronica e accesso a Internet sono gestiti dal supporto tecnico interno di Fiberdroid, che adotta tutte le misure necessarie per prevenire attacchi informatici e utilizzi illeciti.</p>

                <p class="title my-6 is-size-2">PARTE 2</p>

                <p class="subtitle-text"><strong>RAPPORTI CON I CLIENTI</strong></p>

                <p class="text">I clienti possono contattare <strong>Fiberdroid</strong> con le seguenti modalità:</p>

                <ul class="text">
                    <li>Via Telefono: numerazione 0283595676 (centralino attivo nei seguenti giorni e orari dal lunedì al venerdì dalle ore 9:00 alle 13:00 e dalle ore 14:00 alle ore 18:00).</li>
                    <li>Via e-mail:
                        <ul>
                            <li>speed@fiberdroid.it (per informazioni tecniche e commerciali).</li>
                            <li>amministrazione@fiberdroid.it (per info in merito allo stato dei pagamenti e degli insoluti, procedure e modalità di esecuzione del contratto, fatturazione dei servizi e richieste generiche).</li>
                            <li>info@pec.fiberdroid.it (per invio comunicazioni formali, richieste di disdetta, contestazioni).</li>
                        </ul>
                    </li>
                </ul>

                <p class="text">La modulistica adottata da <strong>Fiberdroid</strong> individua in modo chiaro e trasparente:</p>

                <ul class="text">
                    <li>a) la tipologia di servizio oggetto del contratto;</li>
                    <li>b) le condizioni tecniche ed economiche, con particolare attenzione alla definizione dei prezzi e dei costi;</li>
                    <li>c) la durata del contratto, le condizioni di cessazione dei servizi e del contratto;</li>
                    <li>d) la modalità di fatturazione;</li>
                    <li>e) le ipotesi di sospensione del servizio in caso di morosità;</li>
                    <li>f) le ipotesi di sviluppo di traffico anomalo;</li>
                    <li>g) le ipotesi di indennizzi e rimborsi;</li>
                    <li>h) una sintesi della procedura da seguire per i reclami e/o i ricorsi all’Autorità competente.</li>
                </ul>

                <p class="subtitle-text"><strong>ADESIONE AI SERVIZI</strong></p>

                <p class="text">L’attivazione di servizi offerti a titolo oneroso avviene soltanto previa richiesta esplicita da parte del Cliente. <strong>Fiberdroid</strong> si impegna a rispettare i termini di attivazione contrattuali. In caso di mancato rispetto di tali tempistiche, saranno concessi al Cliente gli indennizzi contrattuali di cui alla Delibera 347/18/CONS, salvo il ritardo nell’attivazione di un servizio risulti imputabile al Cliente o a terzi o comunque dovuto a forza maggiore. In quest’ultimo caso, infatti, <strong>Fiberdroid</strong> si impegna a comunicare al Cliente i motivi del ritardo e, ove possibile, i tempi di attivazione previsti ma non concederà alcun indennizzo.</p>

                <p class="subtitle-text"><strong>CONTRATTO</strong></p>

                <p class="text">Le condizioni Generali e Particolari del Contratto, unitamente alla Carta dei Servizi, agli Allegati tecnici e all’Offerta commerciale costituiscono il “Contratto”.</p>

                <p class="text">Il Contratto, come definito poc’anzi, si considererà perfezionato, salvo comunicazione contraria da parte di Fiberdroid, con l’attivazione di almeno uno dei servizi richiesti.</p>

                <p class="text">La proposta di contratto da parte del Cliente avviene con la sottoscrizione della modulistica e la trasmissione della medesima a <strong>Fiberdroid</strong> mediante comunicazione PEC all’indirizzo info@pec.fiberdroid.it o inserimento in via telematica dal partner incaricato attraverso il portale dedicato.</p>

                <p class="text">Ad eccezione di alcune tipologie di servizi che richiedono l’esecuzione di opere aggiuntive o investimenti finanziari, il contratto sottoscritto è da intendersi a tempo indeterminato, fatta salva la possibilità del Cliente di esercitare il diritto di disdetta mediante PEC all’indirizzo info@pec.fiberdroid.it o lettera raccomandata a/r presso la sede operativa di <strong>Fiberdroid</strong> sita in Milano, Via Enrico Cosenz n. 54, 20158 (MI).</p>

                <p class="text">Il Cliente potrà recedere in qualsiasi momento dal Contratto, con modalità e tempistiche regolamentate dalle condizioni generali di Contratto e dalle condizioni particolari del singolo servizio.</p>

                <p class="text">Il Cliente sarà tenuto a versare a <strong>Fiberdroid</strong> l’importo dovuto per l’utilizzo del servizio fino alla sua effettiva disattivazione, nonché l’eventuale contributo per il costo di disattivazione dello stesso, se applicabile.</p>

                <p class="subtitle-text"><strong>MODIFICA E RECESSO DAL CONTRATTO</strong></p>

                <p class="text">Il contratto può subire delle modifiche a seguito di:</p>
                <ul class="text">
                    <li>a) intervenute disposizioni di leggi e/o regolamenti e/o provvedimenti da parte dell’Autorità competenti;</li>
                    <li>b) condizioni tecniche e/o economiche migliorative;</li>
                    <li>c) decisione unilaterale di <strong>Fiberdroid</strong> sfavorevoli al Cliente.</li>
                </ul>
                <p class="text">Nelle ipotesi descritte nei punti a) e b) in quanto dovute a cause sopravvenute o perché non comportano alcun onere aggiuntivo per il Cliente o anzi prevedono un miglioramento delle prestazioni del servizio o una riduzione dei costi, saranno immediatamente applicabili.</p>
                <p class="text">Nell’ipotesi descritta al punto c) queste avranno effetto non prima del decorso di 30 giorni dalla data di comunicazione ed entro i successivi 60 giorni il Cliente potrà esercitare la facoltà di recedere dal singolo servizio o dal Contratto senza che ciò comporti l’applicazione di alcun costo o onere aggiuntivo. La comunicazione dovrà essere inviata mediante PEC o lettera raccomandata a/r.</p>

                <p class="subtitle-text"><strong>FATTURAZIONE E MODALITÀ DI PAGAMENTO</strong></p>

                <p class="text"><strong>Fiberdroid</strong> si impegna a garantire gli standard di trasparenza richiesti dall’Autorità anche nel processo dedicato alla fatturazione dei servizi.</p>
                <p class="text">Le fatture vengono emesse con periodicità mensile o bimestrale o trimestrale esclusivamente per il raggiungimento di un importo minimo fatturato pari ad € 10,00 e contengono l’addebito posticipato dei canoni e dei servizi a consumo (traffico).</p>
                <p class="text">La fattura viene trasmessa in via telematica, ma il Cliente potrà ricevere la copia cortesia mediante e-mail o accedere gratuitamente alla propria area riservata per scaricare autonomamente il documento e controllare il dettaglio del traffico sviluppato dalla numerazione.</p>
                <p class="text">Nella fattura saranno indicati in modo chiaro i singoli servizi, il periodo di riferimento della fatturazione, gli eventuali consumi, la tipologia di traffico telefonico generato, i codici di migrazione per richiedere la portabilità verso altro Operatore.</p>
                <p class="text">Il pagamento delle fatture può avvenire a mezzo SDD (SEPA Direct Debit), bonifico bancario o ri.ba in base alla modalità esplicitata nell’offerta commerciale o in base ad una successiva richiesta da parte del Cliente. Ad ogni modo <strong>Fiberdroid</strong> si riserva la facoltà di introdurre nuove modalità di pagamento coerenti con lo sviluppo dei sistemi di pagamento e/o di limitarne alcune in relazione a specifici prodotti e/o servizi offerti.</p>
                <p class="text"><strong>Fiberdroid</strong> si riserva il diritto di effettuare conguagli successivi addebitando in un’unica fattura traffico, canoni e costi di servizio dovuti anche se precedentemente non contabilizzati. Il Cliente avrà diritto di richiedere i relativi giustificativi e, qualora l’importo fatturato a conguaglio risultasse elevato, potrà concordare con <strong>Fiberdroid</strong> una rateizzazione.</p>
                <p class="text">In caso di errori di fatturazione, e salvo i casi di rimborso automatico, le segnalazioni e le richieste di rimborso potranno essere inoltrate all’indirizzo e-mail amministrazione@fiberdroid.it o mediante PEC all’indirizzo info@pec.fiberdroid.it o mediante lettera raccomandata a/r presso la sede operativa di <strong>Fiberdroid</strong> sita in Milano, Via Enrico Cosenz n. 54, 20158 (MI).</p>
                <p class="text">Nel caso di ritardo nei pagamenti, fermo restando il diritto di <strong>Fiberdroid</strong> di sospendere e/o risolvere il Contratto o il singolo Servizio, nel rispetto della normativa vigente, l’utente sarà tenuto a corrispondere a <strong>Fiberdroid</strong> gli interessi moratori nella misura massima prevista dalla vigente normativa, oltre le eventuali spese sostenute per il recupero del credito.</p>
                <p class="text">Eventuali modalità di documentazione diverse da quella base saranno offerte a titolo oneroso con tariffe ragionevoli.</p>

                <p class="subtitle-text"><strong>BLOCCO SELETTIVO DELLE CHIAMATE</strong></p>

                <p class="text">Il Cliente dispone fin dall’attivazione del servizio di blocco selettivo di chiamata permanente configurato per inibire le comunicazioni verso: satellitari, internazionali, premium e blacklist.</p>
                <p class="text">La rinuncia allo sbarramento selettivo di chiamata permanente o la modifica delle opzioni dello stesso, possono essere richieste dal cliente mediante comunicazione e-mail all’indirizzo<a href="mailto:speed@fiberdroid.it">speed@fiberdroid.it</a> oppure tramite ticket su area clienti presente su portale www.fiberdroid.it.</p>

                <p class="subtitle-text"><strong>CONSUMI ED USO INDEBITO DEL COLLEGAMENTO</strong></p>

                <p class="text">Il Cliente può gratuitamente tramite accesso alla propria area riservata, prendere visione o controllare il dettaglio del traffico sviluppato dalla numerazione o farne espressa richiesta a <strong>Fiberdroid</strong> mediante e-mail a<a href="mailto:speed@fiberdroid.it">speed@fiberdroid.it</a>, PEC o lettera raccomandata a/r presso la sede operativa di <strong>Fiberdroid</strong> sita in Milano, Via Enrico Cosenz n. 54, 20158 (MI).</p>
                <p class="text">Il dettaglio del traffico potrà fornire le seguenti informazioni nel rispetto dei principi privacy: data e ora di inizio della conversazione, numero selezionato, tipo, località, durata, costo della conversazione, numerazione chiamata parzialmente oscurata.</p>
                <p class="text">In caso di effettuazione di un volume di traffico anomalo, per direttrici o volumi, rispetto al profilo medio della tipologia di Cliente e indipendentemente delle previsioni del piano tariffario prescelto e dall’offerta commerciale (ad esempio tariffazione Flat), <strong>Fiberdroid</strong> si riserva il diritto di sospendere parzialmente o totalmente il Servizio, in via precauzionale e nell’interesse del Cliente, o di intervenire anche tramite interventi di limitazione della velocità di connessione di cui al piano tariffario.</p>
                <p class="text">Gli interventi sopra indicati sono facoltativi e non costituiscono in alcun modo un obbligo di <strong>Fiberdroid</strong> nei confronti del Cliente, con conseguente diritto di <strong>Fiberdroid</strong> di pretendere il pagamento del traffico generato indipendentemente dall’anomalia relativa al volume di traffico generato.</p>
                <p class="text">Ove possibile, prima di procedere alla sospensione del Servizio, <strong>Fiberdroid</strong> invierà al Cliente comunicazione scritta al fine di verificare il grado di consapevolezza del medesimo in merito alla generazione del traffico anomalo. In ogni caso il Servizio verrà ripristinato non appena il Cliente, assumendosi ogni onere, rischio e responsabilità, avrà effettuato le dovute verifiche tecniche ed autorizzato <strong>Fiberdroid</strong> a procedere.</p>
                <p class="text">È in ogni caso diritto del Cliente disconoscere il traffico anomalo presentando debita denuncia all’Autorità competente.</p>
                <p class="text">In caso di denuncia di frode avente ad oggetto l’uso indebito dei servizi da parte di terzi, presentata dall’utente all’Autorità competente nelle forme previste dalla normativa vigente, i pagamenti relativi al solo traffico di origine fraudolenta possono essere sospesi fino alla definizione della controversia. In caso di frode accertata i pagamenti non imputabili all’utente, qualora già effettuati, verranno rimborsati.</p>
                <p class="text">Resta inteso che tale disposizione si applica ai casi in cui l’uso indebito non sia ascrivibile a responsabilità del Cliente bensì ad atto illecito dovuto ad un’interferenza da parte di terzi non autorizzati.</p>
                <p class="text">In caso di mancata denuncia o in caso di esito negativo dell’accertamento, gli importi saranno addebitati al Cliente.</p>

                <p class="subtitle-text"><strong>ASSISTENZA SEGNALAZIONI E RECLAMI</strong></p>

                <p class="text">Il “Servizio Clienti” è un servizio di assistenza sia tecnica che commerciale adeguato alle esigenze operative dei Clienti che consente di segnalare disservizi, proporre quesiti sui servizi forniti, ottenere informazioni sulle procedure di reclamo ed è disponibile nei giorni, orari e ai riferimenti indicati sul sito www.fiberdroid.it nonché nelle condizioni generali di contratto. Sono esclusi i giorni festivi infrasettimanali e gli eventuali periodi di ferie.</p>
                <p class="text">Il Servizio Clienti è fornito tramite contatto diretto con operatori di <strong>Fiberdroid</strong> e non è basato su call-center.</p>
                <p class="text">In ossequio alla Delibera n. 179/03/CSP, Il Cliente ha diritto di presentare reclami aventi ad oggetto gli importi addebitati in fattura, malfunzionamenti o inefficienze del Servizio, l’inosservanza delle clausole contrattuali o della Carta dei Servizi, nonché dei livelli di qualità in esse stabilite mediante una comunicazione inviata:</p>
                <ul class="text">
                    <li>a) a mezzo raccomandata A/R al seguente indirizzo: <strong>Fiberdroid</strong> s.r.l. – Sede Operativa, Milano, Via Enrico Cosenz n. 54, 20158 (MI);</li>
                    <li>b) mediante PEC all’indirizzo info@pec.fiberdroid.it, indicando in modo chiaro e preciso i dati identificativi del Cliente (denominazione/ragione sociale, codice cliente, p.iva/c.f.) e il motivo del reclamo.</li>
                </ul>
                <p class="text">Il Cliente deve presentare eventuali reclami entro 3 mesi dal momento in cui è venuto a conoscenza del ritardo e/o disservizio o avrebbe potuto venirne a conoscenza secondo l’ordinaria diligenza, altrimenti perde il diritto all’indennizzo, fatto salvo il diritto al rimborso di eventuali somme indebitamente corrisposte.</p>
                <p class="text"><strong>Fiberdroid</strong> si impegna a valutare la fondatezza del reclamo ed a comunicare l’esito di detta valutazione tramite risposta scritta al Cliente entro 45 giorni solari dal momento in cui il reclamo è pervenuto. Nell’ipotesi di particolare complessità dei reclami, che non consentano una risposta esauriente nei termini previsti, <strong>Fiberdroid</strong> informerà entro il predetto termine il Cliente dello stato di avanzamento dell’indagine e comunicherà i tempi stimati per la risposta.</p>
                <p class="text">In caso di rigetto del reclamo, <strong>Fiberdroid</strong> comunicherà la risposta in forma scritta, adeguatamente motivata, indicando gli accertamenti compiuti. In tal caso, il Cliente che non abbia ancora pagato dovrà corrispondere la somma richiesta da <strong>Fiberdroid</strong> tramite la lettera di definizione del reclamo. Qualora il Cliente non reputi soddisfacente l’esito del reclamo ha facoltà di richiedere la documentazione relativa alle verifiche effettuate da <strong>Fiberdroid</strong> ed eventualmente esperire la procedura di conciliazione indicata nel Contratto ai sensi dell’art. 3 della Delibera 182/02/CONS s.m.i..</p>
                <p class="text">In caso di accoglimento, <strong>Fiberdroid</strong> comunicherà eventuali procedure che saranno adottate per la risoluzione delle eventuali irregolarità riscontrate e provvederà alla restituzione degli eventuali importi già corrisposti dal Cliente, operando uno storno nella successiva fattura, salvo che non si renda necessaria l’emissione di una nota credito e la compensazione o il rimborso dell’importo mediante bonifico bancario in favore del Cliente.</p>

                <p class="subtitle-text"><strong>MALFUNZIONAMENTI – SEGNALAZIONE</strong></p>

                <p class="text">In presenza di condizioni di malfunzionamento o di irregolare funzionamento <strong>Fiberdroid</strong> si impegna ad intervenire con la massima tempestività per provvedere all’individuazione ed alla risoluzione della problematica.</p>
                <p class="text">A seguito di segnalazione mediante contatto telefonico, apertura ticket tramite portale dedicato, invio comunicazione e-mail, l’incarico di <strong>Fiberdroid</strong> eseguirà delle prime verifiche per poter elaborare una prima diagnosi della problematica effettuando una serie di test anche in collaborazione con il Cliente.</p>
                <p class="text">Nel caso in cui l’intervento non sia risolutivo, l’incaricato coinvolgerà il supporto tecnico di secondo livello messo a disposizione dall’Operatore proprietario dell’infrastruttura di rete.</p>
                <p class="text">Una volta eseguiti i controlli qualora emerga che il guasto sia ascrivibile alla struttura del gestore di rete, <strong>Fiberdroid</strong> si impegna ad avviare immediatamente tutte le procedure condivise con il Gestore stesso, segnalando il disservizio al reparto competente e mettendosi a disposizione per eseguire le verifiche stesse. Individuata la natura del disservizio, il guasto viene ufficialmente preso in carico dal Gestore di rete e <strong>Fiberdroid</strong> resta in attesa della segnalazione di chiusura guasto verificando il rispetto delle tempistiche previste dai contratti tra Operatori.</p>
                <p class="text">Ricevuta la comunicazione di chiusura guasto, <strong>Fiberdroid</strong> ne darà comunicazione al Cliente e ripeterà con la collaborazione di quest’ultimo le verifiche di primo livello precedentemente eseguite al fine di garantire la risoluzione della problematica e la definitiva chiusura della segnalazione. Nel caso in cui queste verifiche dessero esito negativo, <strong>Fiberdroid</strong> respingerà la chiusura della segnalazione inviata dal Gestore di rete chiedendo continuità all’intervento tecnico.</p>
                <p class="text">Nel caso in cui l’origine della problematica fosse riconducibile ad una responsabilità del Cliente, sarà attivata la procedura di intervento a vuoto prevista nelle condizioni generali di contratto con conseguente addebito del relativo costo nei confronti del Cliente.</p>

                <p class="subtitle-text"><strong>TEMPI DI RISOLUZIONE DEI DISSERVIZI E MALFUNZIONAMENTI E INDENNIZZI</strong></p>

                <p class="text"><strong>Fiberdroid</strong> si impegna ad eliminare eventuali irregolarità funzionali per la parte di propria competenza, ad eccezione dei guasti di particolare complessità e nelle ipotesi ricomprese nel caso fortuito o di forza maggiore, quali a titolo esemplificativo e non esaustivo, calamità naturali, terremoti, inondazioni, interventi di terzi o manomissioni alle apparecchiature ed ogni altro evento non riconducibile o non controllabile da Fiberdroid. In questi casi al Cliente non sarà riconosciuto alcun indennizzo.</p>
                <p class="text">Gli SLA di risoluzione delle irregolarità variano in base al livello di gravità del guasto:</p>
                <ul class="text">
                    <li>Grado 1 – Totale indisponibilità del Servizio. Necessità di un intervento urgente e chiusura entro 24 ore lavorative;</li>
                    <li>Grado 2 – Servizio indisponibile solo in parte. Necessità di un intervento urgente e chiusura entro 48 ore lavorative;</li>
                    <li>Grado 3 – Servizio degradato, funzionante con prestazioni inferiori ai livelli garantiti. Necessità di un intervento normale con chiusura entro 72 ore lavorative.</li>
                </ul>
                <p class="text">In siffatti casi <strong>Fiberdroid</strong> farà tutto il possibile per ripristinare celermente il Servizio al Cliente, comunicando le tempistiche di intervento. Nei casi di manutenzione programmata, ove sia necessaria l’interruzione della fornitura del Servizio, i Clienti interessati dalla sospensione saranno informati con almeno 24 ore di anticipo. <strong>Fiberdroid</strong> si attiverà, in ogni caso, per risolvere i problemi arrecando ai Clienti i minori disagi possibili.</p>
                <p class="text">Nel caso in cui <strong>Fiberdroid</strong> non osservi i termini sopra indicati, il Cliente ha diritto agli indennizzi previsti al punto che segue (“Indennizzi”).</p>
                <p class="text">Sono esclusi dai casi sopra menzionati i guasti di particolare complessità e le ipotesi di caso fortuito o forza maggiore, quali a titolo esemplificativo: calamità naturali, terremoti, inondazioni, esplosioni, insurrezioni, interventi di terzi o manomissioni delle apparecchiature ed ogni altro evento non controllabile o riconducibile a Fiberdroid. In dette circostanze <strong>Fiberdroid</strong> farà tutto quanto necessario per ripristinare il guasto senza però garantirne le relative tempistiche di risoluzione né gli indennizzi di cui al punto che segue.</p>
                <p class="text">Nel caso in cui il malfunzionamento sia dovuto a dolo o colpa del Cliente, a caso fortuito o di forza maggiore, al Cliente non sarà riconosciuto alcun indennizzo.</p>

                <p class="subtitle-text"><strong>INDENNIZZI</strong></p>

                <p class="text"><strong>Fiberdroid</strong> applica gli indennizzi previsti dal Regolamento in materia di indennizzi di cui alla Delibera 347/18/CONS. In particolare, corrisponde gli indennizzi automatici previsti dall’art. 3 della Delibera, previa segnalazione del Cliente, gli indennizzi relativi al ritardo nella risoluzione dei malfunzionamenti rispetto a quanto previsto al punto precedente, definiti all’art. 6, gli indennizzi dovuti in caso di ritardo nella portabilità del numero di cui all’art. 7.</p>
                <p class="text">Qualora il Cliente esegua la misurazione certificata messa a disposizione dall’Autorità e prevista all’articolo 10 della Delibera 156/23/CONS e <strong>Fiberdroid</strong> non ripristini il livello di Servizio a quello promesso contrattualmente a seguito di seconda misurazione, nel caso il Cliente opti per gli indennizzi contrattuali, gli verrà riconosciuto un indennizzo pari al 50% di una mensilità del Servizio in abbonamento impattato dal disservizio.</p>

                <p class="subtitle-text"><strong>PORTABILITÀ</strong></p>

                <p class="text">Fiberdroid, come già inteso nel presente documento, fornisce ai Clienti il Codice di Migrazione al fine di favorire le procedure di cambio operatore; il Cliente può reperire il Codice di Migrazione nel documento di fatturazione.</p>
                <p class="text">Con riferimento alla procedura di passaggio da un operatore all’altro, specificatamente per la portabilità del numero, il Cliente deve offrire a <strong>Fiberdroid</strong> il codice di migrazione, per evitare KO per codice errato. <strong>Fiberdroid</strong> ha cura di controllare con il Cliente la correttezza di tale codice. Una volta verificata la relativa correttezza, il codice viene inserito nel portale OLO2OLO, la piattaforma web attraverso cui vengono gestite le migrazioni in entrata e in uscita.</p>
                <p class="text"><strong>Fiberdroid</strong> segue gli aggiornamenti della procedura e si impegna a dare relativa informazione di ogni cambiamento al Cliente. La procedura tecnica di passaggio segue i principi stabiliti dall’Autorità con la Delibera 274/07/CONS e s.m.i..</p>
                <p class="text">Qualora il Cliente risolva il Contratto con <strong>Fiberdroid</strong> o con altro operatore conserverà il diritto a mantenere il numero telefonico per 60 (sessanta) giorni dalla disattivazione del numero presso l’operatore di provenienza.</p>

                <p class="subtitle-text"><strong>CONTROVERSIE</strong></p>

                <p class="text">La risoluzione delle controversie, ad eccezione di quelle aventi per oggetto il mancato pagamento del servizio in caso di mancata contestazione, è subordinata all’espletamento di un tentativo di conciliazione.</p>
                <p class="text">Il tentativo di conciliazione dovrà essere attivato attraverso la piattaforma “Conciliaweb” davanti al Co.re.com competente per territorio, pena l’improcedibilità del ricorso davanti al giudice ordinario.</p>
                <p class="text">In caso di corretta e tempestiva instaurazione della procedura di reclamo, il Cliente potrà sospendere la corresponsione il pagamento limitatamente al servizio contestato e in tali ipotesi <strong>Fiberdroid</strong> non potrà applicare la procedura di sospensione amministrativa del servizio per mancato pagamento durante il periodo di pendenza della procedura instaurata, fino alla sua definizione.</p>

                <p class="subtitle-text"><strong>TUTELA DELLA PRIVACY</strong></p>

                <p class="text">Ai sensi del Regolamento UE 2016/679 in materia di Protezione delle persone fisiche con riguardo al trattamento dei dati personali (“GDPR”) <strong>Fiberdroid</strong> garantisce il trattamento dei dati nel rispetto dell’informativa consegnata alla sottoscrizione del contratto. L’informativa completa è allegata al Contratto e disponibile sul sito web www.fiberdroid.it.</p>
                <p class="text">I diritti elencati dagli artt. 15, 16, 17, 18, 20, 21 del GDPR potranno essere fatti valere scrivendo all’indirizzo e-mail<a href="mailto:speed@fiberdroid.it">speed@fiberdroid.it</a>.</p>

                <p class="text"><em>Ultimo aggiornamento: 05 novembre 2024</em></p>


            </div>
        </div>
    </div>

    <style>
        ul li {
            color: white;
        }

        .subtitle-text {
            margin-top: 40px;
        }
    </style>
@endsection

