@extends('layouts._layout_v2')

@section('title', "Tutto ciò di cui siamo appassionati | Fiberdroid")
@section('description', "Visita il nostro blog, post recenti su tecnologie Internet, telecomunicazioni e progetti interessanti.")

@section('content')
    <div class="columns is-multiline" style="padding-top: 59px">
        <div class="column is-12 has-text-centered">
            <h1 class="landing-text-title">
                Tutto ciò di cui<br>siamo appassionati
            </h1>
            <p class="text mt-4">
                Post recenti su tecnologie Internet,<br> telecomunicazioni e progetti interessanti.
            </p>
        </div>
    </div>

    <!-- FEATURED ARTICLE -->
    <div class="columns mt-6 py-6 px-7-tablet px-7-desktop px-6-mobile">
        <div class="column mobile-12">
            @if($featured && $featured->img)
            <figure class="blog-featured-image" style="background-image: url('{{ json_decode($featured->img->image)->path ?? '' }}'); background-size: cover; background-position: center;">
            </figure>
            @endif
        </div>
        <div class="column" style="display: flex;">
            <div style="display: inline; width: 100%; margin: auto; padding-right: 1.5rem">
                <h2 class="blog-featured-text-title">{{ $featured->title }}</h2>
                <p class="text mt-5">
                    {{ $featured->meta_description }}
                </p>
                <a class="button blog-button is-fd-primary mt-5" href="{{ $featured->url }}">
                    Read More
                </a>
            </div>
        </div>
    </div>

    <div class="columns mx-6 is-multiline">
        <div class="column is-12">
            <hr style="background-color: rgba(255,255,255,0.13)">
        </div>
        <div class="column is-4" id="search">
            <form action="{{ url('/blog?#search') }}" method="get">
                <div class="control has-icons-right">
                    <input name="search" class="cover-input input is-medium" placeholder="Cerca un articolo" @if(request()->search) value="{{ request()->search }}" @endif>
                    <button type="submit" class="icon is-inline is-small is-clickable" style="background: none; border: none; pointer-events: initial">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
        @php($search = request()->search)
        <div class="column is-7 is-offset-1 hidden-mobile">
            <nav class="pagination" role="navigation" aria-label="pagination">
                @if($articles['last_page'] > 1)
                    <ul class="pagination-list is-justify-content-flex-end">
                        @if($articles['current_page'] > 1)
                            <li>
                                @php($back = $articles['current_page'] - 1)
                                <a href="{{ url("/blog?seach=$search&page={$back}#search") }}" style="background-color: transparent; border: none; color: white" class="pagination-link">
                                    <i class="fa fa-chevron-left"></i>
                                </a>
                            </li>
                        @endif
                        @for($i = 1; $i <= $articles['last_page']; $i++)
                            <li>
                                <a href="{{ url("/blog?search=$search&page=$i#search") }}" style="background-color: #333; @if($i == $articles['current_page']) border: solid white 1px; @else border: none; @endif color: white" href="#" class="pagination-link">{{ $i }}</a>
                            </li>
                        @endfor

                            @if($articles['current_page'] < $articles['last_page'])
                                <li>
                                    @php($next = $articles['current_page'] + 1)
                                    <a href="{{ url("/blog?search=$search&page={$next}#search") }}"  style="background-color: transparent; border: none; color: white" class="pagination-link">
                                        <i class="fa fa-chevron-right"></i>
                                    </a>
                                </li>
                            @endif
                    </ul>
                @endif
            </nav>
        </div>
    </div>


    <div class="columns is-multiline px-6-mobile px-7-tablet px-7-desktop mt-3-mobile">
        @foreach($articles['data'] as $key => $article)
            @php($article = collect($article))
            <div class="column desktop-4 tablet-6 mobile-12 mt-6">
                <a href="{{ $article["url"] }}">
                    <div class="blog-container is-clickable {{ $key % 3 == 1 ? 'mx-auto' : '' }} {{ $key % 3 == 2 ? 'ml-auto' : '' }}">
                        <div class="blog-image" style="background-size: cover; background-image: url('{{ json_decode($article['img']['image'])->path ?? '' }}'); background-position: center; border-radius: 12px"></div>
                        <div>
                            <h2 class="blog-text-title mt-3">{{ $article["title"] }}</h2>
                            <p class="text mt-3">
                                {{ $article["meta_description"] }}
                            </p>
                            <a href="{{ $article["url"] }}">
                                <p class="blog-continue-text mt-3 is-clickable">
                                    Continua >>
                                </p>
                            </a>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach

            @if($articles['last_page'] > 1)
                <div class="column is-12 has-text-centered is-size-5 mt-4 hidden-tablet-only hidden-desktop">
                    @php($next = ($articles['current_page'] + 1) % $articles['last_page'])
                    <a href="{{ url("/blog?search=$search&page={$next}#search") }}"><b>Carica altri articoli...</b></a>
                </div>
            @endif
    </div>

    <div class="columns hidden-touch">
        <div class="column is-12">
            @component('pages.components.banner')@endcomponent
        </div>
    </div>
@endsection

