@extends('layouts._layout_v2')

@section('title', "Listino VoIP | Fiberdroid")
@section('description', "Siamo l’azienda che ti permette di avere un’unica assistenza per Internet, wifi, centralino cloud, fibra e connettivita’, piattaforma tecnologica. Il nostro team è in servizio da oltre 10 anni nel settore IT & TLC per garantire alle aziende, in Italia e all’estero, connettività veloce, assistenza affidabile, tecnologie innovative.")

@section('content')
    <div class="columns">
        <div class="column is-6 tablet-12 mx-auto px-6-mobile" style="padding-top: 129px">
            <h1 class="title-text has-text-centered mb-6" data-invisible data-fade-in-text>
               Listino VoIP 2025 Italia
            </h1>
            <div class="columns">
                <div class="column is-10 mx-auto">
                    <div class="columns" style="border-bottom: solid rgba(255,255,255, 0.3) 1px; font-weight: bold; color: white">
                        <div class="column is-6">
                            Tariffe Nazionali
                        </div>
                        <div class="column is-6 has-text-right">
                            &euro;/minuto
                        </div>
                    </div>
                    <div class="columns is-multiline" style="color: #c4c2c2">
                        <div class="column is-6">
                            Fissi Nazionali
                        </div>
                        <div class="column is-6 has-text-right">
                            0.007 &euro;/minuto
                        </div>
                        <div class="column is-6">
                            Mobili Nazionali
                        </div>
                        <div class="column is-6 has-text-right">
                            0.0065 &euro;/minuto
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="columns is-multiline">
        <div class="column is-12">
            <hr style="background-color: rgba(255,255,255,0.13)">
        </div>
    </div>

    @php($search = request()->search)
    <div class="columns">
        <div class="column is-6 tablet-12 mx-auto px-6-mobile">
            <div class="columns is-multiline">
                <div class="column is-10 mx-auto">
                    <h1 class="title-text has-text-centered mb-6" data-invisible data-fade-in-text>
                        Listino VoIP 2025 Estero
                    </h1>
                </div>
                <div class="column is-10 mx-auto" id="search">
                    <form action="{{ url('/listini?#search') }}" method="get" style="float: right; margin-right: 40px">
                        <div class="control has-icons-right">
                            <input name="search" class="cover-input input is-medium" placeholder="Cerca una direttrice" @if(request()->search) value="{{ request()->search }}" @endif>
                            <button type="submit" class="icon is-inline is-small is-clickable" style="background: none; border: none; pointer-events: initial">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="columns">
                <div class="column is-10 mx-auto">
                    <div class="columns" style="border-bottom: solid rgba(255,255,255, 0.3) 1px; font-weight: bold; color: white">
                        <div class="column is-6">
                            Direttrice
                        </div>
                        <div class="column is-3 has-text-centered">
                            &euro;/minuto
                        </div>
                        <div class="column is-3 has-text-right">
                            Scatto alla risposta
                        </div>
                    </div>
                    <div class="columns is-multiline" style="color: #c4c2c2">
                        @foreach($directories['data'] as $key => $directory)
                            @php($directory = collect($directory))
                            <div class="column is-6">
                                {{ $directory['name'] }}
                            </div>
                            <div class="column is-3 has-text-centered">
                                {{ number_format($directory['price']['sub_charge'] ?? 0, 4, ',', '.') }} &euro;
                            </div>
                            <div class="column is-3 has-text-right">
                                {{ number_format($directory['price']['answer_charge'] ?? 0, 4, ',', '.') }} &euro;
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="column is-7 is-offset-1 hidden-mobile">
        <nav class="pagination" role="navigation" aria-label="pagination">
            @if($directories['last_page'] > 1)
                <ul class="pagination-list is-justify-content-flex-end">
                    @if($directories['current_page'] > 1)
                        <li>
                            @php($back = $directories['current_page'] - 1)
                            <a href="{{ url("/listini?seach=$search&page={$back}#search") }}" style="background-color: transparent; border: none; color: white" class="pagination-link">
                                <i class="fa fa-chevron-left"></i>
                            </a>
                        </li>
                    @endif
                    @php($current = $directories['current_page'])
                    @if($current < $directories['last_page'] - 4 && $current >= 3)
                            @for($i = $current-3 > 0 ? $current - 2 : 1; $i <= $current+1; $i++)
                                <li>
                                    <a href="{{ url("/listini?search=$search&page=$i#search") }}" style="background-color: #333; @if($i == $directories['current_page']) border: solid white 1px; @else border: none; @endif color: white" href="#" class="pagination-link">{{ $i }}</a>
                                </li>
                            @endfor
                        @else
                            @for($i = 1; $i <= 4; $i++)
                                <li>
                                    <a href="{{ url("/listini?search=$search&page=$i#search") }}" style="background-color: #333; @if($i == $directories['current_page']) border: solid white 1px; @else border: none; @endif color: white" href="#" class="pagination-link">{{ $i }}</a>
                                </li>
                            @endfor
                        @endif
                        <li>
                            <span class="pagination-ellipsis">&hellip;</span>
                        </li>
                        @for($i = $directories['last_page'] - 3; $i <= $directories['last_page']; $i++)
                            <li>
                                <a href="{{ url("/listini?search=$search&page=$i#search") }}" style="background-color: #333; @if($i == $directories['current_page']) border: solid white 1px; @else border: none; @endif color: white" href="#" class="pagination-link">{{ $i }}</a>
                            </li>
                        @endfor

                    @if($directories['current_page'] < $directories['last_page'])
                        <li>
                            @php($next = $directories['current_page'] + 1)
                            <a href="{{ url("/listini?search=$search&page={$next}#search") }}"  style="background-color: transparent; border: none; color: white" class="pagination-link">
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </li>
                    @endif
                </ul>
            @endif
        </nav>
    </div>

@endsection
