@extends('layouts._layout_v2')

@section('title', ($service_product->meta_title ?: "$service_product->name") . " | Fiberdroid")
@section('description', $service_product->meta_description)

@section('content')
    <div class="columns pt-6-mobile">
        <div class="column is-12 px-6-mobile" style="padding-top: 59px">
            <div class="columns is-multiline mb-6 is-vcentered">
                <div class="column is-5 is-offset-1">
                    <h1 class="product-text-title mt-6-tablet mt-6-desktop mt-2-mobile">
                        Gestisci la disponibilità dei tuoi servizi di connettività
                        in tempo reale
                    </h1>
                    <p class="service-text-description mt-6-tablet mt-6-desktop mt-3-mobile">
                        Noi di Fiberdroid sappiamo quanto sia importante avere monitorata la propria rete di servizi, per questo motivo abbiamo ideato un sistema di monitoraggio operativo h24 che consente di aver la piena consapevolezza del corretto funzionamento della propria linea.
                        <br><br>
                        Non solo manteniamo sempre aggiornati i nostri apparati, ma forniamo anche analisi complete in tempo reale senza bloccare la tua operatività.

                    </p>
                    <div class="control w-50-desktop w-75-mobile mt-6" style="position: relative; z-index: 1000">
                        <a rel="noopener noreferrer" href="https://calendly.com/fiberdroid/richiesta-chiamata" target="_blank" class="button is-fd-primary" style="width: 100%">
                            Organizza una call
                        </a>
                    </div>
                </div>
                <div class="column is-6 is-flex">
                    <div style="display: flex; flex-direction: column; position: relative" class="product-main-image-wrapper-{{ \Illuminate\Support\Str::slug($service_product->name) }}">
                        <img alt="Mappa router fiberdroid" src="{{ asset('pages/products/router/map.png') }}" class="product-main-image-{{ \Illuminate\Support\Str::slug($service_product->name) }}" style="width: 700px; border-radius: 20px; position: relative; z-index: 999">
                        <img alt="Notifica positiva" src="{{ asset('pages/products/router/success.png') }}" class="product-map-tag tag-success" style="">
                        <img alt="Notifica di warning" src="{{ asset('pages/products/router/warning.png') }}" class="product-map-tag tag-warning" style="">

                    </div>
                </div>

                <div class="column is-6 my-6 hidden-mobile is-flex">
                    <div style="display: flex; flex-direction: column" class="product-main-image-wrapper-{{ \Illuminate\Support\Str::slug($service_product->name) }}">
                        <img alt="Router Draytek 2915" src="{{ asset('pages/products/draytek.png') }}" style="width: 384px; border-radius: 20px; position: relative; z-index: 999">
                        <p class="product-check-text has-text-centered mt-3">
                            <img alt="Icona di ok" src="{{ asset('pages/icons/check.png') }}" style="width: 14px; position: relative; margin-bottom: -2px">
                            Manutenzione avvenuta con successo
                        </p>
                    </div>

                </div>
                <div class="column is-6 my-6 py-6 pr-6">
                    <h2 class="product-text-subtitle">
                        Manutenzioni e <br>
                        configurazioni non saranno più un tuo problema, <br>
                        pensiamo a tutto noi!
                    </h2>
                    <p class="service-text-description mt-6-tablet mt-6-desktop mt-3-mobile">
                        I router saranno gestiti in maniera esclusiva dai tecnici Fiberdroid, in questo modo garantiamo maggiore sicurezza e più tranquillità per l’utente finale.
                        <br><br>
                        Non dovrai più preoccuparti di nulla, se ti occorre una configurazione sull’apparato, basta richiederlo tramite la sezione “Supporto” presente nell’area clienti e penseremo a tutto.
                    </p>
                </div>

                <div class="column is-6 my-6 hidden-desktop hidden-tablet-only is-flex">
                    <div style="display: flex; flex-direction: column" class="product-main-image-wrapper-{{ \Illuminate\Support\Str::slug($service_product->name) }}">
                        <img alt="Router Draytek 2915" src="{{ asset('pages/products/draytek.png') }}" style="width: 384px; border-radius: 20px; position: relative; z-index: 999">
                        <p class="product-check-text has-text-centered mt-3">
                            <img alt="Icona di ok" src="{{ asset('pages/icons/check.png') }}" style="width: 14px; position: relative; margin-bottom: -2px">
                            Manutenzione avvenuta con successo
                        </p>
                    </div>

                </div>

                <div class="column is-5 is-offset-1 py-6 my-6">
                    <h2 class="product-text-subtitle">
                        Identifica Problemi<br>
                        in Pochi Secondi
                    </h2>
                    <p class="service-text-description mt-6-tablet mt-6-desktop mt-3-mobile">
                        L'analisi completa in tempo reale ti consente di identificare i problemi in pochi secondi, così tu (e noi) possiamo reagire ancora più velocemente.
                    </p>
                </div>
                <div class="column is-6 my-6 is-flex is-flex-direction-column" style="position: relative">
                    <img alt="Esempio di notifica di errore del monitoraggio router fiberdroid" class="product-tags error-tags" src="{{ asset('pages/products/router/error.png') }}" >
                    <img alt="Esempio di notifica di successo del monitoraggio router fiberdroid" class="product-tags success-tags" src="{{ asset('pages/products/router/success.png') }}">
                    <img alt="Esempio di notifica di warning del monitoraggio router fiberdroid" class="product-tags warning-tags" src="{{ asset('pages/products/router/warning.png') }}">
                </div>

                <div class="column is-6 my-6 is-flex hidden-mobile">
                    <div style="display: flex; flex-direction: column" class="product-main-image-wrapper-{{ \Illuminate\Support\Str::slug($service_product->name) }}">
                        <img alt="Popup funzionalità blocca errori del monitoraggio router fiberdroid" src="{{ asset('pages/products/control.png') }}" style="width: 384px; border-radius: 20px; position: relative; z-index: 999">
                    </div>

                </div>
                <div class="column is-6 my-6 py-6-desktop pt-6-mobile pr-6">
                    <h2 class="product-text-subtitle">
                        Gestisci l'accesso<br>
                        al Sito Web proteggi ulteriormente la tua rete
                    </h2>
                    <p class="service-text-description mt-6-tablet mt-6-desktop mt-3-mobile">
                        Gestisci l'accesso ai siti dannosi e di phishing al fine di proteggere ulteriormente la tua rete. Il servizio è disponibile esclusivamente con i router Fiberdroid.
                    </p>
                </div>
                <div class="column is-6 my-6 is-flex hidden-desktop hidden-tablet-only">
                    <div style="display: flex; flex-direction: column" class="product-main-image-wrapper-{{ \Illuminate\Support\Str::slug($service_product->name) }}">
                        <img alt="Popup funzionalità blocca errori del monitoraggio router fiberdroid" src="{{ asset('pages/products/control.png') }}" style="width: 384px; border-radius: 20px; position: relative; z-index: 999">
                    </div>

                </div>

            </div>
        </div>
    </div>

    @if($products)
        <h2 class="product-text-subtitle has-text-centered">
            Router per ogni budget e esigenza <br class="hidden-mobile">tecnica.
        </h2>

        @foreach($products->values() as $key => $productInfo)
            <div class="columns hidden-mobile ml-4 mt-4 pt-6" @if($key % 2 == 1) style="display: none; position: relative; left: -14%" @else style="display: none" @endif>
                @foreach($productInfo['items'] ?? [] as $item)
                    <div class="column is-carousel-column">
                        @component('pages.components.products.carousel-product-box', $item)@endcomponent
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif

    @php
        function get_product_tags($products) {
            $tags = collect([]);

            foreach ($products as $productInfo) {
                $productTags = $productInfo['products']->pluck('tags')->map(function ($el) {return json_decode($el);})->toArray();

                foreach ($productTags as $productTagArray) {
                    if($productTagArray) {
                        $tags->push(...$productTagArray);
                    }
                }
            }

            return $tags->unique();
        }
        $tags = get_product_tags($products);
    @endphp
    @if($products && $tags->isNotEmpty())
        <div class="columns is-mobile my-6-desktop my-6-tablet pt-6 pb-2" style="position: relative">
            <div class="column is-12 has-text-centered is-flex">
                <div class="field is-grouped is-grouped-multiline mx-auto">
                    @foreach($tags as $tag)
                        <div class="control">
                            <div class="tags is-clickable filter-tags" data-tag-content="{{ $tag }}" style="border: 1.638px solid #FFF; position:relative; z-index: 1000">
                                <span class="tag filter-tag is-white is-unselectable" style="margin: 0">{{ $tag }}</span>
                                <span class="tag filter-tag is-delete is-hidden"></span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @else
        <div class="mt-6 pt-6"></div>
    @endif

    <div class="columns">
        <div class="column is-8 mx-auto">
            @if($products)
                @foreach($products->where('type', 'double') as $type => $productInfo)
                    <div class="columns is-multiline mx-3-mobile" data-section data-{{$type}}>
                        @foreach($productInfo['products'] as $key => $product)
                            <div class="column is-6 px-5 py-5 is-flex" @if($key == intval(sizeof($productInfo['products']) / 2)) id="all" @endif>
                                @component('pages.components.service-box', [
                                    'title' => $product['website_name'] ?? $product['name'],
                                    'type' => "Connettività",
                                    'price' => $product['prices']['fee_amount'],
                                    'image' => $product['image'] ?? null,
                                    'bg' => $product['gradient'] ?? ($productInfo['bg_box'] ?? $productInfo['bg']),
                                    'mark' => $product['mark'] ?? null,
                                    'tags' => $product['tags'] ?? null,
                                    'classes' => $key % 2 == 0 ? "ml-auto" : "mr-auto",
                                    'items' => $product['items'] ? json_decode($product['items']) : ["empty", "empty", "empty"]
                                ])@endcomponent
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    @if($products)
        @foreach($products->where('type', 'triple') as $type => $productInfo)
            <div class="columns is-multiline mx-6-tablet mx-6-desktop mx-3-mobile" data-section data-{{ $type }}>
                @foreach($productInfo['products']->values() as $key => $product)
                    <div class="column is-4 px-4 py-5 is-flex" @if($key == intval(sizeof($productInfo['products']) / 2)) id="all" @endif>
                        @component('pages.components.service-box', [
                            'title' => $product['website_name'] ?? $product['name'],
                            'type' => "Connettività",
                            'price' => $product->prices['fee_amount'],
                            'image' => $product['image'] ?? null,
                            'bg' => $product['gradient'] ?? ($productInfo['bg_box'] ?? $productInfo['bg']),
                            'mark' => $product->mark ?? null,
                            'tags' => $product['tags'] ?? null,
                            'classes' => $key % 3 == 0 ? "ml-auto" : ($key % 3 == 1 ? "mx-auto" : "mr-auto") ,
                            'items' => $product->items ? json_decode($product->items) : ["empty", "empty", "empty"]
                        ])@endcomponent
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif

    @if($service_product->views['termini'] ?? false)
        <div class="columns my-6 py-6 px-4-mobile">
            <div class="column is-12">
                <h2 class="product-text-subtitle has-text-centered mb-6">
                    Preferisci utilizzare il tuo router?<br>
                    Nessun Problema.
                </h2>

                <div class="box service-termini-box py-6 mx-auto">
                    {!! $service_product->views['termini'] !!}
                </div>
            </div>
        </div>
    @endif

    <div class="columns">
        <div class="column is-12">
            <web-component-fiberdroid-quiz
                id="quiz-component"
                icon="{{ $service_product->quiz_icon ?? "quiz/digital.png" }}"
                gradient="{{ $service_product->quiz_gradient ?? url("pages/home/quiz-mask.png") }}"
            ></web-component-fiberdroid-quiz>
        </div>
    </div>

    @if($service_product->hasMainView)
        <div class="columns mt-6 pt-6">
            <div class="column is-11 px-6 mx-auto has-text-white service-content">
                {!! $service_product->views['main'] !!}
            </div>
        </div>
    @endif

    <style>
        body, html, .has-background-black {
            background-color: black!important;
        }
    </style>
@endsection
