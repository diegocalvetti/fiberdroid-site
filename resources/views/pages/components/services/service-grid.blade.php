<div class="box service-grid-box">
    <div class="service-grid-box-wrapper">
        <div class="service-grid-box-grid-wrapper p-6">
            <span class="pt-6">
                <div class="figure is-64x64 mb-5">
                    <img class="image" src="{{ asset('pages/icons/'.$icon) }}">
                </div>

                <span class="service-grid-text mx-auto">
                    {{ $slot }}
                </span>
            </span>
        </div>
    </div>
</div>
