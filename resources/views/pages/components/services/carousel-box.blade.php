<div data-anchor data-target="{{ \Illuminate\Support\Str::slug(\Illuminate\Support\Str::replace("<br>", "", $slot)) }}" class="box service-carousel-box-shadow is-unselectable" style="background: {{ $bg }}; background-position: center; background-size: cover">
    <div class="box service-carousel-box">
        <div class="service-carousel-box-wrapper">
            <div class="service-carousel-box-grid-wrapper">
                    <span class="service-carousel-text mx-auto">
                        {{ $slot }}
                    </span>
            </div>
        </div>
    </div>
</div>
