@php
  $slug = $slug ?? \Illuminate\Support\Str::slug($title);
@endphp
<div
    class="box service-box-gradient {{ $classes ?? "" }} mx-auto-mobile"
    style="background-image: {{ $bg }}; backdrop-filter: blur(50px);"
    data-tags="{{ join(",", json_decode($tags ?? "[]")) }}"
    data-product
    id="{{ $slug }}"
    data-ref="{{ $slug }}"
    data-cart-name="{{ $title }}"
    data-service="{{ $service->name ?? "" }}"
    data-icon-bg="bluebox.svg"
    data-icon="{{ $service->icon ?? "" }}"
    data-price="{{ $price }}"
    data-activation-price="{{ $activation_price ?? "0" }}"
    data-max-speed="{{ $max_speed ?? 0 }}"
    @if($image ?? null) data-image="{{ json_decode($image['image'])->path }}" @endif
>
    <div class="box service-box-box" style="margin-right: -25px; margin-bottom: -25px; display: flex; flex-direction: column">
        <!-- Header -->
        @if($image ?? null)
            <div class="figure" style="display: flex; height: 180px">
                <img loading="lazy" alt="Immagine servizio Fiberdroid: {{ strip_tags($title) }}" class="mx-auto" src="{{ json_decode($image['image'])->path }}" style="@if($slug == 'uap-acbroutdoor') height: 180px; @else height: 130px @endif">
            </div>
        @else
            <div class="columns is-mobile is-vcentered">
                <div class="column pl-0">
                    <div class="service-box-tag tag">
                        <p class="service-box-tag-text">{{ $type }}</p>
                    </div>
                </div>
                @if($mark)
                    <div class="column">
                        <div class="image">
                            <img loading="lazy" src="{{ $mark }}" alt="Bollino Fibra" style="width: 40px; height: 40px"/>
                        </div>
                    </div>
                @endif
            </div>
        @endif

        <p class="service-box-title-text is-uppercase mt-4">{!! $title !!}</p>


        <!-- Bullet List -->
        <div class="mt-auto mb-auto">
            <div class="my-6 py-5" style="border: solid 1px rgba(217, 217, 217, 0.3); border-left: none; border-right: none">
                @if(sizeof($items) == 1)
                    <p class="icon-text mt-4">
                        <span class="service-box-text">{{ $items[0] }}</span>
                    </p>
                @else
                    @foreach($items as $key => $item)
                        <p class="icon-text {{ $key != 0 ? "mt-4" : "" }}" style="flex-wrap: nowrap">
                        <span class="icon is-unselectable">
                            <img loading="lazy" src="{{ asset('pages/home/icons/check.svg') }}" class="image" alt="Check">
                        </span>
                            <span class="service-box-text">{!! $item !!}</span>
                        </p>
                    @endforeach
                @endif
            </div>
        </div>

        <!-- Footer -->
        <div class="mt-auto">
            <div class="columns is-mobile is-vcentered" style="width: 100%; margin: 0">
                <div class="column @if($price && $price != 0) is-6 @else is-3 @endif pl-0">
                    <p class="price-text">
                        @if($price && $price != 0)
                            <span>{{ price($price) }}&euro;</span>
                            <span class="is-unselectable">/ mese</span>
                        @else
                            <span class="is-unselectable"></span>
                            <span class="is-unselectable">A progetto</span>
                        @endif
                    </p>
                </div>
                <div class="column @if($price && $price != 0) is-6 @else is-9 @endif has-text-right">
                    @if($price && $price != 0)
                        <button aria-label="Aggiungo al carrello il prodotto: {{ strip_tags($title) }}" class="button is-smaller is-fd-primary" data-add-to-cart data-target="{{ $slug }}">
                            <span class="icon">
                               <img loading="lazy" src="https://fiberdroid.it/pages/home/icons/cart.svg" class="image" alt="Carello">
                            </span>
                            <span style="width: 100%; text-align: right">Aggiungi</span>
                        </button>
                    @else
                        <button aria-label="Organizza una call con Fiberdroid per il servizio: {{ strip_tags($title) }}" target="_blank" rel="noopener" href="https://calendly.com/fiberdroid/richiesta-chiamata" class="button is-smaller is-dark" style="width: 100%" data-add-to-cart data-target="{{ $slug }}">
                            <span class="icon">
                               <img loading="lazy" src="https://fiberdroid.it/pages/home/icons/cart.svg" class="image" alt="Carello">
                            </span>
                            <span style="width: 100%; text-align: right">Organizza una call</span>
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
