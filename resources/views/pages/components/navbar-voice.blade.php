<div class="my-auto" data-voice data-voice-{{ \Illuminate\Support\Str::replace("/", "-", $link ?? "-".$dataTarget) }}>
    <span>
        @if(isset($link))
            <a class="menu-link" href="{{ $link }}">{{ $label }}</a>
        @else
            <span class="menu-link @if(isset($disabled)) menu-link-disabled @endif is-clickable" data-target="{{ $dataTarget }}">{{ $label }}</span>
        @endif
    </span>
</div>
