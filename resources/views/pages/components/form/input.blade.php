<label class="label input-label mt-4">{{ $label  }}<span class="has-text-danger"> *</span></label>
<div class="control">
    <input class="cover-input fd-input input is-medium has-text-left" data-send-{{ $id }} placeholder="{{ $placeholder }}">
    <p data-error-{{ $id }} class="error has-text-danger"></p>
</div>
