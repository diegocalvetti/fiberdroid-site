<form class="cover-form">
    @component('pages.components.form.input', ['id' => 'name', 'label' => "Nome e Cognome", 'placeholder' => "Inserisci il tuo nome"])@endcomponent
    @component('pages.components.form.input', ['id' => 'email', 'label' => "Email", 'placeholder' => "Inserisci il tuo indirizzo email"])@endcomponent
    @component('pages.components.form.input', ['id' => 'phone', 'label' => "Numero di telefono", 'placeholder' => "Inserisci un numero di telefono"])@endcomponent
    @component('pages.components.form.input', ['id' => 'address', 'label' => "Indirizzo di installazione", 'placeholder' => "Inserisci indirizzo di installazione"])@endcomponent
</form>
