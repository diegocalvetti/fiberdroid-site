@php($opened = $opened ?? false)
@php($ref = "accordion-" . \Illuminate\Support\Str::slug($title))

<div class="box accordion is-unselectable is-clickable mt-6" data-target="{{ $ref }}" data-accordion="@if($opened) opened @else closed @endif">
    <div class="columns is-mobile is-vcentered">
        <div class="column is-8">
            <h2 class="accordion-title">
                {{ $title }}
            </h2>
        </div>
        <div class="column is-4 is-flex">
            <img class="accordion-arrow ml-auto {{ $opened ? "accordion-arrow-up" : "accordion-arrow-down" }}" src="{{ asset('pages/icons/arrow-down.svg') }}">
        </div>
    </div>
</div>

<div class="accordion-content" @if(!$opened) style="display: none" @endif data-ref="{{ $ref }}">
    {!! $slot !!}
</div>

