@php
    props([
        [&$icons, ['twitter', 'linkedin', 'linkedin-full', 'youtube', 'chain']],
        [&$links, []],
        [&$size, 24],
        &$color,
        &$label,
    ]);
@endphp

<div class="is-flex mt-4">
    @if($label)
        <p class="share-text mr-4"><b>{{ $label }}</b></p>
    @endif

    @foreach($icons as $icon)
        <a
            @if($icon != "chain")
                href="{{ $links[$icon] ?? "#" }}" target="_blank" rel="noreferrer, noopener" class="mr-3"
            @else
                data-copy-url
            @endif
        >
            <div class="image is-{{ "{$size}x{$size}" }} is-clickable" data-icons style="display: flex">
                <img
                    alt="Icona {{ $icon }}"
                    class="icon {{ $icon == "youtube" ? "mt-2" : "" }}"
                    src="https://fiberdroid.it/images/icons/social/{{ $icon }}{{ $color ? "-".$color : '' }}.svg"
                />
                <img
                    alt="Icona {{ $icon }} attiva"
                    class="icon-active {{ $icon == "youtube" ? "mt-2" : "" }}"
                    style="display: none"
                    src="https://fiberdroid.it/images/icons/social/{{ $icon }}-active.svg"
                />
            </div>
        </a>
    @endforeach
</div>
