<div class="columns mt-4 vcentered-tablet-only is-gapless is-multiline">

    @foreach(($data ?? []) as $datum)
        <div class="column is-2 mt-6">
            <div class="figure" style="display: flex;width: 60px;height: 60px;justify-content: center;align-items: center;gap: 60px;border-radius: 200px;border: 2px solid #313131;background: #000;">
                <img class="image mx-auto" src="https://fiberdroid.it/pages/home/icons/chat-check.svg" alt="" />
            </div>
        </div>
        <div class="column is-10 mt-6-desktop mt-4-touch">
            <h3 class="subtitle-text">{!! $datum['title'] !!}</h3>
            <p class="text">
                {{ $datum['description'] }}
            </p>
        </div>
    @endforeach
</div>
