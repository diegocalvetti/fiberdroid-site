<a @if($link ?? false) href="{{ $link }} @endif">
    <div class="card" style="background: transparent; border-radius: 30px; @if($disabled ?? false) cursor:default; @endif">
        <div class="card-image">
            <div class="figure">
                <img loading="lazy" src="{{ url('images/solutions/'.$image) }}" alt="Scheda soluzione per: {{ $title }}" class="image mr-auto" style="border-radius: 20px; @if($disabled ?? false) filter: grayscale(1); @endif">
            </div>
        </div>
        <div class="card-content pl-0">
            <p class="industries-card-title" @if($disabled ?? false) style="color: grey" @endif>
                {{ $title }}
            </p>
            <p class="text pt-2-mobile">
                {!! $description !!}
            </p>
        </div>
    </div>
</a>
