<a @if($link ?? false) href="{{ $link }} @endif" target="_blank">
    <div class="card" style="background: transparent; border-radius: 30px; @if($disabled ?? false) cursor:default; @endif">
        <div class="card-image">
            <div class="figure" style="position: relative">
                <img loading="lazy" src="{{ url('images/solutions/'.$image) }}" alt="Recensione di: {{ $title }}" class="image mr-auto" style="border-radius: 20px; @if($disabled ?? false) filter: grayscale(1); @endif">
                <img style="position: absolute; top: 20px; right: 20px" class="youtube" src="https://fiberdroid.it/pages/home/union.svg" alt="logo youtube fiberdroid"/>
                <div class="hidden-touch" style="bottom: -22px; padding-left: 20px; width: 100%; position: absolute; background-color: rgba(0,0,0, 0.6)">
                    <span class="is-hidden-touch">
                        <x-quote>
                            <p class="is-size-6">
                                {!! $cit !!}
                            </p>
                        </x-quote>
                    </span>
                    <span class="is-hidden-desktop">
                        <p class="is-size-7 pt-4">
                            {!! $citMobile !!}
                        </p>
                    </span>
                </div>
                <div class="hidden-desktop" style="bottom: 0px; padding-left: 20px; width: 100%; position: absolute; background-color: rgba(0,0,0, 0.6)">
                    <span class="is-hidden-touch">
                        <x-quote>
                            <p class="is-size-6">
                                {!! $cit !!}
                            </p>
                        </x-quote>
                    </span>
                    <span class="is-hidden-desktop">
                        <p class="is-size-6 py-2">
                            {!! $citMobile !!}
                        </p>
                    </span>
                </div>
            </div>
        </div>
        <div class="card-content pl-0">
            <p class="industries-card-title" @if($disabled ?? false) style="color: grey" @endif>
                {{ $title }}
            </p>
            <p class="text pt-2-mobile is-size-7-touch is-size-6-desktop">
                {!! $description !!}
            </p>
            <img src="{{ url('images/stars.png') }}" class="hidden-desktop" style="display: inline; width: 30%">
        </div>
    </div>
</a>
