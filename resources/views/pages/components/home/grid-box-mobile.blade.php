@php
    $grids = array_filter($grids, function ($var) {return $var % 2 == 0;}, ARRAY_FILTER_USE_KEY)
@endphp
<div class="columns is-multiline mt-6 pt-6">

    @foreach(collect($grids)->take(3)->values() as $key => $grid)
        <div class="column is-12 p-5">
            <div class="box p-0 has-background-transparent">
                <figure class="gallery__item gallery__item--{{ $key + 1 }} mx-auto">
                    @if(isset($grid['img']))
                        <img loading="lazy" src="{{ asset('images/grid/' . $grid['img']) }}" class="gallery__img" alt="">
                    @elseif(isset($grid['label']))
                        <div class="gallery-box-container mx-auto-mobile">
                            <div class="box gallery-box-header">
                                <div class="gallery-box-header-content p-5"></div>
                            </div>
                            <div class="p-5">
                                <div class="gallery-text pt-5" style="margin-top: -110px; padding-bottom: 4rem">
                                    {{ $grid['label'] }}
                                </div>
                            </div>
                        </div>
                    @endif
                </figure>
            </div>
        </div>
    @endforeach

    <!--
    <div class="column is-12 p-5">
        <div class="box p-0 has-background-transparent">
            <figure class="gallery__item gallery__item--1">
                <img src="https://fiberdroid.it/pages/home/grid-box/fiber.png" class="gallery__img" alt="Image 1">
            </figure>
        </div>
    </div>
    <div class="column is-12 p-5">
        <div class="box p-0 has-background-transparent">
            <figure class="gallery__item gallery__item--1">
                <img src="https://fiberdroid.it/pages/home/grid-box/work.png" class="gallery__img" alt="Image 1">
            </figure>
        </div>
    </div>
    <div class="column is-12 p-5">
        <div class="box p-0 has-background-transparent">
            <figure class="gallery__item gallery__item--1">
                <img src="https://fiberdroid.it/pages/home/grid-box/office.png" class="gallery__img" alt="Image 1">
            </figure>
        </div>
    </div>
    -->
</div>
