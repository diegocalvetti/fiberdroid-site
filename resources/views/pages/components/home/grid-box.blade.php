<div class="gallery">
    @foreach(collect($grids)->values() as $key => $grid)
        <figure class="gallery__item gallery__item--{{ $key + 1 }}" data-gallery-item="{{ $key + 1 }}">
            <img loading="lazy" src="{{ asset('images/grid/' . $grid['img']) }}" class="gallery__img" alt="{{ $grid['alt'] ?? "" }}">
        </figure>

        <figure class="gallery-card-animation gallery__item gallery__item--{{ $key + 1 }}" data-gallery-card-animation data-target="{{ $key + 1 }}" data-invisible>
            <div class="gallery-box-container mx-auto-mobile">
                <div class="box gallery-box-header">
                    <div class="gallery-box-header-content p-5"></div>
                </div>
                <div class="p-5">
                    <div class="gallery-text pt-5" style="margin-top: -110px">
                        {{ $grid['label'] }}
                    </div>
                </div>
            </div>
        </figure>

@endforeach

    <!--
    <figure class="gallery__item gallery__item--1">
        <img src="https://fiberdroid.it/pages/home/grid-box/fiber.png" class="gallery__img" alt="Image 1">
    </figure>

    <figure class="gallery__item gallery__item--2">
        <img src="https://fiberdroid.it/pages/home/grid-box/work.png" class="gallery__img" alt="Image 1">
    </figure>

    <figure class="gallery__item gallery__item--3">
        <div class="gallery-box-container mx-auto-mobile">
            <div class="box gallery-box-header">
                <div class="gallery-box-header-content p-5"></div>
            </div>
            <div class="p-5">
                <div class="gallery-text pt-5" style="margin-top: -110px">
                    Each time you will speak with the same operator
                </div>
            </div>
        </div>
    </figure>

    <figure class="gallery__item gallery__item--4">
        <img src="https://fiberdroid.it/pages/home/grid-box/office.png" class="gallery__img" alt="Image 1">
    </figure>

    <figure class="gallery__item gallery__item--5">
        <img src="https://fiberdroid.it/pages/home/grid-box/night-office.png" class="gallery__img" alt="Image 1">
    </figure>
    -->
</div>
