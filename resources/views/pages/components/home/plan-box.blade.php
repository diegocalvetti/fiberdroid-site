<div class="box plan-gradient">
    <div class="box plan-box p-6" style="margin-bottom: -25px; margin-right: -25px">
        <div class="plan-tag tag">
            <p class="plan-tag-text">{{ $type }}</p>
        </div>
        <p class="plan-title-text mt-4">{{ $title }}</p>

        <div class="my-6 py-5" style="border: solid 1px rgba(217, 217, 217, 0.3); border-left: none; border-right: none">
            @foreach($items as $key => $item)
                <p class="icon-text {{ $key != 0 ? "mt-4" : "" }}">
                    <span class="icon">
                        <img src="https://fiberdroid.it/pages/home/icons/check.svg" class="image" alt="Image 1">
                    </span>
                    <span class="plan-text">{{ $item }}</span>
                </p>
            @endforeach
        </div>

        <div>
            <div class="columns is-mobile is-vcentered" style="width: 100%; margin: 0">
                <div class="column is-6 pl-0">
                    <p class="price-text">
                        <span>{{ $price }}&euro;</span>
                        <span>/ mese</span>
                    </p>
                </div>
                <div class="column is-6 has-text-right">
                    <button class="button is-smaller is-fd-primary">
                    <span class="icon">
                       <img src="https://fiberdroid.it/pages/home/icons/cart.svg" class="image" alt="Image 1">
                    </span>
                        <span style="width: 100%; text-align: right">Add To Cart</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
