<div class="box-solution-container">
    <div class="box-solution">
        <div class="p-4">
            <p class="title-text">
                {!! $title !!}
            </p>
            <p class="content-text">
                {!! $description !!}
            </p>
        </div>
    </div>

    <div class="columns is-variable is-6">
        <div class="column is-6">
            <button class="button is-fd-primary" style="border-radius: 8px; margin-top: -60px; margin-left: 10px">
                <b>Learn more</b>
            </button>
        </div>
        <div class="column is-6">
            <img loading="lazy" class="image mx-auto" src="https://fiberdroid.it/pages/home/{{ $image }}" style="margin-top: -95px"/>
            <img loading="lazy" class="image mx-auto" src="https://fiberdroid.it/pages/home/{{ $icon }}" style="margin-top: {{ $iconTop ?? "-120px" }}"/>
        </div>
    </div>
</div>
