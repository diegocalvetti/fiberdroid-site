<a @if($link ?? false) href="{{ $link }}" @endif data-open-mobile-submenu data-target="{{ $dataTarget ?? "" }}">
    <div class="columns is-mobile px-6 mb-4" style="width: 100%">
        <div class="column is-10">
            <p class="menu-voice-touch">
                {{ $label }}
            </p>
        </div>
        @if($dropdown ?? false)
            <div class="column is-2">
                <div class="figure" style="width: 20px">
                    <img src="{{ asset('pages/icons/chevron-up.svg') }}">
                </div>
            </div>
        @endif
    </div>
</a>

<div style="display: none" data-menu-mobile-{{ $dataTarget ?? "" }}>
    @switch($dataTarget ?? false)
        @case('servizi')
        @foreach($services as $service)
            @component('pages.components.navbar-subvoice-mobile', [...$service->toArray(), 'prefix' => 'servizi'])@endcomponent
        @endforeach
        @break

        @case('prodotti')
        @foreach($service_products as $product)
            @component('pages.components.navbar-subvoice-mobile', [...$product->toArray(), 'prefix' => 'prodotti'])@endcomponent
        @endforeach
        @break

        @case('soluzioni')
        @foreach($solutions as $solution)
            @component('pages.components.navbar-subvoice-mobile',[...$solution->toArray(), 'prefix' => 'soluzioni'])@endcomponent
        @endforeach

        <!--
        <a href="{{ url('/soluzioni/fiberjet') }}" class="mb-5" style="padding: 0 3rem; display: block;">
            <p class="jetfiber-menu-promo-text mb-3">FIBERJET PROMO</p>
            <div class="figure" style="height: 161px; width: 247px">
                <img src="{{ url('images/menu/jetfiber-promo.webp') }}">
            </div>
            <p class="jetfiber-menu-desc mt-3">Fiberdroid ft. Jet Fighter Training </p>
        </a>
        -->

        @break

        @default
        <span></span>
    @endswitch
</div>
