<a href="/{{ $prefix ?? "" }}/{{ $url }}">
    <div class="columns is-mobile mb-4" style="width: 100%; padding: 0 3rem">
        <div class="column is-1">
            <div class="figure" style="width: 24px">
                <img src="{{ asset('pages/menu/'.$icon) }}">
            </div>
        </div>
        <div class="column is-11">
            <p class="menu-subvoice-touch pl-4">{{ $name }}</p>
        </div>
    </div>
</a>
