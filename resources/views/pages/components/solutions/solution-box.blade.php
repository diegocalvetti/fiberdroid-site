@php
$slug = \Illuminate\Support\Str::slug($title)
@endphp
<div
    class="card service-plan-box {{ $class ?? "" }}"
    id="{{ $slug }}"
    data-ref="{{ $slug }}"
    data-cart-name="{{ $title }}"
    data-price="{{ $price }}"
>
    <div class="card-image">
        <figure class="image is-4by3">
            <img src="{{ asset('images' . $image) }}" alt="Immagine soluzione: {{ strip_tags($title) }}">
        </figure>
    </div>
    <div class="card-content">
        <p class="service-plan-box-title mb-0">{!! $title !!}</p>

        <div class="mb-4 pb-5 pt-4 mt-4" style="border: solid 1px rgba(217, 217, 217, 0.3); border-left: none; border-right: none">
            @foreach($items as $key => $item)
                <p class="icon-text {{ $key != 0 ? "mt-4" : "" }}" style="width: 100%">
                    <span class="icon">
                        <img src="{{ url('pages/home/icons/check.svg') }}" class="image" alt="Image 1">
                    </span>
                    <span class="service-box-text">{{ $item }}</span>
                </p>
            @endforeach
        </div>

        <div>
            <div class="columns is-multiline is-mobile is-vcentered" style="width: 100%; margin: 0">
                <!--<div class="column is-12 pl-0 pb-0">
                    <p class="start-from-text">A partire da</p>
                </div>
                <div class="column is-12 has-text-left pl-0">
                    <p class="price-text-solution">
                        <span>{{ $price }}&euro;</span>
                        <span>/ mese</span>
                    </p>
                </div>-->
                <div class="column is-12 pl-0 has-text-left">
                    <a aria-label="Organizza una call con Fiberdroid"  target="_blank" rel="noopener" href="https://calendly.com/fiberdroid/richiesta-chiamata" style="width: 170px" class="button is-smaller is-dark">
                        <span class="icon">
                           <img src="{{ url('pages/home/icons/cart.svg') }}" class="image" alt="cart icon">
                        </span>
                        <span style="width: 100%; text-align: right">Organizza una call</span>
                    </a>
                </div>
            </div>
        </div>
    </div>


</div>
