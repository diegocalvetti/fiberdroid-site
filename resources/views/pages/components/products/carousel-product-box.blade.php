@php
    $imageData = json_decode($image['image']);
@endphp
<div class="box service-carousel-box-shadow is-product is-unselectable" style="background: {{ $bg }}; background-position: center; background-size: cover">
    <div class="box service-carousel-box">
        <div class="service-carousel-box-wrapper">
            <div class="service-carousel-box-grid-wrapper" style="flex-direction:column;">
                <!--
                    <div class="p-5">
                        <div class="tags">
                            <span class="tag">One</span>
                            <span class="tag">Two</span>
                            <span class="tag">Three</span>
                        </div>
                    </div>
                -->
                <div class="figure" style="display: flex; height: 140px; margin: auto auto 0 auto">
                    <img alt="Prodotto fiberdroid:  {{ strip_tags(html_entity_decode($name)) }}" class="mx-auto" src="{{ $imageData->path }}" style="height: {{ $imageData->height ?? "140px" }}">
                </div>
                <span class="service-product-carousel-text">
                    {!! $name !!}
                </span>
            </div>
        </div>
    </div>
</div>
