<a href="{{ $href }}" target="{{ $target ?? "_blank" }}" rel="noopener" class="href">
    {!! $slot !!}
</a>
