<div class="box assistance-carousel-box-shadow is-unselectable {{ $class ?? "mx-auto" }}" style="background: url('{{ $bg }}'); background-position: center; background-size: cover">
    <div class="box assistance-carousel-box">
        <div class="assistance-carousel-box-wrapper">
            <div class="assistance-carousel-box-grid-wrapper">
                    <span class="assistance-box-text">
                        {{ $slot }}
                    </span>
            </div>
        </div>
    </div>
</div>
