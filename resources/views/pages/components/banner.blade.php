<div class="column quiz-container is-11 mx-auto mt-6 px-6 pb-6 mt-0 pt-0">
    <div class="box quiz-gradient" style="background-size: cover; background-repeat: no-repeat; background-image: url('{{ url('/pages/quiz/gradients/internet.png') }}')">
        <div class="box quiz-box box-small" style="margin-bottom: -50px; margin-right: -50px">
            <div class="columns p-6">
                <div class="column is-6">

                    <img
                        alt="Logo Fiberdroid"
                        src="{{ url("pages/home/logo.svg") }}"
                        class="hover-image-illuminate is-clickable"
                    />

                    <p class="title-text pt-6" data-newsletter-success style="display: none">
                        Grazie per esserti iscritto<br>
                        alla newsletter di Fiberdroid
                    </p>

                    <p class="title-text pt-6" data-hide-on-success>
                        Iscriviti per ricevere <br> i nuovi articoli al volo
                    </p>
                    <p class="text-smaller my-5">
                        Qualche argomento da suggerire? Siamo a tutto orecchie!
                    </p>

                    <div class="control w-50 has-icons-right" data-hide-on-success>
                        <input class="cover-input input is-medium" data-newsletter-input type="email" placeholder="Inserisci il tuo indirizzo">
                        <span class="icon is-small is-right is-clickable" data-send-newsletter data-url="{{ url('') }}" style="pointer-events: initial">
                                <img class="image mx-auto" src="{{ url('/pages/home/input-icon.svg') }}" />
                            </span>
                    </div>

                    <p data-newsletter-error class="text has-text-danger mt-4" data-hide-on-success style="display: none">
                        Formato email non valido.
                    </p>
                    <p class="text mt-4" data-hide-on-success>
                        Iscrivendoti, sei soggetto ad accettare la nostra Informativa sulla privacy. <br>
                        Puoi annullare l'iscrizione in qualsiasi momento.
                    </p>
                </div>
                <div class="column is-6 p-6 quiz-image">
                </div>
            </div>
        </div>
    </div>
</div>
