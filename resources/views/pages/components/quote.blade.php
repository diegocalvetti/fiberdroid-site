<blockquote class="fd-blockquote pl-5 pt-4 pb-0 my-5">
    <x-text color="grey-dark" style="position: relative; z-index: 1">
        <p class="is-h4 mb-0"><i>{{ $slot }}</i></p>
    </x-text>
</blockquote>
