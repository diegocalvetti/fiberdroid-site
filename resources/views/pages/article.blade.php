@extends('layouts._layout_v2')

@section('title', $article->meta_title ?: $article->title)
@section('description', $article->meta_description ?? "")

@section('content')
    <div class="columns is-multiline article-container" style="padding-top: 59px">
        <div class="column desktop-8 touch-12">
            @if ($article->type_id == 24)
            <p class="text">{{ \Illuminate\Support\Carbon::parse($article->created_at)->rawFormat('M j, Y') }}</p>
            @endif
            <h1 class="blog-featured-text-title my-3">
                {{ $article->title }}
            </h1>
            <div class="text mt-4">
                {!! strpos($article->html['intro'], "<p") ? $article->html['intro'] : $article->meta_description !!}
            </div>

            @component('pages.components.icons-list', [
                'icons' => ['twitter', 'linkedin-full', 'chain'],
                'links' => [
                    'twitter' => "https://twitter.com/share?ref_src=twsrc%5Etfw",
                    'linkedin-full' => "https://www.linkedin.com/sharing/share-offsite/?url=" . url()->current()
                ],
            ])@endcomponent
            <div class="article-image mt-6" style="@if($article->url == 'guida-completa-al-filtraggio-dei-siti-web-per-la-rete-aziendale') background-size: contain; @else background-size: cover; @endif background-repeat: no-repeat; background-image: url('{{ json_decode($article->img->image)->path ?? '' }}'); background-position: center; border-radius: 12px"></div>

            <div class="content article-content">
                {!! $article->html['html'] !!}

                <p></p>

                @component('pages.components.icons-list', [
                    'label' => 'Condividi questo post',
                    'icons' => ['twitter', 'linkedin-full', 'chain'],
                    'links' => [
                        'twitter' => "https://twitter.com/share?ref_src=twsrc%5Etfw",
                        'linkedin-full' => "https://www.linkedin.com/sharing/share-offsite/?url=" . url()->current()
                    ],
                    'color' => 'white'
                ])@endcomponent

            </div>
        </div>
        <div class="column desktop-4 touch-12 pl-6 hidden-touch" style="overflow: hidden">
            @component('pages.components.blog.article-index', ['index' => $article->html['index']])@endcomponent
        </div>

        <div class="column is-12 mt-4 has-background-black" style="position: relative; z-index: 1">
            <h2 class="title-text has-text-centered">
                Altri articoli  <br>creati dal nostro team
            </h2>
        </div>

        @foreach($articles as $key => $article)
            <div class="column has-background-black desktop-4 tablet-6 mobile-12 pt-6 {{ $key == 2 ? "hidden-touch" : "" }}" style="position: relative; z-index: 1">
                <a href="{{ $article->url }}">
                    <div class="blog-container {{ $key % 3 == 1 ? 'mx-auto' : '' }} {{ $key % 3 == 2 ? 'ml-auto' : '' }}">
                        <div class="blog-image" style="background-size: cover; background-image: url('{{ json_decode($article->img->image)->path ?? '' }}'); background-position: center; border-radius: 12px"></div>
                        <div>
                            <p class="blog-text-title mt-3">{{ $article->title }}</p>
                            <p class="text mt-3">
                                {{ $article->meta_description }}
                            </p>
                            <p class="blog-continue-text mt-3 is-clickable">
                                Continua >>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>

    <div class="columns hidden-touch pt-6 has-background-black" style="position: relative; z-index: 1">
        <div class="column is-12">
            @component('pages.components.banner')@endcomponent
        </div>
    </div>

    <div style="position: absolute; width: 100%; height: 500px; background: black; bottom: 0"></div>
@endsection

