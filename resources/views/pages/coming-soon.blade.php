@extends('layouts._layout_v2')

@section('content')
    <img class="landing-child" alt="" src="https://fiberdroid.it/pages/home/rectangle-39855.svg" />

    <!-- HEADER INTRO -->
    <div class="columns is-mobile is-multiline has-background-black">
        <div class="column is-12">
            <div>
                <p class="landing-text-title">Coming Soon</p>
                <p class="landing-text-title">as soon as possible</p>
            </div>
        </div>
    </div>
    <div class="columns is-multiline has-background-black mt-4 mb-6 pb-6 px-6-mobile">
        <div class="column is-6-desktop is-12-touch px-0-mobile has-text-right-desktop has-text-centered-touch">
            <button class="button is-fd-primary full-mobile" style="border-radius: 8px" onclick="window.history.back()">
                <b>GO BACK</b>
            </button>
        </div>
        <div class="column is-6-desktop is-12-touch px-0-mobile has-text-left-desktop has-text-centered-touch">
            <a class="button is-dark full-mobile" style="border-radius: 8px" onclick="window.location.href = '/'">
                <b class="has-text-white">GO TO HOME</b>
            </a>
        </div>
    </div>

@endsection

