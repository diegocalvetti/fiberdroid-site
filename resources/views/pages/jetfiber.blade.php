@extends('layouts._layout_v2')

@section('content')
    <div class="columns">
        <div class="column is-12">
            <div class="video-container">
                    <video autoplay muted loop>
                        <source src="{{ asset('video/jetfightertraining.mp4') }}"  type="video/mp4">
                    </video>
            </div>
        </div>
    </div>
    <div class="columns is-multiline">
        <div class="column is-12 my-6 py-6">
            <div class="hidden-tablet-only hidden-desktop" style="display: flex; gap: 10px 10px; justify-content: center">
                <img alt="Lettera F" src="{{ asset('images/jetfiber/f.svg') }}">
                <img alt="Lettera I" src="{{ asset('images/jetfiber/i.svg') }}">
                <img alt="Lettera B" src="{{ asset('images/jetfiber/b.svg') }}">
                <img alt="Lettera E" src="{{ asset('images/jetfiber/e.svg') }}">
                <img alt="Lettera R" src="{{ asset('images/jetfiber/r.svg') }}">
            </div>
            <div class="hidden-tablet-only hidden-desktop" style="display: flex; gap: 10px 10px; justify-content: center">
                <img alt="Lettera J" src="{{ asset('images/jetfiber/J.svg') }}">
                <img alt="Lettera E" src="{{ asset('images/jetfiber/e2.svg') }}">
                <img alt="Lettera T" src="{{ asset('images/jetfiber/T.svg') }}">
            </div>

            <div class="hidden-mobile" style="display: flex; gap: 10px 10px; justify-content: center">
                <img alt="Lettera F" src="{{ asset('images/jetfiber/f.svg') }}">
                <img alt="Lettera I" src="{{ asset('images/jetfiber/i.svg') }}">
                <img alt="Lettera B" src="{{ asset('images/jetfiber/b.svg') }}">
                <img alt="Lettera E" src="{{ asset('images/jetfiber/e.svg') }}">
                <img alt="Lettera R" src="{{ asset('images/jetfiber/r.svg') }}">

                <img alt="Lettera J" class="ml-6" src="{{ asset('images/jetfiber/J.svg') }}">
                <img alt="Lettera E" src="{{ asset('images/jetfiber/e2.svg') }}">
                <img alt="Lettera T" src="{{ asset('images/jetfiber/T.svg') }}">
            </div>
        </div>
        <div class="column is-12">
            <h1 class="has-text-centered">
                <span class="jetfiber-title">
                    FIBERDROID <br>
                </span>
                <br>
                <span class="jetfiber-title is-size-3 mt-5">X</span>
                <br>
                <br>
                <span class="jetfiber-title">
                    JET FIGHTER <br>
                    TRAINING
                </span>
            </h1>
        </div>
        <div class="column desktop-8 mt-6 mx-auto mobile-11">
            <p class="jetfiber-text">
                Scalare in pochi secondi le vette alpine più alte non è cosa di tutti i giorni.
                Prendi il volo su un vero aereo da caccia L-39 Albatross.
                Acquistando uno dei piani in promozione, potrai goderti le sensazioni del volo sopra le montagne della Valle d'Aosta,
                in una delle migliori location per il volo acrobatico, proverai l'emozione di un vero volo in jet.
            </p>
        </div>
        <div class="column desktop-12 mx-auto mt-6" style="background: #F6A906;">
            <div class="columns is-multiline">
                <div class="column desktop-8 mobile-11 mx-3-mobile mx-auto">
                    <h2 class="jetfiber-subtitle pt-6">
                        Scopri l'indimenticabile bellezza,<br>
                        della vera velocità
                    </h2>
                </div>
                <div class="column is-8 py-6 mx-auto">
                    <div class="columns is-variable is-7">
                        <div class="column desktop-6 mobile-11 mx-auto">
                            <p class="jetfiber-small-text">
                                Nel nostro team, chi non ha il brevetto di pilota, ha di sicuro la mania. E non solo delle simulazioni al computer. Nel nostro ultimo evento aziendale abbiamo potuto provare l’ebbrezza del volo in aliante, grazie alla collaborazione di André Claude Benin, Founder di Fiberdroid, e del comandante Massimo Milano, pilota con esperienza ventennale, con l'Aeroclub Valle d'Aosta.
                                Parlare delle proprie passioni è una cosa bella. Condividerle è ancora meglio.
                                <br><br>
                                È per questo che Fiberdroid ha pensato anche ai suoi clienti. Grazie alla collaborazione con l’A.S.D. Jet Fighter Training, oggi siamo in grado di offrire anche a voi l’occasione di volare su un vero aereo da caccia.
                                <br><br>
                                Acquistando un pacchetto in promozione JetFiber avrete la possibilità di effettuare un volo su un vero L-39 Albatross, l’aereo di addestramento avanzato su cui si sono formati per trent’anni i piloti di mezzo mondo. Non è un concorso: ci crediamo davvero.
                            </p>
                        </div>
                        <div class="column desktop-6 mobile-11 mx-auto">
                            <p class="jetfiber-small-text">
                                E vogliamo che ognuno di voi possa provare questa esperienza acquistando le soluzioni che aderiscono a questa promozione.
                                Nella cornice delle montagne della Valle d’Aosta, in una delle migliori location per il volo acrobatico, potrete vivere davvero l’emozione di un volo a reazione. Salire sopra le cime alpine più alte in pochi secondi non è una cosa da tutti i giorni.
                                <br><br>
                                Per noi la velocità è importante tanto quanto l’affidabilità e chi sceglie le nostre connessioni lo sa. Con JetFiber Vi regaliamo di più, un’esperienza indimenticabile.
                                <br><br>
                                Navigate con Fiberdroid su internet: vi faremo volare oltre la Vostra immaginazione.
                                <br><br>
                                Scegliendo tra una delle tre soluzioni avrete diritto all'esperienza sul caccia!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="column is-12">
            <div class="columns is-multiline">
                <div class="column desktop-8 mobile-11 mx-auto mx-auto">
                    <h2 class="jetfiber-subtitle my-6 py-5">
                        Cosa aspettarsi
                    </h2>
                </div>
                <div class="column desktop-8 mobile-11 mx-auto">
                    <div class="columns is-variable is-7 is-vcentered">
                        <div class="column is-6">
                            <p class="jetfiber-small-text">
                                Scegliendo uno dei nostri pacchetti di connettività JetFiber, potrete vivere l’esperienza di volare tra le più alte montagne della Valle d’Aosta.
                                <br><br>
                                L’esperienza di volo è organizzata in due fasi: una prima fase di briefing e l’esperienza a bordo.
                                <br><br>
                                Nella fase di briefing, vengono esposte le basi del volo, con un’introduzione sulle figure acrobatiche e sulle peculiarità degli effetti dell’accelerazione sul corpo umano. Ai partecipanti vengono illustrati i principi di sicurezza e le manovre opportune.
                                Il volo a bordo del jet L-39 Albatross ha una durata approssimativa di 30 minuti.
                                <br><br>
                                Il comandante Milano avrà il compito di valutare le condizioni meteo ed eseguire le evoluzioni e seguire le rotte opportune, in accordo alle procedure di sicurezza. L’esperienza del pilota e la validità del mezzo permettono di offrire ai partecipanti un’esperienza unica, adrenalinica e indimenticabile!
                            </p>
                        </div>
                        <div class="column is-6" style="display: flex">
                            <img alt="Aliante di Fiberdroid al tramonto" src="{{ asset('images/jetfiber/aliante.webp') }}" style="margin-left: auto; height: 650px">
                        </div>
                    </div>
                </div>
                <div class="column desktop-8 mobile-11 mx-auto" style="display: flex; flex-direction: column">
                    <h2 class="jetfiber-subtitle my-6 py-5">
                        Come partecipare
                    </h2>

                    <img alt="Numero 1" class="mx-auto" src="{{ asset('images/jetfiber/one.webp') }}">

                    <h3 class="jetfiber-promo-text mt-5 mb-6 pb-6">
                        Scegli una soluzione che <br>rispetta i termini della promozione:
                    </h3>

                    <div class="jetfiber-promo-box">
                        <div class="box jetfiber-box">
                            <ul class="list">
                                <li>5 Servizi connettività radio o via cavo asimmetrici</li>
                                <li>Numerazione telefonica GNR</li>
                                <li>Centralino VoIp cloud</li>
                            </ul>
                        </div>
                        <p class="jetfiber-or">Oppure</p>
                        <div class="box jetfiber-box mt-5-mobile">
                            <ul class="list">
                                <li>Connettività simmetrica via cavo o radio</li>
                            </ul>
                        </div>
                    </div>

                    <img alt="Numero 2" class="mx-auto mt-6" src="{{ asset('images/jetfiber/two.webp') }}">

                    <h3 class="jetfiber-promo-text mt-5 mb-6 pb-6">
                        Organizza un meeting
                        <br>
                        con un nostro rappresentante per
                        <br>
                        attivare la promozione
                    </h3>

                    <img alt="Numero tre" class="mx-auto mt-6" src="{{ asset('images/jetfiber/three.webp') }}">

                    <h3 class="jetfiber-promo-text mt-5 mb-6 pb-6">
                        Tutto qui! <br>
                        Benvenuto a bordo
                    </h3>
                </div>
                <div class="column is-8 mx-auto hidden-mobile" data-ticket-column style="position: relative; height: 1100px; width: 700px">

                    <p data-ticket-meeting class="ticket-navigate-text has-text-left" style="position: absolute; bottom: 270px; left: -235px">
                        Naviga<br>
                        per organizzare<br>
                        un meeting
                    </p>
                    <img alt="" data-ticket-meeting src="{{ asset('images/jetfiber/arrow.svg') }}" style="z-index: 9999; position: absolute; bottom: 170px; left: -120px"/>

                    <div class="ticket mx-auto" style="width: 500px; display: flex; z-index: 999; flex-direction: column; position: relative; overflow-y: hidden">
                        <img alt="Distributore di Ticket" src="{{ asset('images/jetfiber/ticket-dispenser.svg') }}" class="mx-auto" />

                        <div data-ticket-anchor style="position: relative; top: -984px">

                            <img alt="" src="{{ asset('images/jetfiber/ticket-black.webp') }}" class="black-ticket mx-auto" style="margin-top: -12px; position: relative; visibility: hidden"/>
                            <img alt="Ticket" src="{{ asset('images/jetfiber/ticket.webp') }}" class="mx-auto" style="margin-top: -21px"/>

                            <img alt="" src="{{ asset('images/jetfiber/mitchell.webp') }}" style="position: absolute; left: 0; top: 120px; width: 497px"/>

                            <p class="ticket-mitchell-title has-text-white" style="position: absolute; top: 440px; left: 60px">
                                Pete Mitchell
                            </p>

                            <p class="mitchell-text" style="position: absolute; top: 540px; left: 60px; width: 400px">
                                Il capitano Pete "Maverick" Mitchell esemplifica le caratteristiche per eccellenza di un pilota professionista.
                                Famoso per le sue eccezionali capacità di volo, Maverick quando si tratta di aerei è un vero maestro.
                                Il suo costante impegno verso l'eccellenza è evidente nel suo rigoroso regime di addestramento,
                                che gli garantisce di avere competenze aeronautiche sempre all'avanguardia.
                                La sua dedizione ai protocolli di sicurezza è incrollabile, rendendolo un modello per gli aspiranti piloti.
                            </p>

                            <img alt="Logo di JetFiber" src="{{ asset('images/jetfiber/logos.webp') }}" style="position: absolute; bottom: 1000px; left: 30px"/>
                            <img alt="" src="{{ asset('images/jetfiber/ticket-title.webp') }}" style="position: absolute; bottom: 970px; left: 300px"/>
                            <img alt="QR per organizzare una call con jetfiber" src="{{ asset('images/jetfiber/qr.webp') }}" style="position: absolute; bottom: 100px; left: 0"/>

                            <div style="position: absolute; bottom: 750px; left: 0; width: 100%; text-align: center">
                                <p class="ticket-title" >
                                    Mountains views of Valle D’Aosta
                                </p>
                                <div style="margin-top: 10px; padding: 30px 0; border: solid rgba(0, 0, 0, 0.1) 1px; display: flex; gap: 30px; justify-content: center">
                                    <p class="ticket-text">
                                        Premium <br>
                                        <span>class</span>
                                    </p>
                                    <p class="ticket-text">
                                        Fiberdroid <br>
                                        <span>flight</span>
                                    </p>
                                    <p class="ticket-text">
                                        1 <br>
                                        <span>terminal</span>
                                    </p>
                                </div>
                            </div>

                            <p class="ticket-mitchell-title" style="position: absolute; bottom: 650px; left: 60px">
                                Pete Mitchell
                            </p>

                            <img alt="Icona Jet" src="{{ asset('images/jetfiber/jet.webp') }}" style="position: absolute; bottom: 600px; right: 40px"/>

                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="column is-12 mt-6" style="background: #121212;border: 2px solid #1A1A1A;">
            <div class="columns is-multiline">
                <div class="jetfiber-banner column desktop-6 mobile-11 mx-auto-mobile py-6-desktop">
                    <div style="width: 100%; display: inline; margin: auto">
                        <div style="display: flex; flex-direction: column">
                            <img alt="Logo di Jetfiber" src="{{ asset('images/jetfiber/logos-white.webp') }}" style="width: 50%"/>
                            <img alt="Etichetta Promo Offer" class="mt-6" src="{{ asset('images/jetfiber/promo-tag.webp') }}" style="height: 30px; margin-right: auto"/>
                        </div>
                        <h2 class="jetfiber-subtitle has-text-left mt-4">
                            Scopri la bellezza<br>
                            della vera velocità
                        </h2>
                        <p class="jetfiber-small-text mt-5">
                            Cogli l'opportunità di volare su un vero Jet sopra<br class="hidden-mobile">
                            le splendide montagne della Valle d'Aosta. <br>
                            Bastano solo 3 passaggi per partecipare.
                        </p>

                        <button aria-label="Organizza una call con Fiberdroid" rel="noopener noreferrer" target="_blank" href="https://calendly.com/fiberdroid/richiesta-chiamata" class="button jetfiber-meeting-button is-fd-primary mt-6" >
                            Pianifica un meeting
                        </button>
                    </div>
                </div>
                <div class="column is-6 jetfiber-banner-image" style="background-size: cover; background-repeat: no-repeat; background-image: url('{{ asset('images/jetfiber/aosta-valley-full.webp') }}')"></div>
            </div>
        </div>

    </div>
@endsection
