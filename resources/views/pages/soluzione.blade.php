@extends('layouts._layout_v2')

@section('title', ($solution->meta_title ?? $solution->name) . " | Fiberdroid")
@section('description', $solution->meta_description)

@section('content')
    <div class="columns is-multiline pb-6-desktop pt-6-mobile">
        <div class="column is-8 mx-auto px-6-mobile" style="padding-top: 59px">
            <div class="columns">
                <div class="column is-6">
                    <div class="my-6"></div>
                    <h1 class="service-text-title mt-6-tablet mt-6-desktop mt-2-mobile" style="position: relative; z-index: 999;">
                        {{ $solution->title ?? $solution->name }}
                    </h1>
                    <p class="service-text-description mt-6-tablet mt-6-desktop mt-3-mobile" style="position: relative; z-index: 999;">
                        {!! $solution->description !!}
                    </p>

                    <div class="my-6"></div>
                </div>
                <div class="column is-6 pt-6 hidden-mobile" style="display: flex; vertical-align: middle">
                    <div style="position: relative; z-index:1002; width: 370px; margin-left: auto;border-radius: 20px; background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url('{{ asset('/images/' . $solution->bg) }}')">

                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($solution->model && false)
        <div style="width: 100%">
            <web-component-fiberdroid-model id="model" model="{{ $solution->model }}"></web-component-fiberdroid-model>
        </div>
    @endif

    @if(sizeof($solution->plans) > 0)
        <div class="columns mt-5 pb-4 pt-5" style="position: relative; background: #0a0a0a">
            <div class="column is-12 mb-6">
                <h2 class="service-plan-title">{{ sizeof($solution->plans) != 1 ? 'Scegli tra i pacchetti dedicati' : 'Pacchetti dedicati' }}</h2>
            </div>
        </div>
        <div class="columns is-multiline" style="position: relative; background: #0a0a0a">
           @foreach($solution->plans as $key => $plan)
                <div class="column is-6 {{ sizeof($solution->plans) == 1 ? "mx-auto" : "" }}">

                    @if(!isset($plan['isService']))
                        @component('pages.components.solutions.solution-box', $plan)
                        @endcomponent
                    @else
                        @component('pages.components.service-box', [
                                        'title' => $plan['title'],
                                        'slug' => \Illuminate\Support\Str::slug($plan['title']),
                                        'type' => "Connettività",
                                        'price' => $plan['price'],
                                        'activation_price' => 0,
                                        'bg' => 'url("/pages/gradients/antenna.png")',
                                        'mark' => null,
                                        'classes' => "mx-auto",
                                        'items' => $plan['items']
                                    ])@endcomponent
                    @endif
                </div>
           @endforeach
        </div>
    @endif


    <!-- @DEPRECATED until new order -->

    @if($solution->views['termini'] ?? false)
        <div class="columns mt-6 px-4-mobile">
            <div class="column is-12">
                <div class="box service-termini-box py-6 mx-auto">
                    {!! $solution->views['termini'] !!}
                </div>
            </div>
        </div>
    @endif

    @if($solution->slider && false)
        <div class="columns is-multiline mt-6 py-6 pt-6-mobile">
            <div class="column is-8 mx-auto px-6-mobile">
                <div class="columns is-vcentered">
                    <div class="column is-6 pr-6 is-flex is-flex-direction-column">
                        @foreach($solution->slider['images'] as $key => $image)
                            <img
                                class="mr-auto ml-4 mb-6"
                                src="{{ asset('/pages' . $image) }}"
                                @if($key >= $solution->slider['total_images']) style="display: none" @endif
                            >
                        @endforeach
                    </div>
                    <div class="column is-6">
                        <p class="service-text-title">{{ $solution->slider['title'] }}</p>
                        <p class="service-text mt-4">
                            {!! $solution->slider['text'] !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="columns">
        <div class="column is-12">
            <web-component-fiberdroid-quiz id="quiz-component"></web-component-fiberdroid-quiz>
        </div>
    </div>

    @if($solution->hasMainView)
        <div class="columns">
            <div class="column is-11 px-6 mx-auto has-text-white service-content">
                {!! $solution->views['main'] !!}
            </div>
        </div>
    @endif
@endsection
