<p>
    Costo di attivazione del servizio Cloud WiFi: 89.90€<br>
    Costo di disattivazione del servizio: 99.90€
    <br><br>
    Tutti gli importi sono espressi in euro iva esclusa.<br>
    Ogni servizio è attivabile previa <a target="_blank" href="{{ url('verifica-copertura') }}">verifica tecnica della copertura</a>
</p>
