@component('pages.components.accordion', ['title' => 'Cloud WI-FI: semplice e economico'])
    <p>
        La nostra soluzione Cloud Wi-Fi ti consente di avere la gestione in un'unica interfaccia di tutte le tue antenne Wi-Fi anche su più sedi distribuite sul territorio.
        <br><br>
        Semplifica la gestione senza fare investimenti hardware!
        <br><br>
        Le nostre antenne Wi-Fi sono collegate con i nostri data center e possono essere
    </p>
    <ul class="list">
        <li>noleggiate - con incluso il servizio Controller Cloud</li>
        <li>acquistate - in unica soluzione e pagando il nostro Cloud come servizio in base a quante antenne connetti.</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Vantaggi'])
    <p>
        Come abbiamo visto con la nostra soluzione Cloud Wi-Fi l’amministratore di sistema potrà gestire su un'unica interfaccia tutte le antenne Wi-Fi distribuite su piu’ sedi.
        <br><br>
        In questa modalita’ i vantaggi sono molteplici:
    </p>
    <ul class="list">
        <li>la gestione si semplifica enormemente</li>
        <li>l’analisi e la risoluzione di eventuali problemi viene ridotta</li>
        <li>i costi di avviamento e manutenzione per la nostra soluzione Cloud sono inferiori rispetto a qualsiasi altro prodotto Hardware con installazione fisica presso la sede.</li>
    </ul>
    <p>
        Vediamo più nel dettaglio cosa cambia rispetto ad una soluzione stand-alone.
        <br><br>
        Se oggi hai parecchie antenne Wi-Fi stand-alone, gestite localmente e senza centralizzazione ogni volta che un utente segnala problemi dovrai
    </p>
    <ul class="list">
        <li>ricercare su quale antenna Wi-Fi e’ stato connesso</li>
        <li>comprendere se il problema è dovuto ad una delle antenne oppure al dispositivo dell’utente.</li>
    </ul>
    <p>
        Inoltre con la soluzione stand-alone se stai effettuando una chiamata video o audio con whatsapp, skype o altro sistema di messaggistica questa tipologia di rete Wi-Fi puo’ creare delle interruzioni nel caso in cui  l’utente si muova per gli uffici, in quanto non essendoci un controller il sistema di hand-over (passaggio rapido dei client veloce tra antenne Wi-Fi) non consente la riconnessione rapida del dispositivo tra le antenne Wi-Fi.
        <br><br>
        Con un sistema di gestione centralizzata delle antenne Wi-Fi e’ possibile invece avere tutte le antenne Wi-Fi sincronizzate e integrate all’interno di un'unica piattaforma grazie alla quale è possibile effettuare
    </p>
    <ul class="list">
        <li>il monitoraggio</li>
        <li>le configurazioni</li>
        <li>gli aggiornamenti</li>
        <li>l'analisi rapida di eventuali problemi sulle antenne.</li>
    </ul>
    <p>
        Inoltre la soluzione con controller cloud Wi-Fi consente l’hand over così da non avere neanche piu’ un interruzione in movimento dei tuoi utenti tra le antenne Wi-Fi.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Perché scegliere Fiberdroid'])
    <p>
        Concediti la possibilità di avere un unico partner per tutti i servizi informatici e di telecomunicazione.
        <br><br>
        Scegliendo Fiberdroid, non stai acquistando un semplice servizio ma stai migliorando la tua azienda e riducendo i possibili problemi tecnici del tuo impianto Wi-Fi, in quanto tutte le nostre soluzioni sono studiate per essere integrate.
        <br><br>
        Infatti se oggi acquisti la nostra soluzione Wi-Fi con connettività internet e centralino VoIP ti stai assicurando che tutta la piattaforma sia integrata e funzioni senza problemi.
        <br><br>
        Questo è uno dei nostri punti di forza: fornire soluzioni integrate e che consentano al cliente di non impazzire con i problemi e i diversi fornitori.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'WI-FI'])
    <p>
        Il Wi-Fi è una delle più diffuse tecnologie Wireless, cioè comunicazioni senza fili.
        <br><br>
        Alla pagina Wireless puoi trovare una descrizione più ampia della tecnologia Wireless e dell'evoluzione dei vari protocolli che ne fanno parte, tra cui quindi anche le informazioni più approfondite relative al protocollo Wi-Fi.
        <br><br>
        La tecnologia Wi-Fi, più potente del Bluetooth, è usata in genere sulle brevi distanze, sia ad uso privato che in strutture pubbliche, ma consente, con ripetitori che ne amplino il segnale, un utilizzo in esterno fino a 25 km, permettendone così l'uso nelle MAN (Metropolitan Area Network )
    </p>
@endcomponent
