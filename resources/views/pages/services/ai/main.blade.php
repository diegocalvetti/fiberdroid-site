@component('pages.components.accordion', ['title' => 'Fiber AI'])
    <p>
        Un sistema modulare, che consente alle aziende di attivare solamente quello che è utile per la loro attività. Le soluzioni possono essere applicate a tutti i diversi livelli aziendali, dal customer care, alla customer experience, al management, alla produzione, alla lead generation, ecc.
        <br><br>
        Siamo in grado quindi di individuare l’applicazione migliore per il tuo settore e mercato, per il tuo pubblico e la tua clientela, per il successo e la crescita della tua azienda.
    </p>
@endcomponent


@component('pages.components.accordion', ['title' => 'Fiberbot'])
    <p>
        FIBERBOT è la piattaforma che ti permette di creare l’Assistente Virtuale perfetto per le PMI, distribuirlo su vari canali e misurare le sue performance in real time. Il Virtual Assistant può parlare massimo in una lingua e include:
    </p>
    <ul class="list">
        <li>Riconoscimento e sintesi vocale</li>
        <li>Intent Recognition</li>
        <li>Conversational Form ed Information Extraction Engine</li>
        <li>Browser Automation</li>
        <li>Live Chat, Area operatori Live Chat</li>
        <li>Storico delle conversazioni effettuate</li>
        <li>Area per il reinforcement learning</li>
        <li>Area per personalizzazione del lessico dell’assistente</li>
        <li>Votazione delle risposte da parte degli utenti, Dashboard di Analytics</li>
    </ul>

    <h3 class="accordion-subtitle">
        OPZIONE DHI
    </h3>
    <p>
        L’opzione DHI (Digital Human Interface) ti permette completare l’esperienza dei
        tuoi clienti tramite interazione con un Avatar con cui può conversare.
        Caricamento di una immagine di background, scelta del proprio Avatar da un catalogo
        di circa 20 modelli, Modalità browser automation.
    </p>

    <h3 class="accordion-subtitle">
        INTEGRAZIONE API
    </h3>
    <p>
        API disponibili per integrazione con altri SW (XML, Json) con la possibilità di integrazione
        in software/piattaforme/servizi esterni.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Fiber AI Plus'])
    <p>
        FiberAi+ è la piattaforma che ti permette di creare l’Assistente Virtuale perfetto per la grandi aziende o industrie, distribuirlo su vari canali e misurare le sue performance in real time. Virtual Assistant parlante in più lingue (da acquistare a parte) Include:
    </p>
    <ul class="list">
        <li>Riconoscimento e sintesi vocale</li>
        <li>Intent Recognition</li>
        <li>Conversational Form ed Information Extraction Engine</li>
        <li>Browser Automation</li>
        <li>Live Chat</li>
        <li>Piattaforma di booking</li>
        <li>Area operatori Live Chat</li>
        <li>Storico delle conversazioni effettuate</li>
        <li>Area per il reinforcement learning</li>
        <li>Area per personalizzazione del lessico dell’assistente, Votazione delle risposte da parte degli utenti</li>
        <li>Dashboard di Analytics</li>
    </ul>
@endcomponent
