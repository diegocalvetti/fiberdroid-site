<p>
    Tutti gli importi sono espressi in euro iva esclusa.<br>
    Ogni servizio è attivabile previa <a target="_blank" href='{{ url('/verifica-copertura') }}'>verifica tecnica della copertura</a>.
</p>
