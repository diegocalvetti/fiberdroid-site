<p class="service-faq-title is-uppercase has-text-centered mb-4">Faq</p>

@component('pages.components.accordion', ['title' => 'La tecnologia indispensabile per la ristorazione'])
    <p>
        POS, WIFI, CENTRALINO, SITO E SOCIAL sono le componenti essenziali del successo nella ristorazione.
        Hai scelto di essere il miglior ristoratore, ora scegli di pensare al tuo business senza preoccuparti di eventuali cadute di tensione o problemi di linea.
        <br><br>
        Con Fiberdroid non finirai mai nell’incubo dei call center, ripetendo ad ogni operatore il problema e rimbalzando da un operatore all’altro. Fiberdroid ti garantisce un’assistenza dedicata, chi ti seguirà sarà con te fino alla risoluzione del problema.
    </p>

    <p class="is-uppercase mt-6 mb-4"><strong>IL POS PER LA RISTORAZIONE</strong></p>

    <p>
        Sicuro, efficiente e veloce. Il POS ha semplificato la vita.
        I clienti sono più tranquilli senza lo stress dell’avere abbastanza contante e la transazione è sicura e immediata. Con il POS portatile
        se hai una buona connessione puoi fare pagare al tavolo anche nelle tue aree esterne.
        Comodità e assenza di code alla cassa, di sicuro un bel vantaggio.
        <br><br>
        La cosa fondamentale è che la linea funzioni e in caso di problematiche tu abbia un’assistenza immediata. Per questo Fiberdroid può fare la differenza.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Perché il wi-fi è una priorità nel marketing della ristorazione'])
    <p>
        Ci sono mille motivi per avere un buon Wi-Fi e uno solo è già sufficiente per scegliere Fiberdroid:
        il servizio di qualità che ti segue con assistenza dedicata!
        <br><br>
        Ecco una lista di motivi per cui il Wi-Fi in bar e ristoranti aumenta la soddisfazione e fidelizza il cliente.
    </p>

    <p class="is-uppercase mt-6 mb-4"><strong>IL POS PER LA RISTORAZIONE</strong></p>

    <ul class="list">
        <li class="pb-4">
            <strong class="has-text-white">Aiuta a raccogliere informazioni utili.</strong>
            Puoi dare accesso al WI-FI gratuito rispondendo a una domanda: (il tuo piatto preferito, perché hai scelto noi, cosa ti piace del nostro ristorante…?) oppure con un login che raccolga dati e ti faccia capire se gli utenti tornano. Oltre il 60% degli utenti è disposto a dare la propria email per ricevere la connessione al WIFI gratuito.
        </li>
        <li class="pb-4">
            <strong class="has-text-white">Il cliente che si ferma di più consuma di più.</strong>
            In media oltre il 50%
        </li>
        <li class="pb-4">
            <strong class="has-text-white">Il Wi-Fi allevia l'attesa.</strong>
            Offri la connessione già all'ingresso del tuo ristorante, mentre i clienti attendono
            che si liberi il tavolo, magari facendo aprire la connessione sul tuo menu del giorno!
        </li>
        <li class="pb-4">
            <strong class="has-text-white">Il 30% dei clienti controlla le tue offerte e recensioni.</strong>
            Quando è collegata al tuo WIFI. un buon canale per le tue promozioni!
        </li>
        <li class="pb-4">
            <strong class="has-text-white">Per ottenere recensioni immediate.</strong>
            Davanti a un piatto ben preparato il 65% dei clienti decideranno di condividerne l'immagine sui social taggando
            i loro amici. Se non c'è un'ottima connessione l'occasione di farsi conoscere si perderà.
        </li>
        <li class="pb-4">
            <strong class="has-text-white">Puoi sfruttare i social.</strong>
            Usa le stories di Instagram per fare domande sul tuo servizio o come
            il cliente vorrebbe essere accolto nel ristorante, oppure offri il caffè a
            chi condivide i tuoi piatti sui social.
        </li>
        <li class="pb-4">
            <strong class="has-text-white">Puoi fidelizzare i clienti.</strong>
            Chi si registra ti darà informazioni sui suoi piatti preferiti,
            su cosa ama di più del tuo locale o su eventuali limiti nell'alimentazione,
            come celiaci, allergici, diabetici e tu puoi inviargli e-mail con offerte mirate
            sui loro gusti o con sconti per le serate meno affollate.
        </li>
        <li class="pb-4">
            <strong class="has-text-white">Il cliente ama essere connesso.</strong>
            L'84% degli utenti usa il WIFI in bar e negozi per leggere news e email e il 54% per restare in contatto con gli amici.
        </li>
        <li class="pb-4">
            <strong class="has-text-white">Perchè puoi gestire ordini dai tavoli.</strong>
            Sia all'interno che all'esterno del locale velocizzando il servizio nelle ore di punta.
        </li>
        <li class="pb-4">
            <strong class="has-text-white">E c'è chi lavora anche nel tempo libero.</strong>
            La possibilità di rispondere a una email o controllare dei dati tra un piatto e l'altro diventa un vantaggio per il tuo cliente business.
        </li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Come ottenere il meglio dal tuo wi-fi nel ristorante'])
    <p>
        Per ottenere un’ottima copertura vanno individuate e rimosse le eventuali interferenze provocate da altri strumenti. (microonde, sistemi di sorveglianza ecc)
        L'ideale è che il WiFi sia installato al centro della sala o su una parete che occupa una grande superficie del locale.
        Il protocollo di trasmissione deve essere criptato per la sicurezza tua e dei tuoi clienti.
        <br><br>
        Con Fiberdroid non dovrai preoccuparti di nulla, possiamo aiutarti a valutare ogni situazione e crearti le connessioni ideali perché il tuo lavoro fili liscio e senza intoppi.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Perché il centralino voip è indispensabile nella ristorazione'])
    <p>
        Come ben sai non puoi permetterti di avere la linea occupata quando un cliente chiama.
        Perché un problema di linea può diventare un problema di prenotazione e farti perdere il cliente con il centralino VoIP questo problema non esiste più: aggiungi quante linee ti servono quando ti servono.
        In più un’assistenza sempre presente può evitarti perdite di tempo e di denaro.
        <br><br>
        Fiberdroid può seguirti e risolvere i tuoi problemi con la sua assistenza dedicata prima che si trasformino in una perdita per il tuo business.
        Evita recensioni negative sulle prenotazioni telefoniche! Adesso puoi rispondere ovunque ti trovi con la nostra app per smartphone e non avrai mai la linea occupata!
        Puoi passare facilmente la linea ai tuoi collaboratori, aggiungere numeri in qualunque momento, deviare chiamate e portare la tua linea ovunque ti trovi, oltre ad avere una segreteria telefonica in caso di bisogno sempre con Te.

        <br><br>
        Rispondere sempre velocemente comunica un’idea di efficienza e non ti fa perdere prenotazioni.
        <br><br>
        Con un centralino VoIP puoi avere tutte le linee di cui hai bisogno sempre libere e dedicate al settore che vuoi. Con Fiberdroid sarai seguito durante ogni fase, dall’installazione a eventuali ampliamenti e non dovrai preoccuparti di nulla.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Perché devi avere un sito e social sempre connessi'])
    <p>
        L’esperienza del cliente è fondamentale. Dal social prenderanno informazioni sul tuo locale, il menu i servizi e le offerte: dimostrare affidabilità, sicurezza (soprattutto di questi tempi) e competenza è importante.
        Tenere d’occhio recensioni e prenotazioni è indispensabile!
        <br><br>
        Per alcuni la condivisione dell’esperienza è quasi più importante dell’esperienza stessa, per questo permettere un’agevole condivisione al cliente e avere tu una visione costante di ciò che succede sui social per commentare e ringraziare fa parte del tuo successo come ristoratore.
        Con una connessione veloce e sicura potrai aggiornare in modo facile e veloce il menù e le offerte del tuo sito e visualizzare le recensioni per gestire la tua WEB Reputation al meglio.
        <br><br>
        Tu pensa alla tua immagine social, che a connettere il tuo business ci pensa Fiberdroid!
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Perché un’ottima connessione è indispensabile al tuo negozio'])
    <p>
        Anche nei sabati pomeriggio di dicembre, con negozio pieno di gente che sceglie i regali di Natale e telefonate di chi vuole prenotare un oggetto o sapere se lo avete, non avrete mai problemi di telefonate, nemmeno se vi tranciassero i cavi con una ruspa! Con il VoIP basta deviare su qualunque dispositivo le chiamate per essere sempre raggiungibili.
        <br><br>
        Oltre l’80% dei clienti vuole evitare code pagando a un commesso col POS mobile.
        E il 79% vorrebbe avere la possibilità di pagare gli acquisti in negozio con un’app, senza interagire con un commesso (la percentuale sale all’87% tra i 18 e i 34 anni)
        perché avendo tutti i canali VoIP a disposizione potrai fare più cose in contemporanea senza avere mai problemi di linea occupata:
    </p>
    <ul class="list">
        <li>gestire tutti i tuoi sistemi di sicurezza in remoto, telecamere, controllo accessi</li>
        <li>pubblicare sui social le foto dei tuoi nuovi arrivi e interagire con i tuoi clienti sulla pagina Facebook</li>
        <li>interagire con l’HotSpot WiFi inviando offerte e promozioni</li>
        <li>gestire le carte fedeltà raccogliendo i dati dei tuoi clienti per inviare via email le promozioni e le offerte</li>
        <li>creare stories su Instagram sul tuo negozio e le novità che proponi</li>
        <li>pubblicare sul tuo sito in un attimo ogni pezzo che esponi nel negozio</li>
        <li>gestire spedizioni, ordini e resi senza occupare la linea telefonica o del POS</li>
        <li>gestire la segreteria, le deviazioni di chiamata o la reperibilità in qualunque momento e da qualunque posto ti trovi!</li>
    </ul>
    <p>
        E tutto ciò è fantastico, se funziona e non ti crea problemi.
        Fiberdroid, il tuo partner unico per la connettività è al tuo fianco per garantirti la massima qualità e un’assistenza veloce e dedicata.
        <br><br>
        Per non perdere tempo e poter pensare solo al tuo business!
    </p>
@endcomponent
