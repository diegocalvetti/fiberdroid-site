<p>
    Costo di attivazione del servizio Wireless fino a 100 Mega con installazione premium<b>*</b>: <i>279.90&euro;</i>
    <br>
    Costo di attivazione del servizio Wireless fino a 300 Mega con installazione premium<b>*</b>: <i>380.00&euro;</i>
    <br>
    <b>*</b> Installazione antenna Wireless in base alla copertura dell'indrizzo
    <br>
    Costo di attivazione del servizio LTE 30, 50 Mega: <i>199.90&euro;</i>
    <br>
    Costo di attivazione del servizio 4G/4G Flat: <i>199.90&euro;</i>
    <br>
    Costo di disattivazione del servizio: 199.90€
    <br><br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br>
    Ogni servizio è attivabile previa <a target="_blank" href="{{ url('verifica-copertura') }}">verifica tecnica della copertura</a>.
</p>

