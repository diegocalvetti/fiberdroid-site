<p class="service-faq-title is-uppercase has-text-centered mb-4">Faq</p>

@component('pages.components.accordion', ['title' => 'Ponte radio (WI-MAX / LTE)'])
    <p>
        Grazie alla tecnologia radio, è possibile <strong>fornire connettività dedicata simmetrica e asimmetrica</strong> con ip statici pubblici in <strong>zone del territorio in cui la fibra scarseggia</strong>, ma si ha la necessità di accedere ad internet ad alta velocità.
        <br><br>
        Le nostre connessioni wireless partono da 30 Mbps e possono arrivare su ponti radio dedicati fino a 1 Gbps, sempre con Ip statici pubblici.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Mobile 4G'])
    <p>
        Pensata come <strong>connessione di backup</strong>, per le aziende che voglio una <strong>garanzia di continuità</strong> su diversa tipologia di connettività.
        <br><br>
        Grazie a questa soluzione è possibile avere banda fino a 100Mbps in download e 50 Mbps in upload, con 1 ip statico pubblico.
        <br><br>
        Sono disponibili 2 profili, il primo da 400 GB al mese e il secondo Flat senza limiti.
        <br><br>
        Il nostro servizio Wireless unisce la capillarità della rete wireless alle performance equiparabili alle soluzioni della fibra punto-punto.
    </p>
@endcomponent
