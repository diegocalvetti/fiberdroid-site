<p class="service-faq-title is-uppercase has-text-centered mb-4">Faq</p>

@component('pages.components.accordion', ['title' => 'Perchè scegliere il satellitare?'])
    <p>
        Il servizio ad alta velocità di Internet satellite arriva, infatti, dove non arriva nessun altro tipo di connessione.
        <br><br>
        Sarai <strong>collegato al satellite multifascia KA-SAT</strong>, di nuova generazione, in funzione dal 2011, sia per la ricezione sia per la trasmissione dei dati.
        <br><br>
        Con la connessione internet satellitare non avrai bisogno di una linea telefonica né della Fibra ottica, per collegarti, basta installare una parabola ricevente con cui collegarsi al segnale emesso dal satellite.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Cosa ti offre l\'internet satellitare di Fiberdroid'])
    <p>
        Con il nostro internet satellitare offriamo un accesso ad internet, <strong>affidabile e sicuro</strong>, in <strong>qualunque punto d’Italia</strong>, comprese le aree in Digital Divide non servite dalle reti in rame o in fibra terrestri, a costi molto simili al normale servizio ADSL.
        <br><br>
        Puoi arrivare fino alla velocità di 50 Mbit/s in download e 10 in upload!
        <br><br>
        Poiché è semplice e veloce da installare, puoi utilizzare il servizio satellitare anche per brevi periodi, come ad esempio in occasione di eventi in location atipiche e lontane dalla città... sulle colline, nei boschi o in riva al mare!
        <br><br>
        E con la nostra assistenza personalizzata non avrete mai a che fare con call center esterni ma parlerete solo con i nostri tecnici, competenti e affidabili. Perché la soddisfazione del cliente è la nostra  priorità.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Come funziona l\'internet satellitare'])
    <p>
        A quasi 36.000 Km dalla terra, orbitano centinaia di satelliti  usati anche per le trasmissioni dei segnali Internet. Questi segnali vengono intercettati dai <strong>NOC (centri terrestri per le operazioni di rete)</strong> e da qui trasmessi alle dorsali che fanno da "snodo" indirizzandoli ai server di Rete che ritrasmettono alla parabola e al modem utente ad essa collegato e percorso inverso.
        <br><br>
        Per utilizzare la connessione a Internet via satellite, bisogna installare gli strumenti per intercettare i segnali:
    </p>
    <ul class="list">
        <li>una parabola</li>
        <li>un modem</li>
    </ul>
    <p>
        È una connessione di tipo wireless in quanto non sono necessari nè cavi telefonici nè la fibra.
        <br><br>
        La trasmissione dati è in modalità Wireless, cioè senza i cavi della rete telefonica.
        <br><br>
        La connessione internet satellitare mediante i satelliti di nuova generazione KA-SAT (K-above band) permette un accesso a internet ad alta velocità, 50Mbs, ben superiore alla classica ADSL che in genere non supera i 20Mbs.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Internet satellitare opinioni'])
    <p>
        In generale Internet satellitare riceve delle buone recensioni perché ha innumerevoli vantaggi:
    </p>
    <ul class="list">
        <li>non serve una linea telefonica (risparmio sui costi fissi)</li>
        <li>non serve adsl</li>
        <li>non serve la fibra</li>
        <li>collegamento internet veloce equiparabile all'adsl se non meglio (ADSL 20Mbs satellitare 50Mbs)</li>
        <li>è collaudato da diversi anni</li>
        <li>disponibile anche per brevi periodi (eventi, vacanze)</li>
        <li>ha un'installazione semplice e veloce (una parabola da orientare verso il satellite e collegare al modem)</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Internet satellitare svantaggi'])
    <p>
        L'unico vero svantaggio di questo collegamento internet veloce e affidabile è il tempo di latenza (lag) che ne sconsiglia l'utilizzo nel caso di giochi "sparatutto" basati sulla velocità di azione-reazione. Infatti il segnale dal satellite a terra impiega circa 120ms e per l'utilizzatore il tempo totale è circa 500ms.
        <br><br>
        Come si valuta il lag? Si calcola il ping della connessione. Un ping troppo alto pregiudica la qualità di una connessione per il gaming online del tipo "sparatutto".
        <br><br>
        In questo caso è consigliato ove possibile l'utilizzo della fibra. Il lag in genere non comporta alcun problema con tutti gli altri tipi di giochi online.
        <br><br>
        Una connessione in fibra ottica, infatti, si caratterizza per un ping di pochi millisecondi.
    </p>

@endcomponent

@component('pages.components.accordion', ['title' => 'Curiosità sull\'internet satellitare'])
    <p>
        Il primo satellite artificiale, lo Sputnik 1, fu lanciato nel 1957 dall'Unione Sovietica e restò in orbita solo 92 giorni.
        <br><br>
        In orbita vi sono oltre 4000 satelliti tra cui:
    </p>
    <ul class="list">
        <li>24 satelliti per la trasmissione di dati GPS, più 3 di scorta per garantire copertura globale del servizio.</li>
        <li>un migliaio di satelliti meteorologici per monitorare gli eventi atmosferici comprese tempeste di sabbia, incendi, il fluire delle correnti oceaniche e gli effetti dell'inquinamento</li>
    </ul>
    <h3 class="accordion-subtitle">
        I satelliti KA SAT
    </h3>
    <ul class="list">
        <li>Permettono l'accesso a Internet banda larga a quasi un milione di utenti.</li>
        <li>Il servizio funziona grazie a 10 stazioni terrestri</li>
        <li>Il servizio è gestito centralmente dal NOC (network operations center) di Skylogic con sede a Torino (Skylogic è una filiale di Eutelsat).</li>
        <li>Sono in orbita dal 2011</li>
        <li>Offrono una copertura di tutta l'area europea e una parte del Medio Oriente.</li>
        <li>Sono di proprietà di Eutelsat (il terzo operatore al mondo per ricavi) Skylogic è infatti una filiale di Eutelsat.</li>
        <li>Il termine KA si riferisce alla banda di frequenze utilizzata per la comunicazione: la banda Ka è la porzione superiore delle frequenze a microonde dello spettro elettromagnetico e segue la banda K (e per questo è chiamata K-above band).</li>
    </ul>
    <p>
        Progetti futuri per il satellitare:
    </p>
    <ul class="list">
        <li>Facebook (progetto Internet.org) e Google (Project Look) che sono interessate alla creazione di servizi Internet wireless per garantire una copertura su tutto il pianeta per i loro servizi</li>
        <li>Il progetto Starlink della Spacex (Elon Musk) per posizionare satelliti a bassa quota per ridurre il lag (tempo di latenza della connessione)</li>
    </ul>
@endcomponent

