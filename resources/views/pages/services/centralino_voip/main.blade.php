<p class="service-faq-title is-uppercase has-text-centered mb-4">Faq</p>

@component('pages.components.accordion', ['title' => 'Cos\'è un centralino VoIp?'])
    <p>
        Un centralino telefonico (anche chiamato PBX, Private Branch Exchange) viene usato nelle aziende per fornire una rete telefonica interna e connetterla con la rete telefonica pubblica.
        <br><br>
        La sua funzione è quella di permettere ad un'azienda di ridurrre il numero di linee in uscita da chiedere all'operatore telefonico: il numero di linee garantite in uscita viene dimensionato in base al traffico medio dell'azienda e non è così necessario prevedere una linea dedicata per ogni utente all'interno dell'azienda.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Evoluzione del centralino telefonico'])
    <p>
        In passato il centralino telefonico era il dispositivo utilizzato dalle aziende per comunicare con l'esterno e collegare gli interni degli uffici. Il mondo della telefonia fissa e di quella mobile erano separati.
        <br><br>
        Oggi con l'evolvere delle tecnologie oltre alle funzioni tradizionali accennate sopra il centralino implementa una convergenza tra telefonia fissa e telefonia mobile, oltre ad andare verso un'integrazione sempre maggiore di funzionalità (videoconference, software gestionali, applicativi di produttività).
        <br><br>
        Il Centralino telefonico tradizionale analogico viene sempre più spesso sotituito dai Centralini VoIP di nuova generazione
    </p>
    <ul class="list">
        <li>Centralino VoIP (on premise, installazione fisica presso sede del cliente)</li>
        <li>Centralino Cloud (centralino VoIP esternalizzato presso datacenter)</li>
    </ul>
    <p>
        Per questa soluzione vedi la nostra offerta alla pagina Centralino Cloud
        <br><br>
        Entrambi questi centralini utilizzano nella maggioranza dei casi il protocollo SIP (Session Initiation Protocol) che è lo standard di riferimento per la segnalazione telefonica adoperato per stabilire, modificare e concludere telefonate VoIP e permette che due dispositivi (telefoni IP, centralini...) coinvolti in una telefonata attraverso Internet riescano a dialogare tra di loro.
        <br><br>
        In realtà il SIP è un protocollo utilizzato per gestire lo scambio generale di contenuti multimediali,quindi oltre che per impostare le chiamate telefoniche via Internet la sessione multimediale può contenere qualsiasi multimedia inclusi dati, voce, video, immagini.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Principali funzionalità del centralino VoIp Fiberdroid'])
    <ul class="list">
        <li>Interno VoIP raggiungibile tramite client software oppure apparato SIP standard da Internet ovunque ci si trovi (anche su rete mobile)</li>
        <li>Casella vocale con registrazione e invio messaggi vocali in automatico su mail, in caso di mancata risposta</li>
        <li>Gestione e personalizzazione dei numeri interni, tramite interfaccia web, con possibilità di deviazione automatica chiamate in ingresso, gestione orario di lavoro e messaggi automatici in risposta</li>
        <li>Risponditore automatico con possibilità di post-selezione e musica di attesa</li>
        <li>Possibilità di selezione passante per interni</li>
        <li>Gruppi personalizzati di risposta e gestione code chiamate</li>
        <li>Integrazione con tutti i principali provider VoIP, al fine di lasciare al cliente la libertà di scelta del carrier preferito</li>
        <li>Possibilità di noleggio dei telefoni con assicurazione Kasko</li>
        <li>Assistenza, modifiche e personalizzazioni in remoto incluse nel canone</li>
    </ul>

    <p>
        Questo è un contratto a progetto, contatta il team di fiberdroid alla mail <a href="mailto:speed@fiberdroid.it">speed@fiberdroid.it</a> per maggiori informazioni.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Vantaggi del centralino VoIp'])
    <h3 class="accordion-subtitle">
        Rispetto ad una centrale tradizionale
    </h3>
    <p>
        Se un azienda ha una centrale telefonica tradizionale analogica o digitale, grazie al VoIP avrà numerosi vantaggi:
    </p>
    <ul class="list">
        <li>Notevole risparmio sulle pesanti bollette telefoniche</li>
        <li>Risparmio sui costi di manutenzione e gestione di centrali telefoniche tradizionali.</li>
        <li>Maggiore mobilita’ ed efficienza operativa dell’azienda: il Centralino VoIP ha molte piu’ funzioni e possibilita’ rispetto un impianto di vecchia generazione.</li>
    </ul>

    <h3 class="accordion-subtitle">
        Rispetto ad una soluzione in cloud
    </h3>
    <p>
        Il Centralino VoIP prevede per le aziende l'installazione di un server presso la loro sede al fine di gestire
    </p>
    <ul class="list">
        <li>parecchie centinaia di telefoni.</li>
        <li>servizi di integrazione con gestionali software interni.</li>
    </ul>
    <p>
        Con un Centralino VoIP on premise le personalizzazioni ed integrazioni software con gestionali interni sono un plus importante rispetto ad un Centralino Cloud.
        <br><br>
        Se il Centralino fosse Cloud si dovrebbe infatti investire parecchio in sicurezza nel caso in cui fossero richieste integrazioni software, oltre al fatto che in caso di caduta della connettivita’ o rallentamenti si avrebbe un degrado importante del servizio.
        <br><br>
        Quindi la centrale telefonica installata presso la sede del cliente e’ una garanzia di qualita’ e possibilita’ di maggiori sviluppi.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Perchè scegliere Fiberdroid?'])
    <p>
        Grazie alla piattaforma cloud di Fiberdroid siamo tra i pochi provider a poter installare un centralino telefonico VoIP hardware dal cliente e garantirgli la continuita’ del servizio (con alcuni limitazioni definite in fase di progetto) con un backup in cloud.
        <br><br>
        Si perche’ nei casi in cui installiamo un centralino on premise dal cliente (anche multi sede) impostiamo l’ambiente per un eventuale backup cloud che si attivi nel caso in cui quello locale si guasti.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Come acquisto un centralino VoIp Fiberdroid?'])
    <h3 class="accordion-subtitle">
        Contattaci
    </h3>
    <p>
        Prima cosa contattaci e prendiamo un appuntamento per analizzare la tua attuale piattaforma telefonica.
    </p>

    <h3 class="accordion-subtitle">
        Progetto e scelta tra diverse opzioni
    </h3>
    <p>
        In seguito ad un progetto che ti consegneremo avrai la possibilita’ di scegliere su diverse opzioni che ti forniremo.
    </p>

    <h3 class="accordion-subtitle">
        Installazione e migrazione
    </h3>
    <p>
        L’installazione e migrazione verso la nuova piattaforma, sara’ pianificata in modo tale da ridurre al minimo i disservizi, in modo tale che il processo sia un'evoluzione e non una rivoluzione.
    </p>

    <h3 class="accordion-subtitle">
        Attivazione sistema di monitoraggio
    </h3>
    <p>
        Una volta che tutto sara’ operativo, verra’ attivato il nostro sistema di monitoraggio che consentira’ in caso di problemi di continuare ad operare con l’impianto telefonico (con alcune limitazioni) grazie alla piattaforma cloud.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Cosa include il nostro centralino VoIp?'])
    <p>
        Prima cosa contattaci e prendiamo un appuntamento per analizzare la tua attuale piattaforma telefonica.
    </p>
    <ul class="list">
        <li>Server Hardware</li>
        <li>Installazione e configurazione passo passo con il cliente</li>
        <li>Personalizzazioni e/o integrazioni software</li>
        <li>Sistema di monitoraggio remoto e aggiornamenti periodici</li>
        <li>Assistenza e Manutenzione in remoto</li>
        <li>Backup cloud dell’impianto (opzionale)</li>
    </ul>
@endcomponent
