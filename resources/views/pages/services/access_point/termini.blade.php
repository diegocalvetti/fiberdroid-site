<p>
    Tutti gli importi sono espressi in euro iva esclusa.<br>
    Possibile acquisto degli apparati in alternativa al noleggio, con preventivo personalizzato. <br>
    Possibilità di acquistare il servizio di controller cloud con gestione centralizzata da parte di Fiberdroid.<br>
    Possibilità di acquistare a parte il supporto tecnico per la configurazione del controller locale.
</p>
