@component('pages.components.accordion', ['title' => 'Cos\'è un Access Point'])
   <p>
       Un access point è un apparato che si collega attraverso un cavo collegato al router (o al modem) alla rete cablata principale e fornisce una rete WI-FI a cui collegare ad esempio smartphone, computer e smart tv.
       <br><br>
       Gli access point si differenziano dai ripetitori perché non ripetono solo il segnale ampliandone la copertura (con stesso nome rete e password) ma permettono di creare reti dedicate (per esempio per gli ospiti) creando loro stessi un accesso wireless alla rete attraverso cui servire i dispositivi.
   </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Perché usare un Access Point'])
    <p>
        Hai bisogno di un access point quando:
    </p>
    <ul class="list">
        <li>il tuo router non ha il WI-FI e non puoi collegare facilmente i tuoi device</li>
        <li>il tuo router non copre col WI-FI completamente la casa o l’ufficio</li>
        <li>necessiti di reti separate</li>
    </ul>
    <p>
        Nel primo caso si collega al router di casa per aumentarne la copertura e potendo così collegarsi via WI-FI dalle stanze più lontane, (esistono router senza la funzionalità del collegamento wireless e altri dove questo è presente ma non è sufficiente a sopperire a tutte le richieste.)
        <br><br>
        Nel secondo caso si collega un access point per consentire la creazione di diverse reti, per esempio quella per gli ospiti dove puoi decidere di concedere per esempio un download limitato per evitare rallentamenti al collegamento internet principale oppure puoi impedire l’accesso ai device della LAN, ad esempio se non vuoi che accedano ai tuoi dati riservati o alla tua stampante.
        <br><br>
        L’access point non crea una nuovo accesso alla rete Internet ma estende quella esistente. I router WI-FI di ultima generazione hanno spesso la funzione access point integrata ma se non hai questi apparecchi un access point può risolvere il tuo problema.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Come configurare un Access Point'])
    <p>
        Puoi configurare l’access point dal pannello di controllo attraverso il browser (inserendo l’IP dell’access point)  o dal programma fornito con l’apparecchio (si solito su dischetto). L’IP lo trovi scritto in un’etichetta nella confezione se no spesso è 192.168.0.1 o 192.168.1.1 oppure 10.1.1.1.
        <br><br>
        Le impostazioni da configurare di solito sono sotto le voci “LAN” o “network”.
        <br><br>
        La sicurezza della tua rete WI-FI è importante e quindi consigliamo sempre di rivolgerti a professionisti, noi di Fiberdroid possiamo darti supporto per queste configurazioni e soprattutto mantenere aggiornate le tue antenne sempre con le ultime versioni firmware per garantire la massima affidabilità.
        <br><br>
        Pagina del sito con offerte per antenne a noleggio / acquisto: #offerte-antenne-fiberdroid
        <br><br>
        Ancora meglio, se adotti la nostra piattaforma Cloud WI-FI ti consentirà di gestire tutti gli access point della tua rete anche su sedi diverse sparse sul territorio direttamente da un'unica interfaccia web nel nostro cloud.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Come scegliere un Access Point'])
    <p>
        Per prima cosa stabilisci esattamente i tuoi bisogni: valuta se ti serve a uso casalingo o aziendale (molti client collegati), se da interno o esterno e persino se applicarlo al soffitto, la distanza da coprire valutabile anche in metri quadri il bisogno di accessi in contemporanea, la necessità di isolare alcuni utenti (reti guest) e addirittura assegnargli orari di accesso limitati.
        <br><br>
        Non valutarlo in base alla quantità di antenne ma in base a quanti Mbps erogano e il numero massimo di dispositivi che puoi connettere.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Tipi di Access Point'])
    <p>
        Ci sono diversi tipi di access point  le caratteristiche principali da considerare sono:
    </p>
    <ul class="list">
        <li>la velocità di trasferimento dati cioè la quantità di Mbps che possono supportare gli access point (in ordine: classe b (802.11b)  fino a 11 Mbps, classe g (802.11g) max a 54 Mbps, classe n (802.11n)  fino 300 o 450 Mbps e AC (802.11ac) che arriva fino 1.3Gbps)</li>
        <li>se sono dual band o no (se possono trasmettere sia su 2,4 GHz che 5 Ghz).</li>
        <li>quanti client al massimo supportano (in teoria fino a diverse decine di device per antenna, e tanti quanti sono gli indirizzi ip che si può assegnare, in pratica in condizioni ottimali un access point può gestire fino a 32 client su 2,4 Ghz e altri 32 su 5 GHz, questo perché la banda disponibile viene suddivisa tra i device connessi)</li>
        <li>Multi SSID per creare più connessioni WI-FI virtuali (reti con nomi e password differenti)</li>
        <li>se vanno posizionati in ufficio o in esterno</li>
        <li>se gestiscono il roaming (smistamento dinamico dei client su più access point che permette di restare connesso anche in movimento)</li>
        <li>se hanno antenne interne o esterne, fisse o removibili (più dB hanno più sono potenti)</li>
        <li>la sicurezza con la crittografia WPA/WPA2/WPA3</li>
        <li>supportano o no il beamforming (tecnologia che orienta la trasmissione e la ricezione del segnale WI-FI verso i dispositivi connessi avendo così a disposizione una banda maggiore e una trasmissione più stabile)</li>
        <li>Fiberdroid si serve di access point Ubiquiti, una delle aziende leader del settore</li>
    </ul>

    <h3 class="accordion-subtitle">Access point POE</h3>
    <p>
        l’access point prende sia la rete che l’alimentazione dal cavo ethernet. Si possono quindi installare distante dal modem senza usare cavi apposta per la corrente. L’energia elettrica passa attraverso un alimentatore apposito all’interno dell’access point oppure attraverso uno switch di tipo POE. In genere hanno poca potenza
    </p>

    <h3 class="accordion-subtitle">Access point USB</h3>
    <p>

        Quando vuoi collegare stampanti, smartphone, tablet ecc al tuo pc puoi utilizzare una chiavetta usb che genera una rete WI-FI a cui collegare i device. Basta collegarla e scaricare i driver per renderla operativa.
    </p>

    <h3 class="accordion-subtitle">Access point da esterno</h3>
    <p>
        Per gli access point da esterno le caratteristiche più richieste sono la copertura su lunghe distanze,  la capacità di direzionarsi sulle persone in movimento e la resistenza a caldo e freddo (ma vanno comunque messi al riparo da pioggia e sole). In genere hanno una custodia che protegge da polvere e acqua e spesso con protezione dai fulmini per evitare danni da punte di tensione.
    </p>

    <h3 class="accordion-subtitle">Access point AC</h3>
    <p>
        Access point che usano il protocollo AC cioè  due bande di frequenze,  2,4 GHz e 5 Ghz.
        <br><br>
        Sono chiamate access point dual band e sono l’ideale per avere alte velocità di download e upload.
        <br><br>
        La copertura in genere è 20 metri per la banda 2,4 Ghz e di circa 15 metri sulla banda 5GHz.(raggio d’azione inferiore ma più stabile e con meno interferenze)
        <br><br>
        La quantità di Mbps supportata è indicata nella sigla dell’access point, potreste trovare per esempio per esempio AC750 supporta 750Mbps in dual band che significa che l’access point opera sia sulla banda di frequenza a 2,4 Ghz che a 5GHz.
        <br><br>
        Nella classe AC esistono access point più evoluti che usano la tecnologia MU-MIMO cioè multiple user e  multiple input-multiple output.
        <br><br>
        Con questa tecnologia i client (pc, smartphone ecc collegati all’access point) non devono più restare in attesa finché non è conclusa l’azione del device che sta facendo una richiesta, ma le richieste vengono gestite in contemporanea.
    </p>
@endcomponent
