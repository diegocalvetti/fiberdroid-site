@component('pages.components.accordion', ['title' => 'Perchè scegliere Fiberdroid'])
    <p>
        Da sempre <strong>chi sceglie di essere un libero professionista, lavoratore autonomo mette la sua libert&agrave; davanti a tutto</strong>. Libert&agrave; di orari, di spostamenti, di modalit&agrave; di lavoro.
        L&rsquo;idea di lavorare in spiaggia o in terrazza &egrave; il sogno di tanti.
        Ma questo equilibrio tra dovere e piacere &egrave; subordinato a un&rsquo;ottima connessione e un ottimo servizio di assistenza che sollevi il libero professionista da perdite di tempo indesiderate.
    </p>
    <br>
    <p>
        Per questo Fiberdroid ha creato le <strong>offerte internet business</strong>,
        pensate per chi vuole gestire la sua attivit&agrave; in libert&agrave; e sicurezza, ovunque si trovi,
        offerte <strong>internet senza linea fissa</strong>, con attivazione veloce e assistenza
        professionale e dedicata per ogni l<strong>avoratore autonomo</strong> che non ha tempo da
        perdere! La tecnologia FiberDroid ti supporta ogni giorno, consentendo ai <strong>freelance</strong>
        come te di svolgere l&rsquo;attivit&agrave; velocemente e con regolarit&agrave;, senza intoppi o ritardi: garantiamo
        la continuit&agrave; del servizio internet al meglio delle possibilit&agrave;, anche quando accadono cose come
        il taglio accidentale dei cavi mentre operai stanno lavorando in strada.
    </p>
    <br>
    <p>
        Noi saremo il tuo interlocutore unico fino alla risoluzione del problema e
        attueremo ogni possibile strategia per consentirti di lavorare lo stesso, grazie alla
        nostra soluzione (opzionale) di backup in 4G oppure Wireless.
        <strong>Internet veloce</strong> &egrave; una realt&agrave; non solo nelle grandi citt&agrave;,
        le nostre soluzioni wireless sono alla pari per banda e stabilit&agrave;.
    </p>
    <br>
    <p>
        La scelta &egrave; spesso tra la tradizionale <a href="<?=route('servizio', 'adsl')?>">ADSL</a>
        (fino a 20 Mbps) e la <a href="<?=route('servizio', 'fibra')?>">fibra ottica</a> (oltre I 100Mbps). Inoltre
        <strong>per chi lavora con dati sensibili la sicurezza &egrave; un obbligo</strong>. I nostri servizi garantiscono
        <strong>sicurezza</strong> e affidabilit&agrave; perch&eacute; I tuoi dati sono nei nostri <strong>datacenter</strong>&nbsp;
        con un sistema di <strong>backup in cloud </strong>con crittografia per sicurezza
        e rispetto della privacy. Puoi cos&igrave; condividere I tuoi dati con il cliente o collaboratori aggiornando e
        modificando I progetti in modo semplice e sicuro.
    </p>
    <br>
    <p>Incrementa la tua produttivit&agrave;, ora puoi lavorare, restare connesso e rispondere al telefono in piena mobilit&agrave;, ovunque,&nbsp;come se fossi in ufficio! I nostri servizi possono essere tutti inclusi di centralino telefonico di ultima generazione con Webmeeting e servizi VPN tramite il nostro cloud.</p>
    <br>
    <p>Il Futuro &egrave; gi&agrave; qui. Vuoi ancora aspettare per far crescere il tuo Business?</p>
@endcomponent
