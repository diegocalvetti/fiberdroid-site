@component('pages.components.accordion', ['title' => 'Il centralino cloud di Fiberdroid'])
    <p>
        Il vostro numero telefonico verrà mantenuto e gestito dai nostri server in datacenter così da essere raggiungibile anche in mobilità e non perdere neanche una telefonata.
        <br><br>
        La soluzione Fibedroid di Centralino Cloud non assomiglia affatto ad un comune centralino telefonico. Consente infatti:
    </p>
    <ul class="list">
        <li>L'integrazione con software aziendali</li>
        <li>il fax server integrato</li>
        <li>IVR (Interactive Voice Response) per dirottamento chiamata</li>
        <li>la gestione del Call Center, con report chiamate e sistema di billing.</li>
    </ul>
    <p>
        Un Centralino Cloud ti consente inoltre di
    </p>
    <ul class="list">
        <li>liberarti delle classiche prese</li>
    </ul>
    <p>
        Le chiamate possono essere effettuate sia da pc/smartphone/tablet mediante un'applicazione chiamata Softphone sia con telefoni dedicati chiamati telefoni IP.
    </p>
    <ul class="list">
        <li>
            liberarti dei costi fissi di noleggio degli apparati, in quanto i numeri di telefono possono essere registrati nel centralino via Internet e le tue sedi possono avere più numerazioni su di un unico sistema di comunicazione.
        </li>
    </ul>
    <p>
        Grazie a dei client specifici, inoltre, puoi utilizzare il tuo numero interno aziendale anche quando sei fuori sede, dal tuo cellulare (compatibile con Client Sip), per ricevere e poter fare chiamate come se fossi seduto alla tua scrivania.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Centralini di nuova generazione: un chiarimento'])
    <p>
        Può capitare di confondersi con i vari termini che si trovano in rete relativamente alla nuova generazione di centralini: centralino virtuale, centralino VoIP, centralino PBX o in Cloud. Cerchiamo di fare chiarezza insieme.
    </p>
    <h3 class="accordion-subtitle">
        CENTRALINO VOIP O ON PREMISE
    </h3>
    <p>
        Con entrambi questi termini si intende una soluzione in cui il centralino deve fisicamente essere installato nella sede del cliente (on premise).
        <br><br>
        Nei casi infatti in cui sia meglio non sovraccaricare troppo la connessione Internet può risultare più conveniente gestire localmente tutte le telefonate al cliente.
        <br><br>
        Con un Centralino VoIP si possono gestire:
    </p>
    <ul class="list">
        <li>parecchie centinaia di telefoni</li>
        <li>servizi di integrazione con gestionali software interni</li>
    </ul>
    <p>
        Vedi la nostra soluzione di Centralino VoIP alla pagina<br>
        <a target="_blank" rel="noopener" href="https://fiberdroid.it/servizi/centralino-voip">https://fiberdroid.it/servizi/centralino-VoIP</a>
    </p>

    <h3 class="accordion-subtitle">
        CENTRALINO CLOUD O CLOUD PBX O VIRTUALE
    </h3>
    <p>
        Il centralino Cloud è invece una soluzione in cui il sistema viene virtualizzato esternalizzando il centralino in un datacenter al di fuori degli uffici.
        <br><br>
        In questo modo vengono drasticamente ridotti i costi di installazione e gestione iniziali oltre che quelli di manutenzione nel tempo.
        <br><br>
        Nel caso del centralino cloud l'integrazione con gesionali software interni è meno consigliata dal momento che tutto passerebbe su Internet con il rischio di disservizi in caso di problemi con la banda Internet.
        <br><br>
        La differenza sostanziale tra le due soluzioni descritte è sulla:
    </p>
    <ul class="list">
        <li>dimensione delle aziende</li>
        <li>esigenze richieste</li>
        <li>integrazioni software</li>
        <li>proprietà della centrale telefonica (preferendo quindi la soluzione hardware rispetto a quella virtuale)</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Vantaggi della soluzione cloud'])
    <h3 class="accordion-subtitle">
        TAGLIO DEI COSTI
    </h3>
    <p>
        Grazie alla nostra offerta di Centralino Cloud potrai abbattere in modo importante i costi della telefonia tradizionale e ottenere un servizio di qualità superiore che ti consentirà di ricevere ed effettuare telefonate ovunque ti trovi.
        <br><br>
        Potrai gestire fino a 50 telefoni per filiale senza farti carico dei costi iniziali per l'acquisto e installazione del centralino hardware, oltre che non doverti preoccupare dell'invecchiamento degli apparati nel tempo.
        <br><br>
        Inoltre i nostri listini sono molto vantaggiosi con tariffe valide senza scatto alla risposta e con conteggio sull’effettivo secondo di chiamata.
    </p>
    <h3 class="accordion-subtitle">
        LA DOMOTICA
    </h3>
    <p>
        Una volta che utilizzerai la tecnologia Cloud di Fiberdroid vedrai i vecchi impianti telefonici come qualcosa di antico e lontano.
        <br><br>
        Il nostro impianto si integra non solo con tutti i dispositivi presenti sul mercato ma consente di comandare anche l’apertura di cancelli o porte integrandosi con contatti virtual Relé installabili all’interno di aziende.
        <br><br>
        La domotica e il nostro Centralino Cloud si integrano perfettamente.
    </p>
@endcomponent


@component('pages.components.accordion', ['title' => 'Principali funzionalità del centralino cloud'])
    <ul class="list">
        <li>Interno VoIP raggiungibile tramite client software oppure apparato SIP standard da Internet ovunque ci si trovi (anche su rete mobile)</li>
        <li>Casella vocale con registrazione e invio messaggi vocali in automatico su mail, in caso di mancata risposta</li>
        <li>Gestione e personalizzazione dei numeri interni, tramite interfaccia web, con possibilità di deviazione automatica chiamate in ingresso, gestione orario di lavoro e messaggi automatici in risposta</li>
        <li>Risponditore automatico con possibilità di post-selezione e musica di attesa</li>
        <li>Possibilità di selezione passante per interni</li>
        <li>Gruppi personalizzati di risposta e gestione code chiamate</li>
        <li>Integrazione con tutti i principali provider VoIP, al fine di lasciare al cliente la libertà di scelta del carrier preferito</li>
        <li>Possibilità di noleggio dei telefoni con assicurazione Kasko</li>
        <li>Assistenza, modifiche e personalizzazioni in remoto incluse nel canone</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Perché scegliere Fiberdroid'])
    <p>
        Fiberdroid ti consente di avere un impianto superiore rispetto ad altri in quanto studia l’intero progetto dalle fondamenta, fornendo non solo il centralino ma anche:
    </p>
    <ul class="list">
        <li>le connettività con monitoraggio attivo</li>
        <li>eventuali backup</li>
        <li>portabilità di numerazioni</li>
        <li>noleggio o vendita di apparati e dispositivi per le integrazioni ivi compresi i telefoni IP</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Requisiti minimi'])
    <ul class="list">
        <li>Computer o smartphone per utilizzo APP Softphone</li>
        <li>Connettività internet con banda minima garantita studiata anticipatamente per le contemporaneità necessarie a sostenere le chiamate</li>
        <li>Router / Firewall configurato / predisposto correttamente per gestire la QoS (Quality of Services) per le chiamate VoIP</li>
        <li>Eventuali telefoni IP compatibili con protocollo SIP</li>
    </ul>
@endcomponent
