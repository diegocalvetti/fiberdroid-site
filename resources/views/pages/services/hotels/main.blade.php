<p class="service-faq-title is-uppercase has-text-centered mb-4">Faq</p>

@component('pages.components.accordion', ['title' => 'Ma cosa compone una efficiente connessione per hotel?'])
    <p class="is-uppercase mt-6 mb-4"><strong>Un collegamento Internet veloce con la fibra FTTH</strong></p>
    <p>
        La fibra diretta fino alla tua struttura ti consente una velocità ottimale(fino a 1 Gbps in download e banda minima garantita fino a 50 Mbps).
        Niente più film in streaming a scatti, permetti videoconferenze ai tuoi clienti con la velocità e stabilità della fibra diretta!
    </p>

    <p class="is-uppercase mt-6 mb-4"><strong>Una buona connessione di rete WiFi</strong></p>
    <p>
        La connessione WiFi  permette  un lavoro senza interruzioni, pc funzionanti, quindi informazioni, pagamenti, prenotazioni sempre attivi, stampanti sempre connesse per le tue fatture e ricevute, POS funzionante per non fare attendere il cliente in uscita e magari un sistema audio-video per il tuo controllo sicurezza.
        <br><br>
        Un hotspot WiFi per hotel che permetta di accedere alla rete Internet del tuo hotel (o qualunque sia la tua struttura ricettiva) diffondendo il segnale in modo sicuro e efficiente.
        <br><br>
        Potrai gestire sia gli utenti connessi sporadicamente sia meeting con centinaia di utenti connessi in contemporanea in videoconferenza.
        <br><br>
        Per trasmettere dappertutto un efficiente segnale WiFi, è necessaria un‘accurata progettazione della diffusione del segnale, basata sullo studio delle planimetrie della struttura prima di realizzare una rete WiFi per un hotel.
        <br><br>
        Questa pre analisi permette in seguito di gestire gli impianti WiFi per hotel distribuendo gli access-point in modo da ottenere il massimo del risultato. Ovviamente è indispensabile utilizzare apparati hardware di alta qualità, come quelli di cui si serve Fiberdroid.
    </p>

    <p class="is-uppercase mt-6 mb-4"><strong>Collegamenti internet bilanciati per gli ospiti</strong></p>
    <p>
        Se hai centinaia di camere  è indispensabile offrire ai clienti una connessione stabile e protetta ovunque.
        Il nostro sistema permette un bilanciamento del carico della rete WiFi dei clienti dissociando i clienti inattivi e bloccando nuovi accessi.
    </p>
    <br>
    <p>
        Una delle cose di cui  si lamentano di più i clienti sulle piattaforme di prenotazione (come ad esempio Booking) è una connessione WiFi lenta o instabile.
        Ormai essere connessi fa parte della vita quotidiana e tendiamo a connetterci spesso e ovunque.
        Se il segnale si perde devi poter intervenire immediatamente per non creare disservizi agli ospiti.
        Per la tua immagine è importante personalizzare l’accesso a Internet con l’immagine della tua struttura, il meteo, la mappa della città con i mezzi pubblici, i musei ed eventi locali.
    </p>
    <br>
    <p>
        Permettere agli utenti di collegarsi con i social ti consentirà di acquisire informazioni utili per dare sempre un servizio migliore e ovviamente migliorare le basi del tuo Marketing, perché capire quando gli utenti usano di più il WiFi e chi sono le varie tipologie utente può aiutarti a migliorare il servizio e creare proposte di Marketing più efficaci.
    </p>

    <p class="is-uppercase mt-6 mb-4"><strong>Controllo accessi</strong></p>
    <p>
        L’accesso agli ingressi principali, così come ai locali di servizio va sempre monitorato, così come l’accesso alle stanze degli ospiti. Un sistema efficiente basato sull’utilizzo della rete IP ti permetterà di dare ai tuoi ospiti tutta la sicurezza di cui hanno bisogno.
    </p>

    <p class="is-uppercase mt-6 mb-4"><strong>Telecontrollo remoto per la direzione</strong></p>
    <p>
        Possibilità in remoto del tele-controllo, da parte della direzione, di tutti i servizi interconnessi, tramite reti sicure e criptate, per monitorare il controllo accessi del personale e dei clienti, le telecamere allarmi, i servizi domotici. Per ognuna di queste tecnologie è indispensabile una rete efficiente e un’assistenza veloce e sicura.
    </p>

    <p class="is-uppercase mt-6 mb-4"><strong>Gestione centralizzata</strong></p>
    <p>Con piattaforme di gestione cloud posso avere il tracciamento guasti da remoto di access point, router, switch ecc. Sicurezza e velocità di diagnosi e risoluzione problemi.</p>

    <p class="is-uppercase mt-6 mb-4"><strong>Centralino VoIP di ultima generazione</strong></p>
    <p>
        Forniamo inoltre un centralino virtuale di ultima generazione, che non ti fa perdere neppure una chiamata ed assiste tutti i tuoi ospiti con la sua Intelligenza Artificiale. Potrai gestire centinaia di interni in tutta sicurezza grazie all’hardware locale, personalizzare ogni aspetto delle chiamate in entrata e uscita, automatizzare i trasferimenti di chiamata e monitorarne il corretto funzionamento.
    </p>

    <p class="is-uppercase mt-6 mb-4"><strong>Assistenza dedicata</strong></p>
    <p>
        Basta ripetere a operatori diversi qual é il problema! Con la nostra assistenza dedicata avrai un tecnico che ti segue e non ti fa rimbalzare da un operatore all’altro.
        <br><br>
        Non ti abbandona finché non è tutto a posto.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Mettere o no il filtro di sicurezza sulla rete WiFi dell\'hotel?'])
    <p>
        Una rete WiFi protetta  impedisce l’accesso a chi non ha le credenziali. Solo gli ospiti della struttura possono connettersi per navigare.
        <br><br>
        Il sistema di “rieffettua il login”  cioè la richiesta di immettere nuovamente le credenziali serve a controllare periodicamente chi è loggato e a volte può essere installato per impedire l’accesso ad alcuni siti o per bloccare la navigazione.
        <br><br>
        Serve a proteggere la rete WiFi perché essendo pubblica (ha molti utenti) qualcuno potrebbe cercare di accedere al tuo dispositivo.
        <br><br>
        Inoltre spesso viene effettuato un blocco sul download per garantire una buona connessione a tutti gli ospiti,  impedendo agli utenti di guardare film in streaming  o scaricare file pesanti.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Connessione hotel non sufficiente'])
    <p><strong>Lato hotel</strong></p>
    <p>
        Alle volte il problema può essere il router. se non è usato al meglio non riesce a coprire tutta l’area dell’hotel.
        <br>
        Le cause più comuni comprendono:
    </p>
    <ul class="list">
        <li>troppi utenti connessi?</li>
        <li>configurazione errata?</li>
        <li>se il cliente è connesso ma non riesce ad accedere alla maggior parte dei siti (social,  amazon, google ecc) forse viene rifiutato perché proviene da un WiFi non ben funzionante. Controllare il sistema WiFi è il primo passo.</li>
    </ul>

    <p class="mt-5"><strong>Lato cliente</strong></p>
    <ul class="list">
        <li>
            il cliente riesce ad accedere ma il sistema non memorizza I date e deve rifare il login.
            A volte basta scaricare la app per android  https://play.google.com/store/apps/details?id=com.androidyou.WiFiloginnew
            o seguire le istruzioni se hai un iphone https://support.apple.com/it-it/HT204497
        </li>
        <li>
            il cliente ha il risparmio energetico attivato? questa funzione disattiva il WiFi</li>
        </li>
    </ul>
@endcomponent

