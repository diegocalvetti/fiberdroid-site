<p>
    Costo di attivazione del servizio ADSL: <i>79.90&euro;</i>
    <br>
    Costo di disattivazione del servizio: <i>99.90&euro;</i>
    <br><br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br>
    Ogni servizio è attivabile previa <a target="_blank" href="{{ url('verifica-copertura') }}">verifica tecnica della copertura</a>.
</p>
