<p class="service-faq-title is-uppercase has-text-centered mb-4">Faq</p>

@component('pages.components.accordion', ['title' => 'I vantaggi dell\'ADSL'])
   <ul class="list">
       <li><strong>Utilizzo di risorse già installate</strong>: non devo installare un nuovo cavo (si può usare il doppino in rame già presente)</li>
       <li><strong>Alta Velocità</strong>: consente un download senza problemi di dati, video e audio di alta qualità (banda larga)</li>
       <li><strong>Connessione sempre disponibile</strong>: 24 ore su 24 e non si paga sulla base di tariffa oraria</li>
       <li><strong>Si naviga e si telefona contemporaneamente</strong></li>
       <li><strong>Mantenimento</strong> del proprio numero di <strong>telefono</strong></li>
       <li><strong>Sicurezza</strong>: viene garantita attraverso un'offerta che comprende un firewall per difendervi da attacchi esterni</li>
   </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'ADSL significato'])
   <p>
       Cerchiamo ora di capire meglio insieme cosa sia una linea ADSL e come funzioni un pò più nel dettaglio.
       L'ADSL (Asymmetric Digital Subscriber Line) è una tecnologia di trasmissione usata per l'accesso digitale ad Internet ad alta velocità che utilizza gli stessi cavi in rame (il doppino telefonico) della linea telefonica regolare per collegare la singola abitazione alla centrale della compagnia telefonica, ma in modo diverso perchè fa viaggiare i tuoi dati in aggiunta alla tua voce.
       Questo vuol dire che puoi utilizzare la tua linea telefonica per navigare in Internet e per telefonare nello stesso tempo, cosa che non era possibile con i vecchi modem fino a 56 Kbps.
       <br><br>
       Vedremo nei prossimi paragrafi come tutto questo è possibile.
   </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Come funziona l\'ADSL'])
    <p>
        Per capire come funziona l'ADSL bisogna sapere qualcosa circa la normale linea telefonica.
        Le linee telefoniche tradizionali sono state progettate per la comunicazione in voce e sono state costruite limitando il range di frequenze che verranno trasportate. La voce umana può essere trasportata limitandone le frequenze da 0 a 3400 Hertz (la banda minima per l'intelligibilità della voce umana).
        <br><br>
        La ragione per cui ridurre le frequenze trasportate è storica: va ricordato che il sistema telefonico è stato costruito portando una coppia di cavi in rame direttamente ad ogni casa. Limitando le frequenze trasportate potevano essere aggregati insieme molti cavi in uno spazio molto piccolo senza che ci fossero interferenze tra le linee.
        <br><br>
        Questo è un range molto piccolo confrontato con quello che potrebbero trasportare i cavi in rame (fino a diversi milioni di Hertz.)
        <br><br>
        Oggi la connessione ADSL invia dati digitali e non analogici come quelli di una telefonata e per questo può sfruttare tutto il range di frequenze della linea telefonica senza preoccuparsi delle interferenze tra le linee.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Il modem/router'])
    <p>
        Il termine router viene spesso confuso con il termine modem, anche perchè ormai perlopiù il router integra anche le funzioni di modem.
        Usare in maniera intercambiabile i due termini è però tecnicamente sbagliato.
    </p>
    <h3 class="accordion-subtitle">Modem</h3><br>
    <p>
        Per poter sfruttare la linea telefonica è necessario che i dati del computer siano convertiti in segnali analogici. Il modem è il dispositivo che compie questa operazione, gestendo lo scambio di informazioni indispensabili per la navigazione in Internet.
        <br><br>
        Il suo nome deriva proprio dalle due operazioni che compie: modulazione, nella direzione dal computer verso la rete, e demodulazione, quando riconverte il segnale analogico dal doppino in segnale digitale verso il computer.
    </p>

    <h3 class="accordion-subtitle">Router</h3><br>
    <p>
        Il router è il dispositivo che gestisce lo scambio di informazioni tra i vari dispositivi e la rete internet: in pratica fa da interfaccia tra il modem e i computer/smartphone/tablet e si occupa di distribuire via cavo o in Wi-Fi il segnale Internet demodulato dal modem o viceversa di inviare al modem le informazioni provenienti dai vari dispositivi, che verrranno modulate dal modem ed inviate in rete.
        <br><br>
        <i>Curiosità</i>: anche se non ne conosciamo l'esistenza, anche per la rete mobile dei cellulari esiste un router che distribuisce le informazioni ma in questo caso non si trova in casa o in ufficio ma nella centralina dell'operatore telefonico.
        <br><br>
        Se non hai già un tuo router/modem possiamo fornirtelo noi a noleggio o in vendita, vedi le nostre offerte nella pagina Router
        <br><br>
        In questo caso, cioè se scegli di noleggiare o acquistare il router presso di noi, riceverai assistenza anche su tutta la parte hardware.
        <br><br>
        E nel prossimo paragrafo ti spieghiamo un altro motivo per cui ti sarebbe utile ricevere questa assistenza.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'L\'ADSL è sicura?'])
    <p>
        Se non si dispone di un router/firewall adeguato l'ADSL può esporre l'utente al rischio di aggressioni dall'esterno: in tale linea infatti viene generato un IP fisso e questo potrebbe essere identificato ed utilizzato per avere accesso al sistema ed ai dati presenti sui nostri dispositivi.
        <br><br>
        E’ importante a questo scopo far configurare il proprio router / firewall da tecnici specializzati: se acquisti un router/firewall presso di noi avrai tutta l'assistenza Fiberdroid che ti garantirà il massimo della sicurezza.
        <br><br>
        Visita la pagina Router con tutte  le nostre offerte
        https://fiberdroid.it/servizi/router
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Perchè asimmetrica?'])
    <p>
        Una  ADSL è una connessione asimmetrica perchè divide il range di frequenze disponibili in una linea telefonica in maniera non simmetrica tra la parte di ricezione e quella di trasmissione dei dati.
        <br><br>
        Assegna infatti una larghezza di banda/velocità di connessione maggiore per la ricezione (download) dei dati rispetto alla trasmissione (upload) dei dati supponendo che la maggior parte degli utenti “scarichi” molti più dati di quelli che invia in rete.
        <br><br>
        La velocità di download è il parametro più importante nell'uso di un servizio ADSL dal momento che le operazioni principali di cui si compone sono la riproduzione di filmati, lo scaricamento di file, la visualizzazione di pagine.
        <br><br>
        Per questo è il valore che viene indicato primariamente nelle offerte dei vari operatori: "ADSL 20 Mega" si riferisce quindi alla velocità di download.
        Bisogna però fare attenzione, perchè è un valore nominale, misura l'ampiezza massima di banda della connessione e non quella effettiva. Per questo è importante farsi comunicare dall’operatore la distanza dalla centrale pubblica e la banda stimata media oltre alla banda minima garantita, cioè  la velocità  minima sotto la quale la connessione non deve scendere.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Velocità di connessione - SPEED TEST'])
    <p>
        Come abbiamo visto uno dei principali parametri per capire se un'offerta sia valida è la velocità della linea Internet.
        <br><br>
        La prima operazione utile è verificare la velocità effettiva della nostra connessione Internet, collegando un solo computer al router ed eseguendo diversi speed test nell’arco della giornata, così da valutare correttamente la media della nostra banda.
        <br><br>
        Per misurare la velocità esistono diversi speed test, i quali misurano la velocità in download, la velocità in upload e il ping della vostra connessione. Quest'ultimo dato indica il tempo di latenza, cioè il tempo di risposta medio dal nostro computer al server selezionato per lo speed test.    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Fattori che incidono sulla velocità di connessione'])
    <p>
        Esistono diversi fattori che influenzano la velocità di connessione ad Internet, eccone una lista che ci aiuterà a comprendere quali ottimizzazioni possiamo intraprendere per navigare sfruttando al meglio il potenziale della nostra rete:
    </p>

    <ul class="list">
        <li>distanza della cabina o centrale telefonica dall'appartamento: essendo i cavi molto lunghi ci sarà una maggiore dispersione del segnale;</li>
        <li>numero troppo elevato di persone che stanno utilizzando Internet contemporaneamente: durante il lockdown abbiamo tutti constatato che la connessione era molto rallentata visto l'elevato numero di persone che si connettevano da casa alla rete. Questo vale sia per le connessioni ADSL sia per le connessioni fibra, anche se in misura minore.</li>
        <li>condizioni metereologiche: possono andare ad agire sulla cabina pubblica se non di ultime generazione, dove arriva il segnale del fornitore che viene instradato in tutte le abitazioni che hanno effettuato l'allaccio.</li>
        <li>computer, se troppo troppo datato;</li>
        <li>interferenza o dispersione del segnale nell'impianto di rete o wifi dell'appartamento;</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Quanti tipi di DSL ci sono?'])
    <p>
        L'ADSL è una tra le varie connessioni x-DSL esistenti. Oltre all'ADSL Fiberdroid fornisce anche le seguenti x-DSL:
    </p>
    <h3 class="accordion-subtitle">
        H-DSL (HIGH-BIT-RATE DIGITAL SUBSCRIBER LINE)
    </h3>
    <p>
        HDSL si riferisce ad una rete sviluppata nei primi anni Novanta ed è la prima delle tecnologie x-DSL ad essere stata sviluppata. Ancora oggi è una delle soluzioni più veloci in alternativa a quelle su fibra.
        <br><br>
        È un collegamento simmetrico ad alta velocità che viaggia su doppino dedicato, ossia su una linea per ogni utente, ed esiste solo per il traffico dati e non per quello voce anche se supporta il VoIP dal momento che quest'ultimo tratta la fonia vocale come qualsiasi altro dato che viaggi su Internet.
        <br><br>
        A differenza dell'ADSL la connessione è simmetrica nel senso che la velocità di download e di upload è uguale e consente di raggiungere velocità fino a 16 Mb/sec.
    </p>
    <h3 class="accordion-subtitle">
        V-DSL (VERY HIGH DIGITAL SUBSCRIBER LINE)
    </h3>
    <p>
        Il VDSL utilizza sia la fibra ottica che dalla centrale arriva fino ai cosiddetti armadi stradali o "cabinet" degli operatori (FTTC, Fiber To The Cabinet), sia il doppino in rame che dal cabinet arriva fino all'appartamento. Per questo motivo è anche chiamata soluzione "fibra misto rame"
        <br><br>
        Queta tecnologia permette di raggiungere velocità di connessione superiori rispetto all'ADSL, anche se ancora inferiori rispetto ad una connessione che avviene interamente su fibra ottica (anche chiamata FTTH, Fiber To The Home).
        <br><br>
        Il VDSL permette di avere un'ottimizzazione della soluzione ADSL senza stravolgere la rete dei cavi in rame dell'"ultimo miglio" verso gli appartamenti ed per questo motivo è la connessione più utilizzata in Italia. La velocità base generalmente è di 30 Mega e può poi arrivare anche fino a 100 o 200 Mega in download.
        <br><br>
        Vedi le soluzioni fibra misto rame di Fiberdroid
        <br>
        https://fiberdroid.it/servizi/fibra
    </p>
@endcomponent


@component('pages.components.accordion', ['title' => 'Requisiti minimi per l\'uso dell\'ADSL'])
    <p>
        Controlla la disponibilità della rete ADSL nella Tua zona mediante il nostro tool di verifica e
        assicurati di possedere un Router di ultima generazione ADSL/ADSL 2+ (non brandizzato con logo di altro operatore)
    </p>
@endcomponent
