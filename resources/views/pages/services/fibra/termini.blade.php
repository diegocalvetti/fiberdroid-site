<p>
    Costo di attivazione del servizio fibra FTTC: <i>79.90&euro;</i>
</p>
<p>
    Costo di attivazione del servizio fibra FTTH: <i>159.90&euro;</i>
    <br>
    Costo di disattivazione del servizio: <i>99.90&euro;</i>
    <br><br>
    Possibilità di acquisto pacchetti ip statici pubblici aggiuntivi.
    <br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br>
    Ogni servizio è attivabile previa <a target="_blank" href="{{ url('verifica-copertura') }}">verifica tecnica della copertura</a> .
</p>
