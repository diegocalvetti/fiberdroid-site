<p class="service-faq-title is-uppercase has-text-centered mb-4">Faq</p>

@component('pages.components.accordion', ['title' => 'Come funziona?'])
   <p>
       Il servizio di connettività viene realizzato tramite accesso all’<strong>infrastruttura di rete ottica</strong> (il cavo in fibra) presente sul territorio nazionale.
       <br><br>
       Questo tipo di connettività non si basa su infrastrutture IP, ma usa implementazioni di tipo layer 2 (tipicamente con terminazione Ethernet). Le capacità realizzabili nelle varie sedi periferiche variano da un minimo di 10 Mbps simmetrici fino a 10 Gbps simmetrico.
   </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Quali sono le possibilità per casa e ufficio?'])
    <h3 class="accordion-subtitle">FTTC</h3>
    <p>
        La connettività <strong>FTTC (Fiber To The Cabinet)</strong> si basa sulla disponibilità del collegamento al cavo della fibra ottica presso l'armadio su strada più vicino a casa tua.
        Una volta individuato l’armadio, utilizziamo il “classico” cavo in rame per l'ultimo tratto, ovvero fino a dentro l'abitazione, l’azienda o lo stabile.
        <br><br>
        Riusciamo a fornirti, in base alla distanza dall’armadio, fino a 200 Mbps in download e banda minima garantita fino a 20 Mbps, con 1 IPv4 pubblico statico incluso.
    </p>
    <h3 class="accordion-subtitle">FTTH</h3>
    <p>
        <strong>FTTH (Fiber To The Home)</strong> è la connessione ad internet in "pura" fibra, se la tua abitazione, ufficio o stabile è direttamente collegata al cavo in fibra ottica.
        <br><br>
        Riusciamo a fornirti fino a 1 Gbps in download e banda minima garantita fino a 50 Mbps, con possibilità di avere connessioni simmetriche e asimmetriche, con 1 IPv4 pubblico statico.
        <br><br>
        Predisponiamo di progetti taylor made, su misura, adattando tale soluzione a specifiche esigenze del cliente e della realtà aziendale.

    </p>
@endcomponent

