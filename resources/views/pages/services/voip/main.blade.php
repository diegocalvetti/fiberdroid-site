@component('pages.components.accordion', ['title' => 'Come funziona la tecnologia voip?'])
   <h3 class="accordion-subtitle">
       RETE TELEFONICA TRADIZIONALE – COMMUTAZIONE DI CIRCUITO
   </h3>
   <p>
       Nella rete telefonica tradizionale PSTN (Public Switched Telephone Network) viene utilizzata la commutazione di "circuito" in quanto la comunicazione tra due utenti è possibile riservando una "connessione" che li collega per tutta la durata della comunicazione.
       <br><br>
       Questo vuol dire che devono essere disponibili molte risorse per rendere fruibile il servizio a tutti gli utenti, comportando costi della connessione elevati. Il costo del traffico telefonico tradizionale è infatti a tempo.
       <br><br>
       Inoltre più gli utenti sono fisicamente lontani più risorse risultano occupate e maggiore sarà il costo della telefonata. Ed ecco perché il costo dipende fortemente anche dalla distanza (basti pensare ai costi delle chiamate internazionali).
       <br><br>
       Per fare un esempio, fino a circa gli anni '60 se una telefonata durava 10 minuti veniva dedicato un collegamento costituito da connessioni di doppini di rame che coprissero tutta la tratta da un capo all'altro della telefonata per tutta la durata della chiamata.
       <br><br>
       I costi delle telefonate erano notevoli e aumentavano vertiginosamente all'aumentare della distanza: se i due utenti distavano 1000 km, per 10 minuti utilizzavano 1000 km di doppino in rame a loro esclusivamente riservati.
       <br><br>
       Oggi giorno la rete telefonica tradizionale è diventata più efficiente e i costi si sono ridotti rispetto al passato. La voce viene digitalizzata e la conversazione di un utente può essere combinata con quella di migliaia di altri in un singolo cavo di fibra ottica per la maggior parte del tragitto (anche se c'è sempre una parte di trasmissione legata al doppino in rame nell'ultima tratta verso l'appartamento dell'utente).
       <br><br>
       La rete tradizionale però nonostante questi miglioramenti risulta ancora molto poco competitiva a causa della tecnologia di commutazione a circuito utilizzata che pur essendo solida, affidabile e garantendo un'ottima qualità della comunicazione non permette uno sfruttamento efficiente delle risorse di rete e comporta quindi costi decisamente elevati.
   </p>

   <h3 class="accordion-subtitle">
       VOIP – COMMUTAZIONE DI PACCHETTO
   </h3>
    <p>
        Il protocollo VoIP utilizza la tecnologia di commutazione a pacchetto: questo vuol dire che un messaggio viene suddiviso in più parti (pacchetti) prima di essere inviato in rete, e una volta arrivati a destinazione i pacchetti vengono riassemblati nell'ordine corretto.
        <br><br>
        I pacchetti vengono inoltrati in rete attraverso un percorso non definito a priori. Ogni pacchetto seguirà una propria strada venendo trasmesso tra i vari nodi della rete prima di raggiungere la destinazione finale.
        <br><br>
        In questo modo viene ottimizzato l'utilizzo delle risorse di rete dal momento che il percorso tra due nodi della rete non viene assegnato in modo esclusivo ad una comunicazione ma può essere utilizzato contemporaneamente per la trasmissione di più comunicazioni.
        <br><br>
        Una curiosità: La commutazione di pacchetto si sviluppa nel periodo della Guerra Fredda per garantire l'inoltro di informazioni anche in caso di scomparsa di alcuni nodi della rete.
        <br><br>
        L'idea alla base era che ogni pacchetto in base alla disponibilità di nodi liberi potesse decidere autonomamente quale percorso seguire senza che questo venisse stabilito in precedenza: in questo modo se una porzione di rete veniva improvvisamente a mancare il pacchetto poteva scegliere una strada alternativa tra quelle ancora disponibili.
        <br><br>
        Questa scelta dettata da esigenze di sicurezza in un periodo di guerra ha in realtà avuto come conseguenza la possibilità di sfruttare al meglio le risorse di rete: non avendo un percorso prestabilito i pacchetti possono utilizzare di volta in volta i nodi liberi in quel momento velocizzando ed ottimizzando la trasmissione.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Vantaggi del voip'])
    <p>
        Come abbiamo detto uno dei vantaggi di questa tecnologia è che essa elimina l'obbligo di riservare della banda per ogni telefonata sfruttando l'allocazione dinamica delle risorse.
        <br><br>
        Inoltre i pacchetti di dati contenenti le informazioni vocali vengono codificati in forma digitale e instradati in rete solo nel momento in cui uno degli utenti sta realmente parlando (eliminando così l'inutile trasmissione di quelle parti di conversazione in cui entrambi gli interlocutori sono in silenzio e riducendo così la quantità di informazione da trasmettere).
        <br><br>
        Il VoIP permette poi di utilizzare risorse di rete già esistenti consentendo quindi una notevole riduzione dei costi sia per i privati che per le aziende:
        <br><br>
    </p>
    <ul class="list">
        <li>nel primo caso il privato può sfruttare un collegamento ad Internet a banda larga (già esistente e sempre attivo) per effettuare chiamate telefoniche economicamente molto vantaggiose (specialmente per le chiamate internazionali)</li>
        <li>nel secondo caso un'azienda potrà utilizzare la rete aziendale LAN o rete metropolitana MAN già esistente per le comunicazioni vocali, riducendo drasticamente le spese di comunicazioni interaziendali.</li>
    </ul>
    <p>Ecco in sintesi i principali vantaggi che si possono avere scegliendo una soluzione VoIP:</p>
    <ul class="list">
        <li>
            riduzione dei costi delle telefonate
            <ul class="sub-list">
                <li>non vi è più differenza tra chiamate locali e a lunga distanza</li>
                <li>è possibile fare telefonate gratuite tra utenti della stessa rete</li>
                <li>è possibile utilizzare una rete LAN per le comunicazioni interaziendali</li>
            </ul>
        </li>
        <li>delocalizzazione del numero</li>
        <li>possibilità di avere diversi numeri telefonici su un unico apparecchio telefonico</li>
    </ul>
    <p>
        Il numero di telefono non è più legato alla sede fisica: il cliente configura il proprio numero sul telefono IP e collegandolo alla rete diventa raggiungibile indipendentemente dalla sede in cui si trova. Non si è più vincolati ad un cavetto di rame. Un esempio? Se andate in vacanza dove avete una connessione Wi-Fi, con un telefono che abbia un client VoIP portate con voi in vacanza il vostro numero di casa.
        <br><br>
        Aggiungere o spostare numerazioni telefoniche sarà semplice e veloce, non perderete i vostri numeri di rete fissa nazionale o ne aggiungerete facilmente uno nuovo, il numero delle possibili nuove linee dipende solo dalla capacità della linea internet. Pensate alla comodità di decidere quando volete di dedicare un numero solo ai fornitori, uno all’assistenza ecc...
        <br><br>
    </p>
    <ul class="list">
        <li>possibilità di salvare messaggi vocali sul proprio pc</li>
        <li>deviazione di chiamata (per esempio fuori orario vanno su cellulare)</li>
        <li>inoltro a un cellulare senza che si veda il numero ricevente la chiamata</li>
        <li>chiamata in attesa</li>
        <li>chiamata a tre o più interlocutori</li>
        <li>integrazione tra segreteria telefonica ed email (i messaggi possono arrivare via mail come allegati)</li>
        <li>attribuzione di un numero virtuale in un altro Paese con ridirezione via Internet verso l'Italia</li>
        <li>non essendo necessario alcun intervento in sede o sopralluogo l’attivazione sarà semplice e veloce.</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Requisiti minimi'])
<p>
    Per poter scegliere di utilizzare la tecnologia VoIP è necessario avere:
</p>
    <ul class="list">
        <li>Una connessione Internet (preferibilmente flat)</li>
        <li>Computer con cuffie o casse e microfono</li>
        <li>Smartphone con nostra APP (Client VoIP / Soft-phone) installato</li>
        <li>Un modem/router VoIP (come ad esempio il Fritz!) per fare e ricevere telefonate direttamente dai tuoi telefoni di casa con il computer spento.</li>
    </ul>
@endcomponent


@component('pages.components.accordion', ['title' => 'Number portability'])
    <p>
        Nel caso il cliente sia già in possesso di una numerazione telefonica di rete fissa e desideri mantenere tale numerazione può richiedere a Fiberdroid di usufruire del servizio di number portability che consente di trasferire il proprio numero geografico mantenendo la numerazione.
        <br><br>
        Tale richiesta disattiverà tutti i servizi di telefonia erogati dal precedente operatore ed è quindi bene ricordare che è necessario attivare un nuovo contratto dati (Adsl, Fibra o Wireless) per poter usufruire della tecnologia VoIP.
        <br><br>
        Alcuni dettagli relativi a tale passaggio:
    </p>
    <ul class="list">
        <li>la richiesta di portabilità del numero può essere fatta una volta che il nuovo contratto VoIP sia già attivo</li>
        <li>l'attuale gestore deve fornire il codice di sblocco della numerazione geografica</li>
        <li>per il trasferimento del numero sono necessari mediamente 10/15 giorni dalla richiesta</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Numerazioni raggiunte'])
    <p>Le chiamate che possono essere effettuate sono:</p>

    <ul class="list">
        <li>rete fissa nazionale, locali e interurbane</li>
        <li>rete mobile italiana</li>
        <li>reti fisse internazionali</li>
        <li>rete mobile estera</li>
        <li>numerazioni di emergenza</li>
        <li>numeri verdi</li>
        <li>numeri speciali</li>
    </ul>
@endcomponent

<div style="position: relative; width: 1230px; max-width: 1920px" class="my-6 mx-auto hidden-tablet-only hidden-mobile">
    <img class="mt-6" alt="Telefono VoIP con 3cx" src="{{ url('images/products/banner/banner_voip.webp') }}">

    <p class="voip-banner-text" style=" position: absolute; top: 215px; left: 69px">
        Rimani disponibile <br>
        sul posto di lavoro <br>
        e in movimento.
    </p>

    <a class="button is-fd-primary" aria-label="Vedi i telefoni di Fiberdroid" href="{{ url('prodotti/telefoni-voip') }}" style="position: absolute; bottom: 50px; left: 69px">
        <p>
            Vedi i telefoni
        </p>
    </a>
</div>
