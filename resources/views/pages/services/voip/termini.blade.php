<p>
    Costo di attivazione di un numero voip singolo portato o nuovo: <i>19.90&euro;</i>
    <br>
    Costo di attivazione del GNR 10 Numeri portato o nuovo: <i>29.90&euro;</i>
    <br>
    Costo di attivazione del GNR 20, 30 Numeri nuovi: <i>39.90&euro;</i>
    <br>
    Costo di attivazione del GNR 50 Numeri nuovo: <i>49.90&euro;</i>
    <br>
    Costo di attivazione del GNR 100 Numeri portato o nuovo: <i>69.90&euro;</i>
    <br>
    Costo di attivazione del GNR 200 Numeri nuovo: <i>79.90&euro;</i>
    <br>
    Costo di attivazione del GNR 1000 Numeri nuovo o portato: <i>250.00&euro;</i>
    <br><br>
    Telefonate in tutta <b>Italia</b> verso fissi a <b>0,007 Euro/Min</b> con conteggio al secondo e senza scatto
    <br>
    Telefonate in tutta <b>Italia</b> verso mobili a <b>0,065 Euro/Min</b> con conteggio al secondo e senza scatto
    <br>
    Costo di disattivazione del servizio: 19.90€
    <br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br><br>
    Consulta il <a target="_blank" href="{{ url('/listini') }}">listino prezzi</a>.
</p>
