<p>
    Sei libero di scegliere ed installare il router che preferisci, senza essere vincolato a dispositivi specifici. Tieni presente che per ottenere le migliori prestazioni di rete, il tuo router deve soddisfare specifiche caratteristiche tecniche. Se opti per un dispositivo proprietario, assumi la responsabilità della conformità e della manutenzione del dispositivo. Fiberdroid ti fornirà assistenza tecnica solo per problemi legati alla linea Fiberdroid, non per la sostituzione del dispositivo in caso di guasto o problemi di linea associati a quest ultimo.
</p>
