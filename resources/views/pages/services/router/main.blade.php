@component('pages.components.accordion', ['title' => 'Router/Modem Significato'])
   <p>
       Il termine router viene spesso confuso con il termine modem, anche perchè il router integra anche le funzioni di modem nella maggior parte dei casi, come nel caso dei due apparati che abbiamo proposto più sopra.
       <br>
       Usare in maniera intercambiabile i due termini è però tecnicamente sbagliato.
       <br><br>
       Capire cosa sono e a cosa servono modem e router può aiutarci nel momento in cui dobbiamo scegliere quale apparato fa più al caso nostro.
       Se infatti dobbiamo connettere alla rete un solo computer allora un buon modem ci basterà, se all'opposto dobbiamo creare una LAN locale, cioe‘ una rete di computer e‘ necessario un router, meglio ancora con firewall integrato.
   </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Modem'])
   <p>
       Per poter sfruttare la linea telefonica è necessario che i dati del computer siano convertiti in segnali analogici. Il modem è il dispositivo che compie questa operazione, gestendo lo scambio di informazioni indispensabili per la navigazione in Internet.

       Il suo nome deriva proprio dalle due operazioni che compie: modulazione, nella direzione dal computer verso la rete, e demodulazione, quando riconverte il segnale analogico dal doppino in segnale digitale verso il computer.

       I collegamenti necessari in un modem sono:
   </p>
   <ul class="list">
       <li>una porta per collegarsi attraverso la linea telefonica alla rete</li>
       <li>una o più porte Ethernet per interfacciarsi con i vari dispositivi presenti nell'appartamento o con il router se presente.</li>
   </ul>
   <p>
       In conclusione quindi, se si ha la necessità di collegarsi ad Internet è necessario acquistare un modem (o un router che abbia anche la funzione di modem integrata).
   </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Router'])
    <p>
        Il router (o instradatore) è l'apparato che gestisce lo scambio di informazioni tra i vari dispositivi di una rete locale e non è fondamentale per la navigazione in Internet.
        <br><br>
        In pratica il router gestisce i protocolli necessari affinchè i pacchetti "dati" raggiungano la giusta destinazione nel giusto ordine: mediante le routing table (tabelle di indirizzamento)   i pacchetti in transito possono scegliere il percorso più breve per raggiungere la loro destinazione.
        <br><br>
        È dotato di una delle due seguenti modalità di comunicazione attraverso le quali permette la condivisione di connessione tra dispositivi di vario tipo (computer, tablet, smartphone):
        <br><br>
        <span class="pl-5">1. <strong>porte Ethernet</strong>, per una connessione fisica via cavo tra router e apparati.</span>
        <br><br>
        In questo caso essendo una connessione diretta al dispositivo, la maggioranza dei pc riconosce le impostazioni migliori e ottimizza la connessione mediante il meccanismo del "plug and play"
        <br><br>
        <span class="pl-5">2. <strong>senza fili in modalità Wi-Fi</strong>, utilizzando le onde radio per comunicare con gli apparati.</span>
        <br><br>
        Occorre in questo caso impostare una password per proteggere la rete domestica dall'accesso da parte di malintenzionati.
        <br><br>
        Inoltre vengono usati vari protocolli di protezione per crittografare tutti i dati in transito: i più utilizzati sono WEP (Wired Equivalence Privacy), WPA (Wi-Fi Protected Access),  WPA2 (Wi-Fi Protected Access 2) e WPA2 (Wi-Fi Protected Access 2) in ordine crescente di livello di protezione.
        <br><br>
        Nel caso più frequente in cui si desideri avere entrambe le funzionalità di modem (connessione ad Internet) e router (rete LAN locale di più dispositivi) quest'ultimo farà da interfaccia tra il modem connesso alla rete e i dispositivi della rete locale permettendo l'utilizzo contemporaneo della stessa connessione Internet ai vari devices.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Router Wi-Fi'])
    <p>
        Dal momento che il router Wi-Fi sfrutta le onde radio per comunicare con i dispositivi la propagazione del suo segnale può essere soggetta a varie interferenze:
    </p>
    <ul class="list">
        <li>caratteristiche dell'edificio in cui viene installato, muri spessi infatti possono attenuare il segnale.</li>
        <li>interferenza radio dovuta spesso a forni a microonde o ad altri apparecchi radio</li>
        <li>strutture metalliche presenti negli ambienti in cui si dovrebbe propagare il segnale</li>
    </ul>
    <p>
        Per cercare di ridurre queste interferenze si può procedere nel seguente modo:
    </p>
    <ul class="list">
        <li>cambiare il canale di trasmissione del router cioè scegliere una nuova frequenza radio mediante il pannello di controllo del dispositivo</li>
        <li>riposizionare il router in modo da evitare che ostacoli fisici o interferenze radio presenti nell'ambiente attenuino il segnale.</li>
    </ul>
    <p>
        In alcuni casi poi un cattivo funzionamento del router può non essere causato da interferenze ma da un'eccessiva temperatura: è bene dunque fare anche un controllo della temperatura e nel caso fosse eccessiva tenere il router lontano dal riscaldamento, fonti di calore in genere e non posizionarlo in luoghi non areati come ad esempio all'interno di mobili chiusi.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Ti presentiamo DrayTek'])
    <p>
        Fondata nel 1997 da un gruppo di ingegneri di talento si è ora affermata come fornitore leader mondiale di soluzioni di networking.
        <br><br>
        Le soluzioni <a target="_blank" rel="noopener" href="https://www.draytek.com/">Draytek</a> comprendono soluzioni che vanno dal semplice router al firewall aziendale ad alta affidabilità.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Ti presentiamo AVM'])
    <p>
        Azienda tedesca di elettronica fondata nel 1986 a Berlino, conosciuta per i prodotti DSL, ISDN, WiFi, VoIP e LTE destinati al mercato consumer, SoHo e SMB.
        <br><br>
        Detiene il 60% del mercato tedesco con i suoi popolari FRITZ!Box e gli accessori tra cui telefoni cordless, ripetitori wireless e dispositivi powerline.
        <br><br>
        <a target="_blank" rel="noopener" href="https://it.avm.de/">AVM</a> commercializza in Italia le sue soluzioni per la connettività internet, di rete e la telefonia con 5 anni di garanzia fornita dal produttore.
        <br><br>
        Tutte le connettività Fiberdroid sono perciò router-free, sostengono l’iniziativa “modem libero” (<a target="_blank" rel="noopener" href="https://www.modemlibero.it">www.modemlibero.it</a> ) e ti lasciano libero di scegliere ed installare il router che più desideri, senza vincolarti ad usare apparati specifici.
    </p>
@endcomponent
