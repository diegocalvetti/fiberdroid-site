<p class="service-faq-title is-uppercase has-text-centered mb-4">Faq</p>

@component('pages.components.accordion', ['title' => 'Assistenza tecnica dedicata'])
    <p>
        Il vantaggio di Fiberdroid, oltre a velocità e sicurezza è indubbiamente l'assistenza tecnica dedicata al singolo e non al prodotto. Questo significa che per tutti i prodotti acquistati da fiberdroid riceverai assistenza sempre dalla stessa persona, permettendoci così di ridurre al minimo i tempi di supporto tecnico e allo stesso modo garantire la massima qualità del servizio.
        Tu per noi non sei un numero, ma impariamo a conoscerti così da rispondere al meglio alle tue necessità
        <br><br>
        Non a caso Fiberdroid vanta il 90% di chiusura dei ticket dopo 4 ore dall'apertura!
        <br><br>
        Le aziende che si affidano a noi possono vantare le ultime tecnologie in fatto di telecomunicazioni, per rimanere sempre al passo con le esigenze che il mercato di oggi richiede in termini di affidabilità, sicurezza e prezzo.
        Una delle caratteristiche distintive di Fiberdroid è proprio quella di raggiungere anche aziende in luoghi più remoti, in cui la fibra scarseggia, portando connessioni che raggiungono le stesse velocità e ugualmente affidabili.
        Le nostre soluzioni di servizi tecnologici per le aziende non comprendono solo la fibra, ma offriamo un pacchetto completo a seconda delle tue esigenze, in questo modo possiamo essere tuoi partner unici per i servizi tecnologici in azienda e offrirti un'assistenza dedicata.
        <br><br>
        Sulla base della nostra esperienza e quelle delle aziende che si sono affidate a noi, abbiamo individuato 3 categorie di servizi. Sfrutta questa divisione per individuare velocemente i servizi consigliati per te. Ricorda che le offerte fiberdroid sono sempre personalizzabili a seconda delle tue esigenze.
        Se hai il dubbio, o se vuoi semplicemente verificare se la tua zona è coperta da connettività internet puoi usare il nostro tool di verifica della copertura di rete.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Aziende con singola sede'])
    <p>
        I nostri pacchetti di servizi, coprono a 360 gradi le esigenze informatiche attuali. Il pacchetto completo di Fiberdroid include tutti i servizi essenziali per una azienda moderna spaziando dalla connettività internet in fibra ottica a banda larga, fino a un centralino telefonico cloud 3cx a chiamate illimitate con cui creare numeri interni per i dipendenti senza perdersi la possibilità di fare chiamate esterne a tariffe convenienti anche verso numeri esteri.
        Una connessione a banda larga è fondamentale per una azienda al giorno d'oggi. Fibedroid è in grado di portare le connessioni migliori sul mercato, ad un prezzo competitivo.
        <br><br>
        Le soluzioni di connettività in fibra ottica di Fiberdroid partono da 30mega in download e 3mega in upload fino a 1giga in download e 30 mega in upload. Tutte le soluzioni sono disponibili sia in Italia che all'estero e sia in FTTC che in FTTH a seguito di una verifica della copertura che puoi fare tu stesso con il nostro  tool di verifica della copertura di rete.
        <br><br>
        Per scoprire di più ti lasciamo il link alla sezione di Fiberdroid riguardante la fibra ottica in cui puoi trovare tutti i dettagli tecnici compresi prezzi e modalità di installazione.
        <br>
        offerte fibra ottica Fiberdroid
        <br><br>
        Se una fibra ottica top di gamma, ancora non ti basta, è possibile richiedere un preventivo per l'installazione di una linea di fibra dedicata interamente riservata alle tue utenze! Garantendo così una banda MCR del 99,99%
        <br>
        offerte fibra ottica dedicata Fiberdroid
    </p>
    <p class="is-uppercase mt-6 mb-4"><strong>CENTRALINO TELEFONICO CLOUD 3CX</strong></p>
    <p>
        Terzo ma non per importanza, un centralino telefonico cloud moderno come il 3cx. Un centralino telefonico cloud ti da la possibilità di creare interni riservati per i tuoi dipendenti e di effettuare chiamate illimitate (sia tra gli stessi interni, sia in uscita) anche verso numeri esteri ad un prezzo tra i più competitivi sul mercato. Inoltre rispetto a un tradizionale centralino telefonico è già inclusa una comoda interfaccia web con la quale amministrare le utenze, senza richiedere ogni volta l'intervento di un tecnico.
        <br><br>
        Ecco alcuni dei processi da poter gestire in autonomia attraverso l'interfaccia:
    </p>
    <ul class="list">
        <li>Integrazione con tutti i principali provider VoIP, al fine di lasciare al cliente la libertà di scelta del carrier preferito</li>
        <li>Gestione e personalizzazione dei numeri interni, con possibilità di deviazione automatica chiamate in ingresso, gestione orario di lavoro e messaggi automatici in risposta</li>
        <li>Risponditore automatico con possibilità di post-selezione e musica di attesa</li>
        <li>Possibilità di selezione passante per interni</li>
        <li>Gruppi personalizzati di risposta e gestione code chiamate</li>
        <li>Possibilità di noleggio dei telefoni con assicurazione Kasko</li>
        <li>Assistenza, modifiche e personalizzazioni in remoto incluse nel canone</li>
        <li>Ecco il link della sezione di Fiberdroid dedicata al centralino telefonico cloud 3cx in cui potrai trovare tutte le specifiche tecniche e consultare il listino dei canoni</li>
    </ul>

    <p class="is-uppercase mt-6 mb-4"><strong>Backup</strong></p>
    <p>
        In una azienda moderna è indispensabile un servizio di backup di rete, affidandosi ai ponti radio, Fibedroid è in grado di darti un servizio sicuro e affidabile per far sì che qualsiasi eventuale disservizio delle linee internet non faccia rimanere allo scoperto la tua azienda neanche un istante.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Aziende multi sede'])
    <p>
        Per quanto riguarda la connettività, Fiberdroid è in grado di portare la stessa rete WiFi in tutte le sedi, anche all'estero portando fibra ottica fino a 1giga in download e 30mega in upload a un prezzo competitivo.
        <br>
        <a target="_blank" rel="noopener" href="{{ url('servizi/fibra') }}">Offerte fibra ottica Fiberdroid</a>

        <br><br>
        Su progetto è anche possibile richiedere il preventivo per una fibra dedicata interamente riservata alle tue utenze per una garanzia di banda MCR del 99,99%
        <br>
        <a target="_blank" rel="noopener" href="{{ url('servizi/fibra-dedicata') }}">Offerte fibra dedicata Fiberdroid</a>

        <br><br>
        Inoltre fiberdroid include il moderno centralino telefonico cloud 3cx con cui creare e gestire da una comoda interfaccia web tutti i processi(creare interni, risponditori automatici, creare passanti...) per tutte le sedi.
        Con il centralino è possibile effetuare chiamate tra interni e verso l'esterno, compreso l'estero a un canone tra i più competitivi sul mercato.
        Fiberdroid permette anche l'integrazione con tutti i principali provider VoIP, al fine di lasciare al cliente la libertà di scelta del carrier preferito. Di seguito il link alla nostra sezione dedicata ai centralini telefonici cloud in cui potrai trovare tutti i dettagli tecnici e i consultare i vari canoni.
        <br>
        <a target="_blank" rel="noopener" href="{{ url('servizi/centralino-cloud') }}">Offerte centralini telefonici in cloud di Fiberdroid</a>
        <br><br>
        Fiberdroid è anche in grado di fornirti gli strumenti, e un assistenza dedicata per installare dei servizi di backup di rete per ogni sede, sfruttando i ponti radio infatti riusciamo a offrire dei servizi completamente affidabili per fare si che anche in caso di disservizi delle linee internet le tue aziende non rimangano scoperte neanche un istante.
        Se qualcuno trancia i tuoi cavi non c’è problema, riusciamo a tenerti connesso.
        <br>
        <a target="_blank" rel="noopener" href="{{ url('servizi/wireless') }}">Offerte wireless Fiberdroid</a>
    </p>
@endcomponent
@component('pages.components.accordion', ['title' => 'Aziende vittime del digital divide'])
    <p>
        Dove le connessioni a banda larga non arrivano, Fiberdroid è in grado di portare una connessione wireless paragonabile a una fibra ottica moderna con lo stesso grado di affidabilità.
        Infatti il pacchetto completo di Fiberdroid è diviso in sezioni proprio per permettere anche alle aziende che si trovano in luoghi non raggiunti da connettività in fibra ottica di poter creare un pacchetto di servizi che sfrutti una rete ugualmente veloce e con lo stesso grado di affidabilità.
        <br><br>

        <strong>Il wireless arriva ovunque? E il satellitare?</strong>

        <br><br>
        Infatti il pacchetto completo di Fiberdroid è diviso in sezioni proprio per permettere anche alle aziende che si trovano in luoghi non raggiunti da connettività in fibra ottica di poter creare un pacchetto di servizi che sfrutta una rete ugualmente veloce e con lo stesso grado di affidabilità.
        In particolare per le aziende vittime di digital-divide disponiamo di servizi wireless che sfruttando i ponti radio riescono a raggiungere grandi ampiezze di banda per avere comunque una connettività moderna, veloce e affidabile. Il wireless, inoltre, ha il grande vantaggio di avere tempi di installazione relativamente brevi.
        <br>
        <a target="_blank" rel="noopener" href="{{ url('servizi/wireless') }}">Offerte wireless Fiberdroid</a>

        <br><br>
        Se il wireless, per le sue caratteristiche continua non essere la tipologia di connettività adeguata per la tua azienda, è possibile richiedere un preventivo per l'installazione di una linea di fibra dedicata interamente riservata alle tue utenze! Garantendo così una banda MCR del 99,99%
        <br>
        <a target="_blank" rel="noopener" href="{{ url('servizi/fibra-dedicata') }}">Offerte fibra ottica dedicata Fiberdroid</a>
        <br><br>
        Fiberdroid: mettiamo il mondo nelle tue mani. ovunque!
    </p>
@endcomponent

