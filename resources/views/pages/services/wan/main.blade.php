<p class="service-faq-title has-text-centered mb-4">Quando la tua azienda si espande</p>

@component('pages.components.accordion', ['title' => 'Rete MAN o rete metropolitana, per l\'azienda che apre più sedi nella stessa città'])
    <p>
        Una rete Metropolitana (MAN) è una infrastruttura “virtuale” che collega le filiali della tua azienda, nella stessa zona metropolitana.
        <br><br>
        Con la MAN i tuoi uffici anche distanti centinaia di metri lavoreranno in sicurezza come se fossero all'interno dello stesso edificio:
    </p>
    <ul class="list">
        <li>Senza Firewall</li>
        <li>senza particolari apparati</li>
        <li>con lo stesso indirizzo IP</li>
        <li>Senza Vpn</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Vantaggi delle reti MAN/WAN – La proposta di Fiberdroid'])
    <p>
        Una rete geografica, sia MAN che WAN,  ti permette di far lavorare uffici molto distanti tra loro senza dover fare importanti investimenti hardware in sicurezza e connettività.
        <br><br>
        Con la soluzione di Fiberdroid, avrai:
    </p>
    <ul class="list">
        <li>Circuiti fisici e logici dedicati con bande fino a 1 Gigabit simmetrici. Massima velocità per il tuo Business!</li>
        <li>Backup decentralizzati, Cioè un secondo backup di supporto che si effettua in un’altra sede (possibilmente lontana dalla prima), il massimo della sicurezza in ogni situazione!</li>
        <li>Dati localizzati su server nella sede principale: ciò evita alle sedi periferiche investimenti hardware. Collegandosi alla sede principale  non si avranno rallentamenti usando programmi o attingendo ai dati dei progetti, sarà come “essere” fisicamente accanto ai colleghi della sede principale. Condivisione ed efficienza per la tua azienda!</li>
        <li>Niente firewall o apparati di sicurezza, la risposta è la centralizzazione, grazie a questo concetto si può lavorare meglio e con maggiore sicurezza, i Vostri dati non transitano su Internet ma viene creata ad-hoc una rete dedicata, chiusa, protetta e veloce.</li>
        <li>Progetto dedicato. Basta soluzioni standard che si scontrano con le difficoltà di ogni diversa situazione. Rete preesistente, dimensione aziendale, topologia della rete stessa, posizione sul territorio delle varie sedi,.. Ogni Azienda è diversa e merita una proposta dedicata!</li>
        <li>Assistenza dedicata. La tua Azienda verrà seguita passo passo durante qualunque problematica possa verificarsi. Come partner unico rispondiamo a tutte le tue domande e ti guidiamo verso le soluzioni dei tuoi problemi, senza call center e senza rimbalzare tra un referente e l'altro.</li>
    </ul>
@endcomponent
@component('pages.components.accordion', ['title' => 'Come lavora Fiberdroid, il tuo partner unico'])
    <p>
        Noi di Fiberdroid utilizziamo le dorsali in fibra sul territorio per realizzare collegamenti tra le tue reti locali (LAN).
        <br><br>
        Usiamo un circuito virtuale detto "layer 2" che consente una banda riservata per l'intera connessione e anche una velocità di instradamento superiore che si traduce per te in migliore qualità del servizio e permette connessioni fino a 1 Gbps simmetrici. Il "layer 2 " viene considerato la base della Network Security poiché il Livello 2 (Data Link Layer) fornisce il servizio di trasferimento affidabile dei dati su di un mezzo fisico.
        <br><br>
        Usiamo dove possibile la fibra ottica spenta per velocizzare la tua connessione evitando la condivisione della banda o il costo della posa di nuove fibre.
        <br><br>
        Con questa tecnologia è possibile collegare in sicurezza le diverse sedi, sfruttando sia la condivisione di dati e programmi che la decentralizzazione del backup (che come detto prima garantisce un alto livello di sicurezza) e quindi ridurre gli investimenti hardware per la sicurezza e VPN di ogni sito.
        <br><br>
        L’assistenza e la progettualità avanzata e innovativa di Fiberdroid è il nostro miglior biglietto da visita. Il cliente non acquista con noi delle semplici connessioni ma usufruisce di un team di professionisti che analizzando e studiando in modo approfondito le sue esigenze proporrà la migliore soluzione possibile.
        <br><br>
        La soluzione di rete geografica, può essere attivata con diverse tecnologie in base alla disponibilità di zona.
        <br><br>
        E’ consigliato ovviamente l’uso della fibra simmetrica, ma è sempre possibile utilizzare per piccole filiali anche connettività in rame o wireless.
        <br><br>
        Il nostro team saprà comprendere al meglio le Tue richieste e fornirti la soluzione più idonea.
        <br><br>
        Questa tipologia di servizio e piattaforma non ha un listino prezzo, ma necessità di sopralluoghi, studi e analisi, per questo è necessario che l’azienda che necessita di questa soluzione ci scriva alla mail : speed@fiberdroid.it oppure ci chiami al numero +39 02 83595676 così da pianificare un primo incontro tecnico per iniziare la strutturazione del progetto.
    </p>
    <br><br>
    <p>
        Il futuro è già qui, vuoi esplorarlo con Fiberdroid?
    </p>
@endcomponent

<h2 class="service-faq-title has-text-centered mb-4 mt-6 pt-6">Glossario delle reti</h2>

@component('pages.components.accordion', ['title' => 'LAN'])
    <p>
        Local Area Network o rete locale di computer e periferiche condivise. La LAN è una rete informatica (tecnologie più usate attualmente sono Ethernet e WiFi) limitata a un ufficio e può estendersi fino ad alcuni edifici adiacenti.   Permette di trasferire dati ad alta velocità e non necessita di linee telefoniche, di condividere le periferiche (stampanti,hard disk ecc.), di condividere software e database aziendali, si possono automatizzare i backup su server remoti.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Intranet'])
    <p>
        Una Intranet è di norma una LAN a cui gli utenti esterni non possono accedere ma da cui gli utenti interni accedono all'esterno (internet)
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'LAN'])
    <p>
        Una Intranet è di norma una LAN a cui gli utenti esterni non possono accedere ma da cui gli utenti interni accedono all'esterno (internet)
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Firewall'])
    <p>
        Letteralmente "parete tagliafuoco" è lo strumento per la difesa di una rete o parte di essa da parte.  Può essere software o  hardware. Normalmente impedisce l'ingresso in un'area e monitora entrate e uscite.    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Indirizzo IP'])
    <p>
        Codice numerico univoco che permette a tutti i dispositivi (host) connessi a internet tramite Internet Protocol (IP) di comunicare fra loro ricevendo e inviando dati. con il sistema iniziale Ipv4 si hanno 4 blocchi di numeri (tra 0 e 255) che consentono  4 miliardi di combinazioni. esempio di IPv4  171.255.125.10
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'CAN'])
    <p>
        Codice numerico univoco che permette a tutti i dispositivi (host) connessi a internet tramite Internet Protocol (IP) di comunicare fra loro ricevendo e inviando dati. con il sistema iniziale Ipv4 si hanno 4 blocchi di numeri (tra 0 e 255) che consentono  4 miliardi di combinazioni. esempio di IPv4  171.255.125.10    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'MAN Metropolitan Area Network'])
    <p>
        Rete in area metropolitana che connette più LAN. A differenza della LAN  la MAN è una rete che copre un'area geografica più vasta, in genere cittadina. Se si collegano più MAN si arriva ad avere una WAN.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'WAN Wide Area Network'])
    <p>
        E' una rete geografica cioè una rete di telecomunicazioni estesa su un'area molto vasta usando backbone). In pratica si basa sull'affitto di dorsali già preesistenti collegate con router alle  MAN e LAN sparse sul territorio. Esistono anche molte WAN private (esercito, aziende) è utile per condividere software e database anche da uffici molto distanti. Il fornitore globale di soluzioni di internetworking Cisco, prevede che la richiesta di larghezza di banda WAN per le applicazioni business crescerà a un ritmo annuale del 20% medio nei prossimi anni.
        <br><br>
        Una rete WAN è una rete il cui compito è fondamentalmente quello di interconnettere diverse LAN o singoli host separati da distanze geografiche. Diversamente dalle LAN, per realizzare una rete geografica è necessario affidarsi a un ISP (Internet Service Provider) che fornisce i servizi di connessione a livello fisico e data-link.
        <br><br>
        Di seguito una tabella riassuntiva delle distanze coperte da ciascuna tipologia di rete.
        Una WAN permette di collegare per esempio un ufficio o filiale posto in un'altra città all'ufficio centrale di una grande azienda.    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Fibra ottica spenta'])
    <p>
        Nelle telecomunicazioni per fibra ottica spenta si intende una connessione fisica in fibra ottica in cui sono stati predisposti solo i cavi, non le apparecchiature  per trasmettere il segnale ottico.    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Porta WAN'])
    <p>
        Una porta WAN è la porta sul router usata per collegarlo col modem con un cavo Ethernet (e quindi per collegarsi a internet).
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Internet'])
    <p>
        E' la rete che unisce le reti. Internet a volte viene definito la più grande WAN esistente e più spesso come una GAN
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'GAN Global Area Network'])
    <p>
        E' una rete universale che comprende le WAN collegate con fibra ottica e  che usa collegamenti intercontinentali  come i cavi sottomarini intenazionali (dorsali di rete transoceaniche) o le trasmissioni satellitari per unirle tra continenti.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Dorsali o backbone'])
    <p>
        Letteralmente "spina dorsale", normalmente sono collegamenti in fibra ottica tra LAN e/o MAN su zone molto vaste per consentire uno scambio veloce di dati su lunghe distanze. Possono arrivare ad essere su distanze intercontinentali utilizzando cavi sottomarini come tra Europa e Stati Uniti. In Italia una backbone importante, è la GARR, che collega l'università Federico II di Napoli a New York.
    </p>
@endcomponent
@component('pages.components.accordion', ['title' => 'Satelliti per trasmissione'])
    <p>
        Posizionati sull'orbita geostazionaria, a 36.000 km dalla terra sull'equatore. (fascia di Clarke, lo scrittore di 2001 Odissea nello spazio). In media si registrano circa 100 lanci all'anno, ma il numero è destinato a crescere di pari passo con il procedere di progetti come Starlink di SpaceX. Secondo il WEF (World Economic Forum) ci sono circa 6.000 satelliti che orbitano intorno alla terra.    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'VPN Virtual Private Network'])
    <p>
        E' una rete virtuale privata, cioè appoggiandosi su un'infrastruttura pubblica fisica,
        crea un canale di comunicazione virtuale riservato tra dispositivi (anche su LAN diverse e in qualunque parte del mondo)
        e garantisce privacy, anonimato e sicurezza dei dati grazie a una criptatura.
    </p>
@endcomponent
@component('pages.components.accordion', ['title' => 'LAN'])
    <p>
        Local Area Network o rete locale di computer e periferiche condivise. La LAN è una rete informatica (tecnologie più usate attualmente sono Ethernet e WiFi) limitata a un ufficio e può estendersi fino ad alcuni edifici adiacenti.   Permette di trasferire dati ad alta velocità e non necessita di linee telefoniche, di condividere le periferiche (stampanti,hard disk ecc.), di condividere software e database aziendali, si possono automatizzare i backup su server remoti.
    </p>
@endcomponent
