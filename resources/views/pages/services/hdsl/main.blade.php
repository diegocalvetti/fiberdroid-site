<p class="service-faq-title is-uppercase has-text-centered mb-4">Faq</p>

@component('pages.components.accordion', ['title' => 'Cos\'è l\'HDSL'])
   <p>
       L'<strong>HDSL</strong> è la miglior soluzione a banda larga per aziende che abbiano bisogno di:
   </p>
   <ul class="list">
       <li>un collegamento veloce ad Internet</li>
       <li>che sia simmetrico (uguale velocità in ricezione e in trasmissione)</li>
       <li>che abbia massima stabilità</li>
       <li>con una banda minima garantita</li>
       <li>
           minima latenza (molto importante se si utilizzano servizi voce tipo VoIP, ma non solo! limitare il fenomeno della latenza, risolve anche i problemi di chi gioca online. Un vero gamer infatti deve valutare quanto tempo passa tra azione del giocatore e reazione del software. Se non hai accesso alla fibra l’hdsl potrebbe essere la soluzione giusta per te. E se sei un giocatore professionista di sicuro troverai questo molto interessante
           per le aziende che si trovino in zone di “digital divide” dove quindi la fibra non è presente. Se un’azienda vuole usare VoIP, VPN o avere in casa il server di posta, una buona soluzione può essere l’HDSL.
       </li>
   </ul>
    <p>
        In questa pagina vi proporremo le offerte HDSL Fiberdroid e approfondiremo le caratteristiche dell'HDSL.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Offerte per aziende e liberi professionisti'])
   <p>
       tutte le offerte Fiberdroid partendo da 2 mega in download e upload fino a arrivare a 16 mega, sempre sia in upload che download, per poter soddisfare qualunque tuo bisogno aziendale:
       Nel caso ti serva un router possiamo fornirlo a noleggio o in vendita, e in questo caso ti riserveremo la migliore assistenza Fiberdroid anche per il funzionamento di questo dispositivo oltre alla normale assistenza relativa alla connessione.
       <br><br>
       Se sei interessato visita <a target="_blank" rel="noopener" href="{{ url('prodotti/router') }}">la nostra pagina Router</a> !
   </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'HDSL caratteristiche'])
    <p>
        L'HDSL (High-bit-rate digital subscriber line) è una tecnologia di trasmissione usata per l'accesso digitale ad internet e
        sviluppata all'inizio degli anni '90 per aumentare la velocità di connessione per le tecnologie basate sulla
        trasmissione su doppino telefonico, ed è quindi una delle prime tipologie di servizi DSL sviluppate.
    </p>
    <h3 class="accordion-subtitle">
        VELOCITÀ FINO A 16MB/S
    </h3>
    <p>
        Con l'HDSL Fiberdroid si possono raggiungere velocità da 2 Mb/s fino ad un massimo di 16 Mb/s utilizzando come abbiamo detto gli stessi cavi in rame della linea telefonica tradizionale.
        <br><br>
        Non è quindi necessario fare nuove installazioni ma si possono sfruttare le risorse già presenti per la tradizionale linea telefonica. A seconda della velocità che si vuole raggiungere si utilizzeranno una o più delle classiche linee con doppino, e questo è anche il motivo per cui si ha un limite sulla velocità massima raggiungibile.
    </p>
    <h3 class="accordion-subtitle">
        SOLO TRAFFICO DATI
    </h3>
    <p>
        Mediante l'HDSL si può trasportare solo traffico dati e non voce e questo è il motivo per cui ha una banda di frequenze più ampia dell'HDSL: può sfruttare tutto il range di frequenze che nell'HDSL viene dedicato al traffico voce.
        <br><br>
        Questo implica però che se con l'HDSL potevamo avere con una sola connessione sia la connessione ad Internet sia la nostra linea telefonica, con l'HDSL avremo solo la linea dati.
        <br><br>
        Ovviamente il servizio voce VoIP che viaggia su una linea dati come qualunque altro dato è disponibile con l'HDSL, anzi fornirà un servizio ancora maggiore grazie ai ridotti tempi di latenza della tecnologia HDSL.
        <br><br>
        Se sei interessato a mantenere la tua linea telefonica insieme alla connessione Internet visita la nostra pagina HDSL
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Connessione simmetrica'])
    <p>
        Inoltre la trasmissione è simmetrica nel senso che si ha la stessa velocità sia in
        download che in upload. Questa è la ragione per cui l'HDSL viene molto utilizzato da aziende
        ed organizzazioni che hanno bisogno di trasferire grandi quantità di dati da una sede ad un'altra.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'Linea dedicata non condivisa con altri utenti'])
    <p>
        La connessione HDSL simmetrica di Fiberdroid è stabile ed ha garanzia di banda al 99%, questo significa che per il taglio di banda che il cliente sceglierà avrà sempre tutta la velocità disponibile senza possibilità di flessioni nel tempo.
    </p>
    <br><br>
    <p>
        Questo è possibile perchè, a differenza della tecnologia HDSL in cui si attua un multiplexing che divide la banda del doppino tra diversi utenti, nell'HDSL i dati viaggiano su una linea dedicata per ogni utente.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'A chi conviene l\'HDSL?'])
    <p>
        Le aziende che hanno necessità di trasferimenti dati a velocità costanti sia in
        upload che download la HDSL è la scelta giusta, soprattutto se si utilizza il servizio
        VoIP, infatti grazie alle basse latenze nella trasmissione consente di ottenere una qualità
        della voce superiore e vengono gestite più chiamate in contemporanea rispetto a connessioni
        asimmetriche. Quindi se il vostro business implica molte videoconferenze o vi occupate
        di telecontrollo l’HDSL può essere la vostra soluzione.
    </p>
@endcomponent

@component('pages.components.accordion', ['title' => 'I vantaggi dell\'HDSL'])
    <p>
        Riassumendo quanto fin qui detto i principali vantaggi della tecnologia HDSL sono:
    </p>
    <ul class="list">
        <li>Linea dedicata: la sua potenza di traffico è dedicata all'utente che l'ha installata dal momento che non viene condivisa con nessun altro utente ma è un unico circuito virtuale dall'edificio del cliente alla centrale del provider</li>
        <li>Alta Velocità simmetrica fino a 16 Mb/sec: consente la stessa velocità in download ed in upload senza problemi di dati, video e audio di alta qualità (banda larga)</li>
        <li>Banda Minima Garantita (BMG) al 99%: la velocità non scenderà sotto un valore fissato</li>
        <li>Meno latenza e ritardo nel segnale (ottimo per video e gaming)</li>
        <li>Ottima stabilità della connessione</li>
        <li>Sicurezza: viene garantita attraverso un'offerta che comprende un firewall per difendervi da attacchi esterni</li>
    </ul>
@endcomponent

@component('pages.components.accordion', ['title' => 'Requisiti minimi per l’uso dell\'HDSL'])
    <ul class="list">
        <li>Un Modem specifico per HDSL</li>
        <li>Un Router di ultima generazione ETHERNET (no brandizzato con logo di altro operatore)</li>
        <li>Disponibilità della rete HDSL nella Tua zona, richiedi una verifica gratuita ad uno dei nostri tecnici.</li>
    </ul>
@endcomponent
