<!DOCTYPE html>
<html lang="it">

    <head>
        <meta charset="UTF-8">
        <link rel="icon" href="{{ asset('images/loghi/pittogramma-fiberdroid.svg') }}">
        <link rel="canonical" href="{{ $canonical }}" />
        <title>@yield('title','Fiberdroid | Portiamo il tuo business alla velocità della luce')</title>
        <meta name="description" content="@yield('description', 'base description')">

        <meta property="og:title" content="@yield('title','Fiberdroid | Portiamo il tuo business alla velocità della luce')" />
        <meta property="og:description" content="@yield('description', 'base description')" />
        <meta property="og:image" content="{{ asset('images/header-interne-completa.jpg') }}" />
        <meta property="og:locale" content="it-IT" />
        <meta property="og:type" content="@yield('og-type', 'article')" />
        <meta property="og:url" content="{{ $canonical }}" />
        <meta name="twitter:title" content="@yield('title','Fiberdroid | Portiamo il tuo business alla velocità della luce')">
        <meta name="twitter:description" content=" @yield('description', '')">
        <meta name="twitter:image" content="{{ asset('images/header-interne-completa.jpg') }}">
        <meta name="twitter:card" content="Scopri il mondo Fiberdroid">
        <meta name="twitter:image:alt" content="La terra vista da un satellite di Fiberdroid">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">

        @if( config('app.env') != 'prod' )
            <meta name="robots" content="noindex">
        @else
            <meta name="robots" content="@yield('robots', 'index, follow')">
        @endif

        <link rel="stylesheet" href="{{ asset('css/app.css') . (config('app.env') != 'prod' ? "?v=".uniqid():"") }}">
        @stack('css')

        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-GBK6LVXK88"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-GBK6LVXK88');
        </script>

        <link rel="stylesheet" href="{{ asset('css/pages/home/home.css') . (config('app.env') != 'prod' ? "?v=".uniqid():"") }}">

        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=IBM Plex Sans Hebrew:wght@400;500;600;700&display=swap"
        />
        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Outfit:wght@300;500;600;700&display=swap"
        />
        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=IBM Plex Sans:wght@700&display=swap"
        />
        <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Mono&display=swap" rel="stylesheet">
    </head>

    <body class="has-background-light">

        @if(!isset($noNavbar) || !$noNavbar)
            <navbar id="navbar" class="columns" style="background-image: url('{{ asset('images/header-interne.jpg')  }}'); background-size: cover; background-repeat: no-repeat; background-position: center; display: flex">
            <div class="column is-12">
                @component('old.components.nav.navbar', [
                    'paddingX' => 6,
                    'bg' => "transparent",
                    'link_servizi' => $services->where('service_type_id', 1)->pluck('name', 'url'),
                    'link_prodotti' => $services->where('service_type_id', 2)->pluck('name', 'url'),
                    'link_soluzioni' => $solutions->pluck('name', 'url')
                ])@endcomponent

                @yield('banner')
                @yield('banner-secondary')

            </div>
        </navbar>
        @endif

        @yield('content')

        @yield('content-banner')

        @yield('content-secondary')

        <call-us
            style="position: fixed; right: 20px; bottom: 20px;
         font-family: Arial;
         z-index: 99999;
         --call-us-form-header-background:#d97e17;
         --call-us-main-button-background:#d63005;
         --call-us-client-text-color:#d4d4d4;
         --call-us-agent-text-color:#eeeeee;
         --call-us-form-height:330px;"
            id="wp-live-chat-by-3CX"
            channel-url="https://uffici-ztech.3cx.it:5001"
            files-url="https://uffici-ztech.3cx.it:5001"
            minimized="true"
            animation-style="none"
            party="assistenza"
            minimized-style="BubbleRight"
            allow-call="false"
            allow-video="false"
            allow-soundnotifications="true"
            enable-mute="true"
            enable-onmobile="true"
            offline-enabled="true"
            enable="true"
            ignore-queueownership="false"
            authentication="name"
            operator-name="Support"
            show-operator-actual-name="true"
            channel="phone"
            aknowledge-received="true"
            gdpr-enabled="true"
            gdpr-message="Accetto il trattamento dei miei dati personali e l'utilizzo dei cookie al fine di avviare una chat elaborata da Fiberdroid s.r.l., ai fini di Chat/Supporto conservata per un tempo di 30 giorni come da GDPR."
            message-userinfo-format="both"
            message-dateformat="both"
            start-chat-button-text="Chat"
            window-title="Live Chat Fiberdroid"
            button-icon-type="Default"
            invite-message="Ciao! Come possiamo aiutarvi oggi?"
            authentication-message="Potremmo avere il tuo nome e la tua email?"
            unavailable-message="In questo momento non siamo operativi, lasciaci un messaggio!"
            offline-finish-message="Abbiamo ricevuto il tuo messaggio e ti contatteremo presto."
            ending-message="La tua sessione Ë finita. Non esitare a contattarci di nuovo!"
            greeting-visibility="none"
            greeting-offline-visibility="none"
            chat-delay="2000"
            offline-name-message="Potremmo avere il tuo nome?"
            offline-email-message="  Potremmo avere la tua email?"
            offline-form-invalid-name="Italiano  Mi dispiace, il nome fornito non Ë valido."
            offline-form-maximum-characters-reached="Numero massimo di caratteri raggiunto"
            offline-form-invalid-email="Mi dispiace, non sembra un indirizzo email. Puoi provare di nuovo?"
            enable-direct-call="false"
            enable-ga="false"
        >
        </call-us>

        <x-footer></x-footer>
        <x-modal-navbar></x-modal-navbar>
        <x-modal-contact></x-modal-contact>

        <x-modal-cookie :accepted="$cookie"></x-modal-cookie>
    </body>


    <script defer src="https://cdn.3cx.com/livechat/v1/callus.js" id="tcx-callus-js"></script>

    <style>
        html, body{
            width: 100%;
            height: 100%;
        }
        html, body
         {
             margin:0px;
             padding:0px;
             width:100%;
         }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}"></script>

    @stack('js')
    @livewireStyles
    @livewireScripts
</html>
