<!DOCTYPE html>
<html lang="it">

    <head>
        <meta charset="UTF-8">
        <link rel="icon" href="{{ url('pages/fiberdroid-icon.png') }}">
        <link rel="canonical" href="{{ $canonical }}" />
        <title>@yield('title', 'Fiberdroid | Il tuo business sempre connesso, ovunque tu sia.')</title>
        <meta name="description" content="@yield('description', 'base description')">

        <meta property="og:title" content="@yield('title','Fiberdroid | Portiamo il tuo business alla velocità della luce')" />
        <meta property="og:description" content="@yield('description', 'base description')" />
        <meta property="og:image" content="{{ asset('images/meta/banner.png') }}" />
        <meta property="og:locale" content="it-IT" />
        <meta property="og:type" content="@yield('og-type', 'article')" />
        <meta property="og:url" content="{{ $canonical }}" />
        <meta name="twitter:title" content="@yield('title','Fiberdroid | Portiamo il tuo business alla velocità della luce')">
        <meta name="twitter:description" content=" @yield('description', '')">
        <meta name="twitter:image" content="{{ asset('images/meta/banner.png') }}">
        <meta name="twitter:card" content="Scopri il mondo Fiberdroid">
        <meta name="twitter:image:alt" content="La terra vista da un satellite di Fiberdroid">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">

        @if( config('app.env') != 'prod' )
            <meta name="robots" content="noindex">
        @else
            <meta name="robots" content="@yield('robots', 'index, follow')">
        @endif

        <link rel="stylesheet" href="{{ asset('css/app.min.css') . (config('app.env') != 'proddd' ? "?v=".uniqid():"") }}">
        @stack('css')

        @if( config('app.env') == 'prod' )
            <!-- Google tag (gtag.js) -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=G-GBK6LVXK88"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'G-GBK6LVXK88');
            </script>
        @endif
        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

        <link rel="preconnect" href="https://fonts.gstatic.com">

        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=IBM Plex Sans Hebrew:wght@400;500;600;700&display=swap"
        />
        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Outfit:wght@300;500;600;700&display=swap"
        />
        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=IBM Plex Sans:wght@700&display=swap"
        />
        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;800&display=swap"
        >

        <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Prompt:ital,wght@0,400;0,500;0,700;0,800;1,700&display=swap"
        >
        <link
            href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap"
            rel="stylesheet"
        >
        <link
            href="https://fonts.googleapis.com/css2?family=IBM+Plex+Mono&display=swap"
            rel="stylesheet"
        >
    </head>

    <body style="background-color: #0a0a0a">
        <div class="layout-container" style="position: relative; max-width: 1920px; margin: 0 auto; overflow: hidden">
            <header class="header-container pb-0 px-0-mobile mb-0 columns is-mobile is-multiline" style="-webkit-backdrop-filter: blur(21px);backdrop-filter: blur(21px); opacity: 0" data-fade-in>
                <div class="column is-12 m-0 p-0">
                    <div class="columns is-vcentered is-mobile m-0 p-0-desktop px-4-touch" style="position: relative;">
                        <div class="column is-mobile is-multiline touch-6 desktop-1" style="display: flex;position: relative; z-index: 999999999999">
                            <a href="/" class="logo-wrapper" style="margin: 0">
                                <img
                                    alt="Logo di Fiberdroid"
                                    src="{{ url("pages/home/logo.svg") }}"
                                    class="logo hover-image-illuminate is-clickable my-auto"
                                />
                            </a>
                        </div>
                        <nav class="menu-voices column hidden-touch is-9" style="display: flex; position: relative; z-index:999999999999">
                            @foreach($menu as $voice)
                                @if(isset($voice['onlyMobile']) && !$voice['onlyMobile'] || !isset($voice['onlyMobile']) )
                                    @component('pages.components.navbar-voice', $voice)@endcomponent
                                @endif
                            @endforeach
                        </nav>
                        <nav class="menu-voices-end column hidden-touch is-2" style="display: flex; position: relative; z-index:999999999999">
                            <span class="cart-icon is-zero" style="margin: auto 0" data-value="0">
                               <img data-cart class="is-clickable" src="{{ asset('pages/icons/cart.svg') }}" alt="Icona Carrello Fiberdroid" style="width: 20px; height: 20px;">
                            </span>
                            <div style="color: black; background: white; border-radius: 8px; padding: 5px 24px">
                                <a target="_blank" href="https://area-clienti.fiberdroid.it">
                                    <span class="has-text-black">Accedi</span>
                                </a>
                            </div>
                        </nav>
                        <div class="menu-voices column hidden-desktop is-6" style="display: flex; gap: 36px">
                            <span class="cart-icon is-zero" style="margin: auto 0 0 auto;flex: 0 0 auto;" data-value="0">
                               <img data-cart class="is-clickable" src="{{ asset('pages/icons/cart.svg') }}" alt="Icona Carrello Fiberdroid"  style="width: 20px; height: 20px;">
                            </span>
                            <div style="display: inline; flex: 0 0 auto; margin: auto 0; float: right; padding: 10px; width: 38px; height: 38px; border-radius: 12px; background: #F6F6F6;">
                                <img data-open-menu class="image" src="{{ url('pages/home/icons/horizontal-line.svg') }}" alt="Hamburger menu"/>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <header class="header-container pb-0 px-0-mobile mb-0 columns is-mobile is-multiline" style="-webkit-backdrop-filter: blur(21px);backdrop-filter: blur(21px); opacity: 0" data-fade-in>
                <div class="column is-12 m-0 p-0">
                    <div class="columns is-vcentered is-mobile m-0 p-0-desktop px-4-touch" style="position: relative;">
                        <div class="column is-mobile is-multiline touch-6 desktop-1" style="display: flex;position: relative; z-index: 999999999999">
                            <a href="/" class="logo-wrapper" style="margin: 0">
                                <img
                                    alt="Logo di Fiberdroid"
                                    src="{{ url("pages/home/logo.svg") }}"
                                    class="logo hover-image-illuminate is-clickable my-auto"
                                />
                            </a>
                        </div>
                        <nav class="menu-voices column hidden-touch is-9" style="display: flex; position: relative; z-index:999999999999">
                            @foreach($menu as $voice)
                                @if(isset($voice['onlyMobile']) && !$voice['onlyMobile'] || !isset($voice['onlyMobile']) )
                                    @component('pages.components.navbar-voice', $voice)@endcomponent
                                @endif
                            @endforeach
                        </nav>
                        <nav class="menu-voices-end column hidden-touch is-2" style="display: flex; position: relative; z-index:999999999999">
                            <span class="cart-icon is-zero" style="margin: auto 0" data-value="0">
                               <img data-cart class="is-clickable" src="{{ asset('pages/icons/cart.svg') }}" alt="Icona Carrello Fiberdroid" style="width: 20px; height: 20px;">
                            </span>
                            <div style="color: black; background: white; border-radius: 8px; padding: 5px 24px">
                                <a target="_blank" href="https://area-clienti.fiberdroid.it">
                                    <span class="has-text-black">Accedi</span>
                                </a>
                            </div>
                        </nav>
                        <div class="menu-voices column hidden-desktop is-6" style="display: flex; gap: 36px">
                            <span class="cart-icon is-zero" style="margin: auto 0 0 auto;flex: 0 0 auto;" data-value="0">
                               <img data-cart class="is-clickable" src="{{ asset('pages/icons/cart.svg') }}" alt="Icona Carrello Fiberdroid"  style="width: 20px; height: 20px;">
                            </span>
                            <div style="display: inline; flex: 0 0 auto; margin: auto 0; float: right; padding: 10px; width: 38px; height: 38px; border-radius: 12px; background: #F6F6F6;">
                                <img data-open-menu class="image" src="{{ url('pages/home/icons/horizontal-line.svg') }}" alt="Hamburger menu"/>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <main class="py-6-desktop has-background-black" style="position: relative">
                @yield('content')

                @if($bubbles)
                    <div class="bubble-blue">
                        <img src="{{ asset('pages/blue_bubble.png') }}" alt="Bolla blu sfocata">
                    </div>
                    <div class="bubble-orange">
                        <img src="{{ asset('pages/orange_bubble.png') }}" alt="Bolla arancione sfocata">
                    </div>

                    <!--
                    <div class="bubble-decoration-blue hidden-touch">
                        <div class="bubble-decoration-blue-border">
                        </div>
                        <div class="bubble-shadow"></div>
                    </div>

                    <div class="bubble-decoration-gold hidden-touch">
                        <div class="bubble-decoration-gold-border">
                        </div>
                    </div>
                    -->
                @endif
            </main>

            <footer class="footer-container columns is-multiline has-background-black" style="position: relative; z-index: 1">
                <div class="column is-4">
                    <figure class="image w-50">
                        <a href="/">
                            <img src="{{ url("pages/home/logo.svg") }}" alt="Logo Fiberdroid"/>
                        </a>
                    </figure>
                    <p class="my-6-desktop my-2-mobile">
                        Siamo l'azienda che ti permette di avere un'unica assistenza per
                        internet, wifi, centralino cloud, fibra, connettività e
                        piattaforma tecnologica.
                    </p>

                    <div class="columns mt-5 my-2-mobile is-vcentered" style="width: 30%">
                        <div class="column is-12" style="display: flex; gap: 12px; width: 1000px">
                            <div style="margin: auto 0">
                                @component('pages.components.link', ['href' => $config['link_linkedin']])
                                <div class="figure linkedin-container" style="display: flex">
                                    <img class="linkedin" src="https://fiberdroid.it/pages/home/linkedin.svg" alt="logo linkedin fiberdroid"/>
                                    <img class="linkedin-active" src="https://fiberdroid.it/pages/home/linkedin-active.svg" style="display: none" alt="logo linkedin fiberdroid attivo"/>
                                </div>
                                @endcomponent
                            </div>
                            <div style="margin: auto 0">
                                @component('pages.components.link', ['href' => $config['link_youtube']])
                                    <div class="figure youtube-container" style="display: flex; margin-bottom: -5px">
                                        <img class="youtube" src="https://fiberdroid.it/pages/home/union.svg" alt="logo youtube fiberdroid"/>
                                        <img class="youtube-active" src="https://fiberdroid.it/pages/home/union-active.svg" style="display: none" alt="logo youtube fiberdroid attivo"/>
                                    </div>
                                @endcomponent
                            </div>
                            <div style="margin: auto 0">
                                @component('pages.components.link', ['href' => $config['link_instagram']])
                                    <div class="figure instagram-container" style="display: flex; margin-bottom: -5px">
                                        <img class="instagram" src="{{ url('pages/home/instagram.svg') }}" alt="logo instragram fiberdroid"/>
                                        <img class="instagram-active" src="{{ url('pages/home/instagram-active.svg') }}" style="display: none" alt="logo instagram fiberdroid attivo"/>
                                    </div>
                                @endcomponent
                            </div>
                            <div style="margin: auto 0">
                                @component('pages.components.link', ['href' => $config['link_tiktok']])
                                    <div class="figure tiktok-container" style="display: flex; margin-bottom: -5px">
                                        <img class="tiktok" src="{{ url('pages/home/tiktok.svg') }}" alt="logo instragram fiberdroid"/>
                                        <img class="tiktok-active" src="{{ url('pages/home/tiktok-active.svg') }}" style="display: none" alt="logo instagram fiberdroid attivo"/>
                                    </div>
                                @endcomponent
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-2 is-offset-1">
                    <div>
                        <p class="menu-voice has-text-white"><b>SERVIZI</b></p>
                        <p class="menu-voice pb-1 pt-2">
                            <a href="{{ url('/servizi/fibra') }}">Fibra</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/servizi/voip') }}">VOIP</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/servizi/wireless') }}">Wireless</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/servizi/cloud-wifi') }}">Cloud wifi</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/servizi/centralino-cloud') }}">Centralino cloud</a>
                        </p>
                    </div>
                </div>
                <div class="column is-2">
                    <div>
                        <p class="menu-voice has-text-white"><b>PRODOTTI</b></p>
                        <p class="menu-voice pb-1 pt-2">
                            <a href="{{ url('/prodotti/router') }}">Router</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/prodotti/telefoni-voip') }}">Telefoni VoIp</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/prodotti/access-point') }}">Access Point</a>
                        </p>
                    </div>
                </div>
                <div class="column is-2">
                    <div>
                        <p class="menu-voice has-text-white"><b>SOLUZIONI</b></p>
                        <p class="menu-voice pb-1 pt-2">
                            <a href="{{ url('/soluzioni/aziende') }}">Aziende</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/soluzioni/aziende') }}">Partite IVA</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/soluzioni/ristorazione') }}">Ristorazione</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/soluzioni/retail') }}">Retail</a>
                        </p>
                        <p class="menu-voice pb-1">
                            <a href="{{ url('/soluzioni/strutture-ricettive') }}">Strutture Ricettive</a>
                        </p>
                    </div>
                </div>
                <div class="column is-12 pb-0">
                    <hr style="padding: 0 3rem; height: 0.867px; background: rgba(217, 217, 217, 0.2)">
                </div>
                <div class="column is-4 pt-0">
                    <p class="has-text-white"><b>©2023 FIBERDROID S.R.L.</b></p>
                    <p>
                        Via Enrico Cosenz, 54 20158 Milano
                    </p>
                    <p>P.Iva & C.F. 07057060969</p>
                    <p>Tel. <a href="tel:+39 02 83595676">+39 02 83595676</a> </p>
                </div>
                <div class="column is-8 pt-0">
                    <div class="columns pt-6">
                        <div class="column is-2">
                            @component('pages.components.link', ['target' => '_self', 'href' => "/assistenza"])
                                <p class="footer-link">Contattaci</p>
                            @endcomponent
                        </div>
                        <div class="column is-2">
                            @component('pages.components.link', ['target' => '_self', 'href' => "/policy/privacy"])
                                <p class="footer-link">Privacy Policy</p>
                            @endcomponent
                        </div>
                        <div class="column is-2">
                            @component('pages.components.link', ['target' => '_self', 'href' => "/policy/cookie"])
                                <p class="footer-link">Cookie Policy</p>
                            @endcomponent
                        </div>
                        <div class="column is-2">
                            @component('pages.components.link', ['target' => '_self', 'href' => "https://conciliaweb.agcom.it/conciliaweb/login.htm"])
                                <p class="footer-link">Concilia Web</p>
                            @endcomponent
                        </div>
                        <div class="column is-3">
                            @component('pages.components.link', ['target' => '_self', 'href' => "/carta-dei-servizi"])
                                <p class="footer-link">Carta dei Servizi</p>
                            @endcomponent
                        </div>
                    </div>
                </div>
            </footer>

            <div class="menu-dropdown hidden-touch" data-menu-servizi style="display: none">
                <div class="menu-dropdown-content">
                    <p class="menu-dropdown-text pb-5">Servizi</p>

                    <div class="columns is-multiline">
                        @foreach($services as $service)
                            <div class="column is-3 pt-0 px-0">
                                <a @if($service->active) href="/servizi/{{ $service->url }}" @else style="cursor: default" @endif>
                                    <div class="box @if($service->active) menu-dropdown-item is-clickable @else has-background-transparent @endif">
                                        <div class="columns is-vcentered">
                                            <div class="column">
                                                <div class="figure" style="width: 24px">
                                                    <img src="{{ asset('pages/menu/'.$service->icon) }}" alt="Icona servizio Fiberdroid: {{ $service->name }}" loading="lazy">
                                                </div>
                                            </div>
                                            <div class="column is-11" style="position: relative">
                                                <p class="menu-dropdown-text-title">{{ $service->name }}</p>
                                                <p class="menu-dropdown-text">
                                                    {{ $service->short_description }}
                                                </p>

                                                @if($service->name == "Fiber AI")
                                                    <img src="{{ asset('pages/icons/new.png') }}" alt="Icona servizio Fiberdroid: Fiber-AI" style="position: absolute; top: 2px; left: 75px; width: 40px; height: 24px" loading="lazy">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="menu-dropdown hidden-touch" data-menu-prodotti style="display: none">
                <div class="menu-dropdown-content">
                    <div class="columns">
                        @foreach($service_products as $product)
                            <div class="column is-4">
                                <a href="/prodotti/{{ $product->url }}">
                                    <div class="box menu-product-box">
                                        <div class="columns is-vcentered">
                                            <div class="column">
                                                <div class="figure" style="width: 24px">
                                                    <img src="{{ asset('pages/menu/' . $product->icon) }}" alt="Icona prodotto Fiberdroid: {{ $product->name }}">
                                                </div>
                                            </div>
                                            <div class="column is-11" style="position: relative">
                                                <p class="menu-dropdown-text-title">{{ $product->name }}</p>
                                                <p class="menu-dropdown-text">
                                                    {{ $product->short_description }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="menu-dropdown hidden-touch" data-menu-soluzioni style="display: none">
                <div class="menu-dropdown-content">
                    <div class="columns">
                        <div class="column is-9 mx-auto">
                            <p class="menu-dropdown-text">Soluzioni</p>

                            <div class="columns is-multiline">
                                @foreach($solutions->values() as $key => $service)
                                    @if($key % 3 == 2 && $key != 2)
                                        <div class="column is-4 pt-0 px-0"></div>
                                    @endif
                                    <div class="column is-4 pt-0 px-0">
                                        <a @if($service->active) href="/soluzioni/{{ $service->url }}" @else style="cursor: default" @endif>
                                            <div class="box @if($service->active) menu-dropdown-item is-clickable @else has-background-transparent @endif">
                                                <div class="columns is-vcentered">
                                                    <div class="column">
                                                        <div class="figure" style="width: 24px">
                                                            <img src="{{ asset('pages/menu/'.$service->icon) }}" alt="Icona servizio Fiberdroid: {{ $service->name }}">
                                                        </div>
                                                    </div>
                                                    <div class="column is-11">
                                                        <p class="menu-dropdown-text-title">{{ $service->name }}</p>
                                                        <p class="menu-dropdown-text">
                                                            {{ $service->short_description }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-dropdown-closer" style="display: none"></div>
            <div data-drop-mobile class="menu-dropdown-mobile hidden-desktop is-flex-direction-column" style="display: none">
                <div data-drop-mobile style="position: relative; max-height: 100vh; overflow: auto;">
                    @foreach($menu as $voice)
                        @component('pages.components.navbar-voice-mobile', $voice)@endcomponent
                    @endforeach
                </div>
            </div>

            <div class="cart-background" style="display: none"></div>
            <div class="cart" style="display: none">
                <div class="cart-content p-5" style="display: flex; width: 100%">
                    <div class="columns is-multiline" data-cart-editing>
                        <div class="column is-12">
                            <p class="cart-configuration-text mb-3">
                                La tua configurazione
                            </p>
                            <p class="text is-clickable mb-6" style="text-decoration: underline" data-empty-cart>
                                Svuota carrello
                            </p>

                            <div data-cart-is-empty style="border-top: solid rgba(0,0,0,0.4) 1px; border-bottom: solid rgba(0,0,0,0.4) 1px">
                                <p class="has-text-fd-error">
                                    Il tuo carrello è vuoto. Naviga tra
                                    <a href="#" data-target="servizi" class="menu-link" style="color: #f5b4b4">servizi</a>,
                                    <a href="#" data-target="prodotti" class="menu-link" style="color: #f5b4b4">prodotti</a>
                                    e
                                    <a href="#" data-target="soluzioni" class="menu-link" style="color: #f5b4b4">soluzioni</a>
                                    e inizia ora a creare la soluzione perfetta per te.
                                </p>
                            </div>

                            <div class="cart-items" data-cart-items>
                                <div class="columns px-2 mt-2 is-flex-direction-column" data-cart-item style="display: none; border-bottom: solid 1px #282828">
                                    <div class="columns is-mobile mb-0 is-vcentered">
                                        <div class="column cart-item-icon-box" style="background-image: url('{{ asset('pages/cart/bluebox.svg') }}')">
                                            <img src="{{ asset('pages/cart/wifi.svg') }}" alt="Icona Wi-Fi" style="height: 60px"/>
                                        </div>
                                        <div class="column cart-image-box is-hidden">
                                            <img class="cart-image" src="" alt="Carrello Fiberdroid"/>
                                        </div>
                                        <div class="column is-9" style="position: relative">
                                            <p class="cart-item-title" style="width: 90%">
                                                FTTH 50 MEGA
                                            </p>
                                            <span data-remove-cart-item style="cursor: pointer; position: absolute; float: right; top: 20px; right: 20px; color: white">X</span>
                                        </div>
                                    </div>
                                    <div class="columns mb-2">
                                        <div class="column is-12 pr-5 has-text-right">
                                            <p class="cart-price-text">
                                                &euro; 19.99
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="column is-12 mt-auto" data-create-order>
                                        <span class="button mt-2 is-fd-primary" style="width: 100%">
                                            Invia ordine
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="columns is-multiline" data-cart-submitting style="display: none">
                        <div class="column is-12" style="display: flex; flex-direction: column">
                            <p class="cart-configuration-text mb-3">
                                Invia il carello
                            </p>
                            <p class="text is-clickable" data-create-new-cart style="text-decoration: underline">
                                Torna al carrello
                            </p>

                            @component('pages.components.form.form')@endcomponent
                        </div>
                        <div class="column is-12 mt-auto" data-send-order>
                            <span class="button is-fd-primary" style="width: 100%">
                                Invia pacchetto
                            </span>
                        </div>
                        </div>
                    <div class="columns is-multiline" data-cart-submitted style="display: none">
                        <div class="column is-12">
                            <p class="cart-configuration-text mb-3">
                                Grazie di averci contattato
                            </p>
                            <p class="text is-clickable" data-create-new-cart style="text-decoration: underline">
                                Crea un nuovo carrello
                            </p>
                            <p class="mt-4 has-text-white-bis">
                                L'ordine è stato inviato, riceverai un riepilogo all'indirizzo email inserito e verrai
                                contattato al più presto da uno dei nostri tecnici specializzati per
                                completare l'acquisto.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="notification notification-box is-grey is-light">
                <p class="notification-text"></p>
            </div>

            <div id="modal_cookie" class="modal_cookie shadow p-5" style="display: none">
                <div class="columns is-vcentered">
                    <div class="column is-7">
                        <p class="is-size-6 has-text-white cookie-text">
                            Fiberdroid utilizza i cookie per migliorare la funzionalità del sito,
                            fornirti una migliore esperienza di navigazione e per consentire ai nostri partner di pubblicizzarti.
                            Informazioni dettagliate sull'uso dei cookie su questo sito e su come rifiutarli sono fornite nella nostra <a href="{{ route('policy.cookies') }}">politica sui cookie</a>.
                            Utilizzando questo sito o cliccando su "OK", acconsenti all'uso dei cookie.
                            <br>

                            <!--<button class="button is-small is-dark" data-action="close-cookie" data-cookie="accept" data-target="#modal_cookie" aria-label="close">Gestisci i cookie</button>-->
                        </p>
                    </div>
                    <div class="column is-5">
                        <div class="buttons is-pulled-right are-small">
                            <a href="{{ url("policy/cookie") }}" class="button cookie-button is-small is-dark">Cookie Policy</a>
                            <div class="button cookie-button is-small is-fd-primary" data-action="close-cookie" data-cookie="accept" data-target="#modal_cookie" aria-label="close">Ok</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal promo-modal is-unselectable" style="position: fixed; z-index: 999998;">
                <div class="modal-background" data-close-promo-modal></div>
                <div class="modal-content promo-container">
                    <button aria-label="ChiChiudi Popup Promozione" class="modal-close hidden-desktop promo-close is-large" data-close-promo-modal style="position: absolute; top: 42px; width: 89px; height: 89px; max-width: 89px; max-height: 89px"></button>
                    <div class="box p-0" style="margin-top: 59px; background-color: black">
                        <div class="columns m-0 pl-4">
                            <div class="column desktop-7 tablet-12 p-5">

                                <div class="promo-header">
                                    <img class="promo-logo" src="{{ asset('pages/home/logo.svg') }}" alt="Logo Promozione"/>
                                    <div class="hidden-mobile" style="width: 4px;height: 65px;background: #0266FF;"></div>
                                    <p class="promo-title">
                                        L'offerta esclusiva <br>
                                        per i nuovi clienti
                                    </p>
                                </div>

                                <p class="promo-text my-6">
                                    La tua azienda è efficiente <br>
                                    nelle telecomunicazioni?
                                    <br><br>
                                    Oggi puoi avere il tuo<br>
                                    <span class="promo-text-green">progetto personalizzato</span> <br>
                                    gratuito con il nostro specialista.
                                </p>

                                <a role="button" aria-label="Prenota una chiamata con Fiberdroid" target="_blank" rel="noopener" href="https://calendly.com/fiberdroid/richiesta-chiamata" class="button promo-button is-success">
                                    Prenota Ora
                                </a>

                            </div>
                            <div class="column desktop-5 promo-bg hidden-touch" style="position: relative">
                                <button aria-label="Chiudi Popup Promozione"  class="modal-close promo-close is-large" data-close-promo-modal style="width: 89px; height: 89px; max-width: 89px; max-height: 89px"></button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        <style>

                .shadow {
                    -webkit-box-shadow: 0px -5px 16px -6px rgba(0,0,0,0.66);
                    -moz-box-shadow: 0px -5px 16px -6px rgba(0,0,0,0.66);
                    box-shadow: 0px -5px 16px -6px rgba(0,0,0,0.66);
                }
            </style>
        </div>
    </body>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans&family=Inter&family=Outfit&family=Space+Grotesk:wght@300;400;500;600;700&display=swap" rel="stylesheet">

    <script src="{{ asset('js/app.js' . (config('app.env') != 'prodddd' ? "?v=".uniqid():"")) }}"></script>
    @if($useWebcomponent)
        <script src="https://unpkg.com/vue@2.7.16/dist/vue.min.js"></script>
        <script src="{{ asset('webcomponent/dist/web-component.min.js') }}"></script>
    @endif

    @stack('js')

    <style>
        html, body {
            width: 100%;
            background: black;
        }

        body {
            overflow: hidden;
        }
    </style>
    <script>
        $(".linkedin-active").hide()
        $(".linkedin-container").on('mouseover', function () {
            $(".linkedin").hide()
            $(".linkedin-active").show()
        })
        $(".linkedin-container").on('mouseleave', function () {
            $(".linkedin-active").hide()
            $(".linkedin").show()
        })

        $(".youtube-active").hide()
        $(".youtube-container").on('mouseover', function () {
            $(".youtube").hide()
            $(".youtube-active").show()
        })
        $(".youtube-container").on('mouseleave', function () {
            $(".youtube-active").hide()
            $(".youtube").show()
        })
    </script>
</html>
