@extends('layouts._layout', ['noNavbar' => true])

@section('title', 'Connettiamo il tuo business alla velocità della luce | Fiberdroid')
@section('description', 'Fiberdroid è un’azienda di telecomunicazioni che si occupa di connettere in internet aziende e professionisti tramite VOIP, WIFI, cloud e fibra ottica.')

@section('content')
    <div style="max-width: 2800px; margin: auto; background-color: white">
        <x-row bg="white" style="padding: 0!important; background-image: url({{ image('speed-header') }}); background-size: cover">
            <div class="wrapper pl-6 py-6">
                <div class="column is-8-desktop is-12-touch my-6">
                    <x-link to="home">
                        <figure class="image fd-base-logo">
                            <img src="{{ image("logo-bianco-orizzontale") }}" {!! image_attr("logo-blu-orizzontale") !!}>
                        </figure>
                    </x-link>
                    <x-title class="mt-6 mb-4" color="white">
                        <b class="is-size-3-desktop is-size-5-touch" style="font-weight: 900;">Linea Internet Professionale Con Possibilità Di Backup,  Centralino Voip In Cloud E Assistenza Di Alto Livello</b>
                    </x-title>
                    <x-text color="white" class="mb-4">
                    <span class="is-size-5-desktop is-size-6-touch">
                       "Soluzioni su misura, assistenza di qualità e unica per tutti i servizi di telecomunicazione"
                    </span>
                    </x-text>
                    <a href="#contact">
                        <x-button bg="fd-warning" class="has-text-centered mt-4">
                            <b>Contattaci Ora!</b>
                        </x-button>
                    </a>
                </div>
            </div>
        </x-row>

        <x-row bg="white" style="padding: 2.5rem 3rem!important;">
            <div class="column is-flex is-4 has-text-centered">
                <div class="box" style="width: 100%">
                    <figure class="image is-64x64 mx-auto">
                        <span style="display: inline; margin: auto" class="material-symbols-outlined has-text-blue-logo is-size-1">router</span>
                    </figure>

                    <x-text class="mt-3">
                        <b class="is-size-5">Connettività</b>
                    </x-text>

                    <x-text>
                        Soluzioni su misura per far crescere il tuo business. <b>Multiconnettività</b> su diverse tipologie di rete e possibilità di portati
                        la vera fibra <b>ovunque tu sia fino a 10 Gb/s</b>.
                    </x-text>

                </div>
            </div>
            <div class="column is-flex is-4 has-text-centered">
                <div class="box" style="width: 100%">
                    <figure class="image is-64x64 mx-auto">
                        <span style="display: inline; margin: auto" class="material-symbols-outlined has-text-blue-logo is-size-1">perm_phone_msg</span>
                    </figure>

                    <x-text class="mt-3">
                        <b class="is-size-5">Centralino Voip Cloud</b>
                    </x-text>

                    <x-text>
                        <strong>Piattaforma Voip</strong> che ti permette di <strong>comunicare</strong> con gli interni della tua azienda e
                        con qualsiasi altra sede esterna, anche quando sei fuori dall'ufficio.
                        Sistema integrato con intelligenza artificiale.
                    </x-text>

                </div>
            </div>
            <div class="column is-flex is-4 has-text-centered">
                <div class="box" style="width: 100%">
                    <figure class="image is-64x64 mx-auto">
                        <span style="display: inline; margin: auto" class="material-symbols-outlined has-text-blue-logo is-size-1">satellite_alt</span>
                    </figure>

                    <x-text class="mt-3">
                        <b class="is-size-5">Linea di Backup</b>
                    </x-text>

                    <x-text>
                        Possibilità di avere <strong>una o più linee di backup</strong> su differente tecnologia in bilanciamento attivo
                        (wireless, 4G/5G, satellitare, ecc) ciò significa avere linee differenti che <strong>lavorano insieme</strong>,
                        aumentando in modo significativo le prestazioni.
                    </x-text>

                </div>
            </div>
        </x-row>

        <x-row class="is-multiline" bg="white" style="padding: 0!important; background-image: url('{{ asset('image/speed/mondo-fiber.webp') }}'); background-size: cover">
            <div class="columns is-multiline my-6 py-6 px-6 mx-6 px-0-mobile mx-0-mobile">
                <div class="column is-12 has-text-centered">
                    <x-title role="h2" style="font-weight:800">
                    <span class="is-size-2 has-text-white">
                        Promo <span class="has-text-blue-logo">Hybrid 5G</span>
                    </span>
                    </x-title>

                    <p class="has-text-white is-size-6" style="font-weight: 400">
                        L'hybrid 5G è una soluzione di connettività innovativa che combina la connessione di terra FTTC (Fiber to the Cabinet)
                        con la connessione wireless 5G. <br>
                        Questa tecnologia ibrida si basa sull'utilizzo di due modalità di connessione differenti che lavorano insieme per fornire una connessione Internet stabile e veloce.  L'hibrid 5G funziona anche da linea di backup, garantendo sempre una connessione senza interruzioni.
                    </p>
                </div>
                <div class="column is-4 p-3 has-text-centered">
                    @component('old.components.box-servizio-new', [
                        'title' => "Hybrid 5G - <b>30</b>",
                        'image' => '5g-icon',
                        'icon' => asset('image/5g-icon.png'),
                        'features' => [
                            'Connettività 5G e FTTC',
                            '5G Fino a 300/30 Download/Upload',
                            'FTTH Fino a 30/3 Download/Upload',
                            '2 IP Statici pubblici',
                            'Router firewall in comodato d\'uso',
                        ],
                        'price' => 99.90,
                        'sellable' => false, 'noContact' => true])
                    @endcomponent
                </div>
                <div class="column is-4 p-3 has-text-centered">
                    @component('old.components.box-servizio-new', [
                        'title' => "Hybrid 5G - <b>100</b>",
                        'image' => '5g-icon',
                        'icon' => asset('image/5g-icon.png'),
                        'features' => [
                            'Connettività 5G e FTTC',
                            '5G Fino a 300/30 Download/Upload',
                            'FTTH Fino a 100/20 Download/Upload',
                            '2 IP Statici pubblici',
                            'Router firewall in comodato d\'uso',
                        ],
                        'price' => 109.90,
                        'sellable' => false, 'noContact' => true])
                    @endcomponent
                </div>
                <div class="column is-4 p-3 has-text-centered">
                    @component('old.components.box-servizio-new', [
                        'title' => "Hybrid 5G - <b>200</b>",
                        'image' => '5g-icon',
                        'icon' => asset('image/5g-icon.png'),
                        'features' => [
                            'Connettività 5G e FTTC',
                            '5G Fino a 300/30 Download/Upload',
                            'FTTH Fino a 200/20 Download/Upload',
                            '2 IP Statici pubblici',
                            'Router firewall in comodato d\'uso',
                        ],
                        'price' => 119.90,
                        'sellable' => false, 'noContact' => true])
                    @endcomponent
                </div>
            </div>

        </x-row>

        <x-row bg="white" class="is-vcentered reverse-columns">
            <div class="column is-6 p-6">
                <img loading="lazy" style="border-radius: 30px" src="{{ asset('image/speed/assistenza-fiberdroid.webp') }}" alt="assistenza Fiberdroid">
            </div>
            <div class="column is-6">
                <x-title role="h2" style="font-weight:800;margin-bottom:0!important;">
                    Assistenza super veloce sia in fase consulenziale che post vendita
                </x-title>
                <x-text>
                    <br>
                    Abbiamo un <b>team di esperti dedicato</b> a tua disposizione che ti sapranno consigliare la soluzione più idonea alle tue esigenze, non solo nella fase iniziale, ma per tutta la durata del contratto.
                    <br><br>
                    Assistenza tecnica veloce ed efficiente: il 90% dei nostri casi sono risolti entro le <b>entro 4 ore</b>.
                </x-text>
            </div>
        </x-row>

        <x-row bg="white" class="is-vcentered py-6 mb-6">
            <div class="column is-6">
                <x-title role="h2" style="font-weight:800">
                    <span class="has-text-blue-logo">Centralino Cloud</span>
                    Per La Tua Impresa Con Un'affidabilità Ed Una Assistenza <span class="has-text-blue-logo">Garantita</span>
                </x-title>
                <x-text>
                    Disponiamo di una piattaforma di ultima generazione, <b>personalizzabile</b> in base alle esigenze del cliente. <br>
                    Connettiamo persone e aziende ovunque esse siano, anche grazie all'app gratuita da installare <b>direttamente sul tuo smartphone</b>. <br>
                    Potrai beneficiare di un'<b>intelligenza artificiale</b>, una vera e propria segreteria virtuale, per fornire informazioni utili ai tuoi clienti aumentando l'efficenza aziendale. <br>
                    La nostra piattaforma <b>si integra con i CRM più diffusi</b> (come ad esempio Saleforce).
                </x-text>
            </div>
            <div class="is-hidden-desktop is-hidden-tablet my-6"></div>
            <div class="column is-6">
                <div style="position: relative;">
                    <div data-title="image" id="rce_section_10_3_1" class="rce-element rce-image rce-n-ai" data-ai_type="image" data-keyword="Centralino Cloud" data-image_type="img" data-width_unit="px" data-border_radius_unit="px">
                        <img loading="lazy" src="{{ asset('image/speed/fiberdroid-3cx.webp') }}" style="width: 100%; border-radius: 10px;" class="mCS_img_loaded" alt="Telefono 3cx Fiberdroid">
                    </div>
                </div>
            </div>
        </x-row>

        <x-row bg="white" class="py-6 is-vcentered reverse-columns">
            <div class="column is-6 p-4">
                <div style="position: relative;">
                    <div style="position: absolute; left: 10%; top: -20%; right:10%; bottom: -10%; ">
                        <svg width="100%" height="100%" viewBox="0 0 464 498" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M23.7255 28.9504C24.6376 11.6471 39.9769 -1.28791 57.1857 0.734667L437.502 45.4336C452.612 47.2095 464 60.0146 464 75.2285V468C464 484.569 450.569 498 434 498H30.6231C13.4341 498 -0.240175 483.586 0.664669 466.421L23.7255 28.9504Z" fill="#2da3cd" fill-opacity="0.1">
                            </path>
                        </svg>
                    </div>
                    <div data-title="image" id="rce_section_11_3_1" class="rce-element rce-image rce-n-ai" data-ai_type="image" data-keyword="Centralino Cloud" data-image_type="img" data-width_unit="px" data-border_radius_unit="px">
                        <img loading="lazy" src="{{ image('speed-body') }}" style="width: 100%; border-radius: 10px;" class="mCS_img_loaded" alt="server Fiberdroid">
                    </div>
                </div>
            </div>
            <div class="is-hidden-desktop is-hidden-tablet mb-6"></div>
            <div class="column is-6">
                <x-title role="h2" style="font-weight:800">
                    <span class="has-text-blue-logo">Connettività Affidabile</span>
                    Per I Professionisti: Le Soluzioni Di Fibra E Cloud Di <span class="has-text-blue-logo">Fiberdroid</span>
                </x-title>
                <x-text>
                    <b>Fiberdroid</b> offre una soluzione completa di <strong>linea internet professionale</strong> e con possibilità di backup. Multiconnetività con i più <strong>elevati standard di sicurezza e qualità del mercato</strong>. La soluzione di Fibra dedicata è disponibile su <strong>tutto il territorio italiano</strong>, anche nelle zone più remote, attualmente prive di qualsiasi connessione a internet.
                </x-text>
            </div>
        </x-row>

        <x-row style="background-image: url('{{ asset('image/speed/mondo-fiber.webp') }}'); background-size: cover">
            <div class="column is-7">
                <div class="box my-6">
                    <x-title>
                        <b class="is-size-3">
                            "Tutto in un solo provider"
                        </b>
                    </x-title>
                    <x-text>
                    <span class="is-size-6">
                        Siamo l'azienda che offre <b>un'unica soluzione per tutte le tue esigenze di connettività aziendale</b>, inclusi Internet, wifi, centralino cloud, fibra e piattaforma tecnologica. Con oltre 10 anni di esperienza nel settore IT & TLC, siamo pronti a garantirti una connettività veloce, un'assistenza affidabile e tecnologie innovative, sia in Italia che all'estero.
                        <br><br>
                        Siamo specializzati nella <b>customizzazione</b> di progetti di messa in rete e di telecomunicazione, fornendo ai nostri clienti business una consulenza attenta e completa, che sostiene la loro crescita aziendale. Ci impegniamo a essere trasparenti, garantendo la puntualità e la qualità del nostro servizio.
                        <br><br>
                        Grazie alla nostra area clienti, puoi aggiungere, rimuovere o modificare servizi, come linee di backup, modem o nuovi numeri VoIP. E con la nostra multiconnettività, potrai avere <b>tutti i servizi di cui hai bisogno con un unico fornitore e una sola fattura</b>. Non lasciare che la gestione della tua tecnologia ti complichi la vita, scegli la semplicità e l'efficienza con la nostra azienda.
                    </span>
                    </x-text>
                </div>
            </div>
        </x-row>

        <x-row bg="white" class="my-6 py-6 px-6 is-vcentered">
            <div class="column is-6 px-5" style="position:relative;">
                <div style="position: relative; width:100%; max-width:600px; z-index: 0">
                    <svg viewBox="0 0 557 647" fill="none" xmlns="http://www.w3.org/2000/svg" style="width: 90%; position:absolute; left:-30px; top:-30px;">
                        <rect opacity="0.1" x="0.0957031" y="0.515625" width="556.487" height="645.691" rx="24" fill="#2da3cd">
                        </rect>
                    </svg>
                    <div style="position: relative">
                        <img loading="lazy" src="{{ asset('image/speed/routers.webp') }}" alt="Router monitoring portale Fiberdroid" style="width: 100%; border-radius: 31px;">
                    </div>
                    <!--<svg width="271" height="272" viewBox="0 0 271 272" fill="none" xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -100px; top: -100px;">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M215.462 117.123C218.134 133.325 218.638 149.674 213.772 165.357C208.675 181.783 201.644 199.063 187.16 208.335C172.911 217.456 154.402 217.052 137.758 214.018C123.301 211.382 112.404 200.843 100.308 192.5C89.4133 184.987 78.5695 178.026 70.4355 167.587C61.008 155.487 50.125 142.886 49.7019 127.554C49.2661 111.757 57.5665 96.6585 68.1548 84.9267C78.2908 73.696 93.4389 69.91 107.266 63.7726C122.236 57.1282 136.222 46.3012 152.559 47.4637C169.833 48.6929 186.477 57.3818 198.084 70.2351C209.486 82.8617 212.694 100.337 215.462 117.123Z" fill="#FEC460">
                        </path>
                        <mask id="mask0_493_1621" maskUnits="userSpaceOnUse" x="49" y="47" width="169" height="169" data-desktop_style="mask-type:alpha">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M215.462 117.123C218.134 133.325 218.638 149.674 213.772 165.357C208.675 181.783 201.644 199.063 187.16 208.335C172.911 217.456 154.402 217.052 137.758 214.018C123.301 211.382 112.404 200.843 100.308 192.5C89.4133 184.987 78.5695 178.026 70.4355 167.587C61.008 155.487 50.125 142.886 49.702 127.554C49.2661 111.757 57.5665 96.6585 68.1547 84.9267C78.2908 73.696 93.4389 69.91 107.266 63.7726C122.236 57.1282 136.222 46.3012 152.559 47.4637C169.833 48.6929 186.477 57.3818 198.084 70.2351C209.486 82.8617 212.694 100.337 215.462 117.123Z" fill="#FEC460">
                            </path>
                        </mask>
                        <g mask="url(#mask0_493_1621)">
                            <path d="M102.707 90.0138C88.2404 79.4227 54.0587 70.32 33.0611 118.638C6.81407 179.036 57.9166 70.6017 79.2598 118.759C100.603 166.916 1.43111 159.184 60.3731 170.616C119.315 182.048 176.516 131.786 119.997 129.057C63.4782 126.327 38.9171 113.762 62.8642 101.324C86.8113 88.8851 97.8 95.7079 86.3228 67.9584C74.8455 40.2089 -83.1985 43.6285 -6.01614 97.203" stroke="white" stroke-width="2" stroke-linecap="round">
                            </path>
                        </g>
                    </svg>-->
                </div>
            </div>

            <div class="column is-6">
                <x-text color="grey-dark" class="mb-0">
                    <span class="is-size-4">Scopri i nostri punti di forza!</span>
                </x-text>
                <x-title color="blue-logo" uppercase="false">
                    <b class="is-size-2" style="font-weight: 900">Perché scegliere Fiberdroid?</b>
                </x-title>

                @component('old.components.landing-box', ['title' => 'Fibra Ottica Garantita', 'icon' => 'language'])
                    Velocità di banda garantita e linea di backup
                @endcomponent

                @component('old.components.landing-box', ['title' => 'Portiamo la Fibra dove gli altri non arrivano', 'icon' => 'landscape'])
                    Troviamo soluzioni per qualsiasi situazione, anche nei posti più remoti.
                @endcomponent

                @component('old.components.landing-box', ['title' => 'Piattaforma di monitoraggio in tempo reale', 'icon' => 'computer'])
                    Tramite la nostra piattaforma online è possibile monitorare in tempo reale lo stato di tutti i servizi attivi e l'efficenza della rete.
                @endcomponent

                @component('old.components.landing-box', ['title' => 'Multiconnettività di rete', 'icon' => 'cell_tower'])
                    Multiconnettività su diverse tipologie di rete (ftth, fttc, wimax, ecc..) con bilanciamento e failover
                @endcomponent

                @component('old.components.landing-box', ['title' => 'Assistenza tecnica superveloce', 'icon' => 'headset_mic'])
                    Assistenza tecnica con risoluzione guasti superveloce
                @endcomponent
            </div>

        </x-row>

        <x-row class="has-text-centered is-multiline py-6" style="background-color: #2da3cd0a">
            <div class="column is-8 mx-auto">
                <x-text color="blue-logo" class="has-text-centered">
                    Cosa dicono di noi
                    <br>
                    <span class="has-text-black is-size-3">
                        <b>
                            Più di 1.500 servizi e clienti attivi in Italia ci hanno scelto, con il 98% di tasso di soddisfazione
                        </b>
                    </span>
                </x-text>
            </div>
        </x-row>

        <x-row class="pb-6" style="background-color: #2da3cd0a">
            <div class="column is-4">
                @component('old.components.review', ['image' => asset('image/review/denicar.jpeg'), 'title' => "Raffaele", 'alt' => "Denicar Logo", 'review' => "Tutti i servizi in un'unica fattura. Consumi, Prodotti e Centralino: un bel vantaggio per la nostra amministrazione!”"])@endcomponent
            </div>
            <div class="column is-4">
                @component('old.components.review', ['image' => asset('image/review/evoca.png'), 'title' => "Simone", 'alt' => "Evoca Logo", 'review' => "Rapidi, gentili e competenti. E con il sistema di Ticket, ogni emergenza ha il suo supporto: questa sì che è assistenza!"])@endcomponent
            </div>
            <div class="column is-4">
                @component('old.components.review', ['image' => asset('image/review/gheron.jpeg'), 'title' => "Ivan", 'alt' => "Gheron Logo", 'review' => "Abbiamo tutte le filiali sotto controllo grazie alla Dashboard: Consumi, Supporto e Prodotti, tutto in un click"])@endcomponent
            </div>
        </x-row>

        <x-row bg="blue-logo" class="py-6 is-vcentered reverse-columns">
            <div class="column is-6" id="contact">
                <div class="content is-hidden-touch">
                    <x-title color="white" class="is-h3 mb-2">
                        <span class="is-hidden-touch is-size-3">Contattaci Ora!</span>
                    </x-title>
                </div>

                <div class="box">
                    <x-form uname="speed"></x-form>
                </div>
            </div>
            <div class="column is-6">

                <div class="content is-hidden-desktop">
                    <x-title color="white" class="is-h3 mb-2">
                        <span class="is-hidden-touch">Contattaci Ora!</span>
                        <span class="is-hidden-desktop">RICHIEDI MAGGIORI INFORMAZIONI</span>
                    </x-title>

                    <x-separator color="fd-warning" width="20%"></x-separator>

                </div>

                <div class="content py-3 has-text-white">
                    Il nostro servizio clienti è sempre a tua disposizione per rispondere alle tue domande.
                </div>

                <x-title color="white" class="is-h3 mb-2">
                    Contatti
                </x-title>
                <x-separator color="fd-warning" width="20%"></x-separator>
                <div class="content py-3 has-text-white">
                    <x-icon-text icon="fa fa-phone">
                        <x-link to="tel:+39 02 83595676" color="white">
                            <span class="is-h1">02 83595676</span>
                        </x-link>
                    </x-icon-text>
                </div>
            </div>
        </x-row>
    </div>

    <style>
        .wrapper {
            width: 80%;
            background-image: linear-gradient(91.19deg, #865a9d 3.45%, rgba(196, 196, 196, 0) 95.96%);
        }

        @media(max-width: 767px) {
            .reverse-columns {
                flex-direction: column-reverse;
                display: flex;
            }
        }
    </style>
@endsection
