@extends('layouts._layout')

@section('title', "Guide - HOW TO e manuali delle reti internet | Fiberdroid")
@section('description', "Le guide di fiberdroid: HOW TO delle connessioni internet per il Business")

@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-link to="blog">
                <x-title color="white" role="h1">Guide</x-title>
            </x-link>
        </div>
    </x-row>
@endsection

@section('content')

    <x-row thin="true" class="is-multiline">
        @foreach($articles as $article)
            <div class="column is-4-desktop is-6-tablet is-12-mobile is-flex" style="">
                <div class="card no-card fd-card-blog">
                    <div class="card-image">
                        @if($article->image && $article->image->full_url)
                            <figure class="image is-4by5">
                                <img src="{{ $article->image && $article->image->full_url ? $article->image->full_url : 'https://bulma.io/images/placeholders/1280x960.png'}}" alt="{{ $article->image ? $article->image->alt : "" }}" title="{{ $article->image ? $article->image->title : "" }}">
                            </figure>
                        @else
                            @php($image = \App\Models\FdImage::where(['model_id' => $article->id, 'model_type' => \App\Models\Article::class])->first())

                            <figure class="image is-4by5">
                                @if($image)
                                    <img src="{{ config('app.ac_url') }}/images/{{ $image->image->filename }}">
                                @else
                                    <img src="https://bulma.io/images/placeholders/256x256.png" />
                                @endif
                            </figure>
                        @endif
                    </div>
                    <div class="card-content fd-card-content-blog">
                        <div class="content">
                            <x-subtitle role="h2" color="fd-primary">
                                {{ $article->title }}
                            </x-subtitle>
                            <x-text>
                                <p>
                                    {{ $article->meta_description }}...
                                    <br>
                                </p>
                            </x-text>
                        </div>
                        <div class="content mt-auto">
                            <x-text>
                                <p>
                                    <!--<time datetime="{{ $article->created_at }}">{{ $article->published_at }}</time>-->
                                    <a class="has-text-fd-secondary is-hidden-touch" href="{{ route('guide', ['article' => $article->url]) }}">Continua >></a>
                                </p>
                                <a href="{{ route('guide', ['article' => $article->url]) }}" class="button is-fd-primary is-small is-hidden-desktop">Continua</a>
                            </x-text>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    </x-row>

@endsection
