@extends('layouts._layout')

@section('title', "Verifica la tua copertura da servizi in fibra e rame | Fiberdroid")
@section('description', "Verifica lo stato delle linee internet di casa tua o della tua zona")
@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-title color="white" class="is-h4" role="p">Verifica la tua copertura</x-title>
        </div>
    </x-row>
@endsection

@section('content')

    <x-row thin="true" bg="white">
        <div class="column is-12">
            @livewire('verifica-copertura')
        </div>
    </x-row>
@endsection

@push('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endpush
