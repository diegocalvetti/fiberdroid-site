<x-row thin="true" bg="fd-primary" class="is-mobile is-multiline py-4" id="contact">
    <div class="column is-12">
        <x-text color="grey-light" class="mb-0">
            <p>Sei interessato ai nostri servizi<br class="is-hidden-desktop"> ma hai qualche dubbio?</p>
        </x-text>
        <x-subtitle color="white" uppercase="false">
            Non esitare, contattaci senza impegno, <br class="is-hidden-desktop"> richiedi una consulenza gratuita
        </x-subtitle>
    </div>
</x-row>
<x-row thin="true" bg="fd-light" class="is-mobile is-multiline py-4">
    <div class="column is-6-desktop is-12-touch mx-auto">
        <x-title color="fd-warning is-h4" uppercase="false">Richiedi la demo</x-title>
        <x-text>
            <p>Compila il form di contatto, ricontatteremo in brevissimo tempo, grazie.</p>
        </x-text>
        <x-form uname="demo"></x-form>
    </div>
    <!--
    <div class="column is-3-desktop is-offset-1-desktop is-12-touch is-offset-0-touch ">
        <x-title color="fd-warning is-h4" uppercase="false">Download</x-title>

        <x-text>
            <p>Coming soon...</p>
        </x-text>

    </div>
    -->
</x-row>
