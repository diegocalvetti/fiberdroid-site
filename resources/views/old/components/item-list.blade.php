<div class="columns is-vcentered is-mobile">

    <div class="column is-1">
        <div class="circle-container">
            <div class="circle has-background-{{ $color }}"></div>
        </div>
    </div>
    <div class="column is-11 is-size-5 pl-{{ $gap }}">
        {{ $slot }}
    </div>

</div>

@push('css')
    <style>
        .circle-container {
            transform: scale(0.75);
        }
        .circle {
            width: 2rem;
            height: 2rem;
            border-radius: 50%;
        }
        .circle:after {
            content: " ";
            position: absolute;
            width: 1rem;
            height: 1rem;
            border-radius: 50%;
            background: white;

            margin-left: calc(1/2 * 1rem);
            margin-top: calc(1/2 * 1rem);
        }

    </style>
@endpush
