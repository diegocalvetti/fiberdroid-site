<div id="{{ $id }}" class="is-hidden-desktop pb-1" style=" position: absolute; height: 110vh; width: 100vw; left: 110vw; top: 0; z-index: 36">
    <x-row class="is-multiline pt-6" bg="fd-lightblue" style="width: 100vw; height: 110vh;">
        <div class="column is-12" style="width: 100vw">
            <div class="columns is-mobile is-vcentered">
                <div class="column is-5">
                    <x-link to="home">
                        <figure class="image ml-0 fd-base-logo">
                            <img src="{{ image("logo-blu-orizzontale") }}" {!! image_attr("logo-blu-orizzontale") !!}>
                        </figure>
                    </x-link>
                </div>
                <div class="column is-6 is-offset-1 has-text-right">
                    <span data-action="toggle-subnavbar" data-target="#{{ $id }}">
                        <x-icon icon="fa fa-undo" color="grey" float="right"></x-icon>
                    </span>
                </div>
            </div>
        </div>
        <div class="column is-12 pt-6">
            {{ $slot }}
        </div>
    </x-row>
</div>
