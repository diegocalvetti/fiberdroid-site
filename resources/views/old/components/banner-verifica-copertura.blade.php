<x-banner image="{{ asset('images/bg-cta-fai-un-test.jpg') }}">
    <div class="columns is-vcentered is-multiline has-text-centered py-6">
        <div class="column is-12 py-0 my-3">
            <x-subtitle color="white">Fai un test di copertura</x-subtitle>
        </div>
        <div class="column is-12 pt-0 pb-0">
            <x-text color="white" class="mb-1">
                <p class="is-h4">Verifica se la tua zona è coperta</p>
            </x-text>
            <x-link to="verifica-copertura" target="_blank">
                <x-button bg="fd-warning" color="white">Avvia test!</x-button>
            </x-link>
        </div>
    </div>
</x-banner>
