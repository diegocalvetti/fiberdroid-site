<div class="box">
    <div class="columns is-multiline">

        <div class="column is-4 mx-auto has-image-centered">
            <img src="{{ image($icon) }}" class="image" style="width: 64px; height: 64px" {!! image_attr($icon) !!}>
        </div>
        <div class="column is-12 has-text-centered has-text-uppercase">
            <p class="is-size-5 is-h3 has-text-fd-primary">{{ $title }}</p>
        </div>
        <div class="column is-12" style="position: relative; bottom: 0">
            @if($contact)
                <div class="columns is-mobile is-vcentered has-background-grey-lighter mb-0">
                    <div class="column is-1">
                        <i class="fa fa-envelope has-text-fd-primary is-size-5"></i>
                    </div>
                    <div class="column is-11 pl-5">
                        <x-link to="mailto:{{ $mail }}">
                            <x-text>
                                <p>{{ $mail }}</p>
                            </x-text>
                        </x-link>
                    </div>
                </div>
                <div class="columns is-mobile is-vcentered has-background-grey-lighter mt-1">
                    <div class="column is-1">
                        <i class="fa fa-phone has-text-fd-primary is-size-5"></i>
                    </div>
                    <div class="column is-11 pl-5">
                        <x-link to="tel:+39 {{ $phone }}">
                            <x-text>
                                <p>{{ $phone }}</p>
                            </x-text>
                        </x-link>
                    </div>
                </div>
            @endif
            {{ $slot }}
        </div>
    </div>
</div>
