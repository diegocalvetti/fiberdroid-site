<div class="box">
    <div class="columns is-multiline">

        <div class="column is-4 mx-auto has-image-centered">
            <img src="{{ $icon }}" class="image">
        </div>
        <div class="column is-12 has-text-centered has-text-uppercase">
            <p class="is-size-4 is-size-5-desktop is-h3 has-text-fd-primary">{{ $title }}</p>
        </div>
        <div class="column is-12 px-6">
            <div class="content is-size-4 has-text-fd-primary has-text-left">
                {{ $slot }}
            </div>
        </div>
        <div class="column is-12">
            <div class="button mx-auto is-fd-secondary">Scopri</div>
        </div>
    </div>
</div>
