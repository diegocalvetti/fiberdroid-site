<div class="box">
    <div class="columns">
        <div class="column is-2">
            <figure class="image is-64x64" style="background-color: #9ccbd9; display: flex; vertical-align:middle; border-radius: 50%">
                <span style="display: inline; margin: auto" class="material-symbols-outlined has-text-blue-logo is-size-2">{{ $icon }}</span>
            </figure>
        </div>
        <div class="column is-10">
            <x-subtitle color="fd-primary" uppercase="false">
                <span class="is-size-5-touch is-size-6-desktop">
                    <b>{{ $title }}</b>
                </span>
            </x-subtitle>
            <x-text color="grey">
                {!! $slot !!}
            </x-text>
        </div>
    </div>
</div>
