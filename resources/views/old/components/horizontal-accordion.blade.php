<div class="columns is-multiline is-vcentered">
    <div class="column is-12 is-6-desktop">

        @foreach($elements as $key => $title)
            <div class="columns has-border-b-4 has-border-fd-warning has-text-black-ter  has-border-r-4 has-border-fd-warning">
                <div class="column is-12">
                    <p class="is-h3 is-size-4" data-action="open" data-target="{{ $key }}">
                        <i>
                            {{ $title }}
                        </i>
                    </p>
                </div>
            </div>
        @endforeach

    </div>
    <div class="column is-12 is-6-desktop pl-6">
        {{ $slot }}
    </div>
</div>

@push('css')
    <style>
        [data-el] {
            display: none;
        }
    </style>
@endpush