<a href="{{ $to }}" target="{{ $target }}" class="not-link has-text-{{ $color }}" rel="{{ $target == "_blank" ? "noopener, " : "" }}"
   @if($dataAction)data-action="{{ $dataAction }}"@endif
   @if($ariaLabel)aria-label="{{ $ariaLabel }}"@endif
>
    {{ $slot }}
</a>
