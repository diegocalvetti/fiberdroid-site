<div id="fd-banner-{{ $uniqueId }}" class="columns is-vcentered p-5" style="display: flex;  background-position: center; background-size: cover">
    <div class="column is-12">
        {{ $slot }}
    </div>
</div>
@push('css')
    <style>
        #fd-banner-{{ $uniqueId }} {
            background-image: url('{{ $image }}');
        }
    </style>
@endpush