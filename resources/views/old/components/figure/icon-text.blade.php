<span class="icon-text">
  <span class="icon">
    <i class="{{ $icon }}"></i>
  </span>
  <span>{{ $slot }}</span>
</span>
