<figure class="image {{ $float ? "mr-0 has-text-right" : ""}} @if($size) is-{{ $size }} @endif">
    @if( $isImage )
        <img src="{{ asset($icon) }}">
    @else
        <i class="icon @if($disabled) has-text-grey @else is-clickable has-text-{{ $color }} @endif px-2 is-size-3-mobile is-size-2-tablet is-size-4-desktop {{ $icon }} {{ $class }}" style="display: inline"></i>
    @endif
</figure>
