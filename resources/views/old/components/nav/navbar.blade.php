<x-row bg="{{ $bg }}" style="position: relative; z-index: 1; margin-bottom: 0;" class="is-unselectable">
    <div class="column is-12">
        <div class="columns is-multiline is-mobile is-vcentered mt-3">
            <div class="column is-12 fd-nav-separator">
                <span class="is-hidden-touch">
                    <x-navbar-link color="white" to="chi-siamo">
                        <span class="is-size-7 is-h2">Chi siamo</span>
                    </x-navbar-link>
                    <x-navbar-link color="white" class="pl-3" to="blog">
                        <span class="is-size-7 is-h2">Blog</span>
                    </x-navbar-link>
                    <x-navbar-link color="white" class="pl-3" to="guides">
                        <span class="is-size-7 is-h2">Guide</span>
                    </x-navbar-link>
                    <x-navbar-link color="white" class="pl-3">
                        <span class="is-size-7 is-h2" data-action="toggle-slide" data-target="#modal-contact">Contatti</span>
                    </x-navbar-link>
                </span>

                <x-navbar-link float="right" class="pl-5 pr-0" to="{{ $config['link_areaclienti'] }}" target="_blank">
                    <x-text color="fd-warning">
                        <span class="is-size-6-desktop is-h3 is-size-7-touch">Area clienti</span>
                    </x-text>
                </x-navbar-link>
                <x-navbar-link color="fd-warning" class="pl-5 pr-0" float="right" to="assistenza">
                    <x-text color="fd-warning">
                        <span class="is-size-6-desktop is-h3 is-size-7-touch">Assistenza</span>
                    </x-text>
                </x-navbar-link>
                <x-navbar-link color="fd-warning" float="right" target="_blank" to="{{ $config['link_speedtest'] }}">
                    <x-text color="fd-warning">
                        <span class="is-size-6-desktop is-h3 is-size-7-touch">Speed test</span>
                    </x-text>
                </x-navbar-link>
            </div>

            <div class="column is-5-touch is-3-desktop">
                <x-link to="home">
                    <figure class="image fd-base-logo">
                        <img width="420" height="76" src="{{ image("logo-bianco-orizzontale") }}" {!! image_attr("logo-bianco-orizzontale") !!}>
                    </figure>
                </x-link>
            </div>
            <div class="column is-6 has-text-centered is-hidden-touch">

                <x-navbar-link-dropdown text="Servizi" :links="$link_servizi" base="servizio" style="left: -10vw"></x-navbar-link-dropdown>
                <x-navbar-link color="white"> </x-navbar-link>
                <x-navbar-link-dropdown text="Prodotti" :links="$link_prodotti" base="servizio" style="left: -16vw"></x-navbar-link-dropdown>
                <x-navbar-link color="white"> </x-navbar-link>
                <x-navbar-link-dropdown text="Soluzioni" :links="$link_soluzioni" base="soluzione" style="left: -23vw"></x-navbar-link-dropdown>

            </div>
            <div class="column is-3 is-hidden-touch">
                <div class="columns is-vcentered" style="float: right">
                    <x-fiberdroid-icon></x-fiberdroid-icon>
                </div>
            </div>
            <div class="column is-7 is-hidden-desktop">
                <span data-action="toggle-navbar" data-target="#modal-navbar">
                    <x-icon icon="fa fa-bars" float="right"></x-icon>
                </span>
            </div>
        </div>
    </div>
</x-row>

