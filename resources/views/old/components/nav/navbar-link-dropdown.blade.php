<div class="dropdown is-hoverable">
    @php
        $uniqid = uniqid();
    @endphp
    <div class="dropdown-trigger" aria-haspopup="true" aria-controls="dropdown-menu-{{ $uniqid }}">
        <x-navbar-link size="is-size-4" color="white" class="is-h4">{{ $text }}</x-navbar-link>
    </div>
    <div class="dropdown-menu" id="dropdown-menu-{{ $uniqid }}" role="menu" style="width: 40vw; {{ $style ?: "" }}">
        <div class="dropdown-content has-background-fd-primary">
            <div class="dropdown-item">
                <div class="columns is-multiline has-text-left">
                    <div class="column is-12 pb-5">
                        <x-subtitle color="fd-warning" class="is-h3" mb="0">{{ $text }}</x-subtitle>
                    </div>
                    <div class="column is-12 pt-0">
                        <div class="columns is-multiline">
                            @foreach($links as $to => $link)
                                <div class="column pt-0 pb-2 is-4 is-h4">
                                    <div class="columns is-vcentered">
                                        <div class="column is-1">
                                            <figure class="image is-24x24" style="visibility: hidden" id="navlink-{{ $to }}">
                                                <img src="{{ asset("images/loghi/pittogramma-fiberdroid-orange.svg") }}">
                                            </figure>
                                        </div>
                                        <div class="column is-11 pl-4" data-action="toggle-hover" data-target="#navlink-{{ $to }}" >
                                            <x-link to="{{ $base }}" params="{{ $to }}">{{ $link }}</x-link>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
