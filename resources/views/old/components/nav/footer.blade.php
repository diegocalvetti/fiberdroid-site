<x-row margin="false" class="is-mobile">
    <div class="column is-12">
        <x-row bg="fd-primary" class="is-multiline py-6">
            <div class="column is-12-touch is-4-desktop">
                <x-link to="home">
                    <figure class="image" style="width: 40%">
                        <img width="420" height="76" src="{{ asset("images/logo-bianco-horizontal.svg") }}" alt="logo bianco orizzontale Fiberdroid">
                    </figure>
                </x-link>
                <x-text color="white" class="pt-2">
                    <p>
                        Siamo l'azienda che ti permette di avere un'unica assistenza
                        per internet, wifi, centralino cloud, fibra, connettività e
                        piattaforma tecnologica.
                    </p>
                </x-text>
            </div>
            <div
                class="column is-12-touch is-5-desktop is-flex is-flex-direction-column is-justify-content-space-between">
                <x-text color="white" class="mt-auto">
                    <p>
                        <span><b>Via</b> Enrico Cosenz, 54 20158 Milano</span><br>
                        <span><b>P.Iva & C.F.</b> 07057060969</span><br>
                        <span><b>Tel.</b> +39 02 83595676</span>
                    </p>
                </x-text>
            </div>
            <div class="column is-3 is-hidden-touch is-flex is-flex-direction-column is-justify-content-space-between">
                <div class="columns mt-auto">
                    <div class="column has-text-left is-unselectable">
                        <x-text color="grey-lighter">
                            <p>
                                <x-underlined-text>
                                    Seguici sui social
                                </x-underlined-text>
                            </p>
                        </x-text>
                    </div>
                </div>
                <div class="columns is-vcentered" style="width: min-content">
                    <div class="column">
                        <x-link to="{{ $config['link_youtube'] }}" target="_blank" aria-label="Link al canale YouTube di Fiberdroid">
                            <span class="is-hidden">Link Youtube</span>
                            <x-icon icon="fab fa-youtube" class="hover-has-text-danger"></x-icon>
                        </x-link>
                    </div>
                    <div class="column">
                        <x-link to="{{ $config['link_linkedin'] }}" target="_blank" aria-label="Link al pagina Linkedin di Fiberdroid">
                            <span class="is-hidden">Link Youtube</span>
                            <x-icon icon="fab fa-linkedin" class="hover-has-text-fd-secondary"></x-icon>
                        </x-link>
                    </div>
                    <div class="column">
                        <x-link to="{{ $config['link_twitter'] }}" target="_blank" aria-label="Link alla pagina Twitter di Fiberdroid">
                            <span class="is-hidden">Link Youtube</span>
                            <x-icon icon="fab fa-twitter" class="hover-has-text-fd-secondary"></x-icon>
                        </x-link>
                    </div>
                    <div class="column">
                        <x-link to="{{ $config['link_twitter'] }}" target="_blank" aria-label="Link alla pagina Twitter di Fiberdroid">
                            <span class="is-hidden">Link Youtube</span>
                            <x-icon icon="fab fa-youtube" class="hover-has-text-fd-secondary"></x-icon>
                        </x-link>
                    </div>
                </div>
            </div>

            <div class="column is-12 is-hidden-touch py-3"></div>

            <div class="column is-12-touch is-4-desktop">
                <x-text color="grey-lighter">
                    <p>
                        <x-underlined-text>
                            ©2021 Fiberdroid
                        </x-underlined-text>
                    </p>
                </x-text>
            </div>
            <div class="column is-12-touch is-4-desktop">
                <x-text color="grey-lighter">
                    <p>
                        Fiberdroid s.r.l.
                    </p>
                </x-text>
            </div>
            <div class="column is-12-touch is-4-desktop">
                <x-text color="grey-lighter">
                    <x-link to="policy.privacy">
                            <span>
                                <x-underlined-text>
                                    Privacy policy
                                </x-underlined-text>
                            </span>
                    </x-link>

                    <span class="px-2"> - </span>

                    <x-link to="policy.cookies">
                            <span>
                                <x-underlined-text>
                                    Cookie policy
                                </x-underlined-text>
                            </span>
                    </x-link>

                    <span class="px-2"> - </span>
                    <br class="is-hidden-tablet">
                    <x-link to="https://conciliaweb.agcom.it/" target="_blank">
                                <span>
                                    <x-underlined-text>
                                        Concilia web
                                    </x-underlined-text>
                                </span>
                    </x-link>


                </x-text>

            </div>
        </x-row>
    </div>
</x-row>
