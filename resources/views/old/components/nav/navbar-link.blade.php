<{{ $to ? 'a' : 'span' }} @if($to) href="{{ $to }}" target="{{ $target }}" @endif class="{{ $class }} is-clickable has-text-{{ $color }} has-text-uppercase pr-2" style="{{ $float ? "float:$float;" : "" }}" >
    {{ $slot }}
</{{ $to ? 'a' : 'span' }}>
