<div class="column">
    <x-link to="{{ $config['link_youtube'] }}" target="_blank" aria-label="Link al canale YouTube di Fiberdroid">
        <span class="is-hidden">Link Youtube</span>
        <x-icon icon="fab fa-youtube" class="{{ $colored ? 'has-text-fd-primary' : 'hover-has-text-danger' }}"></x-icon>
    </x-link>
</div>
<div class="column">
    <x-link to="{{ $config['link_linkedin'] }}" target="_blank" aria-label="Link alla pagina Linkedin di Fiberdroid">
        <span class="is-hidden">Link Linkedin</span>
        <x-icon icon="fab fa-linkedin" class="{{ $colored ? 'has-text-fd-primary' : 'hover-has-text-fd-primary' }}"></x-icon>
    </x-link>
</div>
<div class="column">
    <x-link to="{{ $config['link_twitter'] }}" target="_blank" aria-label="Link alla pagina Twitter di Fiberdroid">
        <span class="is-hidden">Link Twitter</span>
        <x-icon icon="fab fa-twitter" class="{{ $colored ? 'has-text-fd-primary' : 'hover-has-text-fd-secondary' }}"></x-icon>
    </x-link>
</div>
