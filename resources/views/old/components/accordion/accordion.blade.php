<div class="card @if(!$active) is-shadowless @endif is-fullwidth mb-3" id="{{ $id }}">
    <header class="card-header card-toggle is-clickable is-shadowless">
        <p class="card-header-title has-text-fd-warning" style="font-weight: 500">
            {{ $title }}
        </p>
        @if(!isset($notIcon) && $notIcon)
            <span class="card-header-icon">
                <i class="fa fa-angle-down"></i>
            </span>
        @endif
    </header>
    <div class="card-content pt-0 @if(!$active) is-hidden @endif">
        {{ $slot }}
    </div>
</div>
