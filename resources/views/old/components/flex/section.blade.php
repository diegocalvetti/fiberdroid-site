<div class="column is-12 {{ !$section->isVoid ? "mt-5" : "mt-2" }}" id="{{ \Illuminate\Support\Str::slug($section->title) }}">

    @if( strlen($section->title) > 0)
        <x-title color="fd-primary" role="{{ $titleRole }}">{{ $section->title }}</x-title>
    @endif

    @if( strlen($section->content) > 0 )
        <x-text>
            {!! $section->content !!}
        </x-text>
    @endif

    <x-section-extra :section="$section"></x-section-extra>

    @if($section->subsections->count() > 0)
        <div class="columns is-multiline">
            @foreach($section->subsections as $subsection)
                <div class="column is-12" id="{{ \Illuminate\Support\Str::slug($subsection->title) }}">
                    <x-subtitle color="fd-primary" class="is-h3">{{ $subsection->title }}</x-subtitle>
                    <x-text>
                        {!! $subsection->content !!}
                    </x-text>

                    <x-section-extra :section="$subsection"></x-section-extra>
                </div>

                @foreach($subsection->subsections as $thirdsection)
                    <div class="column is-12" id="{{ \Illuminate\Support\Str::slug($thirdsection->title) }}">
                        <x-subtitle color="fd-primary" class="is-h3" role="h4">{{ $thirdsection->title }}</x-subtitle>
                        <x-text>
                            {!! $thirdsection->content !!}
                        </x-text>

                        <x-section-extra :section="$thirdsection"></x-section-extra>
                    </div>
                @endforeach

            @endforeach
        </div>
    @endif
</div>
