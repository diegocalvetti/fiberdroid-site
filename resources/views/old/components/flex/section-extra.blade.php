@if( sizeof($section->cards) > 0)
    <div class="columns is-multiline">
        @foreach( $section->cards as $card)
            @if(is_array($card))
                <div class="column is-6 mx-auto">
                    @component('old.components.box-servizio', [
                    'title' => $card['title'],
                    'sellable' => !isset($card['sellable']) || $card['sellable'],
                    'price' => isset($card['price_label']) ? $card['price_label'] : $card['price'],
                    'size' => isset($card['size']) ? $card['size'] : "96x96",
                     'features' => $card['features'],
                     'icon' => isset($card['image']) ? $card['image'] : $card['icon']
                    ])
                    @endcomponent
                </div>
            @endif
        @endforeach
    </div>
    @if(isset($section->cards['description']))
        <div class="columns">
            <div class="column is-12">
                <x-text>
                    {!! $section->cards['description'] !!}
                </x-text>
            </div>
        </div>
    @endif
@endif

@if( $section->img )
        <div class="columns">
            <div class="column is-12">
                <img src="{{ asset('images/'.$section->img[0]) }}" class="image">
            </div>
        </div>
@endif

@if( $section->photo )
    <div class="columns">
        <div class="column is-12">
            <img src="{{ $section->photo->full_url }}" class="image" alt="{{ $section->photo->alt }}" title="{{ $section->photo->title }}">
        </div>
    </div>
@endif

@foreach( $section->accordions as $title => $content)
    <x-accordion :active="isset($content['isActive']) && $content['isActive']" title="{{ $title }}" id="{{ \Illuminate\Support\Str::slug($title) }}">
        <x-text>
            {!! $content['text'] !!}
        </x-text>
    </x-accordion>
@endforeach
