<div id="{{ $id }}" class="columns has-background-{{ $bg ?? "" }} {{ $margin ? $thin ? "fd-big-padding" : "fd-base-padding" : "" }} {{ $class }}" @if($style) style="{!! $style !!}" @endif>
    {{ $slot }}
</div>
