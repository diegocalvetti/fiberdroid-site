<div class="columns">
    <div class="column is-12-mobile is-offset-0-mobile is-8-tablet is-offset-2-tablet is-offset-0-desktop is-12-desktop">
        <div class="card fd-box-soluzione">
            <div class="card-image pt-2">
                <figure class="image mx-auto my-2 is-96x96">
                    <img data-src="{{ image($icon) }}" class="image lazy is-96x96" {!! image_attr($icon) !!}>
                </figure>
            </div>
            <div class="card-content pt-2">
                <x-title color="fd-warning" class="is-h4 is-size-6-desktop has-text-centered">{{ $title }}</x-title>
                <x-text class="pt-2 has-text-centered-touch has-text-left-desktop">{{ $slot }}</x-text>
                <x-link to="soluzione" params="{{ \Illuminate\Support\Str::slug($title) }}">
                    <x-button class="has-text-centered">Scopri</x-button>
                </x-link>
            </div>
        </div>
    </div>
</div>
