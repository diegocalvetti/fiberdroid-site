<div class="columns">
    <div class="column">
        <div class="circle-box mx-auto">

            <div class="semi-circle" data-text="{{ $text }}">

            </div>
        </div>

        <p class="has-text-centered is-h4 is-size-6 has-text-white pt-2">
            {{ $desc }}
        </p>

    </div>
</div>

@push('css')
    <style>
        .circle-box {
            display: flex;
            justify-content: center;
            align-items: center;

            width: 6rem;
            height: 6rem;
            border-radius: 50%;
            border: solid white 1px;
        }

        .semi-circle {
            display: inline-block;

            width: 4rem;
            height: 2rem;
            border: solid white 1px;
            border-bottom-left-radius: 8rem;
            border-bottom-right-radius: 8rem;
            border-top: none;
            margin-top: 2rem;

        }

        .semi-circle:before {
            content: attr(data-text);

            position: absolute;


            margin-top: -2.2rem;

            width: 4rem;
            color: white;
            text-align: center;
            font-size: 1.5rem;
        }

    </style>
@endpush