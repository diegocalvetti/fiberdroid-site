<blockquote class="fd-blockquote pl-5 pt-5 pb-0 mb-5">
    <x-text color="grey-dark" style="position: relative; z-index: 1">
        <p class="is-h4 content-text mb-0">{{ $slot }}</p>
    </x-text>
</blockquote>

<style>
    .fd-blockquote {
        background: transparent!important;
        border: none!important;

        &:before {
            content: "\201C";
            position: absolute;
            width: 100%;
            font-size: 10rem;
            margin-top: -4.5rem;
            margin-left: -2rem;
            z-index: 0;
            color: white!important;
            opacity: 0.3;
            font-family: Georgia, serif;
        }
    }
</style>
