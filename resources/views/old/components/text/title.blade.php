<{{ $role }} class="is-h1 mb-3 is-size-4-desktop is-size-6-mobile is-size-4-tablet {{ $uppercase ? "has-text-uppercase" : "" }} has-text-{{ $color }} {{ $class }}" @if($style) {{ "style=$style" }} @endif>
    {{ $slot }}
</{{ $role }}>
