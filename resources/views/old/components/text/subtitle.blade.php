<{{ $role }} class="is-p mb-{{ $mb }} is-size-5-desktop is-size-7-mobile is-size-4-tablet {{ $uppercase ? "has-text-uppercase" : "" }} has-text-{{ $color }} {{ $class }}" @if($style) {{ "style=$style" }} @endif>
    {{ $slot }}
</{{ $role }}>
