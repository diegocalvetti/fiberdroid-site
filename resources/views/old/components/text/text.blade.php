<div class="content fd-content {{ $class }} is-size-6-mobile is-size-4-tablet is-size-6-desktop has-text-{{ $color }} {{ $uppercase ? "has-text-uppercase" : "" }}" @if($style) style="{!! $style !!}" @endif>
   {{ $slot }}
</div>
