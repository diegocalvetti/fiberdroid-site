<div class="box">
    <div class="columns">
        <div class="column is-12 has-text-centered">
            <figure class="image mx-auto is-128x128" style="display: flex; vertical-align: middle">
                <img class="is-rounded" src="{{ $image }}" alt="{{ $alt }}">
            </figure>

            <p class="mt-3 is-size-5"><b>{{ $title }}</b></p>
            <p class="has-text-blue-logo is-size-7">Cliente soddisfatto</p>

            <p class="mt-4">
                “{{ $review }}”
            </p>
            <div class="has-text-warning">
                <i class="fa fa-star">
                </i>
                <i class="fa fa-star">
                </i>
                <i class="fa fa-star">
                </i>
                <i class="fa fa-star">
                </i>
                <i class="fa fa-star">
                </i>
            </div>
        </div>

    </div>
</div>
