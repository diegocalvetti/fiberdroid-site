<div class="modal {{ $active ? "is-active" : "" }} modal_response">
    <div class="modal-background" data-action="close-modal" data-target=".modal_response"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">{{ $title }}</p>
            <div class="delete" data-action="close-modal" data-target=".modal_response"></div>
        </header>
        <section class="modal-card-body">
            <x-text>
                <p>
                    {{ $content }}
                </p>
            </x-text>
        </section>
        <footer class="modal-card-foot">
            <div class="button is-success" data-action="close-modal" data-target=".modal_response">Ok</div>
        </footer>
    </div>
</div>


