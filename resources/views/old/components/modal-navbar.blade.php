<div id="modal-navbar" class="has-background-fd-lightblue is-multiline is-mobile is-vcentered is-hidden-desktop pb-1"
     style="display:none; position: absolute; width: 100%;height: 110vh; left: 0; top: 0; z-index: 32">
    <x-row class="pt-6" style="height: 110vh; width: 100%;" class="is-multiline">
        <div class="column is-12">
            <div class="columns is-multiline is-mobile is-vcentered">
                <div class="column is-5">
                    <x-link to="home">
                        <figure class="image fd-base-logo">
                            <img src="{{ image("logo-blu-orizzontale") }}" {!! image_attr("logo-blu-orizzontale") !!}>
                        </figure>
                    </x-link>
                </div>
                <div class="column is-6 is-offset-1 has-text-right">
                <span data-action="toggle-navbar" data-target="#modal-navbar">
                    <x-icon icon="fa fa-times" color="grey"></x-icon>
                    </span>
                </div>
            </div>
        </div>
        <div class="column is-12 pt-6">
            <x-text>

                <x-bullet-list>
                    <x-link to="home" color="fd-primary">
                        <p class="is-h4 is-size-5">Home</p>
                    </x-link>
                </x-bullet-list>


                <x-bullet-list>
                    <x-link to="chi-siamo" color="fd-primary">
                        <p class="is-h4 is-size-5">Chi siamo</p>
                    </x-link>
                </x-bullet-list>

                <x-bullet-list>
                    <x-link to="blog" color="fd-primary">
                        <p class="is-h4 is-size-5">Blog</p>
                    </x-link>
                </x-bullet-list>

                <x-bullet-list>
                    <p data-action="toggle-slide" data-target="#modal-contact" class="is-h4 is-size-5 has-text-fd-primary">Contatti</p>
                </x-bullet-list>

                <x-bullet-list>
                    <x-text color="fd-primary">
                        <p class="is-h4 is-size-5" data-action="toggle-subnavbar" data-target="#submodal-servizi">
                            Servizi
                            <i class="fa icon fa-chevron-right" style="float: right"></i>
                        </p>
                    </x-text>
                </x-bullet-list>

                <x-bullet-list>
                    <x-text color="fd-primary">
                        <p class="is-h4 is-size-5" data-action="toggle-subnavbar" data-target="#submodal-prodotti">
                            Prodotti
                            <i class="fa icon fa-chevron-right" style="float: right"></i>
                        </p>
                    </x-text>
                </x-bullet-list>

                <x-bullet-list>
                    <x-text color="fd-primary">
                        <p class="is-h4 is-size-5" data-action="toggle-subnavbar" data-target="#submodal-soluzioni">
                            Soluzioni
                            <i class="fa icon fa-chevron-right" style="float: right"></i>
                        </p>
                    </x-text>
                </x-bullet-list>

                <x-sub-modal-navbar id="submodal-servizi">
                    @foreach($services->where('service_type_id', 1)->pluck('name','url') as $to => $link)
                        <x-bullet-list>
                            <x-link to="servizio" params="{{ $to }}" color="fd-primary">
                                <p class="is-h4 is-size-5">
                                    {{ $link }}
                                </p>
                            </x-link>
                        </x-bullet-list>
                    @endforeach
                </x-sub-modal-navbar>
                <x-sub-modal-navbar id="submodal-prodotti">
                    @foreach($services->where('service_type_id', 2)->pluck('name','url') as $to => $link)
                        <x-bullet-list>
                            <x-link to="servizio" params="{{ $to }}" color="fd-primary">
                                <p class="is-h4 is-size-5">
                                    {{ $link }}
                                </p>
                            </x-link>
                        </x-bullet-list>
                    @endforeach
                </x-sub-modal-navbar>
                <x-sub-modal-navbar id="submodal-soluzioni">
                    @foreach($solutions->pluck('name','url') as $to => $link)
                        <x-bullet-list>
                            <x-link to="soluzione" params="{{ $to }}" color="fd-primary">
                                <p class="is-h4 is-size-5">
                                    {{ $link }}
                                </p>
                            </x-link>
                        </x-bullet-list>
                    @endforeach
                </x-sub-modal-navbar>

            </x-text>

        </div>
    </x-row>
</div>
