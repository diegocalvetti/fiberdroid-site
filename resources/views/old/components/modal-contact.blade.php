<x-row id="modal-contact" bg="fd-lightblue" class="is-multiline" style="display:none; position: absolute;left: 0; top: 0; overflow: hidden; width: 105vw; height: 110vh; z-index: 32;">
    <div class="column is-12 is-hidden-touch">
        <div class="columns is-multiline">
            <div class="column is-12 has-text-right mt-3">
        <span data-action="toggle-slide" data-target="#modal-contact">
            <x-icon icon="fa fa-times" color="grey"></x-icon>
        </span>
            </div>
            <div class="column is-3">
                <img src="{{ image('logo-bianco-verticale') }}" class="image fd-base-logo" {!! image_attr('logo-bianco-verticale') !!}>
            </div>
            <div class="column is-3 pt-6 mt-5">
                <x-title color="fd-primary" class="is-h3 mb-2">
                    Contatti
                </x-title>
                <x-separator color="fd-warning" width="20%"></x-separator>
                <div class="content py-3 has-text-fd-primary">
                    <x-icon-text icon="fa fa-envelope">
                        <x-link to="mailto:speed@fiberdroid.it" color="fd-primary">
                    <span class="is-h1">
                        speed@fiberdroid.it
                    </span>
                        </x-link>
                    </x-icon-text>
                    <div class="my-2"></div>
                    <x-icon-text icon="fa fa-phone">
                        <x-link to="tel:+39 02 83595676" color="fd-primary">
                            <span class="is-h1">02 83595676</span>
                        </x-link>
                    </x-icon-text>
                </div>
                <x-separator color="fd-warning" width="20%"></x-separator>
                <div class="content py-3 has-text-fd-primary">
                    <x-icon-text icon="fab fa-youtube">
                        <x-link target="_blank" to="{{ $config['link_youtube'] }}" color="fd-primary">
                            <span class="is-h1">youtube</span>
                        </x-link>
                    </x-icon-text>
                    <div class="my-2"></div>
                    <x-icon-text icon="fab fa-linkedin">
                        <x-link target="_blank" to="{{ $config['link_linkedin'] }}" color="fd-primary">
                            <span class="is-h1">linkedin</span>
                        </x-link>
                    </x-icon-text>
                    <div class="my-2"></div>
                    <x-icon-text icon="fab fa-twitter">
                        <x-link target="_blank" to="{{ $config['link_twitter'] }}" color="fd-primary">
                            <span class="is-h1">twitter</span>
                        </x-link>
                    </x-icon-text>
                </div>
            </div>
            <div class="column is-4 pt-6 mt-5 pr-6">
                <x-subtitle color="fd-primary" uppercase="false" class="is-h4">
                    <span class="is-h4">Compila il form</span>
                </x-subtitle>
                <x-text uppercase="true">
                    <p>Ti ricontatteremo al più presto</p>
                </x-text>
                <x-form uname="contact"></x-form>
            </div>
            <div class="column is-12 py-6 has-text-centered">
                <x-text color="fd-primary" uppercase="false">
                    <p class="is-h4">Fiberdroid s.r.l</p>
                </x-text>
            </div>
        </div>
    </div>
    <div class="column is-12 is-hidden-desktop">
        <div class="columns is-multiline" style="height: 110vh; overflow: auto">
            <div class="column is-12 pt-6">
                <div class="columns is-mobile is-vcentered">
                    <div class="column is-5">
                        <figure class="image fd-base-logo">
                            <img src="{{ image("logo-blu-orizzontale") }}" {!! image_attr("logo-blu-orizzontale") !!}>
                        </figure>
                    </div>
                    <div class="column is-6 is-offset-1 has-text-right">
                        <span data-action="toggle-navbar" data-target="#modal-contact">
                            <x-icon icon="fa fa-times" color="grey"></x-icon>
                        </span>
                    </div>
                </div>
            </div>
            <div class="column is-12 pt-4">
                <x-title color="fd-primary" class="is-h3 mb-2">
                    Contatti
                </x-title>
                <x-separator color="fd-warning" width="20%"></x-separator>
                <div class="content py-3 has-text-fd-primary">
                    <x-icon-text icon="fa fa-envelope">
                        <x-link to="mailto:speed@fiberdroid.it" color="fd-primary">
                    <span class="is-h1">
                        speed@fiberdroid.it
                    </span>
                        </x-link>
                    </x-icon-text>
                    <div class="my-2"></div>
                    <x-icon-text icon="fa fa-phone">
                        <x-link to="tel:+39 02 83595676" color="fd-primary">
                            <span class="is-h1">02 83595676</span>
                        </x-link>
                    </x-icon-text>
                </div>
            </div>
            <div class="column is-12">
                <x-title color="fd-primary" uppercase="false" role="p">
                    <span class="is-h4">Compila il form</span>
                </x-title>
                <x-text uppercase="true">
                    <p>Ti ricontatteremo al più presto</p>
                </x-text>
                <x-form uname="re-contact"></x-form>
            </div>
        </div>
    </div>



</x-row>
