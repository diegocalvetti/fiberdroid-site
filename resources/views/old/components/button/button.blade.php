<button class="button is-{{ $bg }} @if($full) is-fullwidth @endif" @if($disabled) disabled @endif @if($wireLoading) wire:loading.class="is-loading" @endif>
    <x-text color="{{ $color }}" uppercase="{{ $uppercase }}" class="is-h4" wire:loading.class="has-text-{{ $bg }}">
        <p>{{ $slot }}</p>
    </x-text>
</button>
