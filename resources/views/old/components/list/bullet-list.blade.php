<div class="columns is-mobile is-vcentered">
    <div class="column is-1">
        <div class="bullet has-border-fd-primary"></div>
    </div>
    <div class="column pl-5">
        {{ $slot }}
    </div>
</div>
