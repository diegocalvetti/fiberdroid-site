<div class="columns is-mobile is-vcentered is-multiline my-4">

    @foreach($list as $item)
        <div class="column is-one-quarter">
            <div class="circle">
                <i class="fa fa-check circle-icon has-text-success"></i>
            </div>
        </div>
        <div class="column is-three-quarters has-text-left">
            <x-text>
                <p class="is-size-5-tablet">{!! $item !!}</p>
            </x-text>
        </div>
    @endforeach

</div>

@push('css')
    <style>
        .circle {
            width: 30px;
            height: 30px;
            border: solid limegreen 2px;
            border-radius: 50%;
            display: flex;
            vertical-align: middle;
            justify-content: center;
            float: right;
        }
        .circle-icon {
            display: inline;
            margin: auto;
        }
    </style>
@endpush
