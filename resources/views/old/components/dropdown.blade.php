<div class="dropdown is-hoverable">
    @php
        $uniqid = uniqid();
    @endphp
    <div class="dropdown-trigger" aria-haspopup="true" aria-controls="dropdown-menu-{{ $uniqid }}">
        <x-navbar-link size="is-size-4" color="white">Servizi</x-navbar-link>
    </div>
    <div class="dropdown-menu" id="dropdown-menu-{{ $uniqid }}" role="menu">
        <div class="dropdown-content">
            <div class="dropdown-item">
                <p>You can insert <strong>any type of content</strong> within the dropdown menu.</p>
            </div>
        </div>
    </div>
</div>
