<!--<div class="modal {{ !$accepted ? "is-active" : "" }}" id="modal_cookie">
    <div class="modal-background" data-action="close-modal" data-cookie="accept" data-target="#modal_cookie"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Accettazione cookies</p>
            <button class="delete" data-action="close-modal" data-cookie="accept" data-target="#modal_cookie" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <p>
                Fiberdroid utilizza i cookie per migliorare la funzionalità del sito,
                fornirti una migliore esperienza di navigazione e per consentire ai nostri partner di pubblicizzarti.
                Informazioni dettagliate sull'uso dei cookie su questo sito e su come rifiutarli sono fornite nella nostra <a href="{{ route('policy.cookies') }}">politica sui cookie</a> .
                Utilizzando questo sito o cliccando su "OK", acconsenti all'uso dei cookie.
            </p>
        </section>
        <footer class="modal-card-foot">
            <button class="button is-success" data-action="close-modal" data-cookie="accept" data-target="#modal_cookie" aria-label="close">Ok</button>
        </footer>
    </div>
</div>-->
@if(!$accepted)
    <div id="modal_cookie" class="shadow p-5">
        <p class="is-size-6">
            Fiberdroid utilizza i cookie per migliorare la funzionalità del sito,
            fornirti una migliore esperienza di navigazione e per consentire ai nostri partner di pubblicizzarti.
            Informazioni dettagliate sull'uso dei cookie su questo sito e su come rifiutarli sono fornite nella nostra <a href="{{ route('policy.cookies') }}">politica sui cookie</a> .
            Utilizzando questo sito o cliccando su "OK", acconsenti all'uso dei cookie.
            <br>
            <button class="button is-success" data-action="close-cookie" data-cookie="accept" data-target="#modal_cookie" aria-label="close">Ok</button>
        </p>
    </div>
    <style>

        .shadow {
            -webkit-box-shadow: 0px -5px 16px -6px rgba(0,0,0,0.66);
            -moz-box-shadow: 0px -5px 16px -6px rgba(0,0,0,0.66);
            box-shadow: 0px -5px 16px -6px rgba(0,0,0,0.66);
        }
    </style>

@endif
