<div class="field">
    @if($label)
        <label for="input-{{ strtolower($label) . "-" . $uname }}" class="label is-size-6 is-h1">{{ $label }}</label>
    @endif

    <div class="control">
        <input id="input-{{ strtolower($label) . "-" . $uname }}" class="input {{ $error ? "is-danger" : "" }}" @if($list) list="{{ $list }}" @endif type="{{ $type }}" wire:model="{{ $wireModel }}">
    </div>

    @if($error)
        <p class="help is-danger">{{ $error }}</p>
    @endif
</div>
