<label class="checkbox">
    <input type="checkbox" @if($wireModel) wire:model="{{ $wireModel }}" @endif>
    {{ $slot }}

    @if($error)
        <p class="help is-danger">{{ $error }}</p>
    @endif
</label>