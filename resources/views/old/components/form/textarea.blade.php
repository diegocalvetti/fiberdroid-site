<div class="field">
        @if($label)
            <label for="textarea-{{ strtolower($label) . "-" . \Illuminate\Support\Str::slug($uname) }}" class="label is-h1">{{ $label }}</label>
        @endif

        <textarea id="textarea-{{ strtolower($label) . "-" . \Illuminate\Support\Str::slug($uname) }}" class="textarea {{ $error ? "is-danger" : "" }}" wire:model="{{ $wireModel }}"></textarea>

        @if($error)
            <p class="help is-danger">{{ $error }}</p>
        @endif
</div>
