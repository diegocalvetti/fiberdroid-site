<div class="card fd-card">
    <div class="card-image">
        <figure class="image is-3by4">
            <img loading="lazy" src="{{ $photo }}" alt="{{ $alt }}" title="{{ $title }}">
        </figure>
    </div>
    <div class="card-content">
        <div class="media">
            <div class="media-content has-text-centered">
                <p class="title has-text-fd-primary is-4">{{ $name }}</p>
                <p class="subtitle has-text-fd-warning is-6">{{ $role }}</p>
            </div>
        </div>

        <div class="content">
            {{ $slot }}
        </div>
    </div>
</div>
