<div id="{{ $id }}" class="columns mx-6 py-0 is-vcentered is-absolute is-hidden-touch mega-menu" style="display: none; z-index: 1">
    <div class="column is-3">
    </div>

    <div class="column is-6 is-hidden-touch has-text-uppercase has-text-centered has-text-white is-unselectable is-size-3 has-background-fd-warning is-rounded-1 mx-auto">
        {{ $slot }}
    </div>

    <div class="column is-3">

    </div>

</div>