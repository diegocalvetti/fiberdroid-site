<x-row thin="true" bg="fd-warning" class="is-mobile is-vcentered">
    <div class="column is-9">
        <x-subtitle color="fd-primary" class="is-h4">Fai un test di copertura</x-subtitle>
        <x-text color="white">
            <p>Verifica se la tua zona è coperta</p>
        </x-text>
    </div>
    <div class="column is-3 has-text-right">
        <x-link to="verifica-copertura" target="_blank">
            <x-button bg="fd-primary" color="white">Avvia</x-button>
        </x-link>
    </div>

</x-row>
