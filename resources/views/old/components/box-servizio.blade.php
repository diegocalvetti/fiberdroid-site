<div class="card" @if(isset($id) && $id) id="{{ $id }}" @endif style="display: flex; flex-direction: column">
    <div class="card-image">
        <figure class="image is-{{ isset($size) ? $size : "96x96" }} py-3 mx-auto">
            <img width="96" height="96" src="{{ is_string($icon) ? $icon : $icon->full_url }}" alt="{{ is_object($icon) ? $icon->alt : ""}}" title="{{ is_object($icon)? $icon->title : ""}}">
        </figure>
    </div>
    <div class="card-content pt-2">
        <div class="content has-text-centered">
            <x-text color="fd-secondary" class="my-3" uppercase="true">
                @if(isset($id) && $id)
                    <h3 class="is-size-4 has-text-fd-secondary is-h3">{!! $title !!}</h3>
                @else
                    <p class="is-size-4 is-h3">{!! $title !!}</p>
                @endif
            </x-text>

            @if($features && sizeof($features) > 0)
                <x-checked-list :list="$features">

            </x-checked-list>
            @else
                <p class="is-size-6">{{ $description }}</p>
            @endif
        </div>
    </div>
    <div class="card-footer mt-auto p-5" style="border: none">
        <div class="content has-text-centered" style="width: 100%">
            @if($price >= 0)
                <x-subtitle color="fd-warning" class="is-h4 mb-0">A partire da</x-subtitle>
                <x-text>
                    <p class="is-size-2 has-text-success is-h3 mb-0">
                        {!! is_numeric($price) ? number_format($price, 2, '.', '')."&euro;" : $price !!}
                    </p>
                    <p>Canone mensile</p>
                </x-text>
            @endif
            @if(!isset($noContact))
                    @if( $sellable )
                        <x-link to="{{ $config['link_areaclienti'] }}" target="_blank">
                            <x-button bg="fd-warning" color="white">Acquista</x-button>
                        </x-link>
                    @else
                        <x-link to="#contact">
                            <x-button bg="fd-primary" color="white">Contattaci</x-button>
                        </x-link>
                    @endif
            @endif
        </div>
    </div>
</div>
