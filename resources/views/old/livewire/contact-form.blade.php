<div>
    <form wire:submit.prevent="submit">
        <div class="columns is-multiline">
            <div class="column is-6">
                <x-input uname="{{ $uname }}" label="Nome" wireModel="name" :error="session('errors') ? session('errors')->first('name'): false"></x-input>
            </div>
            <div class="column is-6">
                <x-input uname="{{ $uname }}" label="Cognome" wireModel="surname" :error="session('errors') ? session('errors')->first('surname'): false"></x-input>
            </div>
            <div class="column is-6">
                <x-input uname="{{ $uname }}" label="Email" wireModel="email" :error="session('errors') ? session('errors')->first('email'): false"></x-input>
            </div>
            <div class="column is-6">
                <x-input uname="{{ $uname }}" label="Telefono" wireModel="telephone" :error="session('errors') ? session('errors')->first('telephone'): false"></x-input>
            </div>
            <div class="column is-12">
                <x-textarea uname="{{ $uname }}" label="Lasciaci un messaggio" wireModel="message" :error="session('errors') ? session('errors')->first('message'): false"></x-textarea>
            </div>
            <div class="column is-12">
                <x-check-box wireModel="privacy" :error="session('errors') ? session('errors')->first('privacy'): false">
                    Letta e compresa l’informativa circa il trattamento dei miei dati personali: <a href="{{ route('policy.privacy') }}" target="_blank" rel="noopener">Informativa Privacy</a>
                </x-check-box>
            </div>
            <div class="column is-3">
                <x-button bg="fd-primary" wireLoading="true">Contattaci ora</x-button>
            </div>
        </div>

        <x-modal-response :response="session('response')"></x-modal-response>
    </form>
</div>
