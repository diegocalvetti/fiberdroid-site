<div class="columns is-mobile is-multiline py-6" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="column is-12">
        <div class="notification is-danger is-light">
            Siamo spiacenti, il <strong>servizio di copertura</strong> di Fiberdroid non è
            attualmente disponibile. <br>
            Le funzionalità saranno ripristinate il prima possibile.
        </div>
    </div>
    <div class="column is-5-desktop mx-auto px-5 is-10-touch">
        <div class="card card-mark mx-auto" style="overflow: visible; background-image: linear-gradient(120deg, #fdfbfb 0%, #ebedee 100%); border-radius: 1.1rem">
            <div class="card-content">
                <x-subtitle color="fd-primary" class="has-text-right"><span class="is-h2">Inserisci i dati</span></x-subtitle>
                <form wire:submit.prevent="submit" class="mb-0">
                    <div class="columns is-multiline">
                        <div class="column is-12" wire:ignore>

                            <label for="select-province">
                                Provincia
                                <select disabled id="select-province" class="select select-first" data-model="selectedProvince">
                                    <option selected>-- Seleziona una provincia --</option>
                                    @foreach($provinces as $province)
                                        <option value="{{ $province['desc_provincia'] }}" {{ $province['desc_provincia'] == $selectedProvince ? "selected" : ""}}>{{ $province['desc_provincia'] }}</option>
                                    @endforeach
                                </select>
                            </label>

                        </div>

                        <div class="column is-12">

                            <label for="select-city">
                                Città o comune
                                <select id="select-city" class="select" data-model="selectedCity" {{ sizeof($cities) == 0 ? "disabled" : "" }}>
                                    <option selected>-- Seleziona una comune --</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city['desc_comune'] }}" {{ $city['desc_comune'] == $selectedCity ? "selected" : "" }}>{{ $city['desc_comune'] }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>

                        <div class="column is-12">

                            <label for="select-street">
                                Via o luogo
                                <select id="select-street" class="select" data-model="selectedStreet" {{ sizeof($streets) == 0 ? "disabled" : "" }}>
                                    <option selected>-- Seleziona una via --</option>
                                    @foreach($streets as $street)
                                        <option value="{{ $street['desc_part_toponomastica'] }}|{{ $street['via'] }}" {{ $street['desc_part_toponomastica']."|".$street['via'] == $selectedStreet ? "selected" : "" }}>{{ $street['desc_part_toponomastica'] }} {{ $street['via'] }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>

                        <div class="column is-12">
                            <label for="select-street-number">
                                Civico
                                <select id="select-street-number" class="select" data-model="selectedStreetNumber" {{ sizeof($street_numbers) == 0 ? "disabled" : "" }}>
                                    <option selected>-- Seleziona un civico --</option>
                                    @foreach($street_numbers as $street_number)
                                        <option value="{{ $street_number['civico'] }}" {{ $street_number['civico'] == $selectedStreetNumber ? "selected" : "" }}>{{ $street_number['civico'] }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <progress class="progress is-small is-fd-primary is-hidden" max="100" wire:loading.class.remove="is-hidden"></progress>
                        <!--<div class="column is-3">
                            <x-button bg="fd-primary" wireLoading="true" :disabled="!$isOk">Invia</x-button>
                        </div>-->
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="column is-7-desktop mx-auto mt-0 px-5 is-10-touch">

        <div class="card" style="background-image: linear-gradient(120deg, #fdfbfb 0%, #ebedee 100%);">
            <div class="card-content">
                @if($showForm)
                    <x-subtitle color="success"><span class="is-h2">Congratulazioni, puoi attivare dei servizi Fiberdroid!</span> </x-subtitle>
                    <div class="columns is-multiline">
                        <div class="column is-12">
                            <p class="has-text-fd-primary is-h3">Indirizzo di attivazione:</p>
                            <x-text>
                                <p>
                                    {{ $this->selectedCity }} ({{ $this->selectedProvince }}), <br>
                                    <span class="is-size-7">{{ str_replace("|", " ", $this->selectedStreet) }} {{ $this->selectedStreetNumber }}</span>
                                </p>
                            </x-text>

                            <x-text>
                                <p>
                                    Compila il form per essere ricontatto da uno dei nostri specialisti!
                                    Sapranno consigliarti la migliore soluzione disponibile.
                                </p>
                            </x-text>
                            @php($verifica = "{$this->selectedCity} ({$this->selectedProvince}), ". str_replace("|", " ", $this->selectedStreet) ." {$this->selectedStreetNumber}")
                            @livewire('contact-form', ['verifica' => $verifica])
                        </div>
                    </div>
                @else
                    <x-subtitle color="fd-primary"><span class="is-h2">Consulta il risultato</span> </x-subtitle>
                    <x-text>
                        <p>Completa il form <span class="is-hidden-touch">a sinistra</span><span class="is-hidden-desktop">qui sopra</span> e comparirà qui il risultato</p>
                    </x-text>
                @endif
            </div>
        </div>

    </div>
</div>

@push('js')
    <script>
        document.addEventListener('livewire:load', function () {
            $(".select").each(function () {
                $(this).select2({width: "100%"});
            });

            $(".select").on('change', function () {

                let data = $(this).select2("val");
                let model = $(this).attr('data-model');

                @this.set(model, data)
            });


        });

        document.addEventListener('livewire:update', function () {
            $(".select").each(function () {

                if ( !$(this).hasClass("select2-hidden-accessible") ){
                    $(this).select2({width: "100%"});
                }
            });
        });
    </script>
    @endpush
