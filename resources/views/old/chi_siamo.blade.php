@extends('layouts._layout')

@section('title', "l'Azienda con l'assistenza unica per la connettività | Fiberdroid")
@section('description', "Fiberdroid è l'azienda che ti permette di avere un’unica assistenza per Internet, wifi, centralino cloud, fibra e connettivita’, piattaforma tecnologica.")

@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-title color="white" class="is-h4" role="p">Chi siamo</x-title>
        </div>
    </x-row>
@endsection

@section('content')

    <x-row class="is-multiline is-vcentered" thin="true">
        <div class="column is-12">

            <x-title color="fd-primary" role="h1">Tutto in un solo partner</x-title>

            <x-text>
                <p><strong>Siamo l&rsquo;azienda che ti permette di avere un&rsquo;unica assistenza per Internet, wifi, centralino cloud, fibra e connettivita&rsquo;, piattaforma tecnologica.</strong></p>
                <p>Il nostro team &egrave; in servizio da oltre 10 anni nel settore IT &amp; TLC per garantire alle aziende, in Italia e all&rsquo;estero, connettivit&agrave; veloce, assistenza affidabile, tecnologie innovative.</p>
                <p>Offriamo la <em>customizzazione </em>dell&rsquo;intero progetto di messa in rete e di telecomunicazione, nella totale trasparenza e garanzia di puntualit&agrave;, fornendo ai nostri Clienti Business un servizio di consulenza attento e completo, a supporto della crescita aziendale.</p>
                <p><strong>I Valori che guidano la nostra squadra sono la liberta&rsquo;, l&rsquo;Innovazione, l&rsquo;Esplorazione e l&rsquo;attenzione al cliente.</strong></p>
                <p>Ci occupiamo dell&rsquo;installazione, presso i nostri Clienti Business, esclusivamente di apparati certificati e testati. Grazie ai test effettuati preventivamente dal nostro team di supporto, <strong>vantiamo una media del 90% di chiusura ticket entro 4 ore dall&rsquo;apertura.</strong></p>
                <p>La nostra piattaforma &egrave; distribuita su due <em>data-center</em>: il primo presso il MIX, a Milano. Il secondo tra le montagne della Valle d&rsquo;Aosta, e sono operanti totalmente con hardware di propriet&agrave;.</p>
                <p>Solo cos&igrave; possiamo <strong>garantirti la Qualit&agrave;, la Sicurezza e il Controllo gestionale di cui necessit&agrave; la tua Rete aziendale</strong>: implementando e aggiornando continuamente la nostra infrastruttura.</p>
                <p>I nostri sistemi sono sotto monitoraggio continuo, consentendo alla nostra equipe di anticipare eventuali disservizi o problematiche.</p>
                <p><em>Fiberdroid s.r.l., con sede a Milano, operante da oltre 10 anni nel settore delle telecomunicazioni a livello internazionale. </em></p>
            </x-text>

        </div>
        <div class="column is-12-touch is-3-desktop">

            <x-title color="fd-primary" class="is-h4">Team fiberdroid</x-title>
        </div>
        <div class="column is-9 is-hidden-touch has-text-centered">
            <img class="image" width="300" height="300" src="{{ image("logo") }}" style="max-height: 200px" {!! image_attr("logo") !!}>
        </div>
    </x-row>

    <x-row class="is-multiline is-flex" thin="true">
        @foreach($crews as $crew)
            <div class="column is-12-touch is-4-desktop">
                <x-box-equipaggio name="{{ $crew->name }}" role="{{ $crew->role }}" photo="{{ $crew->image->full_url ?? '' }}" alt="{{ $crew->image->alt ?? '' }}" title="{{ $crew->image->title ?? '' }}">
                    <x-text>
                        {!! $crew->content !!}
                    </x-text>
                </x-box-equipaggio>
            </div>
        @endforeach
    </x-row>


@endsection
