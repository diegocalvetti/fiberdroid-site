@extends('layouts._layout')

@section('title', ($service->meta_title ?: "$service->name") . " | Fiberdroid")
@section('description', $service->meta_description)

@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-title color="white" role="p">{{ $service->type->name }}</x-title>
        </div>
    </x-row>
@endsection

@section('banner-secondary')
    <x-banner-copertura></x-banner-copertura>
@endsection

@section('content')

    <x-row thin="true" class="is-multiline">
        @foreach($introSections as $section)
            <x-section :section="$section" isTitle="true"></x-section>
        @endforeach
    </x-row>

    @if( $service->most_selled()->count())
        <x-row thin="true" class="is-multiline pt-5">
            <div class="column is-12">
                <x-subtitle color="fd-primary" role="p">
                    I Nostri pacchetti più venduti
                </x-subtitle>
            </div>

            @foreach($service->most_selled()->get() as $item)
                <div class="column is-12-mobile is-6-tablet is-6-desktop is-flex">
                    @component('old.components.box-servizio', ['title' => $item->name, 'price' => $item->prices->fee_amount, 'features' => $item->features()->pluck('text'), 'icon' => $service->image, 'id' => false, 'sellable' => true, 'description' => $item->description])
                    @endcomponent
                </div>
            @endforeach

            <div class="column is-12 pt-0 has-text-centered has-text-underlined">
                <x-link to="#servizi-{{ $service->url }}">
                    <x-text color="fd-primary" uppercase="true">
                        <p class="is-h3">
                            <x-underlined-text>Guardali tutti</x-underlined-text>
                            <i class="fa fa-chevron-down icon is-size-5 pl-2"></i>
                        </p>
                    </x-text>
                </x-link>
            </div>
        </x-row>
    @endif

    @if( !is_null($firstMainSection) )
        <x-row thin="true" class="mb-0">
            <x-section :section="$firstMainSection"></x-section>
        </x-row>
    @endif

    @if( $displayAnchor && $service->name != "Fiber AI")
        <x-row thin="true" class="is-multiline">

            <div class="column is-12">
                <x-subtitle color="fd-warning" uppercase="false" class="is-h4 mt-3" role="p">Guida nella pagina</x-subtitle>
                <x-base-list>
                    <ul>
                        @foreach($anchorableSections as $section)
                            <li>
                                <a href="{{ url()->current()."#".$section->slug }}">{{ $section->title }}</a>

                                <ul>

                                    @foreach($section->accordions as $title => $content)
                                        <li>
                                            <a href="{{ url()->current()."#".\Illuminate\Support\Str::slug($title) }}">{{ $title }}</a>
                                        </li>
                                    @endforeach

                                    @foreach($section->anchorableSubsections as $subsection)
                                        <li>
                                            <a href="{{ url()->current()."#".$subsection->slug }}">{{ $subsection->title }}</a>
                                            <ul>
                                                @foreach($subsection->anchorableSubsections as $thirdsection)
                                                    <li>
                                                        <a href="{{ url()->current()."#".$thirdsection->slug }}">{{ $thirdsection->title }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                        @if($service->items->count() > 0)
                            <li>
                                <a href="{{ url()->current() }}#servizi-{{ $service->url }}">Tariffe e soluzioni</a>
                                <ul>
                                    @foreach($service->items as $item)
                                        <li>
                                            <a href="{{ url()->current() }}#servizi-card-{{ \Illuminate\Support\Str::slug($item->name) }}">{{ $item->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>

                            </li>
                        @endif
                    </ul>
                </x-base-list>
            </div>
        </x-row>
    @endif

    <x-row thin="true" class="is-multiline is-mobile">
        @foreach($mainSections as $section)
            <x-section :section="$section"></x-section>
        @endforeach
    </x-row>

    @if($service->items->count() > 0)
        <x-row thin="true" class="is-multiline pt-5" id="servizi-{{ $service->url }}">
            <div class="column is-12">
                <x-subtitle color="fd-primary" role="h2">
                    Tariffe e soluzioni
                </x-subtitle>
            </div>

            @foreach($service->items as $item)
                <div class="column is-12-mobile is-6-tablet is-6-desktop mb-0 is-flex">
                    @component('old.components.box-servizio', ['title' => $item->name, 'price' => $item->prices->fee_amount, 'features' => $item->features()->pluck('text'), 'icon' => $service->image, 'id' => "servizi-card-".\Illuminate\Support\Str::slug($item->name), 'sellable' => true, 'description' => $item->description])
                    @endcomponent
                </div>
            @endforeach
            <div class="column is-12">
                <x-text>
                    <p>
                        {!! $service->termini !!}
                    </p>
                </x-text>
            </div>
        </x-row>
    @endif

    <x-banner-demo></x-banner-demo>
@endsection
