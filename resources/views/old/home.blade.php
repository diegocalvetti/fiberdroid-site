@extends('layouts._layout')

@section('title', 'Connettiamo il tuo business alla velocità della luce | Fiberdroid')
@section('description', 'Fiberdroid è un’azienda di telecomunicazioni che si occupa di connettere in internet aziende e professionisti tramite VOIP, WIFI, cloud e fibra ottica.')

@section('banner')
    <x-row margin="false" bg="transparent" class="has-border-b-4 has-border-fd-warning">
        <div class="column is-12 pb-0 has-border-b-4 has-border-fd-primary">
            <div class="card has-background-transparent fd-home-banner-height">
                <div class="card-content is-overlay is-flex is-justify-content-center fd-banner-content fd-home-banner-height" style="vertical-align: middle">
                    <div style="display: inline; margin: auto; padding-bottom: 4.5rem">
                        <x-title color="white" class="is-h2 mb-3" role="p">
                            Sei pronto <br> per il grande salto?
                        </x-title>
                        <x-subtitle uppercase="false" color="fd-warning" class="pl-5 is-h2 is-italic" role="p">
                            Portiamo il tuo business <br> alla velocità della luce
                        </x-subtitle>
                        <div class="is-hidden-mobile" style="position: absolute; bottom: 1.5rem; left: 0; width: 100%; text-align: center">
                            <x-text uppercase="true">
                                <x-link to="#benvenuti-in-fiberdroid" color="grey-lighter" data-action="hover-discover-fd">
                                    <p class="is-size-6" style="line-height: 2px">
                                        Scopri fiberdroid<br>
                                        <i class="fa fa-chevron-down icon has-text-fd-warning hover-has-text-fd-warning-darker is-size-6"></i>
                                    </p>
                                </x-link>
                            </x-text>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-row>
@endsection

@section('content')

    <!-- BENVENUTI IN FIBERDROID -->

    <x-row thin="true" class="is-mobile is-vcentered pt-6">
        <div class="column is-12-touch" id="benvenuti-in-fiberdroid">
            <x-title role="p">Benvenuti in Fiberdroid</x-title>

            <x-subtitle role="p" color="fd-secondary">
                Veloce, connesso, sempre.
            </x-subtitle>

            <x-text class="pt-3">
                <p>
                    <span>Fiberdroid &egrave; un&rsquo;azienda di telecomunicazioni che si occupa di connettere in internet aziende e professionisti tramite il voip, il wifi, il cloud e la fibra ottica.</span>
                    <br>
                    <span>Crea soluzioni cloud ed IT perfette alle TUE esigenze.</span>
                    <br>
                    <span>
                        Aiutiamo le Aziende
                        <img loading="lazy" src="{{ image('logo-isometrico') }}" class="image is-64x64 is-hidden-desktop" style="float: right" {!! image_attr('logo-isometrico') !!}>
                        ad integrare tutti i servizi di comunicazione compreso Internet per far
                        volare il loro business!
                    </span>
                </p>
            </x-text>
        </div>
        <div class="column is-4 is-hidden-touch">
            <figure class="image mx-auto" style="width: 256px; height: 256px">
                <img loading="lazy" src="{{ image('logo-isometrico') }}" class="image" style="width: 256px; height: 256px" {!! image_attr('logo-isometrico') !!}>
            </figure>
        </div>
    </x-row>

    <!--<x-row class="is-vcentered">
        <div class="column is-5 is-hidden-touch">
            <img src="{{ asset('images/schermata.png') }}" class="image">
        </div>
        <div class="column is-12-touch is-4-desktop is-offset-1">
            <x-subtitle color="fd-primary" class="is-h4" role="h2">Fiberdroid in breve</x-subtitle>
            <x-text class="pt-3">
                <p><span>Oltre un decennio di esperienza nella galassia delle telecomunicazioni ha fatto di noi </span><span>un equipaggio esperto ed efficiente. </span><span>La cosa che ci piace di questo lavoro &egrave; che bisogna essere artisti e tecnici per soddisfare le diverse esigenze dei clienti. Qui inizia la nostra sfida nel creare la soluzione ad hoc per te.</span></p>
                <p><span>Siamo al tuo servizio per guidarti verso la connettivit&agrave; pi&ugrave; sicura e adatta alla tua azienda.</span></p>
                <p><span>Tu pensa al tuo business, a collegarti al mondo ci pensiamo noi!</span></p>
            </x-text>
            <img src="{{ asset('images/schermata.png') }}" class="image is-hidden-desktop" style="display: inline-block">
        </div>
    </x-row>-->

    <!-- LE NOSTRE SOLUZIONI PER OGNI TUA ESIGENZA -->
    <x-row thin="true" class="py-6">
        <div class="column is-12 has-text-centered">
            <x-title color="fd-primary" class="is-h2 pb-3" role="h1">Fiberdroid è un’azienda di telecomunicazioni che si occupa di connettere aziende e professionisti</x-title>
            <div class="columns is-variable is-flex is-8-desktop is-5-mobile is-0-tablet is-multiline">
                <div class="column is-12-touch is-6-desktop px-5" style="display: flex">
                    <x-box-soluzione title="Aziende" icon="icon-aziende">
                        <x-text>
                            <p>Per costruire soluzioni personalizzate, affidabili e sicure, al minor prezzo, il nostro equipaggio &egrave; il partner unico per condurre la tua azienda verso il futuro della connettivit&agrave;!</p>
                        </x-text>
                    </x-box-soluzione>
                </div>
                <div class="column is-12-touch is-6-desktop px-5" style="display: flex">
                    <x-box-soluzione title="Ristorazione e retail" icon="icon-ristorazione-e-retail">
                        <x-text>
                            <p>Gestione delle prenotazioni in remoto, connessione veloce e sicura, disponibile per i clienti, sistemi IT avanzati per negozi e ristoranti, per ripartire la tua attivit&agrave; e riaccendere la citt&agrave;!</x-text>
                    </x-box-soluzione>
                </div>
                <div class="column is-12-touch is-6-desktop px-5" style="display: flex">
                    <x-box-soluzione title="Partite iva" icon="icon-piva">
                        <x-text>
                            <p>Il tuo successo dipende dalla tecnologia di cui disponi, perci&ograve; ti offriamo i servizi giusti per essere pi&ugrave; connesso ai tuoi clienti, in piena libert&agrave; di movimento e con la massima velocit&agrave;.</p>
                        </x-text>
                    </x-box-soluzione>
                </div>
                <div class="column is-12-touch is-6-desktop px-5" style="display: flex">
                    <x-box-soluzione title="Strutture ricettive" icon="icon-strutture-ricettive">
                        <x-text>
                            <p>Assicuriamo la massima affidabilità per il tuo hotel o b&b, perché tu possa accogliere i tuoi ospiti con la migliore tecnologia, e far vivere loro un’esperienza indimenticabile.</p>
                        </x-text>
                    </x-box-soluzione>
                </div>
            </div>
        </div>
    </x-row>

    <!-- COSA POSSIAMO FARE PER IL TUO BUSINESS

    <section class="has-background-fd-light">
        <div class="columns is-vcentered is-multiline mx-0 mt-6 px-6 pt-6">
            <div class="column is-8 is-offset-1">
                <x-title class="is-h3" color="fd-warning" uppercase="false">
                    Cosa possiamo fare per <span is="h4">il tuo business</span>
                </x-title>
            </div>
            <div class="column is-5 is-offset-1">
                <h4 class="is-h3 is-size-4 has-text-fd-primary">Fiberdroid può risolvere qualunque problema di connessione della tua azienda</h4>
            </div>
            <div class="column is-2 is-1-desktop">
                <img src="{{ asset('images/home/fiber-scudo.svg') }}" class="image">
            </div>
        </div>

        <div class="columns mx-0 px-6 pb-6">
            <div class="column is-10 mx-auto mt-5">
                @component('old.components.horizontal-accordion', ['elements' => [
                "Qualità del prodotto e del servizio",
                "Connessioni wireless",
                "Connessioni terrestri",
                "Creare e gestire il tuo centralino voip",
                ]])

                    <div data-el="0">
                        <x-item-list color="success">
                            forniamo apparati certificati e sicuri, sempre monitorati e sotto controllo
                        </x-item-list>
                        <x-item-list color="success">
                            teniamo aggiornata la tua infrastruttura utilizzando solo prodotti di ultima generazione
                        </x-item-list>
                        <x-item-list color="success">
                            gestiamo la tua infrastruttura per lasciarti libero di dedicarti al tuo business
                        </x-item-list>
                        <x-item-list color="success">
                            monitoriamo costantemente le tue apparecchiature per prevenire I problemi
                        </x-item-list>
                        <x-item-list color="success">
                            forniamo apparati firewall per la tua sicurezza
                        </x-item-list>
                    </div>

                    <div data-el="1">
                        <x-item-list color="success">
                            usando le connessioni satellitari ovunque tu sia collegheremo te e il tuo business!
                        </x-item-list>
                    </div>

                    <div data-el="2">
                        <x-item-list color="success">
                            utilizziamo le reti geografiche per connettere le tue sedi sul territorio
                        </x-item-list>
                        <x-item-list color="success">
                            connettività ADSL/HDSL dove non arriva la fibra
                        </x-item-list>
                        <x-item-list color="success">
                            connettività Fibra asimmetrica la soluzione economica per le piccole e medie imprese
                        </x-item-list>
                        <x-item-list color="success">
                            connettività Fibra simmetrica per le medie e grandi imprese
                        </x-item-list>
                    </div>

                    <div data-el="3">
                        <x-item-list color="success">
                            le tue telefonate viaggiano su internet indipendentemente dalla tecnologia usata (fibra, mobile, adsl o wifi)                        </x-item-list>
                        <x-item-list color="success">
                            nessuna differenza tra chiamate urbane e interurbane
                        </x-item-list>
                        <x-item-list color="success">
                            dai un taglio ai costi del tuo vecchio telefono! con il VOIP non c’è più bisogno di pagare linea telefonica!
                        </x-item-list>
                        <x-item-list color="success">
                            da 1 a 1000 numeri di telefono nuovi o in portabilità
                        </x-item-list>
                        <x-item-list color="success">
                            colleghi di altre sedi sempre connessi a costo zero
                        </x-item-list>
                        <x-item-list color="success">
                            basta telefoni occupati: ricevi più chiamate con lo stesso numero
                        </x-item-list>
                        <x-item-list color="success">
                            tempi di attivazione brevissimi
                        </x-item-list>
                        <x-item-list color="success">
                            centralino hardware on-premise
                        </x-item-list>
                        <x-item-list color="success">
                            centralino in cloud per chi non vuole pensieri
                        </x-item-list>
                        <x-item-list color="success">
                            Telefoni IP a noleggio per snellire i costi
                        </x-item-list>
                    </div>



                @endcomponent
            </div>
        </div>

    </section>
-->

    <!-- ASSISTENZA SENZA CONFRONTI -->
    <x-row class="is-multiline is-mobile pb-6">
        <div class="column is-12">
            <x-title color="fd-primary" class="is-h4 has-text-centered-desktop has-text-left-touch" role="h2">
                Possiamo offrirti una assistenza senza confronti
                <br>
                <span class="is-hidden-mobile">consulenti professionali e con esperienza</span>
            </x-title>
        </div>
        <div class="column is-4 is-offset-2 is-hidden-touch">
            <figure class="image mx-auto my-2">
                <img data-src="{{ image('home-assistenza-tecnica') }}" class="lazy image" style="width: 70%" {!! image_attr('home-assistenza-tecnica') !!}>
            </figure>
        </div>
        <div class="column is-12-touch is-4-desktop">
            <x-item-list gap="5" color="warning">
                <x-text><p>basta call center! per qualunque domanda tecnici specializzati e dedicati alla tua azienda, non devi ripartire ogni volta</p></x-text>
            </x-item-list>
            <x-item-list gap="5" color="warning">
                <x-text><p>4 ore di tempo medio per la risoluzione dei problemi</p></x-text>
            </x-item-list>
            <x-item-list gap="5" color="warning">
                <x-text><p>prevenzione dei problemi grazie al monitoraggio delle nostre apparecchiature sempre aggiornate</p></x-text>
            </x-item-list>
            <x-item-list gap="5" color="warning">
                <x-text><p>assistenza veloce ed efficiente per contatto diretto (no partner) noi controlliamo tutta l’infrastruttura direttamente</p></x-text>
            </x-item-list>
            <x-item-list gap="5" color="warning">
                <x-text><p>documentazione e video guida nell’area clienti per aiutarti nell’operatività di tutti i giorni</p></x-text>
            </x-item-list>
        </div>
        <div class="column is-12-mobile is-10-desktop mb-6 has-text-right-desktop has-text-left-touch">
            <x-button bg="fd-warning" uppercase="true">
                <span data-action="toggle-slide" data-target="#modal-contact">Richiedi informazioni</span>
            </x-button>
        </div>
    </x-row>

    <!-- PERCHE SCEGLIERE FIBERDROID -->
    <x-row thin="true" class="is-multiline is-mobile has-background-fd-light py-6">
        <div class="column is-12 mb-3 mt-4 has-text-centered-desktop has-text-left-touch">
            <x-title color="fd-primary is-h4" role="h2">Perchè scegliere fiberdroid?</x-title>
        </div>

        <div class="column is-2-mobile is-3-tablet is-3-desktop">
            <x-subtitle color="fd-warning" class="is-h4 pt-1" role="h3">Esperienza</x-subtitle>
        </div>
        <div class="column is-10-mobile is-9-tablet is-9-desktop">
            <x-text style="padding-left: 30px">
                <p>Sono 10 anni che navighiamo nella galassia delle telecomunicazioni e dei servizi internet di ultima generazione per garantirti un servizio costruito sulle tue esigenze e in base alla realt&agrave; della tua azienda.</p>
                <p>Il nostro team di tecnici e appassionati di tecnologia si aggiorna continuamente e studia sempre nuove soluzioni per ogni tipo di azienda presente sul mercato.</p>
                <p>Da quando abbiamo iniziato questo lavoro abbiamo sviluppato molti lati positivi che ci rendono fieri. <br /></p>
            </x-text>
        </div>

        <div class="column is-2-mobile is-3-tablet is-3-desktop">
            <figure class="image is-64x64 mx-auto">
                <img loading="lazy" width="64px" height="64px" class="is-rounded has-border-4 has-border-fd-warning has-border-radius-50 p-2" src="{{ image('home-empatia') }}" {!! image_attr('home-empatia') !!}>
            </figure>
        </div>
        <div class="column is-10-mobile is-9-tablet is-9-desktop">
            <x-text style="padding-left: 30px">
                <p>Siamo empatici e quindi è facile per noi capire le esigenze del cliente</p>
            </x-text>
        </div>

        <div class="column is-2-mobile is-3-tablet is-3-desktop">
            <figure class="image is-64x64 mx-auto">
                <img loading="lazy" width="64px" height="64px" class="is-rounded has-border-4 has-border-fd-warning has-border-radius-50 p-2" src="{{ image('home-organizzazione') }}" {!! image_attr('home-organizzazione') !!}>
            </figure>
        </div>
        <div class="column is-10-mobile is-9-tablet is-9-desktop">
            <x-text style="padding-left: 30px">
                <p>Siamo ben organizzati e questo ci permette di realizzare progetti in modo più rapido efficiente,</p>
            </x-text>
        </div>

        <div class="column is-2-mobile is-3-tablet is-3-desktop is-offset-0-touch">
            <figure class="image is-64x64 mx-auto">
                <img loading="lazy" width="64px" height="64px" class="is-rounded has-border-4 has-border-fd-warning has-border-radius-50 p-2" src="{{ image('home-creatività') }}" {!! image_attr('home-creatività') !!}>
            </figure>
        </div>
        <div class="column is-10-mobile is-9-tablet is-9-desktop">
            <x-text style="padding-left: 30px">
                <p>Siamo creativi ed è fondamentale per creare soluzioni innovative e su misura per il cliente.</p>
            </x-text>
        </div>

        <div class="column is-12-mobile is-8-tablet is-8-desktop mx-auto is-offset-2-desktop is-offset-0-mobile my-6 py-6">
            <x-quote>
                Per noi creare una soluzione è come per il pittore dipingere una tela bianca… non c’è limite alla creatività.
            </x-quote>
        </div>

        <div class="column is-2-mobile is-3-tablet is-3-desktop">
            <x-subtitle color="fd-warning" class="is-h4 pt-1" role="h3">Assistenza</x-subtitle>
        </div>
        <div class="column is-10-mobile is-9-tablet is-9-desktop">
            <x-text style="padding-left: 30px;">
                <p>Facciamo dell&rsquo;assistenza il nostro punto di forza, siamo la differenza in un mondo di call center e numeri. Tu non sei un numero! La nostra assistenza &egrave; diretta e i tecnici specializzati sono sempre aggiornati tramite il ticketing cos&igrave; non perdi tempo a rispiegare il problema. Hai problemi? Ci pensiamo noi, mentre tu puoi concentrarti sul tuo business.</p>
            </x-text>
        </div>

        <div class="column is-2-mobile is-3-tablet is-3-desktop">
            <x-subtitle color="fd-warning" class="is-h4 pt-1" role="h3">Risultati</x-subtitle>
        </div>
        <div class="column is-10-mobile is-9-tablet is-9-desktop mb-6">
            <x-text style="padding-left: 30px;">
                <p>Abbiamo collegato diversi clienti con pi&ugrave; sedi in Italia sulla nostra piattaforma cloud di comunicazione unificata. Questo ha permesso che ogni cliente, con pi&ugrave; sedi lontane, potesse con un unico centralino voip parlare come se fosse una telefonata interna azzerando quindi i costi e facilitando l&rsquo;operativit&agrave; quotidiana.</p>
            </x-text>
        </div>

    </x-row>

    <!-- LE COSE DI CUI SIAMO ORGOGLIOSI -->
    <x-row thin="true" bg="fd-orange-gradient" class="py-6 is-multiline">
        <div class="column is-12 has-text-centered">
            <x-title color="fd-primary is-h4" role="h2">Le cose di cui siamo orgogliosi</x-title>
        </div>
        <div class="column is-12">
            <div class="columns">
                <div class="column is-4 mx-auto">
                    <x-item-circle text="60%" desc="risparmio medio"></x-item-circle>
                </div>
                <div class="column is-4 mx-auto">
                    <x-item-circle text="98%" desc="clienti soddisfatti"></x-item-circle>
                </div>
                <div class="column is-4 mx-auto">
                    <x-item-circle text="4ore" desc="tempo medio di risposta"></x-item-circle>
                </div>
            </div>
        </div>
        <div class="column is-12">
            <x-text color="fd-primary has-text-centered-tablet">
                Possibilità di fornire connettività
                e servizi IT anche all’estero grazie alla nostra collaborazione con provider internazionali
            </x-text>
        </div>
    </x-row>

    <!-- TEST COPERTURA -->
    <x-row margin="false" class="mb-0">
        <div class="column is-12 py-0">
            <x-banner-verifica-copertura></x-banner-verifica-copertura>
        </div>
    </x-row>

    @push('css')
        <style>
            #navbar {
                background-image: url('{{ asset('images/globe-space-colore.webp') }}')!important;
                background-position: bottom!important;
                background-color: #2c0d5d!important;
                display: flex;
            }
        </style>
    @endpush
@endsection
