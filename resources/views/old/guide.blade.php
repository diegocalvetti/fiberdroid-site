@extends('layouts._layout')

@section('title', ($article->meta_title ?: $article->title . " | Le guide di fiberdroid"))
@section('description', $article->meta_description)
@section('og-type', 'article')

@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-link to="blog">
                <x-title color="white" role="p">Guida</x-title>
            </x-link>
        </div>
    </x-row>
@endsection

@section('content')

    <x-row thin="true">
        <div class="column is-8">
            {!! $article->html !!}
        </div>
        <div class="column is-4 pl-5">
            <div class="columns is-multiline">
                <div class="column is-12 mt-5">
                    <x-subtitle color="fd-warning"><b>Condividi</b></x-subtitle>
                    <div class="columns is-mobile is-vcentered" style="float: left">
                        <x-fiberdroid-icon :colored="true"></x-fiberdroid-icon>
                    </div>
                </div>
                <!--<div class="column is-12 pt-5 mt-5">
                    <x-subtitle color="fd-warning"><b>Ultimi articoli</b></x-subtitle>
                    da sistemare!
                </div>-->
                <!--
                <div class="column is-12 mt-5">
                    <x-subtitle color="fd-warning"><b>Servizi</b></x-subtitle>
                    @foreach($services->where('service_type_id', 1)->pluck('name', 'url') as $url => $link)
                        <p class="icon-text has-text-fd-secondary is-h4">
                          <span class="icon is-size-7">
                            <i class="fa fa-chevron-right"></i>
                          </span>
                          <a class="has-text-fd-secondary" href="{{ route('servizio', $url) }}">{{ $link }}</a>
                        </p>
                    @endforeach
                </div>
                <div class="column is-12">
                    <x-subtitle color="fd-warning"><b>Prodotti</b></x-subtitle>
                    @foreach($services->where('service_type_id', 2)->pluck('name', 'url') as $url => $link)
                        <p class="icon-text has-text-fd-secondary is-h4">
                          <span class="icon is-size-7">
                            <i class="fa fa-chevron-right"></i>
                          </span>
                          <a class="has-text-fd-secondary" href="{{ route('servizio', $url) }}">{{ $link }}</a>
                        </p>
                    @endforeach
                </div>
                <div class="column is-12">
                    <x-subtitle color="fd-warning"><b>Soluzioni</b></x-subtitle>
                    @foreach($solutions->pluck('name', 'url') as $url => $link)
                        <p class="icon-text has-text-fd-secondary is-h4">
                          <span class="icon is-size-7">
                            <i class="fa fa-chevron-right"></i>
                          </span>
                          <a class="has-text-fd-secondary" href="{{ route('soluzione', $url) }}">{{ $link }}</a>
                        </p>
                    @endforeach
                </div>
                -->
            </div>
        </div>
    </x-row>

@endsection
