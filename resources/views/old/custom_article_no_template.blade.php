@component('old.components.flex.row', ["thin"=>"true", "id"=>"article-preview", 'margin' => false, 'class' => false, 'style' => false])
    <div class="column is-8 fd-content">
        {!! json_decode($article->html)->html !!}
    </div>
@endcomponent
