@extends('layouts._layout')

@section('title', 'Privacy policy | Fiberdroid')

@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-title color="white" class="is-h4">Privacy policy</x-title>
        </div>
    </x-row>
@endsection

@section('content')
    <x-row class="py-6" thin="true">
        <div class="column is-12">
            <x-text>
                <p>
                    Fiberdroid S.r.l. (C.F & P.IVA 07057060969) con sede legale in Milano, via Enrico Cosenz n.54, (di seguito Fiberdroid e/o Titolare) fornisce al Cliente l’informativa, ai sensi dell’art. 13 Regolamento Privacy Europeo 2016/679 (“GDPR”), sulle finalità e modalità di trattamento dei dati personali.
                </p>
                <x-subtitle color="fd-primary">1.	Oggetto del trattamento</x-subtitle>
                <p>
                    Il Titolare tratta:
                </p>
                <ol style="list-style: lower-roman;">
                    <li>
                        i dati forniti dal Cliente, identificativi e non sensibili (in particolare, nome, cognome, codice fiscale, p. iva, indirizzo e-mail, numero telefonico, riferimenti bancari e di pagamento);
                    </li>
                    <li>
                        i dati di navigazione, eventualmente acquisiti implicitamente mediante l’uso dei protocolli di comunicazione di Internet: indirizzi IP, nomi a dominio dei computer e dei terminali utilizzati dagli utenti, indirizzi in notazione URI/URL delle risorse richieste e relativo orario ed altri parametri relativi al sistema operativo dell’utente. Tali dati possono essere necessari per l’utilizzo dei servizi web e possono essere trattati al fine di ottenere informazioni statistiche sull’uso dei servizi, nonché controllare il corretto funzionamento degli stessi.
                    </li>
                </ol>
                <x-subtitle color="fd-primary">2.	Finalità del trattamento</x-subtitle>
                <p>I dati, saranno trattati in modo lecito e secondo correttezza</p>

                <ul style="list-style-type: lower-alpha; margin-left: 20px">
                    <li>
                        per Finalità di Servizio (art. 6 lett. b), e), f) GDPR), e in particolare:
                        <ul style="list-style-type: lower-roman; margin-left: 40px">
                            <li>per adempiere agli obblighi legali e fiscali cui è soggetto il Titolare;</li>
                            <li>per e nell’ambito dell’esecuzione dei Contratti di cui il Cliente è parte e per l’adozione di misure precontrattuali adottate su richiesta dello stesso;</li>
                            <li>poter ricontattare il Cliente, per evadere eventuali sue richieste formulate all’interno dei form disponibili sul sito web;</li>
                            <li>nell’eventualità che sia necessario accertare, esercitare o difendere un diritto in sede giudiziaria, per il perseguimento del legittimo interesse che il Titolare ha ravvisato sussistere sulla base del bilanciamento degli interessi effettuato;</li>
                            <li>per l’adempimento di obblighi di legge;</li>
                            <li>per l’invio di comunicazioni istituzionali.</li>
                        </ul>
                    </li>
                    <li>
                        Per Finalità di Marketing (art. 6 lett. b) GDPR), previo espresso consenso e in particolare per informare il Cliente con lettere ordinarie, chiamate telefoniche, e-mail, SMS, MMS, notifiche e newsletter delle iniziative e offerte di Fiberdroid.
                    </li>
                </ul>

                <x-subtitle color="fd-primary">3. Obbligatorietà o meno del conferimento</x-subtitle>
                <p>
                    Il conferimento dei dati per le finalità di cui alla lett. a) del punto che precede costituisce un obbligo legale e contrattuale ed è necessario per usufruire dei servizi richiesti. Il conferimento dei dati per le finalità di cui alla lett.b) del punto che precede è facoltativo e il loro mancato conferimento o il mancato consenso al loro trattamento non impedisce al Cliente la fruizione dei servizi, ma di ricevere comunicazioni commerciali ed offerte personalizzate.
                </p>
                <x-subtitle color="fd-primary">4.	Modalità del trattamento</x-subtitle>
                <p>
                    Il trattamento dei dati personali è realizzato per mezzo delle operazioni indicate all’art. 4 n. 2) GDPR e precisamente: raccolta, registrazione, organizzazione, conservazione, consultazione, elaborazione, modificazione, selezione, estrazione, raffronto, utilizzo, interconnessione, blocco, comunicazione, cancellazione e distruzione dei dati. I dati personali sono sottoposti a trattamento sia cartaceo che elettronico e/o automatizzato.
                </p>

                <x-subtitle color="fd-primary">5.	Conservazione dei dati</x-subtitle>
                <p>
                    Il Titolare conserva i dati personali per non oltre 10 anni dalla fine del rapporto per le finalità di servizio e comunque per il tempo di prescrizione previsto dalla legge; fino alla revoca del consenso per finalità di marketing. In seguito, i dati del Cliente saranno cancellati o resi anonimi.
                </p>
                <x-subtitle color="fd-primary">6.	Accesso ai dati</x-subtitle>
                <p>
                    I dati personali del Cliente sono accessibili a: i) dipendenti e/o collaboratori del Titolare nella loro qualità di persone autorizzate e/o amministratori di sistema; ii) partner commerciali e fornitori di servizi che svolgono attività in outsourcing per conto di Fiberdroid nella loro qualità di responsabili esterni del trattamento e/o sub-responsabili svolgendo attività connesse, strumentali o di supporto a quelle del titolare, ad esempio: registrazione domini, gestione e manutenzione dei contenuti dei siti web e delle App, assistenza Clienti, servizi di customer care, gestione dei sistemi di Information Technology, servizi editoriali, recupero credito, servizi di elaborazione dati per la fatturazione, archiviazione della documentazione relativa ai rapporti con i clienti, etc…
                </p>
                <x-subtitle color="fd-primary">7.	Comunicazione dei dati</x-subtitle>
                <p>                Il Titolare può comunicare i dati del Cliente senza il suo espresso consenso per le finalità di servizio:
                </p>
                <ul>
                    <li>alle autorità giudiziarie, su loro richiesta;</li>
                    <li>
                        alle Società terze alle quali è necessario comunicarli, per legge o per contratto. Tali Società tratterranno i dati del Cliente nella loro qualità di Titolari autonomi del trattamento e/o Responsabili. In ogni caso, i dati non saranno diffusi né saranno comunicati i dati che per legge ne è vietata la diffusione.
                    </li>
                </ul>

                <x-subtitle color="fd-primary">8.	Trasferimento dati</x-subtitle>
                <p>
                    I dati del Cliente potranno essere trasferiti al di fuori dell’Unione Europea ai soggetti specificati nel paragrafo 6 e 7. Per proteggere i dati del Cliente nell’ambito di questi trasferimenti, il Titolare adotta le garanzie appropriate previste dal GDPR.
                </p>
                <x-subtitle color="fd-primary">9.	Diritti dell’interessato</x-subtitle>
                <p>
                    Il Cliente nella sua qualità di interessato può esercitare i diritti di cui agli articoli 15,16,17,18,20,21,22 GDPR. Se non ricorrono le limitazioni previste dalla legge, ha infatti il diritto di:
                </p>
                <ul>
                    <li>avere la conferma dell’esistenza o meno dei dati personali, anche se non ancora registrati e richiedere che tali dati vengano messi a disposizione in modo chiaro e comprensibile;</li>
                    <li>
                        chiedere indicazioni e, nel caso, copia:
                        <ol style="list-style-type: lower-alpha;">
                            <li>dell’origine e della categoria dei dati personali:</li>
                            <li>della logica di utilizzo, nel caso le informazioni vengano trattate con strumenti elettronici;</li>
                            <li>delle finalità e modalità del trattamento;</li>
                            <li>degli estremi identificativi del Titolare e dei Responsabili;</li>
                            <li>dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati e che possono venirne a conoscenza;</li>
                            <li>del periodo in cui i dati vengono conservati oppure i criteri che vengono utilizzati per determinare tale periodo, quando possibile;</li>
                            <li>dell’esistenza di un processo decisionale automatizzato, compresa la profilazione. In tal caso può richiedere le logiche utilizzate, l’importanza e le conseguenze previste;
                            <li>dell’esistenza di garanzie adeguate in caso di trasferimento dei dati a un paese extra-UE o ad un’organizzazione internazionale;</li>
                        </ol>
                    </li>
                    <li>ottenere, senza che ci sia un ritardo giustificabile, l’aggiornamento, la modifica, rettifica dei dati non corretti o l’integrazione dei dati non completi, qualora ne avesse interesse;
                    </li>
                    <li>
                        ottenere la cancellazione, il blocco dei dati o, ove possibile, la trasformazione in forma anonima:
                        <ol>
                            <li>se trattati illecitamente;</li>
                            <li>se non più necessari in relazione agli scopi per i quali sono stati raccolti o successivamente trattati;</li>
                            <li>in caso di revoca del consenso su cui si basa il trattamento e in caso non sussista altro fondamento giuridico;</li>
                            <li>nel caso in cui si sia opposto al trattamento e non esistano ulteriori motivi legittimi per continuare ad utilizzare i dati;</li>
                            <li>in caso venga imposto dalla legge;</li>
                            <li>nel caso in cui siano riferiti a minori.</li>
                        </ol>
                        Il Titolare può rifiutare di cancellare i dati nel caso di:
                        -	esercizio del diritto alla libertà di espressione e di informazione;
                        -	adempimento di un obbligo legale, esecuzione di un compito svolto nel pubblico interesse o esercizio di pubblici poteri;
                        -	motivi di interesse sanitario pubblico;
                        -	archiviazione nel pubblico interesse, ricerca scientifica o storica o a fini statistici;
                        -	accertamento, esercizio o la difesa di un diritto in sede giudiziaria;
                    </li>
                    <li>
                        ottenere la limitazione del trattamento nel caso di:
                        <ol>
                            <li>
                                contestazione dell’esattezza dei dati personali, se non ha preferito chiederne la modifica, l'aggiornamento, o la rettifica;
                            </li>
                            <li>
                                trattamento illecito del Titolare per impedirne la cancellazione;
                            </li>
                            <li>
                                esercizio di un diritto in sede giudiziaria;
                            </li>
                            <li>
                                verifica dell’eventuale prevalenza dei motivi legittimi del Titolare rispetto a quelli dell’interessato;
                            </li>
                        </ol>
                    </li>
                    <li>ricevere, qualora il trattamento sia effettuato con mezzi automatici, senza impedimenti e in un formato strutturato, di uso comune e leggibile, i dati personali forniti previo consenso o su contratto per trasmetterli ad altro Titolare o – se tecnicamente fattibile – di ottenere la trasmissione diretta da parte del Titolare ad altro Titolare;
                    </li>
                    <li>
                        opporsi in qualsiasi momento totalmente o in parte:
                        <ol>
                            <li>
                                per motivi legittimi e prevalenti, al trattamento dei dati personali;</li>
                            <li>
                                al trattamento di dati personali per finalità di marketing (es. può opporsi all’invio di materiale pubblicitario o di vendita diretta o al compimento di ricerche di mercato o di comunicazione commerciale).
                                Per tutti i casi menzionati sopra, se necessario, Fiberdroid porterà a conoscenza i soggetti terzi ai quali i dati personali sono comunicati dell’eventuale esercizio dei diritti da parte del Cliente, ad eccezione di specifici casi (es. quando tale adempimento si riveli impossibile o comporti un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato).
                            </li>
                        </ol>
                    </li>
                </ul>

                <x-subtitle color="fd-primary">10.	Modalità di esercizio dei diritti</x-subtitle>
                <p>
                    Il Cliente può in qualsiasi momento modificare e revocare i consensi prestati ed esercitare i suoi diritti contattando il Titolare ai seguenti recapiti: - mediante richiesta rivolta a Fiberdroid ai seguenti contatti:
                    •	mediante invio di lettera raccomandata a.r. presso la sede di Via Cosenz, 54 - 20158 Milano (MI) – Italia;
                    •	via e-mail all’indirizzo amministrazione@fiberdroid.it;
                    •	 a mezzo PEC all’indirizzo fiberdroid@pec.fiberdroid.it.
                    Per i trattamenti di cui alla presente informativa, il Cliente ha, inoltre, il diritto di proporre un reclamo al Garante per la Protezione dei Dati Personali (www.garanteprivacy.it).
                </p>
                <x-subtitle color="fd-primary">11.	Titolare DPO Responsabili</x-subtitle>
                <p>Il Titolare del trattamento è Fiberdroid S.r.l.</p>
                <ul>
                    <li>
                        Fiberdroid ha proceduto alla nomina di un Responsabile della protezione dei dati o Data Protection Officer “DPO” ai sensi dell’art. 37 del GDPR. Ciascun interessato può rivolgersi al Responsabile della Protezione dei Dati inviando la sua richiesta all’indirizzo amministrazione@fiberdroid.it oppure via posta all’indirizzo: Fiberdroid S.r.l. Via Cosenz, 54 - 20158 Milano (MI) – Italia – c.a. : Responsabile della Protezione dei Dati.
                        L’elenco aggiornato dei responsabili e degli incaricati al trattamento è custodito presso la sede del Titolare del trattamento.
                        Fiberdroid S.r.l. (C.F & P.IVA 07057060969) con sede legale in Milano, via Enrico Cosenz n.54, (di seguito Fiberdroid e/o Titolare) fornisce al Cliente l’informativa, ai sensi dell’art. 13 Regolamento Privacy Europeo 2016/679 (“GDPR”), sulle finalità e modalità di trattamento dei dati personali.
                        <ol>
                            <li>
                                Oggetto del trattamento
                                Il Titolare tratta:
                                <ol style="list-style-type: lower-alpha">
                                    <li>i dati forniti dal Cliente, identificativi e non sensibili (in particolare, nome, cognome, codice fiscale, p. iva, indirizzo e-mail, numero telefonico, riferimenti bancari e di pagamento);</li>
                                    <li>i dati di navigazione, eventualmente acquisiti implicitamente mediante l’uso dei protocolli di comunicazione di Internet: indirizzi IP, nomi a dominio dei computer e dei terminali utilizzati dagli utenti, indirizzi in notazione URI/URL delle risorse richieste e relativo orario ed altri parametri relativi al sistema operativo dell’utente. Tali dati possono essere necessari per l’utilizzo dei servizi web e possono essere trattati al fine di ottenere informazioni statistiche sull’uso dei servizi, nonché controllare il corretto funzionamento degli stessi.</li>
                                </ol>
                            </li>
                            <li>
                                Finalità del trattamento
                                I dati, saranno trattati in modo lecito e secondo correttezza
                                <ol style="list-style-type: lower-alpha">
                                    <li>
                                        per Finalità di Servizio (art. 6 lett. b), e), f) GDPR), e in particolare:
                                        <ol style="list-style-type: lower-roman">
                                            <li>per adempiere agli obblighi legali e fiscali cui è soggetto il Titolare;</li>
                                            <li>	per e nell’ambito dell’esecuzione dei Contratti di cui il Cliente è parte e per l’adozione di misure precontrattuali adottate su richiesta dello stesso;</li>
                                            <li>.	poter ricontattare il Cliente, per evadere eventuali sue richieste formulate all’interno dei form disponibili sul sito web;</li>
                                            <li>	nell’eventualità che sia necessario accertare, esercitare o difendere un diritto in sede giudiziaria, per il perseguimento del legittimo interesse che il Titolare ha ravvisato sussistere sulla base del bilanciamento degli interessi effettuato;</li>
                                            <li>per l’adempimento di obblighi di legge;</li>
                                            <li>	per l’invio di comunicazioni istituzionali.</li>
                                        </ol>
                                    </li>
                                </ol>
                            </li>
                            <li>
                                Obbligatorietà o meno del conferimento.
                                Il conferimento dei dati per le finalità di cui alla lett. a) del punto che precede costituisce un obbligo legale e contrattuale ed è necessario per usufruire dei servizi richiesti. Il conferimento dei dati per le finalità di cui alla lett.b) del punto che precede è facoltativo e il loro mancato conferimento o il mancato consenso al loro trattamento non impedisce al Cliente la fruizione dei servizi, ma di ricevere comunicazioni commerciali ed offerte personalizzate.
                            </li>
                            <li>
                                Modalità del trattamento
                                Il trattamento dei dati personali è realizzato per mezzo delle operazioni indicate all’art. 4 n. 2) GDPR e precisamente: raccolta, registrazione, organizzazione, conservazione, consultazione, elaborazione, modificazione, selezione, estrazione, raffronto, utilizzo, interconnessione, blocco, comunicazione, cancellazione e distruzione dei dati. I dati personali sono sottoposti a trattamento sia cartaceo che elettronico e/o automatizzato.
                            </li>
                            <li>
                                Conservazione dei dati
                                Il Titolare conserva i dati personali per non oltre 10 anni dalla fine del rapporto per le finalità di servizio e comunque per il tempo di prescrizione previsto dalla legge; fino alla revoca del consenso per finalità di marketing. In seguito, i dati del Cliente saranno cancellati o resi anonimi.
                            </li>
                            <li>
                                Accesso ai dati
                                I dati personali del Cliente sono accessibili a: i) dipendenti e/o collaboratori del Titolare nella loro qualità di persone autorizzate e/o amministratori di sistema; ii) partner commerciali e fornitori di servizi che svolgono attività in outsourcing per conto di Fiberdroid nella loro qualità di responsabili esterni del trattamento e/o sub-responsabili svolgendo attività connesse, strumentali o di supporto a quelle del titolare, ad esempio: registrazione domini, gestione e manutenzione dei contenuti dei siti web e delle App, assistenza Clienti, servizi di customer care, gestione dei sistemi di Information Technology, servizi editoriali, recupero credito, servizi di elaborazione dati per la fatturazione, archiviazione della documentazione relativa ai rapporti con i clienti, etc…
                            </li>
                            <li>
                                Comunicazione dei dati. Il Titolare può comunicare i dati del Cliente senza il suo espresso consenso per le finalità di servizio
                                <ul>
                                    <li>
                                        alle autorità giudiziarie, su loro richiesta;
                                    </li>
                                    <li>
                                        alle Società terze alle quali è necessario comunicarli, per legge o per contratto. Tali Società tratterranno i dati del Cliente nella loro qualità di Titolari autonomi del trattamento e/o Responsabili. In ogni caso, i dati non saranno diffusi né saranno comunicati i dati che per legge ne è vietata la diffusione.
                                    </li>
                                </ul>
                            </li>
                            <li>
                                Trasferimento dati
                                I dati del Cliente potranno essere trasferiti al di fuori dell’Unione Europea ai soggetti specificati nel paragrafo 6 e 7. Per proteggere i dati del Cliente nell’ambito di questi trasferimenti, il Titolare adotta le garanzie appropriate previste dal GDPR.
                            </li>
                            <li>
                                Diritti dell’interessato
                                Il Cliente nella sua qualità di interessato può esercitare i diritti di cui agli articoli 15,16,17,18,20,21,22 GDPR. Se non ricorrono le limitazioni previste dalla legge, ha infatti il diritto di:
                            </li>
                        </ol>
                    </li>
                    <li>
                        avere la conferma dell’esistenza o meno dei dati personali, anche se non ancora registrati e richiedere che tali dati vengano messi a disposizione in modo chiaro e comprensibile;
                    </li>
                    <li>
                        chiedere indicazioni e, nel caso, copia:
                        <ol style="list-style-type: lower-alpha">
                            <li>dell’origine e della categoria dei dati personali;</li>
                            <li>della logica di utilizzo, nel caso le informazioni vengano trattate con strumenti elettronici;</li>
                            <li>delle finalità e modalità del trattamento;</li>
                            <li>degli estremi identificativi del Titolare e dei Responsabili;</li>
                            <li>dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati e che possono venirne a conoscenza;</li>
                            <li>del periodo in cui i dati vengono conservati oppure i criteri che vengono utilizzati per determinare tale periodo, quando possibile;</li>
                            <li>dell’esistenza di un processo decisionale automatizzato, compresa la profilazione. In tal caso può richiedere le logiche utilizzate, l’importanza e le conseguenze previste;</li>
                            <li>dell’esistenza di garanzie adeguate in caso di trasferimento dei dati a un paese extra-UE o ad un’organizzazione internazionale;</li>
                        </ol>
                    </li>
                    <li>
                        ottenere, senza che ci sia un ritardo giustificabile, l’aggiornamento, la modifica, rettifica dei dati non corretti o l’integrazione dei dati non completi, qualora ne avesse interesse;
                    </li>
                    <li>
                        ottenere la cancellazione, il blocco dei dati o, ove possibile, la trasformazione in forma anonima:
                        <ol style="list-style: lower-alpha;">
                            <li>se trattati illecitamente;</li>
                            <li>se non più necessari in relazione agli scopi per i quali sono stati raccolti o successivamente trattati;</li>
                            <li>in caso di revoca del consenso su cui si basa il trattamento e in caso non sussista altro fondamento giuridico;</li>
                            <li>nel caso in cui si sia opposto al trattamento e non esistano ulteriori motivi legittimi per continuare ad utilizzare i dati;</li>
                            <li>in caso venga imposto dalla legge;</li>
                            <li>nel caso in cui siano riferiti a minori.</li>
                        </ol>
                        Il Titolare può rifiutare di cancellare i dati nel caso di:
                        -	esercizio del diritto alla libertà di espressione e di informazione;
                        -	adempimento di un obbligo legale, esecuzione di un compito svolto nel pubblico interesse o esercizio di pubblici poteri;
                        -	motivi di interesse sanitario pubblico;
                        -	archiviazione nel pubblico interesse, ricerca scientifica o storica o a fini statistici;
                        -	accertamento, esercizio o la difesa di un diritto in sede giudiziaria;
                    </li>
                    <li>
                        ottenere la limitazione del trattamento nel caso di:
                        <ol style="list-style: lower-alpha;">
                            <li>contestazione dell’esattezza dei dati personali, se non ha preferito chiederne la modifica, l'aggiornamento, o la rettifica;</li>
                            <li>trattamento illecito del Titolare per impedirne la cancellazione;</li>
                            <li>esercizio di un diritto in sede giudiziaria;</li>
                            <li>verifica dell’eventuale prevalenza dei motivi legittimi del Titolare rispetto a quelli dell’interessato;</li>
                        </ol>
                    </li>
                    <li>
                        ricevere, qualora il trattamento sia effettuato con mezzi automatici, senza impedimenti e in un formato strutturato, di uso comune e leggibile, i dati personali forniti previo consenso o su contratto per trasmetterli ad altro Titolare o – se tecnicamente fattibile – di ottenere la trasmissione diretta da parte del Titolare ad altro Titolare;
                    </li>
                    <li>
                        opporsi in qualsiasi momento totalmente o in parte:

                        <ol style="list-style: lower-alpha;">
                            <li>per motivi legittimi e prevalenti, al trattamento dei dati personali;</li>
                            <li>al trattamento di dati personali per finalità di marketing (es. può opporsi all’invio di materiale pubblicitario o di vendita diretta o al compimento di ricerche di mercato o di comunicazione commerciale).</li>
                        </ol>
                    </li>
                </ul>

                <br>
                <p>
                    Per tutti i casi menzionati sopra, se necessario, Fiberdroid porterà a conoscenza i soggetti terzi ai quali i dati personali sono comunicati dell’eventuale esercizio dei diritti da parte del Cliente, ad eccezione di specifici casi (es. quando tale adempimento si riveli impossibile o comporti un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato).

                    Il Titolare del trattamento è Fiberdroid S.r.l.
                    Fiberdroid ha proceduto alla nomina di un Responsabile della protezione dei dati o Data Protection Officer “DPO” ai sensi dell’art. 37 del GDPR. Ciascun interessato può rivolgersi al Responsabile della Protezione dei Dati inviando la sua richiesta all’indirizzo amministrazione@fiberdroid.it oppure via posta all’indirizzo: Fiberdoid S.r.l. Via Cosenz, 54 - 20158 Milano (MI) – Italia – c.a. : Responsabile della Protezione dei Dati.
                    L’elenco aggiornato dei responsabili e degli incaricati al trattamento è custodito presso la sede del Titolare del trattamento.

                </p>
            </x-text>
        </div>
    </x-row>
@endsection
