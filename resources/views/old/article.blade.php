@extends('layouts._layout')

@section('title', ($article->meta_title ?: $article->title). " | Il blog di fiberdroid")
@section('description', $article->meta_description)
@section('og-type', 'article')

@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-link to="blog">
                <x-title color="white" role="p">Blog</x-title>
            </x-link>
        </div>
    </x-row>
@endsection

@section('content')

    <x-row thin="true">
        <div class="column is-8">
            <div class="columns is-multiline">
                    @foreach($introSections as $section)
                        <x-section :section="$section" isTitle="true"></x-section>
                    @endforeach

                    <div class="column is-12 mt-3">
                            <x-subtitle color="fd-warning" uppercase="false" class="is-h4">Guida nella lettura</x-subtitle>
                            <x-base-list>
                                <ul>
                                    @foreach($anchorableSections as $section)
                                        <li>
                                            <a href="{{ url()->current()."#".$section->slug }}">{{ $section->title }}</a>

                                            <ul>

                                                @foreach($section->accordions as $title => $content)
                                                    <li>
                                                        <a href="{{ url()->current()."#".\Illuminate\Support\Str::slug($title) }}" data-action="open-accordion" data-target="#{{ \Illuminate\Support\Str::slug($title) }}">{{ $title }}</a>
                                                    </li>
                                                @endforeach

                                                @foreach($section->subsections as $subsection)
                                                    <li>
                                                        <a href="{{ url()->current()."#".$subsection->slug }}">{{ $subsection->title }}</a>
                                                        <ul>
                                                            @foreach($subsection->subsections as $thirdsection)
                                                                <li>
                                                                    <a href="{{ url()->current()."#".$thirdsection->slug }}">{{ $thirdsection->title }}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </x-base-list>
                        </div>

                    @foreach($mainSections as $section)
                        <x-section :section="$section"></x-section>
                    @endforeach
            </div>
        </div>
        <div class="column is-4 pl-5">
            <div class="columns is-multiline">
                <div class="column is-12 mt-5">
                    <x-subtitle color="fd-warning"><b>Condividi</b></x-subtitle>
                    <div class="columns is-mobile is-vcentered" style="float: left">
                        <x-fiberdroid-icon :colored="true"></x-fiberdroid-icon>
                    </div>
                </div>
                <div class="column is-12 pt-5 mt-5">
                    <x-subtitle color="fd-warning"><b>Ultimi articoli</b></x-subtitle>
                    @foreach($articles as $article)
                        <div class="columns is-vcentered is-mobile is-clickable" onclick="window.location.href='{{ route('article', $article->url) }}'">
                            <div class="column" style="max-width: 64px;">
                                <figure class="image is-64x64" style="overflow: hidden">
                                    <img width="64" height="64" src="{{ $article->image->full_url }}" alt="{{ $article->image->alt }}" title="{{ $article->image->title }}">
                                </figure>
                            </div>
                            <div class="column pl-5">
                                <x-text>
                                    <span class="is-size-7 is-h1 has-text-fd-primary">{{ $article->title }}</span>
                                    <p class="is-size-7">{{ substr($article->meta_description, 0, 40) }}...</p>
                                </x-text>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!--
                <div class="column is-12 mt-5">
                    <x-subtitle color="fd-warning"><b>Servizi</b></x-subtitle>
                    @foreach($services->where('service_type_id', 1)->pluck('name', 'url') as $url => $link)
                        <p class="icon-text has-text-fd-secondary is-h4">
                          <span class="icon is-size-7">
                            <i class="fa fa-chevron-right"></i>
                          </span>
                          <a class="has-text-fd-secondary" href="{{ route('servizio', $url) }}">{{ $link }}</a>
                        </p>
                    @endforeach
                </div>
                <div class="column is-12">
                    <x-subtitle color="fd-warning"><b>Prodotti</b></x-subtitle>
                    @foreach($services->where('service_type_id', 2)->pluck('name', 'url') as $url => $link)
                        <p class="icon-text has-text-fd-secondary is-h4">
                          <span class="icon is-size-7">
                            <i class="fa fa-chevron-right"></i>
                          </span>
                          <a class="has-text-fd-secondary" href="{{ route('servizio', $url) }}">{{ $link }}</a>
                        </p>
                    @endforeach
                </div>
                <div class="column is-12">
                    <x-subtitle color="fd-warning"><b>Soluzioni</b></x-subtitle>
                    @foreach($solutions->pluck('name', 'url') as $url => $link)
                        <p class="icon-text has-text-fd-secondary is-h4">
                          <span class="icon is-size-7">
                            <i class="fa fa-chevron-right"></i>
                          </span>
                          <a class="has-text-fd-secondary" href="{{ route('soluzione', $url) }}">{{ $link }}</a>
                        </p>
                    @endforeach
                </div>
                -->
            </div>
        </div>
    </x-row>

@endsection
