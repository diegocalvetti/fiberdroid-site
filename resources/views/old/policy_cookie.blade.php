@extends('layouts._layout')

@section('title', 'Cookie policy | Fiberdroid')
@section('description', "La presente \"Cookie Policy\" ha lo scopo di fornire, in linea con il principio di trasparenza che disciplina il rapporto con i nostri clienti, informazioni sulle tipologie, le finalità e le modalità di utilizzo/gestione dei cookie inviati durante la navigazione sul sito.")

@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-title color="white" class="is-h4" role="h1">Cookie policy</x-title>
        </div>
    </x-row>
@endsection

@section('content')
    <x-row thin="true">
        <div class="column is-12 has-text-justified-mobile">
            <p>
                La presente “Cookie Policy” ha lo scopo di fornire, in linea con il principio di trasparenza che disciplina il rapporto con i nostri clienti, informazioni sulle tipologie, le finalità e le modalità di utilizzo/gestione dei cookie inviati durante la navigazione sul sito.<br>
                Si tratta di piccoli file di testo che i siti visitati dall'utente inviano al suo software di navigazione (in gergo “browser”) e che vengono memorizzate sul dispositivo (Computer, Tablet, Smartphone, ecc.) da cui si utilizza tale software, dove vengono memorizzati per essere poi ritrasmessi agli stessi siti alla successiva visita del medesimo utente.
                <br>

                Di solito un cookie si presenta come una stringa contenente almeno il nome del sito internet dal quale il cookie stesso proviene, la "durata" del cookie, ed un valore, che di regola è un numero unico generato in modo casuale.
                Di seguito troverai una breve spiegazione delle tipologie di cookies
                <br>
            </p>
            <br>
            <x-subtitle color="fd-primary">Cookie tecnici.</x-subtitle>
            <p>
                I cookie tecnici sono quelli utilizzati al solo fine di "effettuare la trasmissione di una comunicazione su una rete di comunicazione elettronica, o nella misura strettamente necessaria al fornitore di un servizio della società dell'informazione esplicitamente richiesto dall'abbonato o dall'utente a erogare tale servizio".
                Essi non vengono utilizzati per scopi ulteriori e sono normalmente installati direttamente dal titolare o gestore del sito web. Possono essere suddivisi in cookie di navigazione o di sessione, che garantiscono la normale navigazione e fruizione del sito web (permettendo, ad esempio, di realizzare un acquisto o autenticarsi per accedere ad aree riservate); cookie analytics, assimilati ai cookie tecnici laddove utilizzati direttamente dal gestore del sito per raccogliere informazioni, in forma aggregata, sul numero degli utenti e su come questi visitano il sito stesso; cookie di funzionalità, che permettono all'utente la navigazione in funzione di una serie di criteri selezionati (ad esempio, la lingua, i prodotti selezionati per l'acquisto) al fine di migliorare il servizio reso allo stesso.
                Per l'installazione di tali cookie non è richiesto il preventivo consenso degli utenti.
            </p>
            <br>
            <x-subtitle color="fd-primary">Cookie di profilazione.</x-subtitle>
            <p>
                I cookie di profilazione sono volti a creare profili relativi all'utente e vengono utilizzati al fine di inviare messaggi pubblicitari in linea con le preferenze manifestate dallo stesso nell'ambito della navigazione in rete. In ragione della particolare invasività che tali dispositivi possono avere nell'ambito della sfera privata degli utenti, la normativa europea e italiana prevede che l'utente debba essere adeguatamente informato sull'uso degli stessi ed esprimere il proprio valido consenso.
            </p>
            <br>
            <x-subtitle color="fd-primary">Cookie di prima parte.</x-subtitle>
            <p>
                Sono i cookie gestiti dal titolare del sito. Per questi cookie, l’obbligo dell’informativa spetta al titolare del sito. Spetta anche a quest’ultimo l’obbligo di indicare le modalità per l’eventuale blocco del cookie.
            </p>
            <br>
            <x-subtitle color="fd-primary">Cookie di terza parte.</x-subtitle>
            <p>
                Sono i cookie gestiti da un soggetto terzo diverso dal titolare del sito. Per questi cookie, l’obbligo dell’informativa e dell’indicazione delle modalità per l’eventuale blocco del cookie spetta alla terza parte, mentre al titolare del sito è fatto obbligo di inserire nel sito il link al sito della terza parte ove tali elementi sono disponibili.
                In entrambe le tipologie di cookie (di prima parte o di terza parte) la raccolta del consenso, necessario qualora il cookie sia un cookie di profilazione, avviene tramite apposito banner nella home page del sito.
            </p>
            <br>
            <x-subtitle color="fd-primary">I nostri cookie</x-subtitle>
            <div style="overflow-x: auto;">
                <table class="table fd-content is-striped is-fullwidth">
                    <thead>
                    <tr>
                        <th>Nome cookies</th>
                        <th style="width: 50%">Funzione</th>
                        <th>Scadenza</th>
                        <th style="width: 20%">Prima o terza parte?</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>
                            fiberdroid_session
                        </td>
                        <td>
                            Raccoglie informazioni riguardo alla tua sessione corrente. Ci serve per riconoscerti attraverso le varie pagine e
                            di contenere i dati necessari al funzionamento del sito web.
                        </td>
                        <td>
                            30 giorni
                        </td>
                        <td>
                            Prima parte
                        </td>
                    </tr>
                    <tr>
                        <td>
                            fiberdroid_cookie
                        </td>
                        <td>
                            Raccoglie la tua approvazione dei cookie, se hai accettato l'utilizzo dei cookie il suo contenuto sara: <i>true</i>
                            altrimenti conterrà false o non sarà presente. Ricorda che utilizzando questo sito acconsenti comunque all'utlizzo dei cookie indispensabili
                        </td>
                        <td>
                            30 giorni
                        </td>
                        <td>
                            Prima parte
                        </td>
                    </tr>
                    <tr>
                        <td>
                            XSRF-TOKEN
                        </td>
                        <td>
                            Questo cookie contiene un token che ti identifica, ci permette di verificare che i dati inviati alla nostra
                            applicazione arrivino effettivamente da te e da nessun'altro. Ti viene assegnato un nuovo token ogni cambio di sessione.
                        </td>
                        <td>
                            2 ore o fino a quando non scade la sessione
                        </td>
                        <td>
                            Prima parte
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <br>
            <x-subtitle color="fd-primary">Come contattarci</x-subtitle>
            <p>
                Per qualsiasi domanda, commento o dubbio relativo alla presente Cookie Policy o alla Privacy Policy del sito, contattare la Società ai seguenti recapiti che puoi trovare nel footer in fondo a ogni pagina, inclusa questa.
            </p>
            <br>
            <x-subtitle color="fd-primary">Aggiornamento della presente cookie policy</x-subtitle>
            <p>
                La presente Cookie Policy è stata aggiornata il 08/02/2021. Eventuali aggiornamenti saranno sempre pubblicati in questa pagina.
            </p>
        </div>

    </x-row>
@endsection
