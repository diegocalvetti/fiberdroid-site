@extends('layouts._layout')

@section('title', "Assistenza | Fiberdroid")
@section('description', "Con Fiberdroid l’assistenza è una cosa seria. Scegli Fiberdroid, la miglior assistenza per la tua connessione internet e non sprecare più tempo con i call center!")

@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-title color="white" class="is-h4" role="h1">Assistenza</x-title>
        </div>
    </x-row>
@endsection

@section('content')

    <x-row class="is-multiline" thin="true">
        <div class="column is-12 has-text-justified-desktop-only pt-6 is-size-4">
            <x-text>
                <p>
                    Ci rendiamo perfettamente conto di quanto sia difficile per te scegliere la miglior assistenza  per la connessione internet e soprattutto il giusto partner tra le decine che si propongono sul WEB, ma se sei qui è perché sai che l’assistenza è fondamentale nel rapporto tra provider e cliente.
                    <br>
                    Forse sei tra quelli che hanno scelto in base a un’offerta molto competitiva e poi nel momento del bisogno hai avuto a che fare con call center che ingoiano il tuo problema come un buco nero, dove tu sei solo un numero di chiamata e ogni volta che cade la linea passi allo step successivo e devi rispiegare da capo il tuo problema.
                </p>
            </x-text>
        </div>
        <div class="column is-12 has-text-justified-desktop-only">
            <x-title color="fd-primary" class="is-h2">NELLE TELECOMUNICAZIONI L’ASSISTENZA  AI CLIENTI FA LA DIFFERENZA!</x-title>
            <x-text>
                <p>
                    Se sei stanco di buttare ore/lavoro preziose per il tuo business, se vuoi avere un’assistenza veloce, precisa e attenta alle tue problematiche ma soprattutto un’assistenza personalizzata, dedicata a te, con un tecnico di riferimento che sa esattamente di cosa stai parlando, in Fiberdroid l’hai trovata.
                </p>
            </x-text>
        </div>
    </x-row>

    <x-row thin="true" class="is-flex-desktop-only is-vcentered is-multiline">
        <div class="column is-12-touch is-6-desktop">
            <x-box-assistenza
                    icon="assistenza-amministrazione"
                    title="Amministrazione"
                    mail="{{ $config['contact']['mail_amministrazione'] }}"
                    phone="{{ $config['contact']['tel_amministrazione'] }}"
            ></x-box-assistenza>
        </div>
        <div class="column is-12-touch is-6-desktop">
            <x-box-assistenza
                    icon="assistenza-tecnica"
                    title="Assistenza"
                    mail="{{ $config['contact']['mail_assistenza'] }}"
                    phone="{{ $config['contact']['tel_assistenza'] }}"
            ></x-box-assistenza>
        </div>
        <div class="column is-12-touch is-6-desktop is-offset-3-desktop">
            <x-box-assistenza
                    icon="supporto-tecnico"
                    title="Area supporto tecnico"
                    contact="false"
            >
                <div class="columns is-mobile is-vcentered">
                    <div class="column is-6">
                        <x-text>
                            <p>Apri un ticket</p>
                        </x-text>
                    </div>
                    <div class="column is-6">
                        <x-link to="{{ $config['link_areaclienti'] }}/tickets" target="_blank">
                            <x-button bg="fd-warning" full="true">Crea ticket</x-button>
                        </x-link>
                    </div>
                </div>
                <div class="columns is-mobile is-vcentered">
                    <div class="column is-6">
                        <x-text>
                            <p>Supporto remoto</p>
                        </x-text>
                    </div>
                    <div class="column is-6">
                        <x-link to="{{ $config['download']['supremo'] }}" target="_blank">
                            <x-button bg="success" full="true">Scarica software</x-button>
                        </x-link>
                    </div>
                </div>

            </x-box-assistenza>
        </div>
    </x-row>

    <x-row thin="true" class="is-multiline">
        <div class="column is-12">
            <x-subtitle color="fd-primary">PERCHÉ IL SERVIZIO CLIENTI FIBERDROID CI FA SCEGLIERE  COME PARTNER?</x-subtitle>
            <x-accordion active="true" title="perché siamo specializzati in assistenza">
                <x-text>
                    <p>
                        I nostri tecnici hanno esperienza sul campo.
                        Sanno come interpretare gli indizi che gli comunicherai
                        perché negli anni ne hanno viste e sentite di ogni tipo.
                        Fidati, ti guideranno verso la soluzione.
                    </p>
                </x-text>
            </x-accordion>
            <x-accordion active="true" title="perché non sarai un numero">
                <x-text>
                    <p>
                        ogni cliente per noi è unico, il suo business è il nostro business e siamo organizzati in modo tale da assegnargli un tecnico di riferimento e assistenza dedicata.
                    </p>
                </x-text>
            </x-accordion>
            <x-accordion title="perché non rimbalzerai tra un partner e l’altro">
                <x-text>
                    <p>
                        gestendo noi  tutto risolviamo velocemente ogni problema di connessione e non dovrai ripetere cento volte il tuo problema a un tecnico diverso.
                    </p>
                </x-text>
            </x-accordion>
            <x-accordion title="perché gestiamo tutta l’infrastruttura">
                <x-text>
                    <p>
                        potendo controllare tutte le parti della connessione e usando solo materiale certificato siamo molto più veloci a trovare I problemi. Quindi niente ritardi burocratici e di incomprensione
                    </p>
                </x-text>
            </x-accordion>
            <x-accordion title="perché abbiamo Hardware certificato e sicuro">
                <x-text>
                    <p>
                        sostituzioni a caldo, elementi ridondati, backup  incrementali. ogni cosa possibile viene attuata per la sicurezza dei tuoi dati e della tua trasmissione.
                    </p>
                </x-text>
            </x-accordion>
            <x-accordion title="perché gestiamo noi i tuoi backup">
                <x-text>
                    <p>
                        e in caso di rete down avrai comunque un collegamento senza perdita di dati e di tempo
                    </p>
                </x-text>
            </x-accordion>
            <x-accordion title="perché saremo veloci">
                <x-text>
                    <p>
                        se non dipende da fattori esterni gravi,  risolverai il tuo problema in meno di 4 ore e senza perdere tempo prezioso  al telefono!                    </p>
                </x-text>
            </x-accordion>
            <x-accordion title="perché non ti abbandoneremo">
                <x-text>
                    <p>
                        lo scaricabarile non fa parte di noi. Ovunque tu sia attiveremo un tecnico di fiducia in loco per farti arrivare l’assistenza HW alla massima velocità possibile. E se il problema dipendesse dalla dorsale, da cavi tranciati, da una guerra atomica, noi ci informeremo per te, solleciteremo per te e ti terremo aggiornato sui progressi.                    </p>
                </x-text>
            </x-accordion>
        </div>
        <div class="column is-12">
            <x-text>
                <p>Quindi se anche tu dai <strong>la precedenza alla qualit&agrave; del servizio, alla migliore assistenza clienti</strong> piuttosto che all&rsquo;offerta dell&rsquo;ultimo minuto, sei nel posto giusto!</p>
                <p><strong>il servizio clienti, come un amico, si vede nel momento del bisogno.</strong>..</p>
                <p>...e un&rsquo;altra cosa che ci contraddistingue &egrave; che nel momento del bisogno non ti chiederemo mai di fare un upgrade per avere pi&ugrave; assistenza! Perch&eacute; noi non abbiamo clienti di serie A e serie B, se diventerai nostro cliente avrai sempre tutta l&rsquo;assistenza di cui avrai bisogno. perch&eacute <strong>la qualit&agrave; del nostro servizio non cambia a seconda del contratto</strong>.</p>
                <p>Nessuno ti chieder&agrave; di attivare tariffe ulteriori per avere un&rsquo;assistenza migliore.</p>
                <p><strong>Sei gi&agrave; con Fiberdroid. Sei gi&agrave; con l&rsquo;assistenza migliore possibile!</strong></p>
            </x-text>
        </div>
        <div class="column is-12">
            <x-subtitle color="fd-primary">TU PENSA AL TUO BUSINESS, A COLLEGARTI AL MONDO CI PENSIAMO NOI</x-subtitle>
            <x-accordion title="perché cade la connessione di rete?" active="true">
                <x-text>
                    <ul>
                        <li>per prima cosa controlla il WI-FI forse il segnale &egrave; troppo debole.</li>
                        <li>Puoi spostare il modem e metterlo pi&ugrave; vicino alla tua postazione o viceversa. i muri indeboliscono il segnale.</li>
                        <li>Controlla se ci sono interferenze (baby monitor? allarme via radio? succede quando hai in funzione il microonde? o stai usando il decoder tv? )</li>
                        <li>Prova a cambiare il canale wifi tramite router</li>
                        <li>Come ultima opzione puoi aggiungere un ripetitore di segnale (un Access Point o un Repeater a seconda che siano collegati via ethernet o via wi-fi)</li>
                    </ul>
                </x-text>
            </x-accordion>
            <x-accordion title="posso condividere il WI-FI con i  miei ospiti?">
                <x-text>
                    <p>
                        l’ideale è creare una sottorete per loro, in modo da non dover comunicare la tua password ad altri.</p>
                </x-text>
            </x-accordion>
            <x-accordion title="quando navigo il PC è lento.">
                <x-text>
                    <p>se succede su qualsiasi sito che visiti il problema &egrave; dalla tua parte della connessione.</p>
                    <ul>
                        <li>per prima cosa controlla la tua rete</li>
                        <li>hai programmi che occupano troppa banda?</li>
                        <li>Aggiornamenti del pc o smartphone in corso?</li>
                        <li>Spegni tutti i dispositivi connessi al router e collega con il cavo di rete solo il tuo pc (spegnendo il wifi) ed esegui uno speed test al fine di verificare la banda media della tua connessione</li>
                    </ul>
                </x-text>
            </x-accordion>
        </div>
    </x-row>


@endsection
