@extends('layouts._layout')

@section('title', ($solution->meta_title ?: "Soluzioni di connettività per ".strtolower($solution->name)). " | Fiberdroid")
@section('description', $solution->meta_description)

@section('banner')
    <x-row bg="transparent" thin="true" class="is-mobile py-4">
        <div class="column is-12 my-5">
            <x-title color="white" class="is-h4" role="p">{{ $solution->name }}</x-title>
        </div>
    </x-row>
@endsection

@section('content')

    <x-row thin="true" class="is-multiline">
        @foreach($introSections as $section)
            <x-section :section="$section" isTitle="true"></x-section>
        @endforeach
    </x-row>


    @if($solution['name'] != "Jetfiber" && $solution['name'] != "Hybrid 5G" && $solution['name'] != "Fibra Ottica")
        <x-banner-verifica-copertura></x-banner-verifica-copertura>
    @endif

    @if( !is_null($firstMainSection)  )
        <x-row thin="true" class="is-multiline">
            <x-section :section="$firstMainSection"></x-section>
        </x-row>
    @endif

    @if($anchorableSections->count() > 0)
        <x-row thin="true" class="is-multiline">
            <div class="column is-12 mt-3">
                <x-subtitle color="fd-warning" uppercase="false" class="is-h4" role="p">Guida nella pagina</x-subtitle>
                <x-base-list>
                    <ul>
                        @foreach($anchorableSections as $section)
                            <li>
                                <a href="{{ url()->current()."#".$section->slug }}">{{ $section->title }}</a>

                                <ul>

                                    @foreach($section->accordions as $title => $content)
                                        <li>
                                            <a href="{{ url()->current()."#".\Illuminate\Support\Str::slug($title) }}" data-action="open-accordion" data-target="#{{ \Illuminate\Support\Str::slug($title) }}">{{ $title }}</a>
                                        </li>
                                    @endforeach

                                    @foreach($section->anchorableSubsections as $subsection)
                                        <li>
                                            <a href="{{ url()->current()."#".$subsection->slug }}">{{ $subsection->title }}</a>
                                            <ul>
                                                @foreach($subsection->anchorableSubsections as $thirdsection)
                                                    <li>
                                                        <a href="{{ url()->current()."#".$thirdsection->slug }}">{{ $thirdsection->title }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </x-base-list>
            </div>
        </x-row>
    @endif

    <x-row thin="true" class="is-multiline">
        @foreach($mainSections as $section)
            <x-section :section="$section"></x-section>
        @endforeach
    </x-row>

    <x-banner-demo></x-banner-demo>
@endsection
