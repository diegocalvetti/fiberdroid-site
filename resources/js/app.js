require('./bootstrap');
require('./tranzit');
require('./components');
require('./animations');
require('./cart');

import {slideToggle} from "./toggler";

$(document).on('mouseenter', '[data-action="hover-discover-fd"]', function () {

    let self = $(this);

    self.find("a").removeClass('has-text-grey-lighter').addClass('has-text-white');
    self.find("i").removeClass('has-text-fd-warning').addClass('has-text-fd-warning-darker');

});
$(document).on('mouseleave', '[data-action="hover-discover-fd"]', function () {

    let self = $(this);

    self.find("a").addClass('has-text-grey-lighter').removeClass('has-text-white');
    self.find("i").addClass('has-text-fd-warning').removeClass('has-text-fd-warning-darker');

});

/**
 * Toggle
 */
$(document).on('mouseenter', '[data-action="toggle-hover"]', function () {

    let self = $(this);
    let target = $( self.attr('data-target') );

    self.find("a").addClass('hover-has-text-fd-warning-dark');
    target.css('visibility', 'visible');

});
$(document).on('mouseleave', '[data-action="toggle-hover"]', function () {

    let self = $(this);
    let target = $( self.attr('data-target') );

    self.find("a").removeClass('hover-has-text-fd-warning-dark');
    target.css('visibility', 'hidden');

});

$(document).on('click', '[data-action="open-accordion"]', function () {

    let self = $(this);
    let target = $( self.attr('data-target') );

    target.find('.card-content').removeClass('is-hidden');
    target.removeClass('is-shadowless')

});


$(document).on('click', '[data-action="toggle-subnavbar"]', function () {
    let self = $(this);
    let targetSelector = $(this).attr('data-target');
    let target = $(targetSelector);


    let newLeft = target.css('left') == "0" || target.css('left') == "0px" ? "110vw" : "0";

    console.log(newLeft);
    target.animate({left: newLeft}, 800);

});

$(document).on('click', '[data-action="toggle-slide"]', function () {

    let self = $(this);
    let targetSelector = $(this).attr('data-target');
    let target = $(targetSelector);

    $("html, body").css('scroll-behavior', 'auto');
    $(window).scrollTop(0);
    $("html, body").css('scroll-behavior', 'smooth');
    let overflow = $("body").css('overflow-y') === 'hidden' ? 'auto' : 'hidden';
    $("html").css('overflow', "hidden");
    $("body").css('overflow-y', overflow);
    slideToggle(document.querySelector(targetSelector), 500);

});

$(document).on('click', '[data-action="toggle-navbar"]', function () {

    let targetSelector = $(this).attr('data-target');
    let target = $(targetSelector);

    $(target).slideToggle(800);
    let overflow = $("body").css('overflow-y') === 'hidden' ? 'auto' : 'hidden';
    let position = $("body").css('overflow-y') === 'hidden' ? 'static!important' : 'fixed!important';
    $("html").css('overflow', "hidden");
    $("body, html").css('overflow-y', overflow).css('position', position);

    /*
    slideToggle(document.querySelector(targetSelector), 800);


    window.setTimeout( () => {
        target.height("105vh");

        let overflow = $("body").css('overflow-y') === 'hidden' ? 'auto' : 'hidden';
        $("body, html").css('overflow-y', overflow);

    }, 800);
    */
});

$(document).on('mouseenter', '[data-action="toggle-blog-card"]', function () {
    let self = $(this);
    self.removeClass('no-card');
});
$(document).on('mouseleave', '[data-action="toggle-blog-card"]', function () {
    let self = $(this);
    self.addClass('no-card');
});

/**
 * Modal
 */
$(document).on('click', '[data-action="close-modal"]', function ()  {

    let target = $( $(this).attr('data-target') );
    target.removeClass("is-active");

});

$(document).on('click', '[data-action="close-cookie"]', function ()  {

    let target = $( $(this).attr('data-target') );
    target.remove();

});


/**
 * Cookies
 */
import Cookies from 'js-cookie'

$(document).on('click', '[data-cookie="accept"]', function ()  {
    Cookies.set('fiberdroid_cookie', 'true', {expires: 30});
});


$(document).ready(function() {
   $("img.lazy").each(function() {
      $(this).attr('src', $(this).attr('data-src'));
   });

   if ( !Cookies.get('fiberdroid_cookie') ) {
       $("#modal_cookie").fadeIn(1000)
   }

   let first_visit = Cookies.get('fiberdroid_first_visit')

   function checkPopup() {
       let promo_visited = Cookies.get('fiberdroid_promo_visited')
       first_visit = Cookies.get('fiberdroid_first_visit')

       if (promo_visited) return

       const diffInSeconds = Math.abs(new Date(first_visit) - new Date()) / 1000;

       if (diffInSeconds >= 7) {
           $(".promo-modal").addClass('is-active')
           Cookies.set('fiberdroid_promo_visited', 'true', {expires: 1})
       }
   }

   if ( !first_visit ) {
       Cookies.set('fiberdroid_first_visit', new Date(), {expires: 7});
   }

    first_visit = Cookies.get('fiberdroid_first_visit')

    setInterval(checkPopup, 1000)

});

$('[data-close-promo-modal]').on('click', function () {
    $(".promo-modal").removeClass('is-active')
})
/**
 * @TODO What?
 */
$("input").unbind();

$("input").on('keypress', function (event) {
    $("input").unbind();
    console.log('keypress', event);
})

$("input").on('keyup', function (event) {
    $("input").unbind();
    console.log('keyup', event);
})

function querySelectorAllShadows(selector, el = document.body) {
    // recurse on childShadows
    const childShadows = Array.from(el.querySelectorAll('*')).
    map(el => el.shadowRoot).filter(Boolean);

    // console.log('[querySelectorAllShadows]', selector, el, `(${childShadows.length} shadowRoots)`);

    const childResults = childShadows.map(child => querySelectorAllShadows(selector, child));

    // fuse all results into singular, flat array
    const result = Array.from(el.querySelectorAll(selector));
    return result.concat(childResults).flat();
}

const interval = setInterval(function () {
    const buttons = querySelectorAllShadows('#wplc-chat-button')

    if(buttons[0]) {
        buttons[0].setAttribute('aria-label', 'Chatta con Fiberdroid in tempo reale!');
        clearInterval(interval);
    }
}, 1000)
