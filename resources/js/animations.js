const on = (element, event, cb) => {
    $(document).on(event, element, function (e) {
        //e.preventDefault();
        cb($(this))
    });
}
const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}
const fadeInAndTranslate = (element, options) => {
    $(`[data-${element}]`).each(function () {
        const self = $(this);

        if(options.reset) {
            self.css('opacity', 0)
            return
        }

        self.css('opacity', 0)

        if(options.translate) {
            self.animate(options.translate, 0)
        }

        setTimeout(function () {
            self.animate({opacity: 1, x: 0, y: 0}, options.duration);
        }, options.pause)
    })
}
const fadeOut = (target, options) => {
    $(target).each(function () {
        const self = $(this);

        if(options.reset) {
            self.css('opacity', 1)
            return
        }

        self.css('opacity', 1)

        setTimeout(function () {
            self.animate({opacity: 0}, options.duration);
        }, options.pause)
    })
}
const fadeIn = (target, options) => {
    $(target).each(function () {
        const self = $(this);

        if(options.reset) {
            self.css('opacity', 0)
            return
        }

        self.css('opacity', 0)

        setTimeout(function () {
            self.animate({opacity: 1}, options.duration);
        }, options.pause)
    })
}

const rotate = (element, options) => {
    $({deg: options.fromAngle ?? 0}).animate({deg: options.angle}, {
        duration: options.duration,
        queue: false,
        step: function(now) {
            // in the step-callback (that is fired each step of the animation),
            // you can use the `now` paramter which contains the current
            // animation-position (`0` up to `angle`)
            $(element).css({
                transform: 'rotate(' + now + 'deg)'
            });
        }
    });
}
const rotateY = (element, options) => {
    $({deg: options.fromAngle ?? 0}).animate({deg: options.angle}, {
        duration: options.duration,
        queue: false,
        step: function(now) {
            // in the step-callback (that is fired each step of the animation),
            // you can use the `now` paramter which contains the current
            // animation-position (`0` up to `angle`)
            $(element).css({
                transform: 'rotateY(' + now + 'deg)'
            });
        }
    });
}
const getVueRoot = (el) => {
    return $(el)[0]?.shadowRoot
}
const rotateAndShowBack = (target, backTarget, options) => {
    const angle = 90;
    const duration = 300

    let front = target
    let back = backTarget

    console.log("rotate", {front, back})

    if ($(target).attr('data-is-rotated-back')) {
        front = backTarget
        back = target

        $(target).removeAttr('data-is-rotated-back');
    } else {

        $(target).attr('data-is-rotated-back', 'true');
    }

    rotateY(front, {angle, duration})
    fadeOut(front, {duration, pause: 0})

    rotateY(back, {angle: 0, duration: 0})
    fadeIn(back, {duration, pause: duration/2})
    //fadeIn(front, {duration: 0,  pause: duration})

    if (!$(target).attr('data-is-rotated-back')) {
        setTimeout(function () {
            $(front).attr('style')
            $(front).removeAttr('style')
        }, duration + duration/2)
    }
}

/** ************************ **/
/**       ANIMATIONS         **/
/** ************************ **/
function homeAnimation() {
    const pause = 600
    const menu_duration = pause + 800

    fadeInAndTranslate('fade-in', {
        duration: menu_duration - pause,
        pause
    })

    fadeInAndTranslate('fade-in-text', {
        translate: {y: 20},
        duration: 1000,
        pause
    })

    fadeInAndTranslate('fade-in-description', {
        translate: {y: 20},
        duration: 500,
        pause
    })

    fadeInAndTranslate('fade-in-button', {
        translate: {y: 45},
        duration: 700,
        pause
    })

    fadeInAndTranslate('fade-in-faster', {
        duration: 500,
        pause
    })

    $(document).ready(function () {
        const observer = new IntersectionObserver(intersections => {
            intersections.forEach(({
                                       target,
                                       isIntersecting
                                   }) => {
                console.log(isIntersecting ? "visible" : "hidden");

                fadeInAndTranslate('fade-coverage-icon', {
                    translate: { y:-75 },
                    duration: 700,
                    pause: menu_duration - 700,
                    reset: !isIntersecting
                })

                fadeInAndTranslate('fade-coverage-text', {
                    duration: 100,
                    pause: menu_duration - 100,
                    reset: !isIntersecting
                })

                fadeInAndTranslate('fade-coverage-input', {
                    translate: { y:35 },
                    duration: 500,
                    pause: menu_duration - 500 + 100,
                    reset: !isIntersecting
                })

            });
        }, {
            threshold: 0
        });

        document.querySelectorAll('[data-animable]').forEach(div => {
            observer.observe(div);
        });
    })
}

function accordionsAnimation() {
    on('[data-accordion]', 'click', function (self) {
        const target = self.data('target')
        const targetElement = $(`[data-ref="${target}"]`)
        const img = self.find(".accordion-arrow")

        const isVisible = targetElement.css('display') !== 'none'
        const duration = 800
        const closeDuration = 400

        if(isVisible) {
            rotate(img, {fromAngle: 270 + 90, angle: 270, closeDuration})
            targetElement.slideToggle(closeDuration)
        } else {
            rotate(img, {fromAngle: 270, angle: 270 + 90, duration})
            targetElement.slideToggle(duration)
            fadeInAndTranslate(targetElement, {duration, pause: 0})
        }

    })
}

function menuAnimation() {
    on('[data-open-menu]', 'click', function (self) {
        const dropdown = $(".menu-dropdown-mobile")

        if(dropdown.is(':visible')) {
            $("html").css('overflow', 'auto')
        } else {
            $("html").css('overflow', 'hidden')
        }

        dropdown.slideToggle()

    })
    on('[data-open-mobile-submenu]', 'click', function (self) {
        const target = self.data('target')
        const targetElement = $(`[data-menu-mobile-${target}]`)

        targetElement.slideToggle()
    })

    const select_tag = (tag) => {
        tag.addClass('active')
        tag.children().each(function () {
            const child = $(this)
            tag.addClass('has-addons')
            child.addClass('active')

            if(child.hasClass('is-delete')) {
                child.removeClass('is-hidden')
                tag.css('border', 'none')
            } else {
                child.css('margin', '')
            }
        })
    }
    const deselect_tag = (tag) => {
        tag.removeClass('active')
        tag.children().each(function () {
            const child = $(this)

            tag.removeClass('has-addons').css('border', '1.638px solid #FFF')
            child.removeClass('active')

            if(child.hasClass('is-delete')) {
                child.addClass('is-hidden')
            } else {
                child.css('margin', 0)
            }
        })
    }
    const align_products = (products, isColumn4) => {
        let count = 0;
        products.each(function () {
            const product = $(this)

            if( product.is(':visible') ) {
                let cls = ""

                if(isColumn4) {
                    cls = count % 3 === 0 ? "ml-auto" : (count % 3 === 1 ? "mx-auto" : "mr-auto");
                } else {
                    cls = count % 2 === 0 ? "ml-auto" : "mr-auto";
                }

                product
                    .removeClass("mx-auto")
                    .removeClass("ml-auto")
                    .removeClass("mr-auto")
                    .addClass(cls)

                count++;
            }
        });
    }

    on('.tags', 'click', function (self) {
        const value = self.data('tag-content')
        const products = $("[data-product]")
        const isColumn4 = products.first().parent().hasClass('is-4')

        let allDisabled = true;

        if(!self.hasClass('active')) {
            select_tag(self)
        } else {
            deselect_tag(self)
        }

        $(".tags").each(function () {
            const tag = $(this)
            if(!tag.is(self)) {
                deselect_tag(tag)
            }

            if (tag.hasClass('active')) {
                allDisabled = false
            }
        })

        if (allDisabled) {


            products.each(function () {
                const product = $(this)
                product.parent().addClass('is-flex').removeClass('is-hidden')
            })

            align_products(products, isColumn4)

            return;
        }

        products.each(function () {
            const product = $(this)

            const raw = product.data('tags') !== "" ? `[${product.data('tags').split(",").map(el => `"${el}"`)}]` : "[]"
            const tags = JSON.parse(raw)

            if (!tags.includes(value) ) {
                product.parent().removeClass('is-flex').addClass('is-hidden')
            } else {
                product.parent().addClass('is-flex').removeClass('is-hidden')
            }
        })

        align_products(products, isColumn4)
    })

    const toggle_cart = (duration = 800, wait = 0) => {
        const dropdown = $(".cart")
        const bg = $(".cart-background")

        setTimeout(function () {
            bg.animate({width: 'toggle'}, duration);
            dropdown.slideToggle(duration)
        }, wait)

        $("[data-voice]").each(function () {
            $(this).removeClass('menu-voice-active')
        })
        activeVoice.addClass('menu-voice-active')

        return duration
    }

    let current_url = window.location.pathname.replaceAll("/", "-")

    if (current_url.startsWith("-servizi")) {
        current_url = "-servizi";
    }
    if (current_url.startsWith("-soluzioni")) {
        current_url = "-soluzioni";
    }
    if (current_url.startsWith("-prodotti")) {
        current_url = "-prodotti";
    }

    let activeVoice = $(`[data-voice-${current_url}]`);
    if (!activeVoice.length) activeVoice = $(`[data-voice--blog]`);

    activeVoice.addClass('menu-voice-active')

    const full_url = window.location.href.split('?')[0];

    if (full_url.endsWith('jetfiber') || full_url.endsWith('fiberjet')) {
        $(".logo").attr('src', location.protocol + '//' + location.host + '/images/jetfiber/logos-white.webp')
    }

    const close_all_menu_except = (target = "") => {
        let wait = 0

        $(".menu-dropdown").each(function() {
            const dropdown = $(this)
            if(typeof dropdown.attr(`data-menu-${target}`) == 'undefined') {
                dropdown.slideUp(200)
                wait = 200
            }
        })

        return wait
    }

    on('.menu-link', 'click', (self) => {
        const target = self.attr('data-target');
        let wait = 0

        if ($(".cart").is(':visible')) {
            wait += toggle_cart(400)
        }

        wait += close_all_menu_except(target)

        if(target) {

            let voice = $(`[data-voice--${target}]`);

            setTimeout(function () {
                const element = $(`[data-menu-${target}]`)

                element.slideToggle(200, function () {
                    if(element.is(':visible')) {
                        $(".menu-dropdown-closer").show()

                        $("[data-voice]").each(function () {
                            $(this).removeClass('menu-voice-active')
                        })
                        voice.addClass('menu-voice-active')
                    } else {
                        $(".menu-dropdown-closer").hide()
                    }
                })
            }, wait)
        }
    })
    on('.menu-dropdown-closer','click', (self) => {
        $(".menu-dropdown").each(function() {
            $(".menu-dropdown-closer").toggle()
            $(this).slideUp(200)
        })

        $("[data-voice]").each(function () {
            $(this).removeClass('menu-voice-active')
        })
        activeVoice.addClass('menu-voice-active')
    })

    on('[data-cart]', 'click', function (self) {
        const wait = close_all_menu_except()
        toggle_cart(800, wait)
    })
    on('.cart-background', 'click', function (self) {
        toggle_cart()
    })
}

function carouselAnimation() {
    $(document).ready(function () {
        let offset = 0
        let offsetLeft = 0

        setInterval(function () {
            const carouselSection = document.getElementsByClassName('carousel')[0]

            if (!carouselSection) return

            offset += 1/2
            offsetLeft += 1/2

            document.body.style.setProperty('--scroll', offset);
            document.body.style.setProperty('--scroll-left', offsetLeft);
        }, 50)
    })
}

function coverageAnimation() {
    $(document).ready(async function () {
        if(window.location.pathname !== "/verifica-copertura") return

        const root = $(document)
        let vueRootCoverage = getVueRoot("#verifica-copertura")

        while (!vueRootCoverage) {
            console.log("ERROR => vueRootCoverage verifica not found... retrying in 1 sec")
            await sleep(1000)
            vueRootCoverage = getVueRoot("#verifica-copertura")
        }

        $(vueRootCoverage.querySelectorAll(".coverage-technology-box")).click(function () {
            const self = $(this)
            const target = self.data('coverage-type')

            if (target === "project") {
                $(vueRootCoverage.querySelector("[data-coverage-products-section]")).hide()
                return
            }

            $(vueRootCoverage.querySelector("[data-coverage-products-section]")).show()

            let productsHTML = ""

            for (const section of root.find(`[data-coverage-section-${target}]`)) {
                productsHTML += section.innerHTML
            }

            vueRootCoverage.querySelector("[data-coverage-products]").innerHTML = productsHTML

            //console.log(vueRootCoverage.querySelector("[data-coverage-products]"));

            const products = vueRootCoverage.querySelectorAll(".service-box-gradient")

            const max_speed_fttc = self.data('max-speed-fttc')
            const max_speed_ftth = self.data('max-speed-ftth')

            products.forEach(product => {

                if (product.getAttribute('data-ref').startsWith('fttc') && parseInt(product.getAttribute('data-max-speed')) > parseInt(max_speed_fttc)) {
                    product.style.display = 'none'
                    product.parentNode.style.display = 'none'
                } else if (product.getAttribute('data-ref').startsWith('ftth') && parseInt(product.getAttribute('data-max-speed')) > parseInt(max_speed_ftth)) {
                    product.style.display = 'none'
                    product.parentNode.style.display = 'none'
                } else {
                    product.style.display = 'block'
                    product.parentNode.style.display = 'flex'
                }

                console.log('ref', product.getAttribute('data-ref'))
            })

            // Fix the margins
        })
    })
}

function jftAnimation() {
    on('.ticket-mitchell-title', 'click',function (self) {

        const anchor = $('[data-ticket-anchor]')

        if (anchor.css('top') == "-984px") {
            $(".black-ticket").css('visibility', 'visible')
            $("[data-ticket-meeting]").fadeOut()
            anchor.animate({
                top: 0
            }, 2000, function () {
                $("[data-ticket-meeting]").fadeIn(1000)
            })

            $("[data-ticket-column]").animate({
                height: '2000px'
            }, 2000)

        } else {
            $("[data-ticket-meeting]").fadeOut()
            anchor.animate({
                top: "-984px"
            }, 2000, function () {
                $(".black-ticket").css('visibility', 'hidden')
                $("[data-ticket-meeting]").fadeIn(1000)
            })

            $("[data-ticket-column]").animate({
                height: '1100px'
            }, 2000)    }
    })
}

function newsletterAnimation() {
    on('[data-send-newsletter]', 'click',function (self) {
        const url = self.data('url')
        const input = $('[data-newsletter-input]')
        const email = input.val()

        async function postData(url = "", data = {}) {
            // Default options are marked with *
            const response = await fetch(url, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                mode: "cors", // no-cors, *cors, same-origin
                cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    "Content-Type": "application/json",
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: "follow", // manual, *follow, error
                referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                body: JSON.stringify(data), // body data type must match "Content-Type" header
            });
            return response.json(); // parses JSON response into native JavaScript objects
        }

        postData(`${url}/api/newsletter`, { email }).then((data) => {
            if (data.data) {
                $("[data-newsletter-error]").hide()
                $("[data-hide-on-success]").hide()
                $("[data-newsletter-success]").show()
            } else {
                $("[data-newsletter-error]").text(data.error.email[0]).show()
            }
        });
    })

    on('[data-newsletter-input]', 'input', function (self) {
        $("[data-newsletter-error]").hide()
    })
}

function chiSiamoAnimation() {
    on('[data-crew-card]', 'mouseenter',function (self) {
        self.find(".crew-box-header").css(
            'background-image', 'url(\'https://fiberdroid.it/pages/home/card/grid-left.svg\'), url(\'https://fiberdroid.it/pages/home/card/grid-right.svg\')'
        )
    })
    on('[data-crew-card]', 'mouseleave',function (self) {
        self.find(".crew-box-header").css(
            'background-image', 'none'
        )
    })

    let current_best_plans = 0
    let scrollLeft = 0

    on('[data-best-plans-next]', 'click', function (self) {
        current_best_plans = current_best_plans + 1

        scrollLeft += $(`#best-plans-${current_best_plans}`).offset().left

        $('#best-plans').animate({scrollLeft}, 1000);
    })
    on('[data-best-plans-previous]', 'click', function (self) {
        current_best_plans = current_best_plans - 1

        scrollLeft += $(`#best-plans-${current_best_plans}`).offset().left

        $('#best-plans').animate({scrollLeft}, 1000);
    })
}

menuAnimation();
carouselAnimation();
coverageAnimation();
jftAnimation();
newsletterAnimation();
chiSiamoAnimation();
homeAnimation();
accordionsAnimation();
