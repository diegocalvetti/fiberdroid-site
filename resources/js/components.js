const on = (element, event, cb) => {
    $(document).on(event, element, function () {
        cb($(this))
    });
}
const click = (element, cb) => on(element, 'click', cb)
const show_notification_box = (text) => {
    const box = $(".notification-box")
    const box_text = $(".notification-text")
    box_text.text(text)
    box.fadeIn()

    setTimeout(() => {
        box.fadeOut()
    }, 2000)
}

function componentsAnimation() {
    on('[data-icons]', 'mouseenter', (self) => {
        self.find(".icon")?.hide()
        self.find(".icon-active")?.show()
    })
    on('[data-icons]', 'mouseleave', (self) => {
        self.find(".icon")?.show()
        self.find(".icon-active")?.hide()
    })

    click('[data-copy-url]',  async () => {
        await navigator.clipboard.writeText(window.location.href);
        show_notification_box("Url copiato con successo")
    });

    click('.notification-box',  async () => {
        $(".notification-box").fadeOut(200)
    });

    $(`[data-fttc]`).hide()
    click('[data-toggle-section]', async (self) => {
        const target = self.attr('data-target')
        $(`[data-section]`).hide()
        $(`[data-${target}]`).show()
    })
}

componentsAnimation();
