import axios from "axios";

const getVueRoot = (el) => {
    return $(el)[0]?.shadowRoot
}
const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const stored = localStorage.getItem('cart')
let cart = stored ? JSON.parse(stored) : []

const stored_form = localStorage.getItem('form')
let form = stored_form ? JSON.parse(stored_form) : []

const storeCart = (cart) => {
    localStorage.setItem('cart', JSON.stringify(cart));
}
const storeForm = (form) => {
    localStorage.setItem('form', JSON.stringify(form));
}

const updateUI = (cart) => {
    const cartIcon = $(".cart-icon")

    $("[data-is-clone]").hide()

    if (cart.length === 0) {
        $(".cart").css('height', 'auto')
        $("[data-cart-is-empty]").show()
        $("[data-empty-cart]").hide()
        $("[data-create-order]").hide()
        cartIcon.addClass('is-zero')
    } else {
        $(".cart").css('height', '700px')
        $("[data-cart-is-empty]").hide()
        $("[data-create-order]").show()
        $("[data-empty-cart]").show()
        cart.forEach(product => {
            const cloned = $("[data-cart-item]").clone()
            cloned.removeAttr('data-cart-item')
            cloned.attr('data-is-clone', 'true')
            cloned.show()
            cloned.find(".cart-item-title").html(
                product.name
            )
            cloned.find(".cart-price-text").html(
                `&euro; ${product.price}`
            )

            console.log({product})

            if(product.image) {
                cloned
                    .find(".cart-item-icon-box").addClass('is-hidden')
                cloned
                    .find(".cart-image-box").removeClass('is-hidden')
                cloned
                    .find(".cart-image").attr('src', product.image)
            } else if(product.icon) {
                cloned
                    .find(".cart-item-icon-box").find('img').attr('src', `/pages/menu/${product.icon}`)
            } else {
                cloned
                    .find(".cart-item-icon-box").removeClass('is-hidden')
                cloned
                    .find(".cart-image-box").addClass('is-hidden')
            }

            $("[data-cart-items]").prepend(cloned)
        })
        if(cart.length !== 0) {
            cartIcon.removeClass('is-zero')
        }
    }


    cartIcon.attr('data-value', `${cart.length}`)
}

function cartAnimation() {
    const on = (element, event, cb) => {
        $(document).on(event, element, function () {
            cb($(this))
        });
    }

    const activation_product = {
        slug: "activation_product",
        name: "Costi di attivazione",
        price: 0
    }


    updateUI(cart)

    on("[data-add-to-cart]", 'click', function (self) {
        const target = self.data('target')

        const productData = $(`[data-ref="${target}"]`)
        const product = {}
        product.name = productData.data('cart-name')
        product.image = productData.data('image')
        product.price = productData.data('price')
        product.icon = productData.data('icon')
        product.activation_price = productData.data('activation-price')

        cart.push(product)
        storeCart(cart)

        updateUI(cart)
    })

    on("[data-empty-cart]", 'click', function (self) {
        cart = []
        storeCart(cart)
        updateUI(cart)
    })

    on("[data-remove-cart-item]", 'click', function (self) {
        const name = self.parent().find(".cart-item-title").html();
        cart = cart.filter(el => el.name != name)
        storeCart(cart)
        updateUI(cart)
    })


    on("[data-create-order]", 'click', function (self) {
        $("[data-cart-submitting]").show()
        $("[data-cart-editing]").hide()
    })

    const fields = ['email', 'name', 'phone', 'address']

    for (const field of fields) {

        if (form && form[field]) {
            $(`[data-send-${field}]`).val(form[field])
        }

        on(`[data-send-${field}]`, 'input', function (self) {
            $(`[data-error-${field}]`).text("")
        })
    }

    let loading = false;

    on("[data-send-order]", 'click', async function (self) {

        if (loading) return

        const data = {cart: JSON.stringify(cart)}

        for (const field of fields) {
            data[field] = $(`[data-send-${field}]`).val()
        }

        storeForm(data)

        let errors = []
        let result = null

        loading = true
        $("[data-send-order] .button").addClass('is-loading')

        try {
            const response = await axios.post(`https://fiberdroid.it/api/order`, data)
            result = response.data
        } catch (err) {
            errors = err.response.data.error
        }

        loading = false
        $("[data-send-order] .button").removeClass('is-loading')

        if (result) {
            $("[data-cart-submitted]").show()
            $("[data-cart-editing]").hide()
            $("[data-cart-submitting]").hide()
        } else {
            for (const field in errors) {
                $(`[data-error-${field}]`).text(errors[field])
            }
        }


    })

    on("[data-create-new-cart]", 'click', function (self) {
        $("[data-cart-editing]").show()
        $("[data-cart-submitting]").hide()
        $("[data-cart-submitted]").hide()
    })

    $(document).ready(async function () {
        const root = $(document)
        let vueRootCoverage = getVueRoot("#verifica-copertura")
        let counter = 0

        while (!vueRootCoverage && counter < 4) {
            console.log("ERROR => vueRootCoverage verifica not found... retrying in 1 sec")
            await sleep(1000)
            vueRootCoverage = getVueRoot("#verifica-copertura")
            counter++
        }

        if (!vueRootCoverage) return

        console.log(
            {vueRootCoverage},
            $(vueRootCoverage.querySelectorAll(".coverage-technology-box")),
            $(vueRootCoverage.querySelectorAll("[data-add-to-cart]"))
        )

        $(vueRootCoverage.querySelectorAll(".coverage-technology-box")).click(function () {
            $(vueRootCoverage.querySelectorAll("[data-add-to-cart]")).click(function () {
                const self = $(this)
                const target = self.data('target')

                const productData = $(`[data-ref="${target}"]`)
                const product = {}
                product.name = productData.data('cart-name')
                product.image = productData.data('image')
                product.price = productData.data('price')
                product.activation_price = productData.data('activation-price')

                cart.push(product)
                storeCart(cart)

                updateUI(cart)
            })
        })
    })
}

function quizAnimation() {
    $(document).ready(async function () {
        let vueRoot = getVueRoot("#quiz-component")

        if (!vueRoot) return

        const quiz = vueRoot.querySelector("#start-quiz")

        console.log("QUIZ", quiz)

        $(quiz).click(async function () {
            setTimeout(function () {
                let step = 0

                console.log("Ok")

                $(vueRoot.querySelectorAll(".quiz-button-down-mobile")).click(function () {
                    step++
                    console.log("ok")

                    if (step === 6) {
                        setTimeout(function () {
                            console.log("LAST STEP!!!")
                            let addToCart = $(vueRoot.querySelectorAll("[data-add-to-cart]"))
                            addToCart.click(function () {
                                const product = {}
                                product.name = $(this).data('title')
                                product.image = ""
                                product.price = $(this).data('price')
                                product.activation_price = addToCart.data('activation-price')

                                cart.push(product)
                                storeCart(cart)

                                updateUI(cart)

                            })
                        }, 1000)
                    }
                })

                $(vueRoot.querySelectorAll(".quiz-configuration-box")).click(function () {
                    const self = $(this)

                    const box = $(vueRoot.querySelector(".quiz-configuration-box"))
                    const optionsBox = $(vueRoot.querySelector("[data-options-box]"))

                    if( box.css('bottom') === "0px" ) {
                        box.animate({
                            bottom: "-400px",
                        })
                        optionsBox.removeClass('light-blur')
                        $("html").css('overflow', 'auto')
                    } else {
                        box.animate({
                            bottom: "0",
                        })
                        optionsBox.addClass('light-blur')
                        $("html").css('overflow', 'hidden')
                    }

                })
            }, 1000)
        })

    })
}

cartAnimation();

setTimeout(function () {
    quizAnimation();
}, 1000)
