module.exports = {
    deploy: {
        prod: {
            user: "zucchinatech",
            host: [
                {
                    host: "195.78.220.216",
                    port: "49623",
                },
            ],
            ref: "origin/master",
            repo: "git@bitbucket.org:diegocalvetti/fiberdroid-site.git",
            path: "/var/www/html/fiberdroid.it",
            "pre-deploy": "git pull",
            "post-deploy": "sh config.sh && cp .env.prod .env && cp .htaccess.prod public/.htaccess",
            env: {
                APP_ENV: "prod",
            },
        },
    },
};
