<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:sm="http://www.sitemaps.org/schemas/sitemap/0.9">

    <xsl:template match="/">
        <html lang="it-IT">
            <head>
                <link rel="stylesheet" href="https://fiberdroid.it/css/app.min.css" />
            </head>
            <body>
                <div class="columns">
                    <div class="column is-7-desktop is-10-tablet is-12-mobile mx-auto">
                        <h1 class="is-size-2-desktop is-size-3-tablet is-size-4-mobile py-5">Sitemap XML di Fiberdroid</h1>
                        <table class="table is-striped">
                            <tr>
                                <th>URL</th>
                                <th>Ultima modifica</th>
                            </tr>
                            <xsl:for-each select="sm:urlset/sm:url">
                                <tr>
                                    <td>
                                        <a href="{sm:loc}"><xsl:value-of select="sm:loc"/></a>
                                    </td>
                                    <td>
                                        <xsl:value-of select="sm:lastmod"/>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </table>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
