(self["webpackChunkvue_lib_web_component"] = self["webpackChunkvue_lib_web_component"] || []).push([[878],{

/***/ 3878:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ FiberdroidQuizshadow; }
});

;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/FiberdroidQuiz.vue?vue&type=template&id=7849d70c&shadow
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "columns is-mobile mb-0"
  }, [_vm.step === 0 ? _c('div', {
    staticClass: "column quiz-container desktop-11 touch-12 mx-auto mt-6 px-6 pb-6 mt-0 pt-0"
  }, [_c('div', {
    staticClass: "box quiz-gradient",
    style: `background: ${_vm.bg};`
  }, [_c('div', {
    staticClass: "box quiz-box box-small"
  }, [_c('div', {
    staticClass: "columns p-0"
  }, [_c('div', {
    staticClass: "column is-6 p-6"
  }, [_c('p', {
    staticClass: "text-small pb-4"
  }, [_vm._v("Hai bisogno di aiuto a scegliere?")]), _vm._m(0), _c('p', {
    staticClass: "text-smaller mb-4"
  }, [_vm._v(" fai il nostro quiz interattivo e lascia che i nostri esperti ti indirizzino alle tecnologie più adatte a te, sulla base delle tue scelte. ")]), _c('button', {
    staticClass: "button is-small is-fd-primary",
    staticStyle: {
      "border-radius": "8px"
    },
    attrs: {
      "id": "start-quiz"
    },
    on: {
      "click": [function ($event) {
        if ($event.ctrlKey || $event.shiftKey || $event.altKey || $event.metaKey) return null;
        return _vm.nextStep.apply(null, arguments);
      }, function ($event) {
        if (!$event.altKey) return null;
        return _vm.endForAdmin.apply(null, arguments);
      }]
    }
  }, [_c('b', [_vm._v("INIZIA QUIZ")])]), _vm._m(1)]), _c('div', {
    staticClass: "column is-6 p-0 quiz-image",
    style: `background-size: cover; background-image: url('https://fiberdroid.it/images/${_vm.icon}')`
  })])])])]) : _vm._e(), _vm.step === 1 ? _c('StepBox', {
    attrs: {
      "bg": _vm.bg,
      "step": _vm.step,
      "options": _vm.options,
      "selected": _vm.selected
    },
    on: {
      "optionClicked": _vm.toggleSelect,
      "nextStep": _vm.nextStep
    },
    scopedSlots: _vm._u([{
      key: "title",
      fn: function () {
        return [_c('p', {
          staticClass: "quiz-text-title pb-4"
        }, [_vm._v("Di quali servizi hai bisogno?")]), _c('p', {
          staticClass: "quiz-text-title quiz-is-subtitle"
        }, [_vm._v("(puoi sceglierne più di uno)")])];
      },
      proxy: true
    }], null, false, 426530816)
  }) : _vm._e(), _vm.step === 2 ? _c('StepBox', {
    attrs: {
      "bg": _vm.bg,
      "step": _vm.step,
      "options": _vm.options,
      "selected": _vm.selected
    },
    on: {
      "optionClicked": _vm.toggleSelect,
      "nextStep": _vm.nextStep,
      "previousStep": _vm.previousStep
    },
    scopedSlots: _vm._u([{
      key: "title",
      fn: function () {
        return [_c('p', {
          staticClass: "quiz-text-title pb-4"
        }, [_vm._v("Quali funzionalità pensi che userai maggiormente?")]), _c('p', {
          staticClass: "quiz-text-title quiz-is-subtitle"
        }, [_vm._v("(puoi sceglierne più di una)")])];
      },
      proxy: true
    }], null, false, 772398460)
  }) : _vm._e(), _vm.step === 3 ? _c('StepBox', {
    attrs: {
      "bg": _vm.bg,
      "step": _vm.step,
      "options": _vm.options,
      "selected": _vm.selected
    },
    on: {
      "optionClicked": _vm.toggleSelectSingle,
      "nextStep": _vm.nextStep,
      "previousStep": _vm.previousStep
    },
    scopedSlots: _vm._u([{
      key: "title",
      fn: function () {
        return [_c('p', {
          staticClass: "quiz-text-title pb-4"
        }, [_vm._v("Quanti dipendenti o clienti "), _c('br'), _vm._v(" si connetteranno ad internet ogni giorno?")])];
      },
      proxy: true
    }], null, false, 4202705731)
  }) : _vm._e(), _vm.step === 4 ? _c('StepBox', {
    attrs: {
      "bg": _vm.bg,
      "step": _vm.step,
      "options": _vm.options,
      "selected": _vm.selected
    },
    on: {
      "optionClicked": _vm.toggleSelectSingle,
      "nextStep": _vm.nextStep,
      "previousStep": _vm.previousStep
    },
    scopedSlots: _vm._u([{
      key: "title",
      fn: function () {
        return [_c('p', {
          staticClass: "quiz-text-title pb-4"
        }, [_vm._v("Hai bisogno di una linea di backup?")])];
      },
      proxy: true
    }], null, false, 3897443752)
  }) : _vm._e(), _vm.step === 5 ? _c('StepBox', {
    attrs: {
      "bg": _vm.bg,
      "step": _vm.step,
      "options": _vm.options,
      "selected": _vm.selected
    },
    on: {
      "optionClicked": _vm.toggleSelectSingle,
      "nextStep": _vm.nextStep,
      "previousStep": _vm.previousStep
    },
    scopedSlots: _vm._u([{
      key: "title",
      fn: function () {
        return [_c('p', {
          staticClass: "quiz-text-title pb-4"
        }, [_vm._v("Serve un router o ne possiedi già uno?")])];
      },
      proxy: true
    }], null, false, 3286749509)
  }) : _vm._e(), _vm.step === 6 ? _c('StepBox', {
    attrs: {
      "bg": _vm.bg,
      "step": _vm.step,
      "options": _vm.options,
      "selected": _vm.selected,
      "last": true
    },
    on: {
      "optionClicked": _vm.toggleSelectSingle,
      "nextStep": _vm.lastStep,
      "previousStep": _vm.previousStep
    },
    scopedSlots: _vm._u([{
      key: "title",
      fn: function () {
        return [_c('p', {
          staticClass: "quiz-text-title pb-4"
        }, [_vm._v("Servono dei telefoni o ne possiedi già alcuni?")])];
      },
      proxy: true
    }], null, false, 961225192)
  }) : _vm._e(), _vm.step === 7 ? _c('div', {
    staticClass: "column is-12 has-text-centered mt-6"
  }, [_c('p', {
    staticClass: "quiz-text-title"
  }, [_vm._v("Ecco i nostri consigli")]), _vm.result ? _c('div', {
    staticClass: "columns my-6"
  }, [_c('div', {
    staticClass: "column is-11 mx-auto px-6"
  }, [_c('div', {
    staticClass: "columns"
  }, _vm._l(_vm.result, function (data, index) {
    return _c('div', {
      key: data.name,
      staticClass: "column is-4"
    }, [_c('QuizResultBox', {
      class: index === 0 ? 'ml-auto' : index === 1 ? 'mx-auto' : 'mr-auto',
      attrs: {
        "title": data.name,
        "items": data.items,
        "price": data.totalAmount
      }
    })], 1);
  }), 0)])]) : _vm._e(), _c('span', {
    staticClass: "button is-dark",
    on: {
      "click": _vm.restart
    }
  }, [_vm._v(" Ripeti quiz ")])]) : _vm._e()], 1);
};
var staticRenderFns = [function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c('p', {
    staticClass: "title-text pb-4 pt-6-mobile",
    staticStyle: {
      "font-weight": "600"
    }
  }, [_vm._v(" Trova la migliore "), _c('br'), _vm._v(" soluzione per il tuo"), _c('br'), _vm._v(" business ")]);
}, function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c('a', {
    staticClass: "button quiz-button-down-mobile is-small is-dark ml-4",
    staticStyle: {
      "border-radius": "8px"
    },
    attrs: {
      "rel": "noopener noreferrer",
      "target": "_blank",
      "href": "https://calendly.com/fiberdroid/richiesta-chiamata"
    }
  }, [_c('b', {
    staticClass: "has-text-white"
  }, [_vm._v("ORGANIZZA UNA CALL")])]);
}];

;// CONCATENATED MODULE: ./src/components/FiberdroidQuiz.vue?vue&type=template&id=7849d70c&shadow

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.push.js
var es_array_push = __webpack_require__(7658);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/helpers/quiz/StepBox.vue?vue&type=template&id=8a0ee948&scoped=true&
var StepBoxvue_type_template_id_8a0ee948_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "column quiz-container desktop-11 touch-12 mx-auto mt-6 px-6 pb-6 mt-0 pt-0"
  }, [_c('ShadowBox', {
    attrs: {
      "bg": _vm.bg
    }
  }, [_c('div', {
    staticClass: "column is-9 has-text-centered",
    attrs: {
      "data-options-box": ""
    }
  }, [_vm._t("title"), _c('div', {
    staticClass: "columns is-multiline py-6",
    class: _vm.size === '-small' ? 'is-mobile' : ''
  }, _vm._l(_vm.options[_vm.step], function (option, index) {
    return _c('div', {
      key: option.label,
      staticClass: "column",
      class: `is-${_vm.columnSize}`,
      on: {
        "click": function ($event) {
          return _vm.$emit('optionClicked', {
            step: _vm.step,
            option
          });
        }
      }
    }, [_c('OptionBox', {
      attrs: {
        "option": option,
        "index": index,
        "size": _vm.size,
        "active": _vm.isSelected(option)
      }
    })], 1);
  }), 0), _vm.step > 1 ? _c('button', {
    staticClass: "button w-100-mobile is-dark",
    on: {
      "click": function ($event) {
        return _vm.$emit('previousStep');
      }
    }
  }, [_vm._v("Indietro")]) : _vm._e(), _c('button', {
    staticClass: "button w-100-mobile is-fd-primary ml-4 quiz-button-down-mobile",
    class: {
      'is-disabled': _vm.selected.length === 0,
      'last-button': _vm.last
    },
    attrs: {
      "disabled": _vm.selected.length === 0
    },
    on: {
      "click": function ($event) {
        return _vm.$emit('nextStep');
      }
    }
  }, [_vm._v("Avanti")])], 2), _c('div', {
    staticClass: "column is-3"
  }, [_c('div', {
    staticClass: "box quiz-configuration-box"
  }, [_c('div', {
    staticClass: "mx-auto mobile-toggler hidden-desktop"
  }), _c('p', {
    staticClass: "quiz-configuration-title"
  }, [_vm._v("La tua configurazione")]), _vm.currentSelected.length === 0 ? _c('p', {
    staticClass: "quiz-configuration-text has-text-danger mt-4"
  }, [_vm._v(" Nessuna opzione selezionata ")]) : _vm._e(), _vm._l(_vm.currentSelected, function (option) {
    return _c('div', {
      key: option.label
    }, [_c('hr', {
      staticClass: "hidden-mobile",
      staticStyle: {
        "background": "#282828"
      }
    }), _c('div', {
      staticClass: "columns is-mobile is-vcentered px-3-mobile my-2-mobile"
    }, [_c('div', {
      staticClass: "column quiz-configuration-icon-box",
      style: `background-image: url('https://fiberdroid.it/pages/quiz/${option.icon}-bg.svg')`
    }, [_c('img', {
      attrs: {
        "src": `https://fiberdroid.it/pages/quiz/${option.addIcon ? option.addIcon : option.icon}.${option.ext ? option.ext : `svg`}`
      }
    })]), _c('div', {
      staticClass: "column is-8"
    }, [_c('p', {
      staticClass: "quiz-configuration-text"
    }, [_vm._v(_vm._s(option.addLabel ?? option.label))])]), _c('div', {
      staticClass: "column is-2"
    })])]);
  })], 2)]), _c('div', {
    staticClass: "column is-12 has-text-right hidden-mobile",
    staticStyle: {
      "display": "flex"
    }
  }, [_c('p', {
    staticClass: "quiz-step-text",
    staticStyle: {
      "margin-top": "auto",
      "width": "100%",
      "text-align": "right"
    }
  }, [_vm._v(_vm._s(_vm.step) + "/6")])])])], 1);
};
var StepBoxvue_type_template_id_8a0ee948_scoped_true_staticRenderFns = [];

;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/helpers/ShadowBox.vue?vue&type=template&id=61fd56e4&scoped=true&
var ShadowBoxvue_type_template_id_61fd56e4_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "box quiz-gradient",
    style: `background: ${_vm.bg};`
  }, [_c('div', {
    staticClass: "box quiz-box"
  }, [_c('div', {
    staticClass: "columns is-multiline px-6-desktop px-3-mobile pt-6 pb-0",
    staticStyle: {
      "height": "100%"
    }
  }, [_vm._t("default")], 2)])]);
};
var ShadowBoxvue_type_template_id_61fd56e4_scoped_true_staticRenderFns = [];

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/helpers/ShadowBox.vue?vue&type=script&lang=js&
/* harmony default export */ var ShadowBoxvue_type_script_lang_js_ = ({
  name: "ShadowBox",
  props: {
    bg: {
      type: String,
      required: true
    }
  }
});
;// CONCATENATED MODULE: ./src/helpers/ShadowBox.vue?vue&type=script&lang=js&
 /* harmony default export */ var helpers_ShadowBoxvue_type_script_lang_js_ = (ShadowBoxvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/helpers/ShadowBox.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  helpers_ShadowBoxvue_type_script_lang_js_,
  ShadowBoxvue_type_template_id_61fd56e4_scoped_true_render,
  ShadowBoxvue_type_template_id_61fd56e4_scoped_true_staticRenderFns,
  false,
  injectStyles,
  "61fd56e4",
  null
  ,true
)

/* harmony default export */ var ShadowBox = (component.exports);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/helpers/quiz/OptionBox.vue?vue&type=template&id=17e629ac&scoped=true&
var OptionBoxvue_type_template_id_17e629ac_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "box",
    class: [`quiz-option-box-shadow${_vm.size}`, `quiz-gradient-${_vm.option.gradient}`, {
      [`quiz-gradient-${_vm.option.gradient}-active`]: _vm.active,
      'ml-auto': _vm.mlAuto,
      'mx-auto': (_vm.size === '-small' || _vm.size === '-medium') && _vm.index % 3 === 1,
      'mr-auto': _vm.mrAuto
    }]
  }, [_c('div', {
    staticClass: "box",
    class: `quiz-option-box${_vm.size}`
  }, [_c('div', {
    class: `quiz-option-wrapper${_vm.size}`
  }, [_c('div', [_c('img', {
    staticClass: "quiz-option-image",
    attrs: {
      "src": `https://fiberdroid.it/pages/quiz/${_vm.option.icon}.svg`
    }
  }), _c('p', {
    staticClass: "quiz-text-option",
    domProps: {
      "innerHTML": _vm._s(_vm.option.label)
    }
  })])])])]);
};
var OptionBoxvue_type_template_id_17e629ac_scoped_true_staticRenderFns = [];

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/helpers/quiz/OptionBox.vue?vue&type=script&lang=js&
/* harmony default export */ var OptionBoxvue_type_script_lang_js_ = ({
  name: "OptionBox",
  computed: {
    mlAuto() {
      return (this.size === '-small' || this.size === '-medium') && this.index % 3 === 0 || !this.size && this.index % 2 === 0;
    },
    mrAuto() {
      return (this.size === '-small' || this.size === '-medium') && this.index % 3 === 2 || !this.size && this.index % 2 === 1;
    }
  },
  props: {
    option: {
      type: Object,
      required: true
    },
    index: {
      type: Number,
      required: true
    },
    size: {
      type: String,
      required: true
    },
    active: {
      type: Boolean,
      required: true
    }
  }
});
;// CONCATENATED MODULE: ./src/helpers/quiz/OptionBox.vue?vue&type=script&lang=js&
 /* harmony default export */ var quiz_OptionBoxvue_type_script_lang_js_ = (OptionBoxvue_type_script_lang_js_); 
;// CONCATENATED MODULE: ./src/helpers/quiz/OptionBox.vue



function OptionBox_injectStyles (context) {
  
  
}

/* normalize component */

var OptionBox_component = (0,componentNormalizer/* default */.Z)(
  quiz_OptionBoxvue_type_script_lang_js_,
  OptionBoxvue_type_template_id_17e629ac_scoped_true_render,
  OptionBoxvue_type_template_id_17e629ac_scoped_true_staticRenderFns,
  false,
  OptionBox_injectStyles,
  "17e629ac",
  null
  ,true
)

/* harmony default export */ var OptionBox = (OptionBox_component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/helpers/quiz/StepBox.vue?vue&type=script&lang=js&


/* harmony default export */ var StepBoxvue_type_script_lang_js_ = ({
  name: "StepBox",
  components: {
    OptionBox: OptionBox,
    ShadowBox: ShadowBox
  },
  props: {
    step: {
      type: [Number, String],
      required: true
    },
    options: {
      type: Object,
      required: true
    },
    selected: {
      type: Array,
      required: true
    },
    bg: {
      type: String,
      required: true
    },
    last: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    size() {
      switch (this.options[this.step].length) {
        case 6:
          return "-small";
        case 3:
          return "-medium";
        case 2:
        default:
          return "";
      }
    },
    columnSize() {
      switch (this.options[this.step].length) {
        case 6:
        case 3:
          return "4";
        case 2:
        default:
          return "6";
      }
    },
    currentSelected() {
      return this.selected.filter(el => {
        return parseInt(el.step) < parseInt(this.step) && el.visible !== false;
      });
    }
  },
  methods: {
    isSelected(option) {
      return !!this.selected.find(el => el.label === option.label);
    }
  }
});
;// CONCATENATED MODULE: ./src/helpers/quiz/StepBox.vue?vue&type=script&lang=js&
 /* harmony default export */ var quiz_StepBoxvue_type_script_lang_js_ = (StepBoxvue_type_script_lang_js_); 
;// CONCATENATED MODULE: ./src/helpers/quiz/StepBox.vue



function StepBox_injectStyles (context) {
  
  
}

/* normalize component */

var StepBox_component = (0,componentNormalizer/* default */.Z)(
  quiz_StepBoxvue_type_script_lang_js_,
  StepBoxvue_type_template_id_8a0ee948_scoped_true_render,
  StepBoxvue_type_template_id_8a0ee948_scoped_true_staticRenderFns,
  false,
  StepBox_injectStyles,
  "8a0ee948",
  null
  ,true
)

/* harmony default export */ var StepBox = (StepBox_component.exports);
// EXTERNAL MODULE: ../../node_modules/axios/index.js
var axios = __webpack_require__(9644);
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/helpers/quiz/QuizResultBox.vue?vue&type=template&id=551e4dbe&scoped=true&
var QuizResultBoxvue_type_template_id_551e4dbe_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "box service-box-gradient",
    staticStyle: {
      "backdrop-filter": "blur(50px)",
      "display": "flex",
      "height": "100%",
      "background-repeat": "no-repeat",
      "background-size": "cover",
      "background-image": "url('https://fiberdroid.it/pages/gradients/quiz.png')"
    }
  }, [_c('div', {
    staticClass: "box service-box-box p-6",
    staticStyle: {
      "margin-right": "-25px",
      "margin-bottom": "-25px",
      "display": "flex",
      "flex-direction": "column"
    }
  }, [_c('p', {
    staticClass: "service-box-title-text is-uppercase mt-4"
  }, [_vm._v(_vm._s(_vm.title))]), _c('div', {
    staticClass: "mt-auto"
  }, [_c('div', {
    staticClass: "my-6 pt-5 pb-0",
    staticStyle: {
      "border-bottom": "solid 1px rgba(217, 217, 217, 0.3)"
    }
  }, _vm._l(_vm.items, function (item) {
    return _c('div', {
      key: item.name,
      staticStyle: {
        "display": "flex",
        "gap": "14px",
        "padding": "12px 0",
        "border-top": "solid 1px rgba(217, 217, 217, 0.3)"
      }
    }, [_c('img', {
      attrs: {
        "src": `https://fiberdroid.it/pages/services/items/${item.icon}`
      }
    }), _c('p', {
      staticClass: "quiz-result-box-item has-text-left"
    }, [_vm._v(_vm._s(item.name))])]);
  }), 0)]), _c('div', {
    staticClass: "mt-auto"
  }, [_c('div', {
    staticClass: "columns is-mobile is-vcentered",
    staticStyle: {
      "width": "100%",
      "margin": "0"
    }
  }, [_c('div', {
    staticClass: "column is-12 has-text-left pl-0"
  }, [_c('p', {
    staticClass: "quiz-result-box-price-text"
  }, [_c('span', [_vm._v(_vm._s(_vm._f("formatPrice")(_vm.price)) + " €")]), _c('span', {
    staticClass: "is-unselectable"
  }, [_vm._v("/ mese")])])])]), _c('div', {
    staticClass: "columns is-mobile is-vcentered",
    staticStyle: {
      "width": "100%",
      "margin": "0"
    }
  }, [_c('div', {
    staticClass: "column is-6 has-text-right pl-0"
  }, [_c('button', {
    staticClass: "button is-smaller is-fd-primary",
    attrs: {
      "data-add-to-cart": "",
      "data-title": _vm.title.replaceAll('+', '<br><hr class=\'separator\' style=\'background: transparent; border-bottom: 1px solid rgb(40, 40, 40); margin: 8px 0\'>'),
      "data-price": _vm._f("formatPrice")(_vm.price),
      "data-activation-price": 0
    }
  }, [_vm._m(0), _c('span', {
    staticStyle: {
      "width": "100%",
      "text-align": "center"
    }
  }, [_vm._v("Add To Cart")])])])])])])]);
};
var QuizResultBoxvue_type_template_id_551e4dbe_scoped_true_staticRenderFns = [function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c('span', {
    staticClass: "icon"
  }, [_c('img', {
    staticClass: "image",
    attrs: {
      "src": "https://fiberdroid.it/pages/home/icons/cart.svg",
      "alt": "Image 1"
    }
  })]);
}];

;// CONCATENATED MODULE: ./src/helpers/quiz/QuizResultBox.vue?vue&type=template&id=551e4dbe&scoped=true&

;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/helpers/quiz/QuizResultBox.vue?vue&type=script&lang=js&
/* harmony default export */ var QuizResultBoxvue_type_script_lang_js_ = ({
  name: "QuizResultBox",
  props: {
    title: {
      type: String,
      required: true
    },
    price: {
      type: [String, Number],
      required: true
    },
    items: {
      type: Array,
      required: true
    }
  },
  computed: {
    dataProducts() {
      return JSON.stringify([{}]);
    }
  },
  filters: {
    formatPrice(value) {
      let val = (value / 1).toFixed(2).replace('.', ',');
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
  }
});
;// CONCATENATED MODULE: ./src/helpers/quiz/QuizResultBox.vue?vue&type=script&lang=js&
 /* harmony default export */ var quiz_QuizResultBoxvue_type_script_lang_js_ = (QuizResultBoxvue_type_script_lang_js_); 
;// CONCATENATED MODULE: ./src/helpers/quiz/QuizResultBox.vue



function QuizResultBox_injectStyles (context) {
  
  
}

/* normalize component */

var QuizResultBox_component = (0,componentNormalizer/* default */.Z)(
  quiz_QuizResultBoxvue_type_script_lang_js_,
  QuizResultBoxvue_type_template_id_551e4dbe_scoped_true_render,
  QuizResultBoxvue_type_template_id_551e4dbe_scoped_true_staticRenderFns,
  false,
  QuizResultBox_injectStyles,
  "551e4dbe",
  null
  ,true
)

/* harmony default export */ var QuizResultBox = (QuizResultBox_component.exports);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/FiberdroidQuiz.vue?vue&type=script&lang=js&shadow

/* eslint-disable */




/* harmony default export */ var FiberdroidQuizvue_type_script_lang_js_shadow = ({
  components: {
    QuizResultBox: QuizResultBox,
    StepBox: StepBox
  },
  data() {
    return {
      step: 0,
      options: {
        1: [{
          label: "Internet",
          id: "internet",
          icon: "wifi",
          gradient: "internet"
        }, {
          label: "Telefonia",
          id: "phone",
          icon: "phone",
          gradient: "voip"
        }],
        2: [{
          label: "Email/<br>Messaggistica",
          id: "email",
          icon: "paper-plane",
          bg: "wifi",
          gradient: "internet",
          visible: false
        }, {
          label: "App <br> in Cloud",
          id: "cloud",
          icon: "cloud",
          bg: "wifi",
          gradient: "internet",
          visible: false
        }, {
          label: "Video <br> Conferenze",
          id: "video",
          icon: "camera",
          bg: "wifi",
          gradient: "internet",
          visible: false
        }, {
          label: "File <br>Sharing",
          id: "file",
          icon: "file",
          bg: "wifi",
          gradient: "internet",
          visible: false
        }, {
          label: "WiFi per <br>ospiti e dipendenti",
          id: "guest-wifi",
          icon: "wifi",
          gradient: "internet",
          visible: false
        }, {
          label: "Telefonia <br>VoIp",
          id: "voip-phone",
          icon: "phone",
          gradient: "voip",
          addLabel: "Centrale Voip Cloud",
          addIcon: "pbx"
        }],
        3: [{
          label: "1-5",
          power: 0,
          id: "how-many",
          icon: "user",
          gradient: "internet",
          visible: false
        }, {
          label: "6-20",
          power: 1,
          id: "how-many",
          icon: "double-user",
          gradient: "internet",
          visible: false
        }, {
          label: "Più 20",
          power: 2,
          id: "how-many",
          icon: "users",
          gradient: "internet",
          visible: false
        }],
        4: [{
          label: "Si, grazie",
          id: "backup",
          value: true,
          icon: "backup",
          gradient: "backup",
          addLabel: "Linea di Backup"
        }, {
          label: "No, siamo aposto",
          id: "backup",
          value: false,
          icon: "danger",
          gradient: "backup",
          visible: false
        }],
        5: [{
          label: "Si, sicuro",
          id: "router",
          value: true,
          icon: "router",
          gradient: "internet",
          addLabel: "Router"
        }, {
          label: "No, grazie",
          id: "router",
          value: false,
          icon: "times",
          gradient: "internet",
          visible: false
        }],
        6: [{
          label: "Yes, grazie",
          id: "desk-phone",
          value: true,
          icon: "desk-phone",
          gradient: "internet",
          visible: false
        }, {
          label: "No, grazie",
          id: "desk-phone",
          value: false,
          icon: "times",
          gradient: "internet",
          visible: false
        }]
      },
      selected: [],
      result: null
    };
  },
  props: {
    icon: {
      type: String,
      default: "quiz/background/fiber.webp"
    },
    gradient: {
      type: String,
      default: "https://fiberdroid.it/images/quiz/gradient/blue.webp"
    },
    url: {
      type: String,
      default: "http://localhost:8001"
    }
  },
  methods: {
    async quizApi(data) {
      const response = await axios_default().post(`${this.url}/api/quiz`, {
        data
      });
      return response.data;
    },
    getServiceName(service, phone, centrale, backup) {
      const service_name = service ? service.name : '';
      const centrale_name = centrale ? centrale.name : '';
      const centrale_prefix = centrale && service ? ' + ' : '';
      const phone_name = phone ? phone.name : '';
      const phone_prefix = phone && centrale || phone && service ? ' + ' : '';
      const backup_name = backup ? backup.name : '';
      const backup_prefix = service && backup || centrale && backup || phone && backup ? ' + ' : '';
      return service_name + centrale_prefix + centrale_name + phone_prefix + phone_name + backup_prefix + backup_name;
    },
    async lastStep() {
      this.nextStep();
      const response = await this.quizApi(this.selected);
      this.result = response.map(data => {
        data = data[0];
        const {
          service,
          router,
          phone,
          centrale,
          backup,
          deskPhone,
          totalAmount
        } = {
          ...data
        };
        return {
          name: this.getServiceName(service, phone, centrale, backup),
          items: [...(service ? service.items : []), ...(router ? router.items : []), ...(centrale ? centrale.items : []), ...(phone ? phone.items : []), ...(deskPhone ? deskPhone.items : []), ...(backup ? backup.items : [])],
          totalAmount
        };
      });
      console.log('result', this.result);
    },
    nextStep() {
      this.step = this.step + 1;
    },
    previousStep() {
      this.step = this.step - 1;
    },
    endForAdmin() {
      this.step = 6;
      this.selected = [...this.options[1], ...this.options[2]];
      this.lastStep();
    },
    toggleSelect({
      step,
      option
    }) {
      if (!this.isSelected(option)) {
        option.step = step;
        this.selected.push(option);
      } else {
        const index = this.selected.findIndex(el => el.label === option.label);
        if (index !== -1) {
          this.$delete(this.selected, index);
        }
      }
    },
    toggleSelectSingle({
      step,
      option
    }) {
      const options = this.options[step];
      for (const other of options) {
        const index = this.selected.findIndex(el => el.label === other.label);
        if (index !== -1) {
          this.$delete(this.selected, index);
        }
      }
      option.step = step;
      this.selected.push(option);
    },
    isSelected(option) {
      return !!this.selected.find(el => el.label === option.label);
    },
    restart() {
      this.step = 0;
      this.selected = [];
    },
    checkAll() {
      alert("Coming soon....");
    }
  },
  computed: {
    bg: function () {
      if (this.gradient.startsWith("http")) {
        return `url("${this.gradient}")`;
      } else {
        return this.gradient;
      }
    }
  }
});
;// CONCATENATED MODULE: ./src/components/FiberdroidQuiz.vue?vue&type=script&lang=js&shadow
 /* harmony default export */ var components_FiberdroidQuizvue_type_script_lang_js_shadow = (FiberdroidQuizvue_type_script_lang_js_shadow); 
;// CONCATENATED MODULE: ./src/components/FiberdroidQuiz.vue?shadow



function FiberdroidQuizshadow_injectStyles (context) {
  
  var style0 = __webpack_require__(7090)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var FiberdroidQuizshadow_component = (0,componentNormalizer/* default */.Z)(
  components_FiberdroidQuizvue_type_script_lang_js_shadow,
  render,
  staticRenderFns,
  false,
  FiberdroidQuizshadow_injectStyles,
  null,
  null
  ,true
)

/* harmony default export */ var FiberdroidQuizshadow = (FiberdroidQuizshadow_component.exports);

/***/ }),

/***/ 7090:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidQuiz_vue_vue_type_style_index_0_id_7849d70c_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5374);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidQuiz_vue_vue_type_style_index_0_id_7849d70c_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidQuiz_vue_vue_type_style_index_0_id_7849d70c_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidQuiz_vue_vue_type_style_index_0_id_7849d70c_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidQuiz_vue_vue_type_style_index_0_id_7849d70c_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
___CSS_LOADER_EXPORT___.push([module.id, "@import url(https://fiberdroid.it/css/app.css);"]);
// Module
___CSS_LOADER_EXPORT___.push([module.id, "", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 5374:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(307);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to Shadow Root
var add = (__webpack_require__(2339)/* ["default"] */ .Z)
module.exports.__inject__ = function (shadowRoot) {
  add("143d7a0e", content, shadowRoot)
};

/***/ })

}]);
//# sourceMappingURL=web-component.878.js.map