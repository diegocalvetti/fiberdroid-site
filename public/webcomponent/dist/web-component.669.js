(self["webpackChunkvue_lib_web_component"] = self["webpackChunkvue_lib_web_component"] || []).push([[669],{

/***/ 7669:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ FiberdroidModelshadow; }
});

;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/loaders/templateLoader.js??ruleSet[1].rules[3]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/FiberdroidModel.vue?vue&type=template&id=14d0fc62&shadow
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticStyle: {
      "width": "100%",
      "display": "flex"
    }
  }, [_c('canvas', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: !_vm.loading,
      expression: "!loading"
    }],
    ref: "render",
    staticStyle: {
      "margin": "auto",
      "width": "1100px",
      "height": "720px",
      "background": "transparent"
    }
  })]);
};
var staticRenderFns = [];

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.push.js
var es_array_push = __webpack_require__(7658);
// EXTERNAL MODULE: ../../node_modules/babylonjs/babylon.js
var babylon = __webpack_require__(8574);
// EXTERNAL MODULE: ../../node_modules/babylonjs-gui/babylon.gui.js
var babylon_gui = __webpack_require__(1232);
// EXTERNAL MODULE: ../../node_modules/babylonjs-loaders/babylonjs.loaders.js
var babylonjs_loaders = __webpack_require__(8477);
;// CONCATENATED MODULE: ./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js??clonedRuleSet-40.use[1]!./node_modules/@vue/vue-loader-v15/lib/index.js??vue-loader-options!./src/components/FiberdroidModel.vue?vue&type=script&lang=js&shadow




/* harmony default export */ var FiberdroidModelvue_type_script_lang_js_shadow = ({
  name: "FiberdroidModel",
  data() {
    return {
      canvas: null,
      engine: null,
      scene: null,
      loading: true
    };
  },
  methods: {
    getMeshByProperty(prop) {
      // Property of mesh are custom and
      // setted on blender using the mesh name
      // For example: MeshName|Property1|Property2
      // This find all meshes with a given Property
      const result = [];
      for (const mesh of this.scene.meshes) {
        if (mesh.name.includes("|")) {
          const properties = mesh.name.split("|").slice(1);
          for (const property of properties) {
            if (property.startsWith(prop)) {
              result.push(mesh);
            }
          }
        }
      }
      return result;
    },
    getMeshByPrefix(prefix) {
      const result = [];
      for (const mesh of this.scene.meshes) {
        if (mesh.name.startsWith(prefix)) {
          result.push(mesh);
        }
      }
      return result;
    },
    async createScene() {
      this.scene = new babylon.Scene(this.engine);
      const scene = this.scene;
      const camera = new babylon.ArcRotateCamera("Camera", 0, 0, 17, new babylon.Vector3(0, 0, 0), scene);

      // This positions the camera
      camera.setPosition(new babylon.Vector3(-18, 6, 25));

      // Attach the camera to the canvas
      camera.attachControl(this.canvas, false);

      // 1.18 <= alpha <= 1.44
      // 1.43 <= beta <= 3.5
      //camera.lowerRadiusLimit = camera.radius;
      //camera.upperRadiusLimit = camera.radius;
      camera.lowerBetaLimit = 1.18;
      camera.upperBetaLimit = 1.44;
      camera.lowerAlphaLimit = 1.43;
      camera.upperAlphaLimit = 3.5;
      const glowLayer = new babylon.GlowLayer("glow", scene);
      glowLayer.intensity = 0.4;
      const hdrTexture = new babylon.HDRCubeTexture("https://fiberdroid.it/hdr/ocean.hdr", scene, 512);
      const hemiLight = new babylon.HemisphericLight("HemiLight", new babylon.Vector3(0, 1, 0), scene);
      hemiLight.intensity = 1.2;
      await babylon.SceneLoader.ImportMeshAsync("", "https://fiberdroid.it/", "office2.glb", scene, function (progress) {
        console.log("Progress...", progress);
      }, ".glb");
      scene.getMaterialByName("Wood").alphaMode = 0;
      const glasses = this.getMeshByPrefix("Glass");
      const neon = this.getMeshByPrefix("NeonText");
      const cabinets = this.getMeshByPrefix("Cabinet");
      const {
        glassMaterial,
        metallicMaterial
      } = this.materials(hdrTexture);
      for (const glass of glasses) {
        glass.material = glassMaterial;
      }
      for (const cabinet of cabinets) {
        cabinet.material = metallicMaterial;
      }
      for (const neonMesh of neon) {
        glowLayer.addIncludedOnlyMesh(neonMesh);
      }
      for (const mesh of scene.meshes) {
        mesh.freezeWorldMatrix();
        mesh.doNotSyncBoundingInfo = true;
      }
      const pickables = ['Phone', 'WifiPoint', 'WifiPoint2', 'Router', 'Cabinet', 'PingPong'].map(el => this.getMeshByPrefix(el));
      const self = this;
      scene.onPointerDown = function (evt, pickResult) {
        // We try to pick an object
        if (pickResult.hit) {
          //console.log(pickResult.pickedMesh)

          const closest = self.findClosest(pickResult.pickedPoint, pickables);
          camera.setTarget(closest);
        }
      };
      this.scene.performancePriority = babylon.ScenePerformancePriority.Aggressive;
      this.loading = false;
      this.scene = this.debug(scene);
      return this.scene;
    },
    debug(scene) {
      // Instrumentation
      var instrumentation = new babylon.EngineInstrumentation(this.engine);
      instrumentation.captureGPUFrameTime = true;
      instrumentation.captureShaderCompilationTime = true;

      // GUI
      var advancedTexture = babylon_gui.AdvancedDynamicTexture.CreateFullscreenUI("UI");
      var stackPanel = new babylon_gui.StackPanel();
      stackPanel.verticalAlignment = babylon_gui.Control.VERTICAL_ALIGNMENT_TOP;
      stackPanel.isVertical = true;
      advancedTexture.addControl(stackPanel);
      var text1 = new babylon_gui.TextBlock();
      text1.text = "";
      text1.color = "white";
      text1.fontSize = 16;
      text1.height = "30px";
      //stackPanel.addControl(text1);

      var text2 = new babylon_gui.TextBlock();
      text2.text = "";
      text2.color = "white";
      text2.fontSize = 16;
      text2.height = "30px";
      //stackPanel.addControl(text2);

      var text3 = new babylon_gui.TextBlock();
      text3.text = "";
      text3.color = "white";
      text3.fontSize = 16;
      text3.height = "30px";
      //stackPanel.addControl(text3);

      var text4 = new babylon_gui.TextBlock();
      text4.text = "";
      text4.color = "white";
      text4.fontSize = 16;
      text4.height = "30px";
      //stackPanel.addControl(text4);

      var text5 = new babylon_gui.TextBlock();
      text5.text = "";
      text5.color = "white";
      text5.fontSize = 16;
      text5.height = "30px";
      //stackPanel.addControl(text5);

      const engine = this.engine;
      scene.registerBeforeRender(function () {
        text1.text = "current frame time (GPU): " + (instrumentation.gpuFrameTimeCounter.current * 0.000001).toFixed(2) + "ms" + `(${engine.getFps().toFixed()} FPS)`;
        text2.text = "average frame time (GPU): " + (instrumentation.gpuFrameTimeCounter.average * 0.000001).toFixed(2) + "ms";
        text3.text = "total shader compilation time: " + instrumentation.shaderCompilationTimeCounter.total.toFixed(2) + "ms";
        text4.text = "average shader compilation time: " + instrumentation.shaderCompilationTimeCounter.average.toFixed(2) + "ms";
        text5.text = "compiler shaders count: " + instrumentation.shaderCompilationTimeCounter.count;
      });
      return scene;
    },
    materials(hdrTexture) {
      const glassMaterial = new babylon.PBRMaterial("glass", this.scene);
      glassMaterial.reflectionTexture = hdrTexture;
      glassMaterial.indexOfRefraction = 1.450;
      glassMaterial.alpha = 0.5;
      glassMaterial.directIntensity = 0.3;
      glassMaterial.environmentIntensity = 0.3;
      glassMaterial.cameraExposure = 0.66;
      glassMaterial.cameraContrast = 1.66;
      glassMaterial.microSurface = 1;
      glassMaterial.reflectivityColor = new babylon.Color3(0.2, 0.2, 0.2);
      glassMaterial.albedoColor = new babylon.Color3(0.95, 0.95, 0.95);
      var metallicMaterial = new babylon.PBRMaterial("mettalic", this.scene);
      metallicMaterial.albedoColor = new babylon.Color3(1.0, 0.766, 0.336);
      metallicMaterial.metallic = 1.0;
      metallicMaterial.roughness = 0.5;
      metallicMaterial.reflectionTexture = hdrTexture;
      return {
        glassMaterial,
        metallicMaterial
      };
    },
    findClosest(from, targets) {
      let bestDist = Number.MAX_VALUE;
      let closest = null;
      console.log("FROM ", from.x, from.y, from.z);
      for (const target of targets) {
        if (!target || !target[0]) continue;
        const mesh = target[0];
        const point = mesh.getAbsolutePosition();
        const dist = Math.sqrt((point.x - from.x) ** 2 + (point.y - from.y) ** 2 + (point.z - from.z) ** 2);
        console.log({
          mesh: mesh.name,
          point,
          dist
        });
        if (dist < bestDist) {
          bestDist = dist;
          closest = mesh;
        }
      }
      return closest;
    }
  },
  async mounted() {
    this.canvas = this.$refs.render;
    this.engine = new babylon.Engine(this.canvas, true, null, true);
    const scene = await this.createScene();
    scene.clearColor = new babylon.Color4(0, 0, 0, 0);
    this.engine.resize();
    this.engine.runRenderLoop(function () {
      scene.render();
    });
    window.addEventListener('resize', function () {
      this.engine.resize();
    });
  }
});
;// CONCATENATED MODULE: ./src/components/FiberdroidModel.vue?vue&type=script&lang=js&shadow
 /* harmony default export */ var components_FiberdroidModelvue_type_script_lang_js_shadow = (FiberdroidModelvue_type_script_lang_js_shadow); 
// EXTERNAL MODULE: ./node_modules/@vue/vue-loader-v15/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1001);
;// CONCATENATED MODULE: ./src/components/FiberdroidModel.vue?shadow



function injectStyles (context) {
  
  var style0 = __webpack_require__(765)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = (0,componentNormalizer/* default */.Z)(
  components_FiberdroidModelvue_type_script_lang_js_shadow,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  null
  ,true
)

/* harmony default export */ var FiberdroidModelshadow = (component.exports);

/***/ }),

/***/ 765:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidModel_vue_vue_type_style_index_0_id_14d0fc62_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6500);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidModel_vue_vue_type_style_index_0_id_14d0fc62_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidModel_vue_vue_type_style_index_0_id_14d0fc62_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidModel_vue_vue_type_style_index_0_id_14d0fc62_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_12_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_vue_loader_v15_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_vue_loader_v15_lib_index_js_vue_loader_options_FiberdroidModel_vue_vue_type_style_index_0_id_14d0fc62_prod_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ 3377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8081);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3645);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
___CSS_LOADER_EXPORT___.push([module.id, "@import url(http://127.0.0.1:8000/css/app.css?v=64d5f3d63ec6f);"]);
// Module
___CSS_LOADER_EXPORT___.push([module.id, "canvas{outline:none;-webkit-tap-highlight-color:rgba(255,255,255,0)}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 6500:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(3377);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to Shadow Root
var add = (__webpack_require__(2339)/* ["default"] */ .Z)
module.exports.__inject__ = function (shadowRoot) {
  add("33c05b0e", content, shadowRoot)
};

/***/ })

}]);
//# sourceMappingURL=web-component.669.js.map