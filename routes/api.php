<?php

use App\Http\Controllers\FiberdroidController;
use App\Http\Controllers\v2\FiberdroidApiControllerV2;
use App\Models\CustomArticle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('article/{url}', [FiberdroidApiControllerV2::class, "articleApi"]);
Route::get('sitemap', [FiberdroidController::class, "sitemapApi"]);

Route::post('newsletter', [FiberdroidApiControllerV2::class, "newsletter"]);
Route::post('order', [FiberdroidApiControllerV2::class, "order"]);

Route::group(['prefix' => 'quiz'], function () {
    Route::post('/', [FiberdroidApiControllerV2::class, "quiz"] );
});

Route::group(['prefix' => 'coverage'], function () {
    Route::get('cities', [FiberdroidApiControllerV2::class, "cities"]);
    Route::get('streets/{city}/{street}', [FiberdroidApiControllerV2::class, "streets"]);
    Route::get('civics/{city}/{street}/{civic}', [FiberdroidApiControllerV2::class, "civics"]);
    Route::get('stairs/{city}/{street}/{civic}', [FiberdroidApiControllerV2::class, "stairs"]);
    Route::get('coverage/{city}/{street}/{civic}/{stair?}', [FiberdroidApiControllerV2::class, "coverage"]);
    Route::get('boundaries/{lat}/{lng}/{radius}', [FiberdroidApiControllerV2::class, "boundaries"]);
    Route::get('cabinets/{provider}/{north}/{south}/{east}/{west}', [FiberdroidApiControllerV2::class, "cabinets"]);
    Route::get('centrals/{provider}/{north}/{south}/{east}/{west}', [FiberdroidApiControllerV2::class, "centrals"]);
    Route::get('bts', [FiberdroidApiControllerV2::class, "bts"]);
    Route::get('elevation/{from_lat}/{from_lng}/{to_lat}/{to_lng}', [FiberdroidApiControllerV2::class, "elevation"]);
});
