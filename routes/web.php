<?php

use App\Http\Controllers\v2\FiberdroidControllerV2;
use App\Http\Middleware\AddHeaders;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FiberdroidController;

/*
|--------------------------------------------------------------------------
| Fiberdroid sito web
|--------------------------------------------------------------------------
*/
Route::middleware(AddHeaders::class)->group(function () {
    Route::get('/', [FiberdroidControllerV2::class, "showHome"])->name("home");

    Route::get('/policy/privacy', [FiberdroidControllerV2::class, "showPrivacyPolicy"])->name("policy.privacy");
    Route::get('/policy/cookie', [FiberdroidControllerV2::class, "showCookiePolicy"])->name("policy.cookies");
    Route::get('/carta-dei-servizi', [FiberdroidControllerV2::class, "showCartaServizi"])->name("carta");

    Route::get('/chi-siamo', [FiberdroidControllerV2::class, "showChiSiamo"])->name('chi-siamo');
    Route::get('/verifica-copertura', [FiberdroidControllerV2::class, "showVerificaCopertura"])->name('verifica-copertura');

    Route::get('/assistenza', [FiberdroidControllerV2::class, "showAssistenza"])->name('assistenza');

    // Old website redirect
    Route::get('/soluzioni/ristorazione-e-retail', [FiberdroidControllerV2::class, "redirectToRestaurant"]);


    Route::get('/servizi/{name}', [FiberdroidControllerV2::class, "showServizio"])->name("servizio");
    Route::get('/soluzioni/jetfiber', [FiberdroidControllerV2::class, "showFiberJet"])->name("fiberjet");
    Route::get('/soluzioni/fiberjet', [FiberdroidControllerV2::class, "showFiberJet"])->name("fiberjet");
    Route::get('/soluzioni/{name}', [FiberdroidControllerV2::class, "showSoluzione"])->name("soluzione");
    Route::get('/prodotti/{name}', [FiberdroidControllerV2::class, "showProdotto"])->name("prodotto");

    Route::get('/fibra-dedicata-e-centralino-voip', [FiberdroidControllerV2::class, "showComingSoon"])->name("speed");
    Route::get('/thank-you', [FiberdroidControllerV2::class, "showComingSoon"])->name("thanks");
    Route::get('/listini', [FiberdroidControllerV2::class, "showListini"])->name("listini");

    Route::get('/blog', [FiberdroidControllerV2::class, "showBlog"])->name("blog");
    Route::get('/guide', [FiberdroidControllerV2::class, "redirectToBlog"]);
    Route::get('/guide/{article}', [FiberdroidControllerV2::class, "redirectToGuide"])->name("guide");
    Route::get('/{article}', [FiberdroidControllerV2::class, "showArticle"])->name("article");
});


