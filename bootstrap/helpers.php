<?php

if( !function_exists('image') ){
    function image($alias) {
        return \App\Models\Image::url($alias);
    }
}

if( !function_exists('image_attr') ){
    function image_attr($alias) {
        return \App\Models\Image::attr($alias);
    }
}

if( !function_exists('props') ){
    function props($props)
    {
        foreach ($props as &$prop) {
            if (is_array($prop)) {
                $var = &$prop[0];
                $default = $prop[1] ?? false;
            } else {
                $var = &$prop;
                $default = false;
            }

            $var = $var ?? $default;
        }
    }
}

if( !function_exists('param') ){
    function param(&$var, $default = false)
    {
        $var = $var ?? $default;
    }
}

if( !function_exists('price') ){
    function price($var): string
    {
        return number_format($var, 2, ',', ' ');
    }
}

if( !function_exists('ac_url') ){
    function ac_url($url): string
    {
        return config('app.ac_url') . $url;
    }
}

if( !function_exists('gradient') ){
    function gradient($gradient): string
    {
        return [
            'ffth' => 'linear-gradient(135deg, #A1FF8B 0%, #3F93FF 100%)',
            'fftc' => 'linear-gradient(135deg, #FBDA61 0%, #F76B1C 100%)',
            'fftc&ffth' => 'linear-gradient(135deg, #FBD75F 9.39%, #9CF992 35.5%)'
        ][$gradient];
    }
}
