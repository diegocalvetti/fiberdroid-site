const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */


mix
    .js('resources/js/app.js', 'public/js')
    .sass('resources/css/app.scss', 'public/css', [])
    .sass('resources/css/lazy.scss', 'public/css/lazy.css', [])
    .minify('public/js/app.js')
    .minify('public/css/app.css')
    .minify('public/css/lazy.css')

