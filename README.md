## Installation

Clone the project in a new directory, for example `/fiberdroid`. The project is developed and tested under **php 7.3** / **7.4**

-   `clone git@bitbucket.org:diegocalvetti/fiberdroid-site.git fiberdroid`
-   enter into new directory: `cd fiberdroid`
-   install **composer** dependencies with: `composer install`

## Setup Pre-Deploy

-   launch your ssh command to connect on the server. You can't use root user but the default **apache/nginx** user.
-   If you can connect exit from server (`CTRL + d`) and launch the same command with `ssh-copy-id` (replace `ssh`) to copy your `id_rsa.pub` key on the server. Use your password again and now you can connect by ssh without a password.
-   Generate a id_rsa key if not exists on the server with `ssh-keygen`, copy the new key from `~/.ssh/id_rsa.pub` and insert in your git repository. This operation permit **deploy** without password directly from git to the server.

## Deploy

This project use **PM2** to deploy server source code on **stage** and **prod** host.
To install **pm2** locally launch: `npm i -g pm2`.

-   launch `pm2 deploy ecosystem stage setup` for first time (replace `stage` with `prod` if you want to deploy production host). This command create the ambient in path directory and create a subdirectory names `source` linked by `current` symbolic link (the domain point directory).
-   For next deploys use simply `pm2 deploy ecosystem.config.js stage` (replace `stage` with `prod` if you want to deploy on production host.

---

**NB:** if you can't execute deploy setup, try cloning your project repository directly into server by ssh connection and retry. This operation add the `github/bitbucket rsa key` on server.
