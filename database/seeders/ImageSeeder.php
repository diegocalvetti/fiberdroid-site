<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Service;
use App\Models\ServiceHasItem;
use App\Models\ServiceType;
use App\Models\Solution;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ImageSeeder extends Seeder
{
    public $images = [];

    public function __construct()
    {
        $this->images = [
            /**
             * CREW
             */
            [
                'alias' => "andrè",
                'url' => "crew/andre.webp",
                'alt' => "Andrè Claude Benin CEO Fiberdroid"
            ],
            [
                'alias' => "diego",
                'url' => "crew/diego.webp",
                'alt' => "Diego Calvetti programmatore di Fiberdroid"
            ],
            [
                'alias' => "gabriele",
                'url' => "crew/gabriele.webp",
                'alt' => "Gabriele Pallotta - assistenza tecnica di Fiberdroid"
            ],
            [
                'alias' => "massimo",
                'url' => "crew/massimo.webp",
                'alt' => "Massimo Guarnera - attivazione e assistenza tecnica di Fiberdroid"
            ],
            [
                'alias' => "katia",
                'url' => "crew/katia.webp",
                'alt' => "Katia - amministrazione Fiberdroid"
            ],
            [
                'alias' => "jenny",
                'url' => "crew/jenny.webp",
                'alt' => "Jenny - impiegata amministrativa"
            ],
            [
                'alias' => "kate",
                'url' => "crew/kate.webp",
                'alt' => "Kate - grafica e concept"
            ],
            [
                'alias' => "person",
                'url' => "crew/person.webp",
                'alt' => "Generic person"
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->images as $image) {
            $new = new Image();
            $new->alias = $image['alias'];
            $new->url = $image['url'];
            $new->alt = $image['alt'] ?? null;
            $new->title = $image['title'] ?? null;
            $new->save();
        }

    }

}
