<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;


class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::query()->truncate();

        $cities = json_decode(file_get_contents(__DIR__ . '/data/cities.json'), true);

        foreach ($cities as $city) {
            City::query()->create($city);
        }
    }

}
