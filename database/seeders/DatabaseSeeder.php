<?php

namespace Database\Seeders;

use App\Models\Crew;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ImageSeeder::class);
        $this->call(CrewSeeder::class);
        $this->call(FiberdroidServiceSeeder::class);
        //$this->call(CitySeeder::class);
    }
}
