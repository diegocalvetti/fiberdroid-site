<?php

namespace Database\Seeders;

use App\Models\FiberdroidService;
use App\Models\Image;
use App\Models\Service;
use App\Models\ServiceHasItem;
use App\Models\ServiceType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class FiberdroidServiceSeeder extends Seeder
{
    public $services = [];

    public function __construct()
    {
        $this->services = [
            // FIBRA
            [
                'name' => 'Fibra',
                'type' => 'Connettività',
                'description' => 'La Fibra Ottica è la connessione per eccellenza per la banda ultra larga. La rete punto-punto consente una velocità, continuità ed espandibilità della connessione senza precedenti.<br><br>Puoi estendere il servizio sulle diverse sedi aziendali per poter garantire la condivisione di risorse ed accedere ad internet, sia per la pubblicazione di contenuti che per l’operatività di base (posta, web, meeting, ecc).',
                'short_description' => 'FTTC and FTTH broadband',
                'icon' => 'globe.svg',
                'bg' => 'services/fiber.webp',
                'meta_title' => 'Fibra: FTTC o FTTH? scopri cosa fa per te! | Fiberdroid',
                'meta_description' => "Scopri insieme a Fiberdroid tutto sulla fibra ottica e come scegliere la connettività più adatta alle tue esigenze! Fiberdroid la connessione per il tuo Business!",
                'views' => ['services/fibra/termini', 'services/fibra/main'],
                'quiz_icon' => 'quiz/background/fiber.webp',
                'quiz_gradient' =>  url('pages/home/quiz-mask.png'),
            ],

            // FIBRA DEDICATA
            [
                'name' => 'Fibra dedicata',
                'type' => 'Connettività',
                'icon' => 'cable.svg',
                'bg' => 'services/fibra-dedicata.webp',
                'description' => 'Se nella tua zona la fibra ottica non è presente, te la portiamo noi!<br><br>La nostra soluzione di Fibra dedicata è disponibile su tutto il territorio italiano, anche nelle aree dove non è presente copertura della rete FTTC / FTTH e ADSL.<br><br>Ci occupiamo di tutte le operazioni necessarie a portare la connessione veloce nella tua azienda ed offrirti il massimo livello di stabilità e prestazioni per la tua connettività.',
                'short_description' => 'Dedicated Fiber',
                'meta_title' => 'Fibra dedicata ILC: Scopri tutto sulla fibra ottica più veloce',
                'meta_description' => "Soluzioni in Fibra Ottica Dedicata, scopri tutto sulla connettività più veloce, stabile e con banda garantita.",
                'views' => ['services/fibra_dedicata/main'],
                'quiz_icon' => 'quiz/background/fiber.webp',
                'quiz_gradient' => 'linear-gradient(135deg, #80F1A6 0%, #EFD000 100%);',
            ],

            // VOIP
            [
                'name' => 'Voip',
                'type' => 'VoIp',
                'description' => 'Proietta la Tua Azienda verso il futuro, abbandona le tradizionali tecnologie telefoniche! Acquista un nostro servizio VoIP. <br><br> Grazie al VoIP (Voice Over IP) puoi trasportare la voce attraverso il collegamento internet. Per la comunicazione vocale basta avere una trasmissione dati, ovvero una connessione a internet (con requisiti tecnici da verificare preventivamente) e non serve più pagare i classici canoni di telefonia alle compagnie telefoniche tradizionali.',
                'short_description' => 'Voice connectivity solutions ',
                'icon' => 'phone.svg',
                'bg' => 'services/voip.webp',
                'meta_title' => 'Abbandona le tradizionali tecnologie telefoniche! Acquista un nostro servizio VoIP, basta avere una connessione a internet. | Fiberdroid',
                'meta_description' => "Non serve più pagare i classici canoni di telefonia alle compagnie telefoniche, acquista un nostro servizio VoIP e potrai trasportare la voce attraverso il collegamento internet."
,                'views' => ['services/voip/termini', 'services/voip/main'],
                'quiz_gradient' => url('pages/quiz/bg-gradients/voip.png'),
            ],

            // FIBER AI
            /*
            [
                'name' => 'Fiber AI',
                'type' => 'Servizio',
                'description' => 'FiberAI non fornisce un singolo prodotto, ma un vero e proprio ecosistema di tecnologie di intelligenza artificiale adeguatamente combinate che producono come risultato quello di permettere alle macchine di agire con livelli di intelligenza riconducibili similarmente a quelli dell’essere umano.',
                'short_description' => 'AI',
                'icon' => 'ai.svg',
                'bg' => 'services/fiber-ai.png',
                'meta_title' => "Fiber AI",
                'meta_description' => "FiberAI non fornisce un singolo prodotto, ma un vero e proprio ecosistema di tecnologie di intelligenza artificiale adeguatamente combinate.",
                'views' => ['services/ai/termini', 'services/ai/main'],
                'quiz_gradient' => 'linear-gradient(135deg, #80F1A6 0%, #EFD000 100%);',
            ],
            */

            // WIRELESS
            [
                'name' => 'Wireless',
                'type' => 'Connettività',
                'icon' => 'wireless.svg',
                'bg' => 'services/wireless.webp',
                'description' => 'Il nostro servizio Wireless unisce la capillarità della rete wireless alle performance delle soluzioni fibra punto-punto.<br><br>Particolarmente indicata per le aziende che vogliono un’alta capacità di banda, e desiderano sfruttare internet per le chiamate, su tecnologia VoIP, o per aziende che desiderino una soluzione di backup alternativa alla connessione cablata.',
                'short_description' => 'Wireless internet',
                'meta_title' => 'Wireless: digital divide addio! prova la soluzione di Fiberdroid!',
                'meta_description' => 'Offerte Wireless per aziende e liberi professsionisti – Connettiti all rete anche dove l\'Adsl o la Fibra ottica non arrivano – Alte prestazioni e capacità di banda',
                'quiz_icon' => 'quiz/background/fiber.webp',
                'quiz_gradient' => 'linear-gradient(135deg, #80F1A6 0%, #EFD000 100%);',
                'views' => ['services/wireless/termini', 'services/wireless/main'],
            ],

            // HDSL
            [
                'name' => 'Hdsl',
                'type' => 'Connettività',
                'icon' => 'hdsl.svg',
                'bg' => 'services/hdsl.webp',
                'description' => 'Quasi tutti conosciamo l\'ADSL, presente ormai nella maggior parte delle case, o le soluzioni in fibra che nelle zone in cui questa connettività è presente rappresenta la soluzione più performante e veloce. Molti però non sanno che esistono alternative alle connessioni ADSL o Fibra e qui vi presentiamo la tecnologia HDSL. <br><br>In questa pagina approfondiremo le caratteristiche dell\'HDSL e vi presenteremo le offerte HDSL Fiberdroid.<br><br> Ma come primissima cosa ti consigliamo di verificare la disponibilità del servizio HDSL nella tua zona, per essere sicuro di poter scegliere questa tecnologia per la tua rete.',
                'short_description' => 'HDSL connectivity',
                'title_plans' => 'Pensati su misura per te',
                'meta_title' => "Hdsl: linea veloce e  minima latenza senza fibra",
                'meta_description' => "Offerte  HDSL Fiberdroid per aziende e liberi professionisti. Verifica la copertura nella tua zona. HDSL: veloce, stabile, simmetrica, dove la fibra non è presente",
                'views' => ['services/hdsl/termini', 'services/hdsl/main'],
                'quiz_icon' => 'quiz/background/fiber.webp',
                'quiz_gradient' => 'linear-gradient(135deg, #80F1A6 0%, #EFD000 100%);',
            ],

            // CLOUD WIFI
            [
                'name' => 'Cloud wifi',
                'type' => 'Connettività',
                'description' => 'Il Cloud Based Networking è una gestione agile e adattiva, contestuale, intelligente e decisamente dinamica per l’impresa.<br><br>Se non puoi prevedere ora il numero di apparati che si collegheranno alla rete è più che mai necessario predisporre un’infrastruttura capace di rispondere in maniera proattiva alla tua evoluzione e crescita.',
                'short_description' => 'Make your internet available',
                'icon' => 'cloud-wifi.svg',
                'bg' => 'services/cloud-wifi.webp',
                'meta_title' => 'Cloud Wi-Fi',
                'meta_description' => "Cloud Wi-Fi – La soluzione di rete scalabile e semplificata – Antenne Wi-Fi gestite da un'unica piattaforma nel nostro data center.",
                'views' => ['services/cloud_wifi/termini', 'services/cloud_wifi/main'],
                'quiz_icon' => 'quiz/background/fiber.webp',
                'quiz_gradient' => 'linear-gradient(135deg, #80F1A6 0%, #EFD000 100%);',
            ],

            // CENTRALINO VOIP (DEPRECATED)
            [
                'name' => 'Centralino voip',
                'type' => 'VoIp',
                'icon' => 'voip.svg',
                'bg' => 'services/fiber.webp',
                'description' => 'Vuoi sostituire il tuo vecchio centralino aziendale con uno in grado di adattarsi alla crescita della tua azienda? Il Centralino VoIP è la soluzione che fa per te.<br><br>Sia che tu scelga di installarlo in sede sia che tu scelga la soluzione virtuale (in outsourcing presso un datacenter) questa nuova tecnologia ti  permette di gestire in maniera semplice e veloce le tue chiamate oltre ad offrirti nuove funzionalità che non sono disponibili con i  centralini tradizionali, permettondoti di ridurre in modo sensibile i costi telefonici e di gestione.',
                'short_description' => 'Centralized voice connection',
                'meta_title' => 'Centralino VoIP',
                'meta_description' => 'Centralino VoIP – Vantaggi rispetto a Centralino tradizionale o Centralino Cloud – Offerta Fiberdroid: la soluzione studiata su misura per te',
                'views' => ['services/centralino_voip/main'],
            ],

            // SATELLITARE
            [
                'name' => 'Satellitare',
                'type' => 'Connettività',
                'icon' => 'satellite.svg',
                'bg' => 'services/satellitare.webp',
                'description' => 'Recentemente, abbiamo compreso quanto sia fondamentale rimanere connessi alla rete per le nostre attività quotidiane. Tuttavia, in molte zone d’Italia, la copertura per le connessioni in fibra ottica è ancora insufficiente e, in alcune aree, la disponibilità di ADSL non è nemmeno garantita. <br><br> In questo scenario, la <strong>connessione tramite satellite</strong> emerge come una soluzione ideale per colmare il divario. Con la tecnologia satellitare, possiamo raggiungere una copertura totale, assicurando accesso a internet anche nei luoghi più isolati e lontani dalle infrastrutture tradizionali. Se desideri una connessione a internet senza dipendere da una linea fissa e avere accesso a una rete affidabile ovunque ti trovi, la nostra offerta di internet satellitare rappresenta la scelta perfetta per te.',
                'short_description' => 'Sattelite and backups',
                'meta_title' => "Servizi Internet Satellitare | Fiberdroid - Connessione Veloce Ovunque",
                'meta_description' => "Esplora i servizi di internet satellitare di Fiberdroid per una connessione stabile anche nelle aree più remote. Scopri le nostre soluzioni avanzate per accesso a banda larga via satellite e come possiamo offrirti una connessione veloce e affidabile ovunque tu sia.",
                'quiz_icon' => 'quiz/background/fiber.webp',
                'quiz_gradient' => 'linear-gradient(135deg, #80F1A6 0%, #EFD000 100%);',
                'views' => ['services/satellitare/termini', 'services/satellitare/main'],
            ],

            // ADSL
            [
                'name' => 'ADSL',
                'type' => 'Connettività',
                'icon' => 'adsl.svg',
                'bg' => 'services/adsl.webp',
                'description' => 'L’ADSL è la soluzione per aziende e per liberi professionisti che hanno bisogno di collegarsi ad Internet e devono scegliere una nuova linea o devono cambiare operatore.<br><br>Le due tecnologie di trasmissione dati più diffuse sono l\'ADSL e la Fibra ottica.',
                'short_description' => 'ADSL connectivity',
                'title_plans' => 'Pensati su misura per te',
                'meta_title' => "Adsl: offerta linea veloce, sicura e disponibile di Fiberdroid",
                'meta_description' => "Offerte Adsl Fiberdroid per aziende e liberi professionisti. Verifica la copertura nella tua zona. ADSL: veloce e stabile dove la fibra non è presente",
                'views' => ['services/adsl/termini', 'services/adsl/main'],
                'quiz_icon' => 'quiz/background/fiber.webp',
                'quiz_gradient' => 'linear-gradient(135deg, #80F1A6 0%, #EFD000 100%);',
            ],

            // RETI WAN
            [
                'name' => 'Reti wan',
                'type' => 'Connettività',
                'description' => 'La tua rete aziendale si sta espandendo e nascono nuove filiali, in altre zone della città e persino in città diverse?<br><br>Noi di Fiberdroid ci occupiamo da anni di progettazione e configurazione di reti di ogni tipo, comprese MAN e WAN e possiamo analizzare la tua situazione specifica per trovare la migliore configurazione per creare la tua rete Aziendale.',
                'short_description' => 'WAN connection',
                'icon' => 'wan.svg',
                'bg' => 'services/reti-wan.webp',
                'meta_title' => "Reti WAN: la connessione tra più sedi aziendali",
                'meta_description' => "Reti WAN: la soluzione quando la tua azienda si espande. Per usare applicazioni e condividere dati  come se fossi in un'unica sede. In totale sicurezza.",
                'views' => ['services/wan/main'],
                'quiz_icon' => 'quiz/background/fiber.webp',
            ],

            // CENTRALINO CLOUD (DEPRECATED)
            [
                'name' => 'Centralino cloud',
                'type' => 'VoIp',
                'icon' => 'cloud.svg',
                'bg' => 'services/voip.webp',
                'description' => 'Lo sai, la voce, il video, i dati già da tempo viaggiano sulla Rete. La piattaforma di Centralino Cloud di Fiberdroid consente alle imprese di dematerializzare i vecchi impianti telefonici.<br><br>Infatti grazie alla nostra soluzione Centralino in Cloud l’azienda può arrivare ad abbandonare completamente i classici telefoni e operare anche totalmente da computer oppure da app su smartphone o tablet.',
                'short_description' => 'Centralino cloud',
                'meta_title' => 'Centralino cloud',
                'meta_description' => 'Centralino Cloud - Soluzione per PMI, ristorazione, strutture recettive e smartworker – Tutti i vantaggi dell\'offerta Fiberdroid',
                'views' => ['services/centralino_cloud/termini', 'services/centralino_cloud/main'],
            ],





            [
                'name' => 'Partite iva',
                'type' => 'Soluzione',
                'model' => 'piva',
                'short_description' => 'Freelancers',
                'icon' => 'solutions/piva.svg',
                'bg' => 'solutions/pages/piva.webp',
                'meta_title' => "Internet business per partite IVA",
                'meta_description' => "Il tuo successo dipende anche dalla tecnologia che usi, noi offriamo la connessione a dati e clienti ovunque alla massima velocità possibile! Affidabilità e libertà!",
                'title' => 'Partita Iva: Offerte Internet Business Per i Professionisti',
                'description' => 'Lo studio del libero professionista non è più quello di una volta! Ora puoi lavorare ovunque, purchè tu abbia una connessione veloce e sicura ai tuoi dati e ai tuoi clienti e un backup sempre attivo e disponibile.',
                'plans' => json_encode([
                    [
                        'title' => 'OFFERTA<br> PARTITE IVA',
                        'image' => '/solutions/packages/piva.webp',
                        'class' => 'mx-auto',
                        'items' => [
                            'Connettività in fibra',
                            'FTTC o FTTH',
                            'Ip statico pubblico',
                            'Centralino VoIP con Tuo numero telefonico',
                            'WiFi sicuro per Clienti'
                        ],
                        'price' => 69.90,
                    ],
                ]),
                'slider' => json_encode([
                    'title' => 'Connessione a internet di qualità che tiene conto dei tuoi bisogni',
                    'text' => '
                        <p class="service-text"><br /><strong>Connessione veloce</strong><br />caricare file, scaricare foto, inviare presentazioni, modificare documenti, tutto deve essere immediato per non farti perdere tempo e denaro.</p>
                        <p class="service-text"><br /><strong>Libertà di movimento<br /></strong>puoi lavorare in città, in una sala d’attesa o in treno, al mare, mentre ti rilassi e dai un’occhiata allo stato dei lavori, in montagna dopo una bella passeggiata. Ovunque tu vada basta solo una buona connessione per il tuo business.</p>
                        <p class="service-text"><br /><strong>Condivisione dei dati</strong><br />in qualunque momento e in qualunque posto ti trovi con i tuoi clienti o collaboratori inviando o modificando in modo semplice e sicuro,  senza perdere tempo tra email e chat.</p>
                        <p class="service-text"><br /><strong>Sicurezza dei backup</strong><br />niente rischi di hard disk rotti, dati inaccessibili o cancellati per problemi tecnici, col nostro sistema di backup e la condivisione in cloud ogni documento è al sicuro per sempre.</p>
                        <p class="service-text"><br /><strong>Bisogno di certezze</strong><br />certezza di essere seguito, di avere un’assistenza veloce ed efficiente e non trovarsi abbandonato se la connessione cade, se qualcuno ha staccato un cavo, se ha dimenticato la password… perché il tempo va usato per il business o per se stessi, non per impazzire per lentezze burocratiche!</p>
                     ',
                    'images' => [
                        '/solutions/piva/meeting.png',
                        '/solutions/piva/office.png',
                        '/solutions/piva/work.png',
                    ],
                    'total_images' => 2,
                ]),
                'views' => ['services/piva/main']
            ],
            [
                'name' => 'Aziende',
                'type' => 'Soluzione',
                'model' => 'office',
                'short_description' => 'Piccole e grandi aziende',
                'icon' => 'solutions/building.svg',
                'bg' => 'services/aziende.webp',
                'meta_title' => "Soluzioni di connettività per aziende",
                'meta_description' => "Aziende con sedi singole o multisede, in città o vittime del digital divide, per tutti le giuste soluzioni e un'assistenza personalizzata e che non ti abbandona mai!",
                'title' => 'Soluzioni di connettività per aziende',
                'description' => 'I nostri servizi godono di flessibilità e possibilità di personalizzazione a seconda delle esigenze e delle caratteristiche della tua zona.<br><br> Le nostre soluzioni non si rivolgono solo chi ha bisogno di un’ infrastruttura completa, ma a qualsiasi tipo di esigenza.',
                'views' => ['services/aziende/main'],
            ],
            [
                'name' => 'Hybrid 5G',
                'type' => 'Soluzione',
                'short_description' => 'Connettività ibride',
                'icon' => 'solutions/5g.svg',
                'bg' => 'services/fiber.webp',
                'meta_title' => "Hybrid 5G, più lontani, più veloci con la doppia connessione",
                'meta_description' => "In questo articolo spiegheremo in dettaglio come funziona questa tecnologia che combina la connessione di terra FTTC (Fiber to the Cabinet) con la connessione wireless 5G per garantire una velocità di connessione elevata e una copertura più ampia.",
                'description' => 'Una buona connessione a Internet è fondamentale per ogni impresa. Per quelle raggiunte dalla fibra ottica, parlare ai propri clienti, interagire con i fornitori e far comunicare i propri dipendenti è una cosa normale; per quelle che si trovano nelle zone ancora non cablate, questo resta un limite.<br><br>Per ovviare a questo problema, una delle soluzioni sviluppate da Fiberdroid è: l\'hybrid 5G. In questo articolo spiegheremo in dettaglio come funziona questa tecnologia che combina la connessione di terra FTTC (Fiber to the Cabinet) con la connessione wireless 5G per garantire una velocità di connessione elevata e una copertura più ampia.',
                'plans' => json_encode([
                    [
                        'title' => 'Hybrid Fibra + 5G',
                        'image' => '/solutions/packages/b&b.webp',
                        'class' => 'mx-auto',
                        'items' => [
                            '5G Download 300 Mega Upload 30 Mega',
                            'FTTC Fino a 200 Mega o<br>FTTH Fino a 1 Giga<b>*</b>',
                            '2 IP Statici pubblici',
                            'Router firewall in comodato d\'uso'
                        ],
                        'price' => 99.19,
                        'isService' => true,
                    ],
                ]),
                'views' => ['services/hybrid5g/termini'],
            ],
            [
                'name' => 'Strutture ricettive',
                'type' => 'Soluzione',
                'model' => 'hotel',
                'short_description' => 'Hotel, B&B e Agriturismi',
                'icon' => 'solutions/hotel.svg',
                'bg' => 'solutions/pages/hotel.webp',
                'meta_title' => "Soluzioni di connettività per Strutture ricettive",
                'meta_description' => "A Hotel, B&B, Agriturismi servono: POS,  Wi-Fi, prenotazioni, controllo accessi, sito, social... Affidati a Fiberdroid  gestore unico per connessioni veloci ed efficienti!",
                'title' => 'Internet per Hotel e Appartamenti',
                'description' => 'In questa pagina trovi i servizi Fiberdroid pensati per offrire un’ottima connessione internet agli hotel e a tutte le strutture ricettive che devono gestire le connessioni dei clienti e dei dipendenti in modo sicuro ed efficiente. <br><br> Affidati a Fiberdroid come gestore unico. Un unico referente, il partner che ti assisterà in qualunque problematica di connessione garantendoti la continuità del servizio.',
                'plans' => json_encode([
                    [
                        'title' => 'SMALL HOTELS <br>AND B&BS',
                        'image' => '/solutions/packages/b&b.webp',
                        'class' => 'ml-auto mr-6',
                        'items' => [
                            'Fiber-Connettività FTTC o FTTH',
                            'IP Statico Pubblico',
                            'Numerazione telefonica',
                            'Centralino VOIP',
                            'WI-FI per i clienti',
                        ],
                        'price' => 69.90,
                    ],
                    [
                        'title' => 'HOTELS AND <br>HOTEL CHAINS',
                        'image' => '/solutions/packages/hotels.webp',
                        'class' => 'mr-auto ml-6',
                        'items' => [
                            'Fiber-Connettività FTTC o FTTH',
                            'IP Statico Pubblico',
                            'Numerazione telefonica',
                            'Centralino VOIP',
                            'WI-FI per i clienti',
                        ],
                        'price' => 69.90,
                    ]
                ]),
                'slider' => json_encode([
                    'title' => 'Soluzioni pensate per rendere migliore il tuo servizio',
                    'text' => '
                        Hotels, B&Bs, Agriturismi, Residence e Motel hanno bisogno di connessioni veloci ed efficenti.
                        <br><br>Infatti hanno bisogno di:
                        <ul class="list">
                            <li>permettere pagamenti tramite POS</li>
                            <li>fornire una connessione wi-fi ai clienti</li>
                            <li>gestire le prenotazioni senza ritardi</li>
                            <li>controllo degli accessi</li>
                            <li>gestire il sito web e i social</li>
                            <li>avere una linea telefonica sempre libera per comunicare con clienti e fornitori simultaneamente</li>
                        </ul>
                     ',
                    'images' => [
                        '/solutions/hotel/worker.png',
                        '/solutions/hotel/pos.png',
                        '/solutions/hotel/reception.png',
                    ],
                    'total_images' => 1,
                ]),
                'views' => ['services/hotels/main']
            ],
            [
                'name' => 'Ristorazione',
                'type' => 'Soluzione',
                'short_description' => 'Ristoranti e tavole calde',
                'icon' => 'solutions/restaurant.svg',
                'bg' => 'solutions/pages/restaurants.webp',
                'meta_title' => "Soluzioni di connettività per Ristorazione",
                'meta_description' => "POS, WI-FI, prenotazioni online, controllo accessi, sicurezza, email, sito, social.  Investire su una linea sicura ed efficiente è fondamentale per ristorazione e negozi.",
                'title' => 'Soluzioni di connettività per ristoranti',
                'description' => 'In questa pagina troverai i servizi Fiberdroid pensati per offrire un\'ottima connessione internet a ristoranti e bar che necessitano di gestire le connessioni di clienti e dipendenti in modo sicuro ed efficiente. Con un assistenza unica e dedicata. <br><br>Ormai ogni cliente si aspetta che in tutti i locali pubblici si possa pagare col POS ed è molto soddisfatto quando scopre che ha a disposizione il WIFI gratuito.',
                'plans' => json_encode([
                    [
                        'title' => 'OFFERTA RISTORAZIONE',
                        'image' => '/solutions/packages/restaurants.webp',
                        'class' => 'mx-auto',
                        'items' => [
                            'Connettività in fibra',
                            'FTTC o FTTH',
                            'Ip statico pubblico',
                            'Centralino VoIP con Tuo numero telefonico',
                            'WiFi sicuro per Clienti'
                        ],
                        'price' => 69.90,
                    ],
                ]),
                'slider' => json_encode([
                    'title' => 'Soluzioni studiate per rendere unico il tuo servizio',
                    'text' => '
                        Ristoranti & Bar necessitano di una connessione veloce ed efficente.
                        <br><br>Infatti è ormai necessario:
                        <ul class="list">
                            <li>permette pagamenti tramite POS</li>
                            <li>offrire connessione WIFI ai clienti</li>
                            <li>gestire le prenotazioni in modo efficente</li>
                            <li>gestire il sito web e i social network</li>
                            <li>avere una linea telefonica sempre libera per la ricezione delle prenotazioni</li>
                        </ul>
                     ',
                    'images' => [
                        '/solutions/restaurants/friends.png',
                        '/solutions/restaurants/payment.png',
                        '/solutions/restaurants/order.png',
                    ],
                    'total_images' => 1,
                ]),
                'views' => ['services/restaurants/main']
            ],
            [
                'name' => 'Retail',
                'type' => 'Soluzione',
                'short_description' => 'Piccoli e grandi negozi',
                'icon' => 'solutions/retail.svg',
                'bg' => 'solutions/pages/retail.webp',
                'meta_title' => "Soluzioni di connettività per Retail",
                'meta_description' => "WI-FI, controllo accessi, sicurezza, email, sito, social.  Investire su una linea sicura ed efficiente è fondamentale per tutti i retailer.",
                'title' => 'Internet per Retail',
                'description' => 'Connettiti al futuro con la nostra incredibile offerta di servizi dedicati a tutti i negozi che necessitano di gestire le connessioni di clienti e dipendenti in modo sicuro ed efficiente. Affidati a Fiberdroid come unico manager. Un unico interlocutore, il partner che ti assisterà per ogni problema di connessione, garantendoti la continuità del servizio.',
                'plans' => json_encode([
                    [
                        'title' => 'OFFERTA RETAIL',
                        'image' => '/solutions/packages/retail.webp',
                        'class' => 'mx-auto',
                        'items' => [
                            'Connettività in fibra',
                            'FTTC o FTTH',
                            'Ip statico pubblico',
                            'Centralino VoIP con Tuo numero telefonico',
                            'WiFi sicuro per Clienti'
                        ],
                        'price' => 69.90,
                    ],
                ]),
                'slider' => json_encode([
                    'title' => 'Solutions built to make your service better',
                    'text' => '
                        Anche nei sabati pomeriggio di dicembre, con negozio pieno di gente che sceglie i regali di Natale e telefonate di chi vuole prenotare un oggetto o sapere se lo avete, non avrete mai problemi di telefonate, nemmeno se vi tranciassero i cavi con una ruspa! Con il VoIP basta deviare su qualunque dispositivo le chiamate per essere sempre raggiungibili.
                        <br><br>
                        Oltre l’80% dei clienti vuole evitare code pagando a un commesso col POS mobile.
                        <br><br>
                        E il 79% vorrebbe avere la possibilità di pagare gli acquisti in negozio con un’app, senza interagire con un commesso (la percentuale sale all’87% tra i 18 e i 34 anni)
                        perché avendo tutti i canali VoIP a disposizione potrai fare più cose in contemporanea senza avere mai problemi di linea occupata:
                        <ul class="list">
                            <li>gestire tutti i tuoi sistemi di sicurezza in remoto, telecamere, controllo accessi</li>
                            <li>pubblicare sui social le foto dei tuoi nuovi arrivi e interagire con i tuoi clienti sulla pagina Facebook</li>
                            <li>interagire con l’HotSpot WiFi inviando offerte e promozioni</li>
                            <li>gestire le carte fedeltà raccogliendo i dati dei tuoi clienti per inviare via email le promozioni e le offerte</li>
                            <li>creare stories su Instagram sul tuo negozio e le novità che proponi</li>
                            <li>pubblicare sul tuo sito in un attimo ogni pezzo che esponi nel negozio</li>
                            <li>gestire spedizioni, ordini e resi senza occupare la linea telefonica o del POS</li>
                            <li>gestire la segreteria, le deviazioni di chiamata o la reperibilità in qualunque momento e da qualunque posto ti trovi!</li>
                        </ul>
                        <br>
                        <p class="service-text">
                            E tutto ciò è fantastico, se funziona e non ti crea problemi. <br>
                            Fiberdroid, il tuo partner unico per la connettività è al tuo fianco per garantirti la massima qualità e un’assistenza veloce e dedicata.
                            Per non perdere tempo e poter pensare solo al tuo business!
                        </p>
                     ',
                    'images' => [
                        '/solutions/hotel/worker.png',
                        '/solutions/hotel/pos.png',
                        '/solutions/hotel/reception.png',
                    ],
                    'total_images' => 2,
                ]),
            ],
            [
                'name' => 'Fibra Ottica',
                'type' => 'Soluzione',
                'short_description' => 'Soluzioni in fibra ottica',
                'icon' => 'solutions/piva.svg',
                'bg' => 'services/pages/fiber.webp',
                'meta_title' => "Connessioni in Fibra Ottica: cosa sono?",
                'meta_description' => "La fibra ottica è una delle tecnologie di connessione a Internet più avanzate e performanti attualmente disponibili. In questo articolo, scopriremo tutti i suoi dettagli e i suoi vantaggi.",
                'description' => 'Una buona connessione a Internet è un punto di forza per ogni impresa. Non tutte le connessioni sono uguali però. la fibra ottica rappresenta oggi una delle soluzioni più performanti e avanzate disponibili sul mercato, grazie alla sua capacità di trasferire grandi quantità di dati a velocità elevatissime. Ma cosa è esattamente la fibra ottica e come funziona?<br><br>In questo articolo, esploriamo in dettaglio questa tecnologia di connessione, offrendo informazioni sulla sua definizione, le sue caratteristiche principali, le diverse tipologie di connessione in fibra ottica, i vantaggi e gli svantaggi, e tanto altro ancora',
                'views' => ['services/fibra-soluzione/main'],
            ],


            [
                'name' => 'Router',
                'type' => 'Prodotto',
                'description' => 'La libertà è uno dei nostri Valori fondanti.<br>Tutte le connettività Fiberdroid sono perciò router-free, sostengono l’iniziativa “modem libero” (www.modemlibero.it) e ti lasciano libero di scegliere ed installare il router/modem che più desideri, senza vincolarti ad usare apparati specifici.<br><br>Se in ogni caso cerchi un apparato certificato e testato sulle nostre connettività, ti consigliamo di adottare uno tra i due seguenti brand: Draytek o AVM Fritz!Box',
                'short_description' => 'Router per le tue linee',
                'icon' => 'router.svg',
                'bg' => 'products/draytek.png',
                'meta_title' => 'Router: le nostre scelte: Draytek e AVM Fritz!Box',
                'meta_description' => "La libertà è uno dei nostri Valori fondanti.",
                'views' => ['services/router/termini', 'services/router/main'],
                'quiz_icon' => 'quiz/background/fiber.webp',
                'quiz_gradient' => 'linear-gradient(135deg, #80F1A6 0%, #EFD000 100%);',
            ],
            [
                'name' => 'Telefoni Voip',
                'type' => 'Prodotto',
                'description' => '<b class="has-text-white">Stai cercando una soluzione di comunicazione efficiente e innovativa per la tua azienda?</b><br><br>

Un telefono VoIP è la risposta perfetta per chi desidera ottimizzare le comunicazioni aziendali utilizzando la tecnologia avanzata del <b>VoIP (Voice over IP)</b>. Progettato per integrarsi facilmente con un centralino VoIP, questo telefono consente di trasmettere la tua voce tramite internet, eliminando la necessità dei tradizionali sistemi telefonici.
<br><br>
Grazie all\'evoluzione dello standard <b>SIP (Session Initiation Protocol)</b>, il protocollo di comunicazione su cui si basa il VoIP, i telefoni aziendali di ultima generazione possono operare senza l\'uso dei vecchi cavi telefonici. Questo ti permette di connettere il telefono VoIP ovunque ci sia una rete internet, garantendo massima flessibilità per i tuoi dipendenti, che potranno gestire le chiamate anche in smart working o in mobilità.
<br><br>
Perfetti per ogni tipo di impresa, questi dispositivi offrono una gestione avanzata delle chiamate, qualità audio superiore e la possibilità di integrare funzionalità aggiuntive come videochiamate, conferenze multiple e rubrica condivisa. Scegliendo un telefono VoIP per la tua azienda, potrai ottimizzare le comunicazioni e ridurre i costi operativi, con la garanzia di una connessione stabile e sicura.',
                'short_description' => 'Telefoni fissi e cordless',
                'icon' => 'phone.svg',
                'bg' => 'products/voip-phone.png',
                'meta_title' => "Telefoni Voip: riduci i costi telefonici con in più l'assistenza Fiberdroid",
                'meta_description' => "Telefoni Voip.  La telefonia di nuova generazione, ti basta una connessione dati attiva e comunichi con il resto del mondo. Porta il tuo ufficio con te ovunque.",
                'views' => ['services/telefoni_voip/termini', 'services/telefoni_voip/main'],
                'quiz_gradient' => url('pages/quiz/bg-gradients/voip.png'),
            ],
            [
                'name' => 'Access Point',
                'type' => 'Prodotto',
                'description' => 'Usiamo solo prodotti Ubiquiti, una delle migliori marche in commercio, per garantirti la miglior stabilità e performance di connessione.',
                'short_description' => 'Access points Ubiquiti',
                'icon' => 'access-point.svg',
                'bg' => 'products/access-point.png',
                'meta_title' => 'Access Point: Ubiquiti la nostra scelta',
                'meta_description' => "Scegli l'apparato che vuoi, oppure acquistalo o noleggialo da noi: avrai assistenza tecnica anche sull'hardware oltre che sulla connessione. Scopri le offerte!",
                'views' => ['services/access_point/termini', 'services/access_point/main'],
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->services as $service) {
            $servizio = new FiberdroidService();
            $servizio->name = $service['name'];
            $servizio->description = $service['description'];
            $servizio->short_description = $service['short_description'];
            $servizio->bg = $service['bg'];
            $servizio->icon = $service['icon'];
            $servizio->quiz_icon = $service['quiz_icon'] ?? null;
            $servizio->quiz_gradient = $service['quiz_gradient'] ?? null;
            $servizio->title = $service['title'] ?? null;
            $servizio->title_plans = $service['title_plans'] ?? null;
            $servizio->type = $service['type'];
            $servizio->model = $service['model'] ?? null;
            $servizio->active = $service['active'] ?? true;
            $servizio->url = Str::slug($service['name']);
            $servizio->json_plans = $service['plans'] ?? null;
            $servizio->json_slider = $service['slider'] ?? null;

            $servizio->meta_description = isset($service['meta_description']) ? $service['meta_description'] : null;
            $servizio->meta_title = isset($service['meta_title']) ? $service['meta_title'] : null;

            $jsonViews = null;
            if(isset($service['views'])) {
                foreach ($service['views'] as $view) {
                    $name = collect(explode("/", $view))->last();

                    $jsonViews[$name] = view('pages/' . $view)->render();
                }
            }
            $servizio->json_views = json_encode($jsonViews);

            $servizio->save();
        }
    }

}
