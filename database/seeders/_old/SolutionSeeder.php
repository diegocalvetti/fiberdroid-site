<?php

namespace Database\Seeders;

use App\Models\Service;
use App\Models\ServiceHasItem;
use App\Models\ServiceType;
use App\Models\Solution;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SolutionSeeder extends Seeder
{
    public $solution = [];

    public function __construct()
    {
        $this->solution = [
            [
                'name' => 'Partite iva',
                'meta_title' => "Internet business per partite IVA",
                'meta_description' => "Il tuo successo dipende anche dalla tecnologia che usi, noi offriamo la connessione a dati e clienti ovunque alla massima velocità possibile! Affidabilità e libertà!"
            ],
            [
                'name' => 'Aziende',
                'meta_title' => "Soluzioni di connettività per aziende",
                'meta_description' => "Aziende con sedi singole o multisede, in città o vittime del digital divide, per tutti le giuste soluzioni e un'assistenza personalizzata e che non ti abbandona mai!"
            ],
            [
                'name' => 'Ristorazione e retail',
                'meta_title' => "Soluzioni di connettività per Ristorazione e retail",
                'meta_description' => "POS, WI-FI, prenotazioni online, controllo accessi, sicurezza, email, sito, social.  Investire su una linea sicura ed efficiente è fondamentale per ristorazione e negozi."
            ],
            [
                'name' => 'Strutture ricettive',
                'meta_title' => "Soluzioni di connettività per Strutture ricettive",
                'meta_description' => "A Hotel, B&B, Agriturismi servono: POS,  Wi-Fi, prenotazioni, controllo accessi, sito, social... Affidati a Fiberdroid  gestore unico per connessioni veloci ed efficienti!",
            ],
            [
                'name' => 'Jetfiber',
                'meta_title' => "Soluzioni di connettività per Strutture ricettive"
            ],
            [
                'name' => 'Hybrid 5G',
                'meta_title' => "Hybrid 5G, più lontani, più veloci con la doppia connessione",
                'meta_description' => "In questo articolo spiegheremo in dettaglio come funziona questa tecnologia che combina la connessione di terra FTTC (Fiber to the Cabinet) con la connessione wireless 5G per garantire una velocità di connessione elevata e una copertura più ampia.",
            ],
            [
                'name' => 'Fibra Ottica',
                'meta_title' => "Connessioni in Fibra Ottica: cosa sono?",
                'meta_description' => "La fibra ottica è una delle tecnologie di connessione a Internet più avanzate e performanti attualmente disponibili. In questo articolo, scopriremo tutti i suoi dettagli e i suoi vantaggi.",
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->solution as $solution) {
            $soluzione = new Solution();
            $soluzione->name = $solution['name'];
            $soluzione->url = Str::slug($soluzione->name);
            $soluzione->meta_description = isset($solution['meta_description']) ? $solution['meta_description'] : null;
            $soluzione->meta_title = isset($solution['meta_title']) ? $solution['meta_title'] : null;
            $soluzione->save();
        }
    }

}
