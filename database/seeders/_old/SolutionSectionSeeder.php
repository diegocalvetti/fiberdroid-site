<?php

namespace Database\Seeders;

use App\Models\Section;
use App\Models\Service;
use App\Models\Solution;
use Illuminate\Database\Seeder;
use ReflectionException;

class SolutionSectionSeeder extends BaseSectionSeeder
{

    public $sections;

    public function __construct()
    {

        $this->setBaseDataPath("/soluzioni");

        $this->sections = [
            'Partite iva' => [
                $this->introText("Partita IVA: offerte internet business per il professionista", $this->html("piva/intro"), "", "first", false),
                $this->text("Perchè un libero professionista ha bisogno di connessione internet di qualità. Che tenga conto delle sue esigenze", "", $this->jsonFromData("piva/offerte"), "first", false),
                $this->text("", $this->html("piva/main"), $this->jsonFromData("piva/card"), "first", false),
                $this->text("", $this->html("piva/description"), "", "first", false)
            ],
            'Strutture ricettive' => [
                $this->introText("Soluzioni di connettività per Strutture ricettive", $this->html("strutture/intro"), "", "first", false),
                $this->text("", $this->html("strutture/offerte"), "", "first", false),
                $this->text("Come funziona?", "", $this->jsonFromData("strutture/come_funziona"), "first", false),
                $this->text("Qualche esempio?", "", $this->jsonFromData("strutture/extra"), "first", false),
                $this->text("", $this->html("strutture/description"), "", "first", false),
                $this->text("Ma cosa compone una efficiente connessione per Hotel?", "", $this->jsonFromData("strutture/accordions"), "first", true),
                $this->text("Q&A", $this->html("strutture/q&a"), "", "first", true),
             ],
            'Aziende' => [
                $this->introText("Soluzioni di connettività per Aziende", $this->html("aziende/intro"), "", "first", true),
                $this->void(),
                $this->text("Assistenza tecnica dedicata", $this->html("aziende/assistenza"), "", "first", true),
                $this->text("Aziende con singola sede", $this->html("aziende/single-sede"), "", "first", true),
                $this->text("centralino telefonico cloud 3cx", $this->html("aziende/3cx"), "", "sub", true),
                $this->text("backup", $this->html("aziende/backup"), "", "sub", true),
                $this->text("Aziende multi sede", $this->html("aziende/multi-sede"), "", "first", true),
                $this->text("Aziende vittime del digital divide", $this->html("aziende/digital-divide"), "", "first", true),
            ],
            'Ristorazione e retail' => [
                $this->introText("Soluzioni di connettività per Ristorazione e retail", $this->html("risto/intro"), "", "first", false),
                $this->text("", $this->html("risto/preAnchor"), "", "first", false),
                $this->text("La tecnologia indispensabile per la ristorazione", $this->html("risto/tech_indispensabile"), "", "first", true),
                $this->text("Il pos per la ristorazione", $this->html("risto/pos"), "", "sub", true),
                $this->text("Perché il wi-fi è una priorità nel marketing della ristorazione", $this->html("risto/wifi"), $this->jsonFromData("risto/wifi_extra"), "first", true),
                $this->text("Come ottenere il meglio dal tuo wi-fi nel ristorante", $this->html("risto/ottenere_il_meglio"), "", "first", true),
                $this->text("Perché il centralino voip è indispensabile nella ristorazione", $this->html("risto/centralino_indispensabile"), "", "sub", true),
                $this->text("Perché devi avere un sito e social sempre connessi", $this->html("risto/social"), $this->jsonFromData("risto/social_extra"), "sub", true),
                $this->text("", $this->html("risto/description"), "", "sub", false),
                $this->text("Perché un’ottima connessione è indispensabile al tuo negozio", $this->html("risto/ottima_connessione"), "", "first", true),
            ],
            'Jetfiber' => [
                $this->introText("JETFIBER", $this->html("jet/intro"), "", "first", false),
                $this->text("Di cosa si tratta?", $this->html("jet/what"), "","sub", true),
                $this->text("Come aderire?", $this->html("jet/come-aderire"), "","sub", true),
                $this->text("Quali soluzioni o pacchetto rientrano nella promozione", $this->html("jet/pack"), "","sub", true),
                $this->text("", "", $this->jsonFromData("jet/card"), "first", false),
                $this->text("Contattaci per maggiori informazioni", $this->html("jet/info"), "","sub", true),
            ],
            'Hybrid 5G' => [
                $this->introText("Hybrid 5g, più lontani, più veloci con la doppia connessione", $this->html("hybrid/intro"), "", "first", false),
                $this->text("Cos'è l'hybrid 5G?", $this->html("hybrid/what"), "","sub", true),
                $this->text("Come funziona l'hybrid 5G?", $this->html("hybrid/how"), "","sub", true),
                $this->text("I vantaggi dell'hybrid 5g", $this->html("hybrid/vantages"), "","sub", true),
                $this->text("", "", $this->jsonFromData("hybrid/card"), "first", false),
                $this->text("", $this->html("hybrid/termini"), "","sub", false),
            ],
            'Fibra Ottica' => [
                $this->introText("Connessioni in Fibra Ottica: cosa sono?", $this->html("fibra/intro"), "", "first", false),
                $this->void(),
                $this->text("Cos’è la fibra ottica?", $this->html("fibra/what"), "","first", true),
                $this->text("Cavi Monomodali e Multimodali", $this->html("fibra/mono_vs_multi"), "","sub", true),
                $this->text("Vantaggi della fibra ottica", $this->html("fibra/vantages"), "","first", true),
                $this->text("Come funziona la fibra ottica", $this->html("fibra/how"), "","first", true),
                $this->text("Tipologie di connessione in fibra ottica", $this->html("fibra/types"), "","first", true),
                $this->text("AON, PON, GPON ed EPON: Reti attive e reti passive", $this->html("fibra/attive_vs_passive"), "","first", true),
                $this->text("Reti AON", $this->html("fibra/aon"), "","sub", true),
                $this->text("Reti PON", $this->html("fibra/pon"), "","sub", true),
                $this->text("WAN e LAN: reti locali e reti geografiche", $this->html("fibra/locali_vs_geografiche"), "","first", true),
                $this->text("Velocità di connessione in fibra ottica", $this->html("fibra/speed"), "","first", true),
                $this->text("Come scegliere la migliore connessione in fibra ottica", $this->html("fibra/choose"), "","first", true),
            ],
        ];
    }


    /**
     * Run the database seeds.
     *
     * @return void
     * @throws ReflectionException
     */
    public function run()
    {
        $this->seed("App\Models\Solution");
    }
}
