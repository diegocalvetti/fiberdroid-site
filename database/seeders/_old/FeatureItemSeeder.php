<?php

namespace Database\Seeders;

use App\Models\ItemFeature;
use Illuminate\Database\Seeder;

class FeatureItemSeeder extends Seeder
{
    public $features = [
        'FFTC_30' => [
            'item_id' => 2,
            'features' => ["30 Mega in download", "3 Mega in upload", "IP statico pubblico"]
        ],
        'FTTC_50' => [
            'item_id' => 1,
            'features' => ["50 Mega in download", "10 Mega in upload", "IP statico pubblico"]
        ],
        'FTTC_100' => [
            'item_id' => 3,
            'features' => ["100 Mega in download", "20 Mega in upload", "IP statico pubblico"]
        ],
        'FTTC_200' => [
            'item_id' => 4,
            'features' => ["200 Mega in download", "20 Mega in upload", "IP statico pubblico"]
        ],
        'FIBRA_500' => [
            'item_id' => 75,
            'features' => ["500 Mega in download", "300 Mega in upload", "IP statico pubblico"]
        ],
        'FIBRA_1G' => [
            'item_id' => 74,
            'features' => ["1 Giga in download", "500 Mega in upload", "IP statico pubblico"]
        ],
        'FIBRA_2.5G' => [
            'item_id' => 162,
            'features' => ["1 Giga in download", "500 Mega in upload", "IP statico pubblico"]
        ],
        'FIBRA_2.5G_ULTRA' => [
            'item_id' => 163,
            'features' => ["2.5 Giga in download", "1 Giga in upload", "IP statico pubblico"]
        ],
        'FTTH_200' => [
            'item_id' => 7,
            'features' => ["200 Mega in download", "100 Mega in upload", "IP statico pubblico"]
        ],
        'FTTH_500' => [
            'item_id' => 8,
            'features' => ["500 Mega in download", "100 Mega in upload", "IP statico pubblico"]
        ],
        'FTTH_1G' => [
            'item_id' => 9,
            'features' => ["1 Giga in download", "300 Mega in upload", "IP statico pubblico"]
        ],
        'ADSL7' =>  [
            'item_id' => 10,
            'features' => ["7 Mega in download", "700 kBps in Upload", "IP statico pubblico"]
        ],
        'ADSL20' =>  [
            'item_id' => 11,
            'features' => ["20 Mega in download", "1 Mega in Upload", "IP statico pubblico"]
        ],
        'HDSL2' =>  [
            'item_id' => 12,
            'features' => ["2 Mega in download", "2 Mega in Upload", "IP statico pubblico"]
        ],
        'HDSL4' =>  [
            'item_id' => 13,
            'features' => ["4 Mega in download", "4 Mega in Upload", "IP statico pubblico"]
        ],
        'HDSL8' =>  [
            'item_id' => 14,
            'features' => ["8 Mega in download", "8 Mega in Upload", "IP statico pubblico"]
        ],
        'HDSL10' =>  [
            'item_id' => 15,
            'features' => ["10 Mega in download", "10 Mega in Upload", "IP statico pubblico"]
        ],
        'HDSL16' =>  [
            'item_id' => 16,
            'features' => ["16 Mega in download", "16 Mega in Upload", "IP statico pubblico"]
        ],
        'SAT_INSTALLAZIONE' => [
            'item_id' => 39,
            'features' => []
        ],
        'SAT15' => [
            'item_id' => 17,
            'features' => ["15 Mega in download", "3 Mega in Upload", "10 gb/mese di traffico", "IP statico pubblico"]
        ],
        'SAT30' => [
            'item_id' => 18,
            'features' => ["30 Mega in download", "6 Mega in Upload", "25 gb/mese di traffico", "IP statico pubblico"]
        ],
        'SAT30MP' => [
            'item_id' => 19,
            'features' => ["30 Mega in download", "6 Mega in Upload", "50 gb/mese di traffico", "IP statico pubblico"]
        ],
        'SAT50' => [
            'item_id' => 20,
            'features' => ["50 Mega in download", "6 Mega in Upload", "25 gb/mese di traffico", "IP statico pubblico"]
        ],
        'SAT50MP' => [
            'item_id' => 21,
            'features' => ["50 Mega in download", "6 Mega in Upload", "50 gb/mese di traffico", "IP statico pubblico"]
        ],
        'SAT50MF' => [
            'item_id' => 22,
            'features' => ["50 Mega in download", "10 Mega in Upload", "100 gb/mese di traffico", "IP statico pubblico"]
        ],
        'SAT50MS' => [
            'item_id' => 23,
            'features' => ["50 Mega in download", "10 Mega in Upload", "200 gb/mese di traffico", "IP statico pubblico"]
        ],
        'VOIP SINGOLO' => [
            'item_id' => 24,
            'features' => ["Numero singolo", "Fino a 128 chiamate in contemporanea", "Nuovo o portato, anche da altro operatore"]
        ],
        'GNR10' => [
            'item_id' => 25,
            'features' => ["10 numeri", "Fino a 128 chiamate in contemporanea", "Nuovi o portati, anche da altro operatore"]
        ],
        'GNR20' => [
            'item_id' => 26,
            'features' => ["20 numeri", "Fino a 128 chiamate in contemporanea", "Nuovi o portati, anche da altro operatore"]
        ],
        'GNR30' => [
            'item_id' => 27,
            'features' => ["30 numeri", "Fino a 128 chiamate in contemporanea", "Nuovi o portati, anche da altro operatore"]
        ],
        'GNR50' => [
            'item_id' => 28,
            'features' => ["50 numeri", "Fino a 128 chiamate in contemporanea", "Nuovi o portati, anche da altro operatore"]
        ],
        'GNR100' => [
            'item_id' => 29,
            'features' => ["100 numeri", "Fino a 128 chiamate in contemporanea", "Nuovi o portati, anche da altro operatore"]
        ],
        'GNR200' => [
            'item_id' => 30,
            'features' => ["200 numeri", "Fino a 128 chiamate in contemporanea", "Nuovi o portati, anche da altro operatore"]
        ],
        'GNR1000' => [
            'item_id' => 31,
            'features' => ["1000 numeri", "Fino a 128 chiamate in contemporanea", "Nuovi o portati, anche da altro operatore"]
        ],
        'INTERNOCLOUD' => [
            'item_id' => 44,
            'features' => [
                'Singolo interno telefonico',
                'Piattaforma 3CX/Asterisk',
                'Supporto tecnico',
                'App per PC/Smartphone',
            ],
        ],
        'CLOUDAUDIO' => [
            'item_id' => 45,
            'features' => [
                'Numero voip',
                'Piattaforma Asterisk',
                'Supporto tecnico',
                'Stanza personalizzabile',
            ],
        ],
        '4CONTEMPORAINETA' => [
            'item_id' => 63,
            'features' => [
                '4 contemporaneità, interni illimitati',
                'Piattaforma 3CX/Asterisk',
                'Supporto tecnico',
                'App per PC/Smartphone',
            ],
        ],
        '8CONTEMPORAINETA' => [
            'item_id' => 56,
            'features' => [
                '8 contemporaneità, interni illimitati',
                'Piattaforma 3CX/Asterisk',
                'Supporto tecnico',
                'App per PC/Smartphone',
            ],
        ],
        '16CONTEMPORAINETA' => [
            'item_id' => 57,
            'features' => [
                '16 contemporaneità, interni illimitati',
                'Piattaforma 3CX/Asterisk',
                'Supporto tecnico',
                'App per PC/Smartphone',
            ],
        ],
        '24CONTEMPORAINETA' => [
            'item_id' => 58,
            'features' => [
                '24 contemporaneità, interni illimitati',
                'Piattaforma 3CX/Asterisk',
                'Supporto tecnico',
                'App per PC/Smartphone',
            ],
        ],
        'LTE100' => [
            'item_id' => 80,
            'features' => [
                'Ponte radio LTE',
                '100 Mega in download',
                '10 Mega in upload',
                'IP statico pubblico',
                'Banda garantita 128k',
            ],
        ],
        'LTE100ultra' => [
            'item_id' => 81,
            'features' => [
                'Ponte radio LTE',
                '100 Mega in download',
                '10 Mega in upload',
                '8 IP statici pubblici',
                'Banda garantita 5/1 Mega',
            ],
        ],
        'WR30' => [
            'item_id' => 41,
            'features' => [
                "Ponte radio 5 Gigahertz",
                "30 Mega in download",
                "3 Mega in upload",
                "IP Statico pubblico",
                "Banda garantita 6/1 Mega"
            ],
        ],
        'WR30MB' => [
            'item_id' => 69,
            'features' => [
                "Ponte radio 5 Gigahertz",
                "30 Mega in download",
                "3 Mega in upload",
                "IP Statico pubblico",
                "Banda garantita 128k"
            ],
        ],
        'WR50MB' => [
            'item_id' => 70,
            'features' => [
                "Ponte radio 5 Gigahertz",
                "50 Mega in download",
                "10 Mega in upload",
                "IP Statico pubblico",
                "Banda garantita 128k"
            ],
        ],
        'WR100MB' => [
            'item_id' => 71,
            'features' => [
                "Ponte radio 5 Gigahertz",
                "100 Mega in download",
                "10 Mega in upload",
                "IP Statico pubblico",
                "Banda garantita 128k"
            ],
        ],
        'WR100MBUIS' => [
            'item_id' => 72,
            'features' => [
                "Ponte radio 5 Gigahertz",
                "100 Mega in download",
                "10 Mega in upload",
                "IP Statico pubblico",
                "Banda garantita 7/1 Mega"
            ],
        ],
        'WR100P' => [
            'item_id' => 73,
            'features' => [
                "Ponte radio 5 Gigahertz",
                "100 Mega in download",
                "20 Mega in upload",
                "IP Statico pubblico",
                "Banda garantita 10/1 Mega"
            ],
        ],
        'WR4G' => [
            'item_id' => 43,
            'features' => [
                "Ponte radio 4G",
                "30 Mega in download",
                "30 Mega in upload",
                "IP Statico pubblico",
                "Traffico dati mensile: 400 Giga"
            ],
        ],
        'WR4GFLAT' => [
            'item_id' => 42,
            'features' => [
                "Ponte radio 4G",
                "30 Mega in download",
                "30 Mega in upload",
                "IP Statico pubblico",
                "Traffico dati illimitato"
            ],
        ],
        'WorkBot Base' => [
            'item_id' => 84,
            'features' => []
        ],
        'AI_VoceTesto' => [
            'item_id' => 90,
            'features' => []
        ],
        'AI_ModuloVoce' => [
            'item_id' => 85,
            'features' => []
        ],
        'AI_ModuloDHI' => [
            'item_id' => 86,
            'features' => []
        ],
        'AI_10000chat' => [
            'item_id' => 87,
            'features' => []
        ],
        'AI_2000voce' => [
            'item_id' => 88,
            'features' => []
        ],
        'AI_Booking' => [
            'item_id' => 89,
            'features' => []
        ],
        'AI_Secondalingua' => [
            'item_id' => 91,
            'features' => []
        ],
        'AI_Terzalingua' => [
            'item_id' => 92,
            'features' => []
        ],
        'AI_DHi' => [
            'item_id' => 93,
            'features' => []
        ],
        'AI_DHiPro' => [
            'item_id' => 94,
            'features' => []
        ],
        'AI_BI' => [
            'item_id' => 95,
            'features' => []
        ],
        'AI_10kchat' => [
            'item_id' => 96,
            'features' => []
        ],
        'AI_20kvoce' => [
            'item_id' => 97,
            'features' => []
        ],
        'AI_SmartDocument' => [
            'item_id' => 98,
            'features' => []
        ],
        'AI_NLG' => [
            'item_id' => 99,
            'features' => []
        ],
        'AI_WhatsappMobile' => [
            'item_id' => 100,
            'features' => []
        ],
        'AI_WhatsappFisso' => [
            'item_id' => 101,
            'features' => []
        ],
        'AI_10kWhatsapp' => [
            'item_id' => 102,
            'features' => []
        ],
        'AI_25kWhatsapp' => [
            'item_id' => 103,
            'features' => []
        ],
        'AI_Voip5' => [
            'item_id' => 104,
            'features' => []
        ],
        'AI_Voip10' => [
            'item_id' => 105,
            'features' => []
        ],
        'AI_Voip50' => [
            'item_id' => 106,
            'features' => []
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->features as $value) {
            foreach ($value['features'] as $feature) {
                $itemFeature = new ItemFeature();
                $itemFeature->item_id = $value['item_id'];
                $itemFeature->text = $feature;
                $itemFeature->price_label = isset($value['price_label']) ? $value['price_label'] : null;
                $itemFeature->price = isset($value['price']) ? $value['price'] : null;
                $itemFeature->save();
            }
        }
    }
}
