<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Service;
use App\Models\ServiceHasItem;
use App\Models\ServiceType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ServiceSeeder extends Seeder
{
    public $services = [];

    public function __construct()
    {
        $this->services = [
            [
                'name' => 'Fibra',
                'type' => 'Servizio',
                'items' => array_merge( range(1, 4), range(7,9), [74, 75, 162, 163]),
                'icon' => 'fibra',
                'description' => 'La Fibra Ottica è la connessione per eccellenza per la banda ultra larga. La rete punto-punto consente una velocità, continuità ed espandibilità della connessione senza precedenti.',
                'meta_title' => 'Fibra: FTTC o FTTH? scopri cosa fa per te! | Fiberdroid',
                'meta_description' => "Scopri insieme a Fiberdroid tutto sulla fibra ottica e come scegliere la connettività più adatta alle tue esigenze! Fiberdroid la connessione per il tuo Business!",
                'termini' => "<p>
    Costo di attivazione del servizio fibra FTTC: <i>79.90&euro;</i>
    <br>
    Costo di attivazione del servizio fibra FTTH: <i>159.90&euro;</i>
    <br>
    Costo di disattivazione del servizio: <i>99.90&euro;</i>
    <br>
    Possibilità di acquisto pacchetti ip statici pubblici aggiuntivi.
    <br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br>
    Ogni servizio è attivabile previa <a href='". url('/verifica-copertura') ."'>verifica tecnica della copertura</a>.
</p>"
            ],
            [
                'name' => 'Fibra dedicata',
                'type' => 'Servizio',
                'items' => [],
                'icon' => 'fibra',
                'meta_title' => 'Fibra dedicata ILC: Scopri tutto sulla fibra ottica più veloce',
                'meta_description' => "Soluzioni in Fibra Ottica Dedicata, scopri tutto sulla connettività più veloce, stabile e con banda garantita."
            ],
            [
                'name' => 'Reti wan',
                'type' => 'Servizio',
                'items' => [],
                'meta_title' => "Reti WAN: la connessione tra più sedi aziendali",
                'meta_description' => "Reti WAN: la soluzione quando la tua azienda si espande. Per usare applicazioni e condividere dati  come se fossi in un'unica sede. In totale sicurezza.",
            ],
            [
                'name' => 'Router',
                'type' => 'Prodotto',
                'items' => [],
                'icon' => 'voip',
                'meta_title' => 'Router: le nostre scelte: Draytek e AVM Fritz!Box',
                'meta_description' => "Scegli l'apparato che vuoi, oppure acquistalo o noleggialo da noi: avrai assistenza tecnica anche sull'hardware oltre che sulla connessione. Scopri le offerte!"
            ],
            [
                'name' => 'Centralino voip',
                'type' => 'Servizio',
                'items' => [],
                'meta_title' => 'Centralino VoIP',
                'meta_description' => 'Centralino VoIP – Vantaggi rispetto a Centralino tradizionale o Centralino Cloud – Offerta Fiberdroid: la soluzione studiata su misura per te'
            ],
            [
                'name' => 'Cloud wifi',
                'type' => 'Servizio',
                'items' => [],
                'meta_title' => 'Cloud Wi-Fi',
                'meta_description' => "Cloud Wi-Fi – La soluzione di rete scalabile e semplificata – Antenne Wi-Fi gestite da un'unica piattaforma nel nostro data center."
            ],
            [
                'name' => 'Telefoni voip',
                'type' => 'Prodotto',
                'items' => [],
                'icon' => 'voip',
                'meta_title' => "Voip: riduci i costi telefonici con in più l'assistenza Fiberdroid",
                'meta_description' => "Telefoni Voip.  La telefonia di nuova generazione, ti basta una connessione dati attiva e comunichi con il resto del mondo. Porta il tuo ufficio con te ovunque.",
                'termini' => ""
            ],
            [
                'name' => 'Satellitare',
                'type' => 'Servizio',
                'items' => array_merge(range(17, 23)),
                'icon' => 'satellitare',
                'meta_title' => "Internet Satellitare",
                'meta_description' => "Internet Satellitare: velocità e sicurezza anche senza Fibra o ADSL. Fiberdroid propone soluzioni fino a 30 mega in upload o fino a 50 mega in download!",
                'termini' => "<p>
    Costo di attivazione del servizio satellitare: <i>469.00&euro;</i>
    <br>
    Costo di disattivazione del servizio: <i>99.90&euro;</i>
    <br>
    Installazione certificata 389,00&euro;
    <br>
    Kit antigelo per parabola satellitare 149,00&euro;
    <br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br>
    Ogni servizio è attivabile previa <a target="_blank" href="{{ url('verifica-copertura') }}">verifica tecnica della copertura</a>.
</p>"
            ],
            [
                'name' => 'Voip',
                'type' => 'Servizio',
                'items' => range(24,31),
                'icon' => 'voip',
                'meta_title' => "Voip: cos'è vantaggi e tariffe",
                'meta_description' => "Proietta la Tua Azienda verso il futuro con Fiberdroid! Abbandona le tradizionali tecnologie telefoniche! Acquista un nostro servizio VoIP!",
                'termini' => '
    Costo di attivazione di un numero voip singolo portato o nuovo: <i>19.90&euro;</i>
    <br>
    Costo di attivazione del GNR 10 Numeri portato o nuovo: <i>29.90&euro;</i>
    <br>
    Costo di attivazione del GNR 20, 30 Numeri nuovi: <i>39.90&euro;</i>
    <br>
    Costo di attivazione del GNR 50 Numeri nuovo: <i>49.90&euro;</i>
    <br>
    Costo di attivazione del GNR 100 Numeri portato o nuovo: <i>69.90&euro;</i>
    <br>
    Costo di attivazione del GNR 200 Numeri nuovo: <i>79.90&euro;</i>
    <br>
    Costo di attivazione del GNR 1000 Numeri portato o nuovo: <i>250.00&euro;</i>
    <br>
    Telefonate in tutta l\'<b>Unione Europa</b> verso fissi a <b>0,007 Euro/Min</b> con conteggio al secondo e senza scatto
    <br>
    Telefonate in tutta l\'<b>Unione Europa</b> verso mobili a <b>0,065 Euro/Min</b> con conteggio al secondo e senza scatto
    <br>
    Costo di disattivazione del servizio: 19.90€
    <br>
    Tutti gli importi sono espressi in euro iva esclusa.',
                ],
            [
                'name' => 'Wireless',
                'type' => 'Servizio',
                'items' => array_merge(range(41, 43), [81, 80], range(69, 73)),
                'icon' => 'wireless',
                'meta_title' => 'Wireless: digital divide addio! prova la soluzione di Fiberdroid!',
                'meta_description' => 'Offerte Wireless per aziende e liberi professsionisti – Connettiti all rete anche dove l\'Adsl o la Fibra ottica non arrivano – Alte prestazioni e capacità di banda',
                'termini' => '
Costo di attivazione del servizio wireless 30, 50, 100 Mega: <i>169.90&euro;</i>

<br>
Costo di attivazione del servizio LTE 30, 50 Mega: <i>199.90&euro;</i>
<br>
Costo di attivazione del servizio 4G/4G Flat: <i>199.90&euro;</i>
    <br>
    Costo di disattivazione del servizio: 199.90€
    <br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br>
    Ogni servizio è attivabile previa <a target="_blank" href="{{ url('verifica-copertura') }}">verifica tecnica della copertura</a>.',
            ],
            [
                'name' => 'Centralino cloud',
                'type' => 'Servizio',
                'items' => array_merge([63, 44, 45], range(56, 58)),
                'meta_title' => 'Centralino cloud',
                'meta_description' => 'Centralino Cloud - Soluzione per PMI, ristorazione, strutture recettive e smartworker – Tutti i vantaggi dell\'offerta Fiberdroid',
                'termini' => 'Costo di attivazione del servizio Cloud VoIp: <i>199.00&euro;</i>
 <br>
 Costo di disattivazione del servizio: <i>99.90&euro;</i>
    <br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br>
    Ogni servizio è attivabile previa <a target="_blank" href="{{ url('verifica-copertura') }}">verifica tecnica della copertura</a>.',
                ],
            [
                'name' => 'Adsl',
                'type' => 'Servizio',
                'items' => [10,11],
                'icon' => 'adsl',
                'meta_title' => "Adsl: offerta linea veloce, sicura e disponibile di Fiberdroid",
                'meta_description' => "Offerte Adsl Fiberdroid per aziende e liberi professionisti. Verifica la copertura nella tua zona. ADSL: veloce e stabile dove la fibra non è presente",
                'termini' => "<p>
    Costo di attivazione del servizio ADSL: <i>79.90&euro;</i>
    <br>
    Costo di disattivazione del servizio: <i>99.90&euro;</i>
    <br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br>
    Ogni servizio è attivabile previa <a target="_blank" href="{{ url('verifica-copertura') }}">verifica tecnica della copertura</a>.
</p>"
                ],
            [
                'name' => 'Hdsl',
                'type' => 'Servizio',
                'items' => range(12, 16),
                'icon' => 'hdsl',
                'meta_title' => "Hdsl: linea veloce e  minima latenza senza fibra",
                'meta_description' => "Offerte  HDSL Fiberdroid per aziende e liberi professionisti. Verifica la copertura nella tua zona. HDSL: veloce, stabile, simmetrica, dove la fibra non è presente",
                'termini' => "<p>
    Costo di attivazione del servizio HDSL 2 Mega: <i>159.90&euro;</i>
    <br>
    Costo di attivazione del servizio HDSL 4 Mega: <i>199.90&euro;</i>
    <br>
    Costo di attivazione del servizio HDSL 8 Mega: <i>299.90&euro;</i>
    <br>
    Costo di attivazione del servizio HDSL 10 Mega e 16 Mega: <i>399.90&euro;</i>
    <br>
    Costo di disattivazione del servizio: <i>199.00&euro;</i>
    <br>
    Tutti gli importi sono espressi in euro iva esclusa.
    <br>
    Ogni servizio è attivabile previa <a target="_blank" href="{{ url('verifica-copertura') }}">verifica tecnica della copertura</a>.
</p>"
            ],
            [
                'name' => 'Access point',
                'type' => 'Prodotto',
                'items' => [],
                'meta_title' => "Access point",
                'meta_description' => "In Fiberdroid usiamo solo Access Point Ubiquiti, una delle migliori marche in commercio, e solo AC dual band per garantirti con i 5GHz  la miglior stabilità",
                'icon' => 'hdsl'
            ],
            [
                'name' => 'Fiber AI',
                'type' => 'Servizio',
                'items' => [93, 85, 84, 112, /*113,*/ 114, /*115, 116, 117, 118, 119, 120*/],
                'meta_title' => "Fiber AI",
                'meta_description' => "In Fiberdroid usiamo solo Access Point Ubiquiti, una delle migliori marche in commercio, e solo AC dual band per garantirti con i 5GHz  la miglior stabilità",
                'icon' => 'hdsl',
                'termini' => 'Tutti gli importi sono espressi in euro iva esclusa. <br> Ogni servizio è attivabile previa verifica tecnica.'
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->services as $service) {

            $type = ServiceType::where('name', 'like', $service['type'])->value('id');

            $servizio = new Service();
            $servizio->name = $service['name'];
            $servizio->description = "cristo";
            $servizio->url = Str::slug($service['name']);
            $servizio->service_type_id = $type;
            $icon = isset($service['icon']) ? $service['icon'] : "adsl";
            $servizio->image_id = Image::getId($icon);
            $servizio->termini = isset($service['termini']) ? $service['termini'] : null;
            $servizio->meta_description = isset($service['meta_description']) ? $service['meta_description'] : null;
            $servizio->meta_title = isset($service['meta_title']) ? $service['meta_title'] : null;
            $servizio->save();

            foreach ($service['items'] as $itemId) {
                $rel = new ServiceHasItem();
                $rel->service_id = $servizio->id;
                $rel->item_id = $itemId;
                $rel->save();
            }
        }
    }

}
