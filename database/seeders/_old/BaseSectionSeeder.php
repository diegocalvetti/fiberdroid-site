<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Section;
use Illuminate\Database\Seeder;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Blade;
use ReflectionException;

abstract class BaseSectionSeeder extends Seeder
{

    protected $baseDataPath = "/";

    protected function setBaseDataPath($base) {
        $this->baseDataPath = $base;
    }

    public function jsonFromData($key) {
        $key = str_replace("\\", "/", $key);
        return include base_path("database/seeders/data$this->baseDataPath/$key.php");
    }

    public function html($key) {
        $key = str_replace("\\", "/", $key);
        $file_html = base_path("database/seeders/data$this->baseDataPath/$key.html");
        $file_php = base_path("database/seeders/data$this->baseDataPath/$key.php");

        if( file_exists($file_php) ) {
            ob_start();
            include $file_php;
            $html = ob_get_clean();
        } else {
            $html = file_get_contents($file_html);
        }
        $html = str_replace("\\\"", "\"", $html);
        return str_replace("\'", "'", $html);
    }



    public function introText($title, $text, $extra, $level = "any", $anchor = true) {

        $sectionFlag = [
            $level == "sub" || $level == "third",
            $level == "third",
        ];

        return [
            'is_anchor' => $anchor,
            'is_intro' => true,
            'title' => $title,
            'content' => $text,
            'subsection' => $sectionFlag[0],
            'thirdsection' => $sectionFlag[1],
            'extra' => $extra,
        ];
    }

    public function text($title, $text, $extra, $level = "any", $anchor = true) {
        $sectionFlag = [
            $level == "sub" || $level == "third",
            $level == "third",
        ];

        return [
            'is_anchor' => $anchor,
            'is_intro' => false,
            'title' => $title,
            'content' => $text,
            'subsection' => $sectionFlag[0],
            'thirdsection' => $sectionFlag[1],
            'extra' => $extra,
        ];
    }

    public function void($level = "any", $anchor = false) {
        $sectionFlag = [
            $level == "sub" || $level == "third",
            $level == "third",
        ];

        return [
            'is_anchor' => $anchor,
            'is_intro' => false,
            'title' => "",
            'content' => "",
            'subsection' => $sectionFlag[0],
            'thirdsection' => $sectionFlag[1],
            'extra' => "",
        ];
    }

    /**
     * @param $model
     * @param string $modelField
     * @throws ReflectionException
     */
    protected function seed($model, $modelField = "name") {
        foreach ($this->sections as $serviceName => $value) {
            $service = $model::where($modelField, $serviceName)->first();

            $lastFirstLevelSection = null;
            $lastSecondLevelSection = null;

            foreach ($value as $item) {
                $section = new Section();
                $section->model_id = $service->id;
                $section->model_type = (new \ReflectionClass($service))->getName();
                $section->is_intro = $item['is_intro'];
                $section->is_anchor = $item['is_anchor'];
                $section->title = $item['title'];
                $section->content = $item['content'];
                $section->image = isset($item['extra']['img']['src']) ? json_encode($item['extra']['img']['src'], true) : null;
                $section->image_id = isset($item['extra']['image']['alias']) ? Image::getId($item['extra']['image']['alias']) : null;
                $section->json_accordions = isset($item['extra']['accordions']) ? json_encode($item['extra']['accordions']) : null;
                $section->json_cards = isset($item['extra']['cards']) ? json_encode($item['extra']['cards'], true) : null;
                $section->section_id = $item['subsection'] ? $item['thirdsection'] ? $lastSecondLevelSection : $lastFirstLevelSection : null;
                $section->save();


                if( !$item['subsection'] ) {
                    $lastFirstLevelSection = $section->id;
                }
                if( !$item['thirdsection'] ) {
                    $lastSecondLevelSection = $section->id;
                }
            }

        }
    }
}
