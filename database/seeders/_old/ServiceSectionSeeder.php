<?php

namespace Database\Seeders;

use App\Models\Section;
use App\Models\Service;
use Illuminate\Database\Seeder;
use ReflectionException;

class ServiceSectionSeeder extends BaseSectionSeeder
{

    public $sections;

    public function __construct()
    {

        $this->setBaseDataPath("/servizi");

        $this->sections = [
            'Fibra' => [
                $this->introText("Fibra", $this->html("fibra/intro"),"", "first", false),
                $this->text("", $this->html("fibra/preAnchor"), "", "first",  false),
                $this->text("Quali sono le possibilità per casa e ufficio?", "", "", "first", true),
                $this->text("FTTC", $this->html("fibra/fttc"),  "", "sub", true),
                $this->text("FTTH", $this->html("fibra/ftth"),  "", "sub", true),
            ],
            'Fibra dedicata' => [
                $this->introText("Fibra ottica dedicata ILC", $this->html("fibra-dedicata/intro"), "", "first", false),
                $this->text("", "", "", "first", false),
                $this->text("Fibra Ottica Dedicata ILC: come funziona", $this->html("fibra-dedicata/come_funziona"), "", "first", true),
                $this->text("Differenza tra Fibra Dedicata ILC e Fibra FTTH", $this->html("fibra-dedicata/dedicata_vs_ftth"), "", "sub", true),
                $this->text("Vantaggi", $this->html("fibra-dedicata/vantaggi"), "", "first", true),
                $this->text("Perchè scegliere Fiberdroid", $this->html("fibra-dedicata/perche"), "", "first", true),
                $this->text("Offerte Fibra Dedicata ILC Fiberdroid", $this->html("fibra-dedicata/soluzioni"), "", "first", true),
                $this->text("FAQ, Le domandi più frequenti riguardo la fibra dedicata ILC", "", $this->jsonFromData('fibra-dedicata/faq'), "first", true),
            ],
            'Hdsl' => [
                $this->introText("Hdsl", $this->html("hdsl/intro"), "", "first", false),
                $this->text("", $this->html("hdsl/preAnchor"), "", "first", false),
                $this->text("Offerte per aziende e liberi professionisti", $this->html("hdsl/offerte"), ""),
                $this->text("HDSL Caratteristiche", $this->html("hdsl/caratteristiche"), ""),
                $this->text("Velocità fino a 16Mb/s", $this->html("hdsl/16mb"), "", "sub"),
                $this->text("Solo traffico dati", $this->html("hdsl/traffico"), "", "sub"),
                $this->text("Connessione simmetrica", $this->html("hdsl/connessione_simmetrica"), ""),
                $this->text("Linea dedicata non condivisa con altri utenti", $this->html("hdsl/dedicata"), ""),
                $this->text("A chi conviene l'HDSL?", $this->html("hdsl/conviene"), ""),
                $this->text("I vantaggi dell'HDSL", $this->html("hdsl/vantaggi"), ""),
                $this->text("Requisiti minimi per l’uso dell’hdsl", $this->html("hdsl/requisiti"), "")
            ],
            'Adsl' => [
                $this->introText("Adsl", $this->html('adsl/intro'), "", "first", false),
                $this->void(),
                $this->text("I vantaggi dell'ADSL", $this->html("adsl/vantaggi"), ""),
                $this->text("Adsl significato", $this->html("adsl/significato"), ""),
                $this->text("Adsl come funziona", $this->html("adsl/come_funziona"), "", "sub"),
                $this->text("Il Modem/Router", $this->html("adsl/modem-router"), "", "sub"),
                $this->text("Modem", $this->html("adsl/modem"), "", "third"),
                $this->text("Router", $this->html("adsl/router"), "", "third"),
                $this->text("L'ADSL è sicura?", $this->html("adsl/sicura"), "", "sub"),
                $this->text("Perchè Asimmetrica?", $this->html("adsl/asimmetrica"), "", "sub"),
                $this->text("La banda della mia linea ADSL viene condivisa con altri utenti?", $this->html("adsl/asimmetrica"), "", "sub"),
                $this->text("Velocità di connessione - Speed test", $this->html("adsl/speedtest"), "", "sub"),
                $this->text("Fattori che incidono sulla velocità di connessione", $this->html("adsl/velocita"), "", "sub"),
                $this->text("Quanti tipi di DSL ci sono?", $this->html("adsl/tipi_dsl"), ""),
                $this->text("H-DSL (High-bit-rate Digital Subscriber Line)", $this->html("adsl/hdsl"), "", "sub"),
                $this->text("V-DSL (Very high Digital Subscriber Line)", $this->html("adsl/vdsl"), "", "sub"),
                $this->text("Requisiti minimi per l'uso dell'adsl", "<ul class=\"\&quot;ul1\&quot;\"><li class=\"\&quot;li4\&quot;\"><span class=\"\&quot;s1\&quot;\">Disponibilit&agrave; della rete ADSL nella Tua zona, </span><span class=\"\&quot;s6\&quot;\">verificalo mediante il nostro tool</span></li><li class=\"\&quot;li4\&quot;\"><span class=\"\&quot;s1\&quot;\">Un Router di ultima generazione ADSL/ADSL 2+ (non brandizzato con logo di altro operatore)</span></li></ul>", ""),
                ],
            'Reti wan' => [
                $this->introText("Reti wan: quando la tua azienda si espande", $this->html("reti-geo/intro"), "", "first", false),
                $this->void(),
                $this->text("Rete man o rete metropolitana, per l'azienda che apre più sedi nella stessa città", $this->html("reti-geo/man_vs_metro"), "", "first", true),
                $this->text("Vantaggi delle reti man/wan – la proposta di fiberdroid", $this->html("reti-geo/vantaggi"), "", "first", true),
                $this->text("Come lavora fiberdroid, il tuo partner unico", $this->html("reti-geo/partner"), "", "first", true),
                $this->text("Glossario reti", "", $this->jsonFromData("reti-geo/glossario"), "first", true),
            ],
            'Router' => [
                $this->introText("Router", $this->html("router/intro"), "", "first", false),
                $this->void(),
                $this->text("Ti presentiamo DrayTek", $this->html("router/draytech"), "", "first", true),
                $this->text("Ti presentiamo Avm", $this->html("router/avm"), "", "first", true),
                $this->text("Router/modem - Significato", $this->html("router/significato"), $this->jsonFromData("router/significato_extra"), "first", true),
                $this->text("Tariffe e soluzioni", "", $this->jsonFromData("router/soluzioni"), "first", false),
                $this->text("", $this->html("router/description"), "", "first", false),
            ],
            'Cloud wifi' => [
                $this->introText("Cloud wifi", $this->html("cloud-wifi/intro"), "", "first", false),
                $this->void(),
                $this->text("Cloud Wi-Fi: semplice e economico", $this->html("cloud-wifi/semplice"), "", "first", true),
                $this->text("Vantaggi", $this->html("cloud-wifi/vantaggi"), "", "first", true),
                $this->text("Perché scegliere Fiberdroid", $this->html("cloud-wifi/perche"), "", "first", true),
                $this->text("Offerte Cloud Controller Fiberdroid", "", $this->jsonFromData("cloud-wifi/offerte"), "first", true),
                $this->text("", $this->html("cloud-wifi/description"), "", "first", false),
                $this->text("Wi-Fi", $this->html("cloud-wifi/wifi"), "", "first", true),
            ],
            'Telefoni voip' => [
                $this->introText("Telefoni voip", $this->html("telefoni-ip/intro"), "", "first", false),
                $this->void(),
                $this->text("Come funziona un telefono Voip", $this->html("telefoni-ip/come_funziona"), "", "first", true),
                $this->text("Protocolli di comunicazione", $this->html("telefoni-ip/protocolli"), "", "first", true),
                $this->text("Tipologie di telefoni VoIP", $this->html("telefoni-ip/tipologie"), $this->jsonFromData("telefoni-ip/tipologie_extra"), "first", true),
                $this->text("Funzionalità di un telefono VoIP", $this->html("telefoni-ip/funzionalita"), "", "first", true),
                $this->text("Vantaggi di un telefono Voip", "", $this->jsonFromData("telefoni-ip/vantaggi"), "first", true),
                $this->text("Produttori - Certificazioni Fiberdroid", $this->html("telefoni-ip/certificazioni"), $this->jsonFromData("telefoni-ip/certificazioni_extra"), "first", true),
                $this->text("", $this->html("telefoni-ip/conclusion"), "", "first", false),
                $this->text("Tariffe e soluzioni", "", $this->jsonFromData("telefoni-ip/soluzioni"), "first", false),
                $this->text("", $this->html("telefoni-ip/description"), "", "first", false),
            ],
            'Satellitare' => [
                $this->introText("Internet satellitare", $this->html("satellitare/intro"), "", "first", false),
                $this->text("Cosa ti offre internet satellitare di fiberdroid", $this->html("satellitare/cosa_offre"), "", "sub", false),
                $this->text("Personalizza la tua offerta Internet satellitare 2021", $this->html("satellitare/personalizza"), "", "first", true),
                $this->text("Internet satellitare come funziona", "", $this->jsonFromData("satellitare/come_funziona"), "first", true),
            ],
            'Voip' => [
                $this->introText("Voip", $this->html("voip/intro"), "", "first", false),
                $this->text("", $this->html("voip/preAnchor"), "", "first", false),
                $this->text("Come funziona la tecnologia VoIp?", "", ""),
                $this->text("Rete telefonica tradizionale – Commutazione di circuito", $this->html("voip/rete-tradizionale"), "", "sub"),
                $this->text("VoIP – Commutazione di pacchetto", $this->html("voip/commutazione-pacchetti"), "", "sub"),
                $this->text("Vantaggi del voip", $this->html("voip/vantaggi"), ""),
                $this->text("Requisiti minimi", $this->html("voip/requisiti"), ""),
                $this->text("Number portability", $this->html("voip/portability"), ""),
                $this->text("Numerazioni raggiunte", $this->html("voip/numerazioni"), ""),
            ],
            'Wireless' => [
                $this->introText("Wireless", $this->html("wireless/intro"), "", "first", false),
                $this->text("Ponte Radio (Wi-Max / LTE)",  $this->html("wireless/ponte-radio"), ""),
                $this->text("Mobile 4G",  $this->html("wireless/4g"), ""),
            ],
            'Centralino cloud' => [
                $this->introText("Centralino cloud", $this->html("centralino-cloud/intro"), "", "first", false),
                $this->void(),
                $this->text("Centralini di nuova generazione: un chiarimento", $this->html("centralino-cloud/chiarimento"), "", "first", true),
                $this->text("Centralino VoIP o on premise", $this->html("centralino-cloud/c_voip"), "", "sub", true),
                $this->text("Centralino Cloud o Cloud PBX o Virtuale", $this->html("centralino-cloud/c_cloud"), "", "sub", true),
                $this->text("Vantaggi della soluzione Cloud", "", "", "first", true),
                $this->text("Taglio dei costi", $this->html("centralino-cloud/costi"), "", "sub", true),
                $this->text("La domotica", $this->html("centralino-cloud/domotica"), "", "sub", true),
                $this->text("Principali Funzionalità del Centralino Cloud", $this->html("centralino-cloud/funzionalita"), "", "first", true),
                $this->text("Perché scegliere Fiberdroid", $this->html("centralino-cloud/perche_fiberdroid"), "", "first", true),
                $this->text("Requisiti Minimi", $this->html("centralino-cloud/requisiti"), "", "first", true),

            ],
            'Centralino voip' => [
                $this->introText("Centralino VoIP", $this->html("centralino-voip/intro"), "", "first", false),
                $this->void(),
                $this->text("Cosa è un centralino telefonico", $this->html("centralino-voip/cosa_e"), ""),
                $this->text("Evoluzione del Centralino telefonico", $this->html("centralino-voip/evoluzione"), ""),
                $this->text("Principali Funzionalità del Centralino VoIP Fiberdroid", $this->html("centralino-voip/funzionalita"), "", "sub"),
                $this->text("Vantaggi del Centralino VoIP", "", ""),
                $this->text("Rispetto ad una centrale tradizionale", $this->html("centralino-voip/vantaggi_tradizionale"), "", "sub"),
                $this->text("Rispetto ad una soluzione in cloud", $this->html("centralino-voip/vantaggi_cloud"), "", "sub"),
                $this->text("Perché scegliere Fiberdroid", $this->html("centralino-voip/perche_fd"), ""),
                $this->text("Come acquisto un Centralino VoIP Fiberdroid?", "", $this->jsonFromData("centralino-voip/come_acquisto")),
                $this->text("Cosa include il nostro Centralino VoIP", $this->html("centralino-voip/cosa_include"), ""),
            ],
            'Access point' => [
                $this->introText("Access point", $this->html('antenne/intro'), "", "first", false),
                $this->text("Offerte Antenne Fiberdroid", "", $this->jsonFromData("antenne/offerte_antenne"), "first", false),
                $this->text("Cos’è un access point", $this->html('antenne/cose'),  $this->jsonFromData('antenne/cose_extra'), "first"),
                //$this->text("Perché usare un access point", $this->html('antenne/perche'), "", "sub"),
                //$this->text("Come configurare un access point", $this->html('antenne/configurare'), "", "sub"),
                $this->text("Come scegliere un access point", $this->html('antenne/scegliere'), "", "first"),
                $this->text("Tipi di access point", $this->html('antenne/tipi'), $this->jsonFromData('antenne/tipi_extra'), "sub"),
            ],
            'Fiber AI' => [
                    $this->introText("Fiber AI", $this->html('ai/intro'), "", "first", false),
                    $this->text("FiberBot", $this->html("ai/workbot"), "", "first"),
                    $this->text("Opzione DHI", $this->html("ai/dhi"), "", "sub"),
                    $this->text("Integrazione API", $this->html("ai/api"), "", "sub"),
                    $this->text("Fiber AI Plus", $this->html("ai/open-ai"), "", "first"),
                    $this->text("Integrazione API", $this->html("ai/api"), "", "sub"),
            ]
        ];
    }


    /**
     * Run the database seeds.
     *
     * @return void
     * @throws ReflectionException
     */
    public function run()
    {
        $this->seed("App\Models\Service");
    }
}
