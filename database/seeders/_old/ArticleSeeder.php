<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Image;
use App\Models\Service;
use App\Models\ServiceHasItem;
use App\Models\ServiceType;
use App\Models\Solution;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ArticleSeeder extends Seeder
{
    public $articles = [];

    public function __construct()
    {
        $this->articles = [
            [
                'title' => "Smart Working, perchè non smettere.",
                'meta_title' => "Smart Working, perchè non smettere.",
                'meta_description' => 'Durante questo periodo difficile, ci siamo dovuti rendere conto dell\'importanza e della fortuna che abbiamo di vivere in un mondo così "digitalizzato".',
                'photo' => 'blog-smart-working'
            ],
            [
                'title' => "Fibra ottica, come imparare a sceglierla.",
                'meta_title' => "Fibra ottica, come imparare a sceglierla.",
                'meta_description' => 'Al giorno d\'oggi in cui internet è parte fondamentale delle nostre vite, c\'è la necessità di essere sempre connessi alla rete.',
                'photo' =>'blog-fibra'
            ],
            [
                'title' => "4G e Ponti radio, come funziona il wireless senza fili.",
                'meta_title' => "4G e Ponti radio, come funziona il wireless senza fili.",
                'meta_description' => 'Con wireless dall\'inglese senza fili, si indica una grande famiglia che comprende tutte le tecnologie di comunicazioni tra due dispositivi che non usano cavi fisici.',
                'photo' =>'blog-ponti-radio'
            ]
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->articles as $article) {
            $newArticle = new Article();
            $newArticle->title = $article['title'];
            $newArticle->url = Str::slug($newArticle->title);
            $image = $article['photo'] ?? "blog-smart-working";
            $newArticle->image_id = Image::getId($image);
            $newArticle->meta_description = isset($article['meta_description']) ? $article['meta_description'] : null;
            $newArticle->meta_title = isset($article['meta_title']) ? $article['meta_title'] : null;
            $newArticle->save();
        }
    }

}
