<?php

namespace Database\Seeders;

use App\Models\Section;
use App\Models\Service;
use App\Models\Solution;
use Illuminate\Database\Seeder;
use ReflectionException;

class ArticleSectionSeeder extends BaseSectionSeeder
{

    public $sections;

    public function __construct()
    {

        $this->setBaseDataPath("/blog");

        $this->sections = [
            'Smart Working, perchè non smettere.' => [
                $this->introText("Smart Working, perchè non smettere.", $this->html("smart/intro"), "", "first", false),
                $this->text("Smart working, cos'è?", $this->html("smart/cosa-e"), "", "first", true),
                $this->text("Smart working definizione", $this->html("smart/definizione"), "", "sub", true),
                $this->text("Smart working, per quanto tempo ancora?", $this->html("smart/quanto-tempo"), "", "first", true),
                $this->text("Strumenti per smart working", $this->html("smart/strumenti"), "", "first", true),
                $this->text("Cosa serve a una azienda per uno smart working efficiente?", $this->html("smart/regola"), "", "sub", true),
                $this->text("Come creare una strategia efficace di smart working?", $this->html("smart/strategia"), "", "sub", true),
                $this->text("Smart working, conviene davvero?", $this->html("smart/conviene"), "", "first", true),
                $this->text("Vantaggi e svantaggi dello smart working in breve", $this->html("smart/vantaggi"), "", "sub", true),
                $this->text("Vantaggi per le aziende", $this->html("smart/vantaggi-aziende"), "", "third", true),
                $this->text("Vantaggi per i lavoratori", $this->html("smart/vantaggi-lavoratori"), "", "third", true),
                $this->text("Smart working in Italia", $this->html("smart/italia"), "", "first", true),
                $this->text("Lo smart working secondo noi di Fiberdroid", $this->html("smart/fiberdroid"), "", "first", true),
            ],
            'Fibra ottica, come imparare a sceglierla.' => [
                $this->introText("Fibra ottica, come imparare a sceglierla.", $this->html("fibra/intro"), "", "first", false),
                $this->text("Che cos'è la fibra ottica?", $this->html("fibra/cose"), "", "first", true),
                $this->text("Fibra ottica vs adsl tradizionale, le differenze.", $this->html("fibra/diff"), $this->jsonFromData("fibra/diff_extra"), "first", true),
                $this->text("", $this->html("fibra/diff_end"), "", "first", false),
                $this->text("Fibra ottica, costa davvero così tanto?", $this->html("fibra/costo"), "", "first", true),
                $this->text("fttc", $this->html("fibra/fttc"), "", "sub", true),
                $this->text("ftth", $this->html("fibra/ftth"), "", "sub", true),
                $this->text("Vantaggi e svantaggi della fibra", $this->html("fibra/vantaggi"), $this->jsonFromData("fibra/vantaggi_extra"), "first", true),
                $this->text("Storia della fibra ottica", $this->html("fibra/storia"), "", "first", true),
                $this->text("Storia della fibra ottica in Italia", $this->html("fibra/in-italia"), "", "sub", true),
                $this->text("Come funziona la fibra ottica?", $this->html("fibra/come"), "", "first", true),
                $this->text("Come si trasmette un dato con la luce?", $this->html("fibra/come-luce"), "", "sub", true),
            ],
            '4G e Ponti radio, come funziona il wireless senza fili.' => [
                $this->introText("4G e Ponti radio, come funziona il wireless senza fili.", $this->html("4g/intro"), "", "first", false),
                $this->text("Tipologie di connessioni wireless", "", $this->jsonFromData("4g/tipologie_extra"), "first", true),
                $this->text("", $this->html('4g/postintro'), "", "first", false),
                $this->text("Connettività con ponti radio", $this->html('4g/ponti'), "", "first", true),
                $this->text("Connettività tramite 4G", $this->html('4g/4g'), "", "first", true),
                $this->text("4G Funzionamento e un po' di storia.", $this->html('4g/storia'), "", "first", true),

            ]
        ];
    }


    /**
     * Run the database seeds.
     *
     * @return void
     * @throws ReflectionException
     */
    public function run()
    {
        $this->seed("App\Models\Article", 'title');
    }
}
