<?php

return [

    'accordions' => [
        'Connessione tramite onde radio' => [
            'text' => "<p>Onde elettromagnetiche propagate nell'aria dai ripetitori.</p>",
            'isActive' => true
        ],
        'Connessione tramite infrarossi' => [
            'text' => "<p>Tecnologia IrDa per propagazione a laser infrarossi su brevi distanze. Utilizzata principalmente per mouse, tastiere, stampanti e codici qr o simili.</p>",
            'isActive' => true
        ]
    ]
];