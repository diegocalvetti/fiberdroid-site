<p><span class="\&quot;s1\&quot;"><strong>il decreto emergenza finir&agrave; 15 ottobre 2020 o sar&agrave; prorogato?</strong> </span><span class="\&quot;s1\&quot;"><strong>verr&agrave; approvata una legge che incoraggi lo smart working?</strong></span></p>
<p>A causa della proroga al 15 ottobre 2020 della fine dello stato di emergenza, &egrave; ancora possibile accedere alla forma di smart working semplificata, in particolare l'utilizzo del lavoro agile &egrave; stato prorogato:</p>
<table class="table">
    <tbody class="is-striped">
        <tr>
            <td>
                fino al 14 settembre
            </td>
            <td>
                Per igenitorilavoratori dipendenti che hannoalmeno un figlio minore di anni 14.
            </td>
        </tr>
        <tr>
            <td>
                fino al 15 ottobre
            </td>
            <td>
                Per tutti il gli altri lavoratori. In particolare quelli più esposti a una situazione di rischio di contagio da coronavirus, in seguito a una visita medica e in ragione dell'età, delle patologie pregresseo in situazioni di maggiore rischiosità certificate dal medico.
            </td>
        </tr>
    </tbody>
</table>

<p>Per saperne di pi&ugrave; sulla proroga del decreto che semplifica lo smart working e lo permette indipendentemente dall'accordo tra le parti ti invitiamo a leggere qui da il sole 24 ore:</p>
<p><a href="https://www.ilsole24ore.com/art/smart-working-chi-puo-farlo-fino-meta-ottobre-e-chi-deve-interromperlo-l-inizio-scuola-ADjs4Di?refresh_ce=1" target="_blank" rel="noopener">https://www.ilsole24ore.com/art/smart-working-chi-puo-farlo-fino-meta-ottobre-e-chi-deve-interromperlo-l-inizio-scuola</a></p>
<p>Ti alleghiamo anche il documento ufficiale rilasciato dalla camera riguardante le misure adottate per fronteggiare l'emergenza coronavirus in italia:</p>
<p><a href="https://www.camera.it/temiap/documentazione/temi/pdf/1203754.pdf?_1588279335853" target="_blank" rel="noopener">Scarica pdf della camera</a></p>
<p>Quello che noi di <strong>Fiberdroid</strong> ci auguriamo &egrave; che la pratica dello smart working non venga dimenticata ma anzi incentivata almeno dove il lavoro si presta bene ad essere svolto in modalit&agrave; agile.&nbsp; Come potrai leggere sotto abbiamo creato una lista di possibili vantaggi e svantaggi derivanti da questa modalit&agrave; di lavoro e una <strong>piccola guida per imparare a conoscere meglio lo smart working</strong> e cosa ne consegue.</p>
