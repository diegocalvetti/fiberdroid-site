<p>Ai sensi del DPCM del 1&deg; marzo 2020 &egrave; possibile, se si soddisfano i requisiti accedere a una procedura semplificata per favorire il ricorso alla modalit&agrave; di lavoro agile, fino al termine dello stato di emergenza. In questo modo &egrave; possibile ricorrere al lavoro agile anche se non previsto dal contratto, a parit&agrave; di diritti con i colleghi non agili.&nbsp;</p>
<p>In questo caso il datore di lavoro sar&agrave; tenuto a scrivere un accordo che presenti almeno i requisiti minimi di seguito elencati</p>
<ol>
    <ol>
        <li>Esecuzione della mansione fuori dall'ufficio, con riguardo agli strumenti tecnologici e gli spazi lavorativi.</li>
        <li>Durata del contratto</li>
        <li>Retribuzione</li>
    </ol>
</ol>
<p>&nbsp;</p>
<p>Di seguito ti alleghiamo un template riconosciuto del contratto per la procedura semplificata per i lavoratori agile.</p>
<p><a style="color: red" href="#">contratto smart working pdf</a></p>
