<?php

return [

    'accordions' => [
        'Stabilità' => [
            'text' => "<p>La fibra ottica ha il vantaggio di far \"sopravvivere\" il segnale per distanze molto più elevate rispetto al rame, in cui si degrada molto facilmente. Inoltre la fibra ottica ha un elevata larghezza di banda senza perdite di segnale, anche ad alte frequenze.</p>"
        ],
        'Affidabilità' => [
            'text' => "<p>
La fibra ottica non risente degli eventi atmosferici e delle interferenze elettriche ed elettromagnetiche, sempre più diffuse a causa della comunicazione wireless senza fili.</p>"
        ],
        'Sicurezza' => [
            'text' => "<p>
Poichè in un cavo di fibra ottica scorrono impulsi di luce, al posto della corrente dei cavi in rame tradizionali, risulta molto difficile per un malintenzionato intercettare informazioni.   </p>"
    ]
        ]
];