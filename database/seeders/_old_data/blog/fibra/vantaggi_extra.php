<?php

return [
        'accordions' => [
            'Vantaggi' => [
                    'text' => "<ul>
<li>La fibra ottica &egrave; pi&ugrave; veloce che la connessione con il tradizionale cavo in rame, grazie alle grandi velocit&agrave; di trasmissione che raggiungono i 100 GBs.</li>
<li>La potenza del segnale &egrave; una cosa fondamentale , nelle connessioni internet in rame il segnale si degrada molto facilmente (100 m) mentre in una connessione in fibra si degrada pi&ugrave; tardi e su distanze molto pi&ugrave; elevate (2 km).</li>
<li>Larghezze di banda elevate senza perdita di segnale ad altre frequenze.</li>
<li>La fibra ottica &egrave; pi&ugrave; stabile, affidabile e sicura</li>
</ul>",
                'isActive' => true,
            ],
            'Svantaggi' => [
                    'text' => "</ul>
<p>Per quanto riguarda gli svantaggi ricordiamo:</p>
<li>Non disponibile su tutto il territorio nazionale</li>
<li>Pi&ugrave; costosa di una tradizionale adsl</li>
</ul>",
                'isActive' => true,
            ]
        ]
];