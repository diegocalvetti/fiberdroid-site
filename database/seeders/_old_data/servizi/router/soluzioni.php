<?php

return [

    'cards' => [
        ['title' => "Draytek 2765AC", 'size' => "128x128", 'image' => 'draytek-2765',
            'features' => [
            '1 porta Rj-11 VDSL2/2+ / ADSL2',
            '4 porte Lan gigabit',
            'WiFi dual band',
            'Firewall + vpn server'
        ], 'price' => 9.90],
        ['title' => "Draytek 2927AC", 'size' => "128x128", 'image' => 'draytek-2927',     'features' => [
                '2 porte Wan gigabit',
                '5 porte Lan gigabit',
                'WiFi dual band',
                'Firewall + vpn server'
            ], 'price' => 19.90],
        ['title' => "Draytek 2915AC", 'size' => "128x128", 'image' => 'draytek-2915',
            'features' => [
                '1 porta Wan gigabit',
                '4 porte Lan gigabit',
                'WiFi dual band',
                'Firewall + vpn server'
            ], 'price' => 9.90],
        ['title' => "Draytek 2865AC", 'size' => "128x128", 'image' => 'draytek-2865',
            'features' => [
                '1 porta Rj-11 VDSL2/2+ / ADSL2 + 1 porta Wan gigabit',
                '4 porte Lan gigabit',
                'WiFi dual band',
                'Firewall + vpn server'
            ], 'price' => 19.90],
        ['title' => "FRITZ!Box AVM 7590", 'size' => "128x128", 'image' => 'avm-7950',
            'features' => [
                '1 porta Rj-11 VDSL2/2+ / ADSL2 + 1 porta Wan gigabit',
                '4 porte Lan gigabit',
                'WiFi dual band',
                'Dect + porta isdn + 2 porte analogiche',
            ], 'price' => 19.90],
    ]



];
