<?php

return [
    'accordions' => [
        'Modem' => ['text' => "<p>Per poter sfruttare la linea telefonica &egrave; necessario che i dati del computer siano convertiti in segnali analogici.<strong> Il modem </strong>&egrave; il dispositivo che compie questa operazione, gestendo lo scambio di informazioni indispensabili per la navigazione in Internet.</p>
<p>Il suo nome deriva proprio dalle due operazioni che compie: <strong>modulazione, </strong>nella direzione dal computer verso la rete, e <strong>demodulazione,</strong> quando riconverte il segnale analogico dal doppino in segnale digitale verso il computer.</p>
<p>I collegamenti necessari in un modem sono</p>
<ul>
<li>una porta per collegarsi attraverso la linea telefonica alla rete</li>
<li>una o pi&ugrave; porte Ethernet per interfacciarsi con i vari dispositivi presenti nell'appartamento o con il router se presente.</li>
</ul>
<p>In conclusione quindi, se si ha la necessit&agrave; di collegarsi ad Internet &egrave; necessario acquistare un modem (o un router che abbia anche la funzione di modem integrata).</p>"],
        'Router' => ['text' => "<p><strong>Il router </strong>(o instradatore) &egrave; l'apparato che gestisce lo scambio di informazioni tra i vari dispositivi di una rete locale e non &egrave; fondamentale per la navigazione in Internet.</p>
<p>In pratica il router gestisce i protocolli necessari affinch&egrave; i pacchetti \"dati\" raggiungano la giusta destinazione nel giusto ordine: mediante le <strong>routing table</strong> (tabelle di indirizzamento)&nbsp;&nbsp; i pacchetti in transito possono scegliere il percorso pi&ugrave; breve per raggiungere la loro destinazione.</p>
<p>&Egrave; dotato di una delle due seguenti modalit&agrave; di comunicazione attraverso le quali permette la condivisione di connessione tra dispositivi di vario tipo (computer, tablet, smartphone):</p>
<ol>
<li><strong>porte Ethernet,</strong> per<strong> una connessione fisica via cavo tra router e apparati.</strong></li>
</ol>
<p>In questo caso essendo una connessione diretta al dispositivo, la maggioranza dei pc riconosce le impostazioni migliori e ottimizza la connessione mediante il meccanismo del \"plug and play\"</p>
<ol start=\"2\">
<li><strong>senza fili in modalit&agrave; Wi-Fi,</strong> utilizzando le onde radio per comunicare con gli apparati.</li>
</ol>
<p>Occorre in questo caso <strong>impostare una password </strong>per proteggere la rete domestica dall'accesso da parte di malintenzionati.</p>
<p>Inoltre vengono usati vari <strong>protocolli di protezione per crittografare</strong> tutti i dati in transito: i pi&ugrave; utilizzati sono <strong>WEP</strong> (Wired Equivalence Privacy), <strong>WPA </strong>(Wi-Fi Protected Access),&nbsp; <strong>WPA2</strong> (Wi-Fi Protected Access 2) e <strong>WPA2</strong> (Wi-Fi Protected Access 2) in ordine crescente di livello di protezione.</p>
<p>Nel caso pi&ugrave; frequente in cui si desideri avere entrambe le funzionalit&agrave; di <strong>modem</strong> (connessione ad Internet) e <strong>router</strong> (rete LAN locale di pi&ugrave; dispositivi) quest'ultimo far&agrave; da interfaccia tra il modem connesso alla rete e i dispositivi della rete locale permettendo <strong>l'utilizzo contemporaneo della stessa connessione Internet</strong> ai vari devices.</p>"],
        'Il router Wi-Fi' => ['text' => "<p>Dal momento che il router Wi-Fi sfrutta le onde radio per comunicare con i dispositivi la propagazione del suo segnale pu&ograve; essere soggetta a <strong>varie interferenze:</strong></p>
<ul>
<li>caratteristiche dell'edificio in cui viene installato, <strong>muri spessi</strong> infatti possono attenuare il segnale.</li>
<li><strong>Interferenza radio</strong> dovuta spesso a forni a microonde o ad altri apparecchi radio</li>
<li>strutture metalliche<strong> presenti negli ambienti in cui si dovrebbe propagare il segnale</strong></li>
</ul>
<p>Per cercare di ridurre queste interferenze si pu&ograve; procedere nel seguente modo</p>
<ul>
<li>cambiare il canale di trasmissione del router cio&egrave; scegliere <strong>una nuova frequenza radio </strong>mediante il pannello di controllo del dispositivo</li>
<li><strong>riposizionare il router</strong> in modo da evitare che ostacoli fisici o interferenze radio presenti nell'ambiente attenuino il segnale.</li>
</ul>
<p>In alcuni casi poi un cattivo funzionamento del router pu&ograve; non essere causato da interferenze ma da un'<strong>eccessiva temperatura</strong>: &egrave; bene dunque fare anche un controllo della temperatura e nel caso fosse eccessiva tenere il router lontano dal riscaldamento, fonti di calore in genere e non posizionarlo in luoghi non areati come ad esempio all'interno di mobili chiusi.</p>"],
    ]
];