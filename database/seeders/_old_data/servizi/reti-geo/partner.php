<p>Noi di <strong>Fiberdroid</strong> utilizziamo le <a href="<?= url('#dorsali-o-backbone') ?>"><strong>dorsali in
            fibra</strong></a> sul territorio per realizzare collegamenti tra le tue reti locali (LAN).</p>
<p>Usiamo un <strong>circuito virtuale</strong> detto "<strong>layer 2</strong>" che consente una banda riservata per
    l'intera connessione e anche una velocit&agrave; di instradamento superiore che si traduce per te in migliore qualit&agrave;
    del servizio e permette connessioni fino a 1 Gbps simmetrici. Il "layer 2 " viene considerato la base della Network
    Security poich&eacute; <strong>il Livello 2 </strong>(Data Link Layer) fornisce il servizio di trasferimento
    affidabile dei dati su di un mezzo fisico.</p>
<p>Usiamo dove possibile la <a href="<?= url('#fibra-ottica-spenta') ?>"><strong>fibra ottica spenta </strong></a> per
    velocizzare la tua connessione evitando la condivisione della banda o il costo della posa di nuove fibre.</p>
<p>Con questa tecnologia &egrave; possibile <strong>collegare in sicurezza le diverse sedi</strong>, sfruttando sia la
    condivisione di dati e programmi che la <strong>decentralizzazione del backup</strong> (che come detto prima
    garantisce un alto livello di sicurezza) e quindi ridurre gli investimenti hardware per la sicurezza e VPN di ogni
    sito.</p>
<p><strong>L&rsquo;assistenza e la progettualit&agrave; avanzata e innovativa di Fiberdroid</strong> &egrave; il nostro
    miglior biglietto da visita. Il cliente non acquista con noi delle semplici connessioni ma usufruisce di un team di
    professionisti che analizzando e studiando in modo approfondito le sue esigenze proporr&agrave; la migliore
    soluzione possibile.</p>
<p>La soluzione di rete geografica, pu&ograve; essere attivata con diverse tecnologie in base alla disponibilit&agrave;
    di zona.</p>
<p>E&rsquo; consigliato ovviamente l&rsquo;uso della <strong>fibra simmetrica,</strong> ma &egrave; sempre possibile
    utilizzare per piccole filiali anche connettivit&agrave; in rame o wireless.</p>
<p>Il nostro team sapr&agrave; comprendere al meglio le Tue richieste e fornirti la soluzione pi&ugrave; idonea.</p>
<p>Questa tipologia di servizio e piattaforma non ha un listino prezzo, ma necessit&agrave; di sopralluoghi, studi e
    analisi, per questo &egrave; necessario che l&rsquo;azienda che necessita di questa soluzione ci scriva alla mail :
    <a href="mailto:speed@fiberdroid.it">speed@fiberdroid.it</a> oppure ci chiami al numero <a
            href="tel:+39 02 40706604">+39 02 83595676</a> cos&igrave; da pianificare un primo incontro tecnico per
    iniziare la strutturazione del progetto.</p>
<p><strong>Il futuro &egrave; gi&agrave; qui, vuoi esplorarlo con Fiberdroid?</strong></p>
