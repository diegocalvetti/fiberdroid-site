<p>Una rete Metropolitana (<a href="<?= url("#man-metropolitan-area-network") ?>"><strong>MAN</strong></a>) &egrave; una
    infrastruttura &ldquo;virtuale&rdquo; che collega le filiali della tua azienda, nella stessa zona metropolitana.</p>
<p>Con la <strong>MAN</strong> i tuoi uffici anche distanti centinaia di metri lavoreranno in sicurezza come se fossero
    all'interno dello stesso edificio:</p>
<ul>
    <li>Senza <a href="<?= url('#firewall') ?>"><strong>Firewall</strong></a></li>
    <li>senza particolari apparati</li>
    <li>con lo stesso <a href="<?= url('#indirizzo-ip') ?>"><strong>indirizzo IP</strong></a></li>
    <li>senza <strong><a href="<?= url('#vpn') ?>"><strong>Vpn</strong></a></strong></li>
</ul>
