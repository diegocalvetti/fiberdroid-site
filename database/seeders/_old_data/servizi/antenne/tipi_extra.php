<?php

return [

    'accordions' => [
        "Access point POE" => [
            'text' => "<p>l&rsquo;access point prende sia la rete che l&rsquo;alimentazione dal cavo ethernet. Si possono quindi installare distante dal modem senza usare cavi apposta per la corrente. L&rsquo;energia elettrica passa attraverso un alimentatore apposito all&rsquo;interno dell&rsquo;access point oppure attraverso uno switch di tipo POE. In genere hanno poca potenza</p>"
        ],
        "Access point USB" => [
            'text' => "<p>Quando vuoi collegare stampanti, smartphone, tablet ecc al tuo pc puoi utilizzare una <strong>chiavetta usb</strong> che genera una rete WI-FI a cui collegare i device. Basta collegarla e scaricare i driver per renderla operativa.</p>",
        ],
        "Access point da esterno" => [
            'text' => "<p>Per gli <strong>access point da esterno</strong> le caratteristiche pi&ugrave; richieste sono la copertura su lunghe distanze,&nbsp; la capacit&agrave; di direzionarsi sulle persone in movimento e la resistenza a caldo e freddo (ma vanno comunque messi al riparo da pioggia e sole). In genere hanno una custodia che protegge da polvere e acqua e spesso con protezione dai fulmini per evitare danni da punte di tensione.</p>",
        ],
        "Access point AC" => [
            'text' => "<p>Access point che usano il protocollo AC cio&egrave;&nbsp; due bande di frequenze,&nbsp; 2,4 GHz e 5 Ghz.</p>
<p>Sono chiamate <strong>access point dual band</strong> e sono l&rsquo;ideale per avere <strong>alte velocit&agrave; di download e upload.</strong></p>
<p>La copertura in genere &egrave; 20 metri per la banda 2,4 Ghz e di circa 15 metri sulla banda 5GHz.(raggio d&rsquo;azione inferiore ma pi&ugrave; stabile e con meno interferenze)</p>
<p><strong>La quantit&agrave; di Mbps supportata</strong> &egrave; indicata nella sigla dell&rsquo;access point, potreste trovare per esempio per esempio AC750 supporta 750Mbps in dual band che significa che l&rsquo;access point opera sia sulla banda di frequenza a 2,4 Ghz che a 5GHz.</p>
<p>Nella classe AC esistono access point pi&ugrave; evoluti che usano la<strong> tecnologia MU-MIMO</strong> cio&egrave; multiple user e&nbsp; multiple input-multiple output.</p>
<p>Con questa tecnologia i <strong>client</strong> (pc, smartphone ecc collegati all&rsquo;access point) <strong>non devono pi&ugrave; restare in attesa</strong> finch&eacute; non &egrave; conclusa l&rsquo;azione del device che sta facendo una richiesta, ma le richieste vengono gestite in contemporanea.</p>"
        ]
    ],
];