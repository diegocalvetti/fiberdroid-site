<?php

return [
    'accordions' => [
        'Perché usare un access point' => [
            'text' => "<p>Hai bisogno di un access point quando:</p>
<ul>
    <li>il tuo router non ha il WI-FI e non puoi collegare facilmente i tuoi device</li>
    <li>il tuo router non copre col WI-FI completamente la casa o l&rsquo;ufficio</li>
    <li>necessiti di reti separate</li>
</ul>
<p>Nel primo caso <strong>si collega al router di casa per aumentarne la copertura</strong> e potendo cos&igrave; collegarsi via WI-FI dalle stanze pi&ugrave; lontane, (esistono router senza la funzionalit&agrave; del collegamento wireless e altri dove questo &egrave; presente ma non &egrave; sufficiente a sopperire a tutte le richieste.)</p>
<p>Nel secondo caso si collega <strong>un access point per consentire la creazione di diverse reti</strong>, per esempio quella per gli ospiti dove puoi decidere di concedere per esempio un download limitato per evitare rallentamenti al collegamento internet principale oppure puoi impedire l&rsquo;accesso ai device della LAN, ad esempio se non vuoi che accedano ai tuoi dati riservati o alla tua stampante.</p>
<p>L&rsquo;access point non crea una nuovo accesso alla rete Internet ma estende quella esistente.</p>
<p>I router WI-FI di ultima generazione hanno spesso la funzione access point integrata ma se non hai questi apparecchi un access point pu&ograve; risolvere il tuo problema.</p>"
        ],
        'Come configurare un access point' => [
            'text' => "<p>Puoi configurare l&rsquo;access point dal pannello di controllo attraverso il browser (inserendo l&rsquo;IP dell&rsquo;access point)&nbsp; o dal programma fornito con l&rsquo;apparecchio (si solito su dischetto). L&rsquo;IP lo trovi scritto in un&rsquo;etichetta nella confezione se no spesso &egrave; 192.168.0.1 o 192.168.1.1 oppure 10.1.1.1.</p>
<p>Le impostazioni da configurare di solito sono sotto le voci &ldquo;LAN&rdquo; o &ldquo;network&rdquo;.</p>
<p><strong>La sicurezza della tua rete WI-FI &egrave; importante</strong> e quindi consigliamo sempre di rivolgerti a professionisti, <strong>noi di Fiberdroid</strong> possiamo darti supporto per queste configurazioni e soprattutto mantenere aggiornate le tue antenne sempre con le ultime versioni firmware per garantire la massima affidabilit&agrave;.</p>
<p>Pagina del sito con offerte per antenne a noleggio / acquisto: <a href=\"".url('#offerte-antenne-fiberdroid')."\">".url('#offerte-antenne-fiberdroid')."</a></p>
<p>Ancora meglio, <strong>se adotti la nostra piattaforma Cloud WI-FI ti consentir&agrave; di gestire tutti gli access point della tua rete anche su sedi diverse sparse sul territorio direttamente da un'unica interfaccia web nel nostro cloud.</strong></p>"
        ]
    ]
];
