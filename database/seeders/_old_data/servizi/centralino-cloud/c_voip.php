<p>Con entrambi questi termini si intende una soluzione in cui il centralino deve fisicamente essere installato nella sede del cliente (on premise).</p>
<p>Nei casi infatti in cui sia meglio non sovraccaricare troppo la connessione Internet pu&ograve; risultare pi&ugrave; conveniente gestire localmente tutte le telefonate al cliente.</p>
<p>Con un Centralino VoIP si possono gestire</p>
<ul>
    <li>parecchie <strong>centinaia di telefoni</strong></li>
    <li>servizi di<strong> integrazione con gestionali </strong>software interni.</li>
</ul>
<p>Vedi la nostra soluzione di Centralino VoIP alla pagina <br> <a href="<?=route('servizio', 'centralino-VoIP')?>"><?=route('servizio', 'centralino-VoIP')?></a> </p>
