<p>Il centralino Cloud &egrave; invece una soluzione in cui il sistema viene <strong>virtualizzato esternalizzando il centralino</strong> in un datacenter al di fuori degli uffici.</p>
<p>In questo modo vengono drasticamente <strong>ridotti i costi di installazione e gestione</strong> iniziali oltre che quelli di manutenzione nel tempo.</p>
<p>Nel caso del centralino cloud <strong>l'integrazione con gesionali software </strong>interni &egrave;<strong> meno consigliata</strong> dal momento che tutto passerebbe su Internet con il rischio di disservizi in caso di problemi con la banda Internet.</p>
<p><strong>La differenza sostanziale</strong> tra le due soluzioni descritte &egrave; sulla</p>
<ul>
    <li><strong>dimensione delle aziende</strong></li>
    <li>esigenze richieste</li>
    <li><strong>integrazioni software</strong></li>
    <li><strong>propriet&agrave; della centrale telefonica</strong> (preferendo quindi la soluzione hardware rispetto a quella virtuale)</li>
</ul>
