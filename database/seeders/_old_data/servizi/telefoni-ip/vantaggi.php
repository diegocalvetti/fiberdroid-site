<?php

return [

    'accordions' => [
        "sicurezza" => ['text' => "<p>il traffico passa tra due utenti senza passaggi attraverso un centralino di commutazione. Volendo puoi anche utilizzare la crittografia per impedire intercettazioni ma la comunicazione è già più sicura della linea tradizionale. E se tutto è basato sul Cloud e ridondato, qualunque cosa succeda potrai continuare a fare e ricevere telefonate.</p>"],
        "risparmio" => ['text' => "<p>le chiamate tra diverse sedi dell'azienda a zero costi, le chiamate internazionali costano come le nazionali, e persino quelle intercontinentali! questo perché le chiamate Internet hanno costi inferiori alle chiamate tradizionali.</p>"],
        "libertà di movimento" => ['text' => "<p>puoi rispondere da ovunque tu ti trovi, e oltre che con il tuo smartphone con un tablet un pc, o un notebook. Devia le chiamate e vai. E puoi anche spostare il tuo telefono dove vuoi, qualunque postazione va bene anche in un'altra città, basta avere una connessione a Internet!</p>"],
        "chiama come vuoi" => ['text' => "<p>chiamata, videochiamata o call conference; puoi mandare fax con la posta elettronica, gestire la segreteria temporizzata, avere numeri interni illimitati (standard 20) poi configurazioni su misura; avere una casella vocale personale per ogni utente, usare più derivazioni, anche in edifici differenti, su cui fare squillare il telefono! tutto è possibile con i <strong>telefoni VoIP!</strong></p>"],
        "attivazione veloce" => ['text' => "<p>nessun sopralluogo, in genere il servizio è attivo in 24 ore dalla ricezione contratto</p>"],
        "trasparenza della gestione" => ['text' => "<p>puoi gestire il tuo centralino VoIP tramite browser per visualizzare il pannello di controllo e gestire i tuoi servizi e verificare costi e telefonate. Tutto chiaro e a portata di clic!</p>"],
        "vantaggi e comodità" => ['text' => "<p>si possono avere interazioni aggiuntive, come rispondere al citofono e aprire stando seduto alla scrivania grazie a un numero configurato appositamente. Puoi creare la rubrica aziendale che ogni dipendente può consultare. Puoi gestire delle liste di numeri da ridirigere automaticamente su alcuni interni o fare risultare sempre occupato il tuo numero per loro (per esempio un call center particolarmente fastidioso).</p>"]
    ]

];