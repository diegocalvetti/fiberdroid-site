<?php

return [

    'cards' => [
        ['title' => "Telefono<br>desktop", 'size' => "128x128", 'image' => 'telefono-desktop',
            'features' => [
            '2 porte gigabit',
            'Display grafico retroilluminato',
            'Due pulsanti programmabili',
            'Alimentazione PoE o tramite alimentatore esterno'
        ], 'price' => 2.90],
        ['title' => "Telefono<br>direzionale", 'size' => "128x128", 'image' => 'telefono-direzionale',
            'features' => [
                '2 porte gigabit',
                'Display a colori da 7 pollici, touch',
                '29 pulsanti programmabili',
                'Alimentazione PoE o tramite alimentatore esterno'
            ], 'price' => 6.90],
        ['title' => "Telefono posto <br> operatore", 'size' => "128x128", 'image' => 'telefono-direzionale',
            'features' => [
                '2 porte gigabit',
                'Display a colori da 7 pollici, touch',
                '50+ pulsanti DSS programmabili',
                'Alimentazione PoE o tramite alimentatore esterno'
            ], 'price' => 9.90],
        ['title' => "Telefono cordless <br> wifi", 'size' => "128x128", 'image' => 'telefono-cordless',
            'features' => [
                'WiFi dual band',
                'Display a colori',
                'Base di ricarica',
                '7.5 ore di<br> conversazione',
            ], 'price' => 5.90],
    ]



];
