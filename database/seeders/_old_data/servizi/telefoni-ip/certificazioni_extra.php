<?php

return [
    'accordions' => [
        "Yealink" => [
            'text' => "<p>Nata nel 2001 nel Paese del Dragone diventa il primo produttore mondiale di sistemi di Unified Communication, di&nbsp; cui detiene il <strong>54% della produzione mondiale</strong> di apparati.</p>
<p>Yealink &egrave; anche l'unico partner selezionato di TI nella Cina continentale.</p>
<p>I prodotti di Yealink sono di alta qualit&agrave; e la filiera di produzione &egrave; a basse emissioni di carbonio.</p>
<p>Yealink &egrave; specializzata nello sviluppo di <strong>telefoni IP, videotelefoni e videoconferenza IP</strong>.</p>
<p>I suoi terminali VoIP sono caratterizzati da un'<strong>ampia interoperabilit&agrave;</strong> con i sistemi VoIP pi&ugrave; diffusi e da numerose funzioni che semplificano le comunicazioni con elevati standard di sicurezza.</p>
<p>La linea di prodotti Yealink dispone di numerosi modelli ideali <strong>sia per il piccolo ufficio che per postazioni operatore professionali</strong>.</p>"],
        "Grandstream" => ['text' => "<p>Fondata nel <strong>2002</strong>, Grandstream &egrave; un'azienda <strong>statunitense</strong> produttrice di soluzioni IP per la videosorveglianza, la videotelefonia e la telefonia VoIP per le piccole e medie imprese.</p>
<p>I prodotti Grandstream si basano sullo <strong>standard aperto SIP</strong> e hanno quindi ampia interoperabilit&agrave; nel settore oltre a caratteristiche esclusive e grande flessibilit&agrave;.</p>
<p>Inoltre Grandstream produce telefoni adatti anche per le grandi aziende. Grazie all'utilizzo di <strong>moduli di estensione</strong> &egrave; possibile espandere la configurazione del teminale allo scopo di aumentare il volume del traffico telefonico.</p>
<p>I principali vantaggi che offrono i telefoni di questa amrca sono:</p>
<ul>
<li>ottimo rapporto qualit&agrave;/prezzo</li>
<li>tecnologie plug and play</li>
<li>interoperabilit&agrave; con terminali SIP</li>
<li>uso di sofisticati codec audio-video.</li>
</ul>"],
        "Mitel/Aastra" => ['text' => "<p>Mitel Aastra &egrave; un'<strong>azienda canadese </strong>nata dalla fusione di Mitel e Aastra, due colossi nello sviluppo di soluzioni di Unified Communications.</p>
<p>Mitel Aastra crea sinergia tra piattaforme fisse e mobili per la <strong>comunicazione in azienda</strong> con centrali virtuali VoIP e cloud, telefoni IP, phone manager, softphone e cordless.</p>
<p>Le soluzioni Mitel/Aastra sono ideali per favorire la produttivit&agrave; e la comunicazione <strong>tra pi&ugrave; sedi,</strong> permettendo una collaborazione ottimale tra colleghi.</p>
<p>Si concentra verso i clienti business rivolgendosi ad imprese di ogni dimensione e alla pubblica amministrazione, oltra ad avere applicazioni specifiche per il settore medico sanitario e alberghiero.</p>
<p>Propone un'ampia gamma di <strong>telefoni VoIP fissi e cordless DECT</strong> multicella.</p>"],
        "Gigaset" => ['text' => "<p>Gigaset &egrave; un'azienda che opera a livello internazionale nel settore della tecnologia delle telecomunicazioni.</p>
<p>Fondata nel <strong>2008 a Monaco di Baviera </strong>&egrave; diventata leader di mercato in Europa in particolare per i telefoni <strong>cordless DECT </strong>ma produce anche telefoni fissi, sistemi IP dect, soluzioni aziendali wireless e VoIP.</p>
<p>Grazie alle funzionalit&agrave; avanzate i suoi dispositivi sono adatti ad ogni tipo di contesto, anche gli ambiti aziendali in cui sono necessarie coperture su pi&ugrave; piani.</p>
<p>Le soluzioni Gigaset combinano un'<strong>elevata qualit&agrave; audio </strong>con un design moderno, adattandosi ad ogni esigenza.</p>"],
        "Fanvil" => ['text' => "<p>Fanvil Technology Co. (Fanvil) fondata nel 2002 in Cina &egrave; divenuta leader mondiale nel settore delle Unified Communications (UC).</p>
<p>Fornisce dispositivi VoIP di telefonia, videocitofonia e accessoristica ed &egrave; specializzata nella progettazione di teminali IP per la comunicazione basati su standard SIP, garantendo un'elevata qualit&agrave; ed aiutando a ridurre i costi delle comunicazioni in azienda.</p>"],
    ]
];