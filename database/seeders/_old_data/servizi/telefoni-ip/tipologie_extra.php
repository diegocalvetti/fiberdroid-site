<?php

return [
    'accordions' => [
        "Telefono VoIP USB" => ['text' => "<p>Questo telefono essenzialmente &egrave; composto da un microfono con un altoparlante e viene <strong>collegato alla porta USB di un computer </strong>e tramite l'uso di un software apposito si comporta come un telefono.</p>"],
        "Adattatore ATA" => ['text' => "<p>Questo dispositivo permette di utilizzare un normale telefono analogico con la tecnologia VoIP. L'adattatore viene collegato direttamente alla rete LAN o al router mentre <strong>uno o pi&ugrave; telefoni tradizionali possono essere collegati all'ATA</strong> mediante normali cavetti telefonici.</p><p>In questo modo il sistema telefonico VoIP vedr&agrave; i vecchi telefoni collegati all'ATA come dei normali telefoni VoIP.</p><p>In pratica ci&ograve; che fa l'adattatore &egrave;<strong> trasformare il segnale analogico</strong> proveninete dal vecchio telefono<strong> in segnale digitale</strong> che pu&ograve; essere trasmesso via Internet.</p>"],
        "Telefono VoIP fisso" => ['text' => "<p>I telefoni VoIP fissi hanno un aspetto molto simile ai telefoni analogici tradizionali.</p><p>La differenza principale &egrave; che invece che alla linea PSTN devono essere collegati mediante una <strong>porta ethernet </strong>alla rete LAN o alla rete Internet.</p>"],
        "Telefono VoIP cordless" => ['text' => "<p>Il cordless <strong>DECT (Digital Enhanced Cordless Telecommunication</strong>, vale a dire &ldquo;telecomunicazioni digitali senza fili migliorate&rdquo; &egrave; un telefono cordless in grado di garantire il comfort della mobilit&agrave; all'interno di un ufficio.</p>
<p>Un telefono VoIP cordless &egrave; in tutto molto simile ad un normale cordless tranne per il fatto che la connessione alla rete avviene <strong>collegando la base del cordless al router</strong>.</p>
<p>Nel caso di un'azienda questo sistema pu&ograve; offrire una copertura <strong>in tutto l'edificio o stabilimento</strong>, anche nel caso siano presenti mura spesse o costruzioni in acciaio.</p><p>A seconda delle esigenze della tua azienda potrai scegliere tra</p><ul><li>soluzioni DECT <strong>a singola cella.</strong></li></ul><p>Per piccoli uffici, con non pi&ugrave; di 4/6 dispositivi</p><ul><li>soluzioni DECT<strong> a multi-cella</strong></li></ul><p>Per aziende di maggiori dimensioni, fino a 100 dispositivi a disposizione.</p>"],
        "Telefono VoIP cordless Wi-Fi" => ['text' => "<p>Il cordless <strong>WiFi </strong>offre una funzione molto utile per chi non vuole connettere fisicamente la base del cordless al router.</p>
<p>Mediante il cordless WiFi infatti <strong>si pu&ograve; sfruttare il Wi-Fi gi&agrave; presente</strong> nel proprio ufficio o abitazione per far comunicare il telefono con il modem: sar&agrave; suffciente collegare la base del cordless ad una presa di corrente e procedere con la configurazione della connessione Wi-Fi del telefono (inserimento password etc..).</p>
<p>In questo modo</p>
<ul>
<li>si <strong>evita </strong>di ricorrere a qualunque tipo di <strong>cavo</strong></li>
<li>si pu&ograve; posizionare la base del cordless in un qualunque punto, non necessariamente vicino al modem, ma <strong>dove risulti pi&ugrave; comodo</strong></li>
<li>si hanno a disposizione una <strong>vasta gamma di funzionalit&agrave; avanzate </strong>(quelle che ormai siamo abituati ad avere sui nostri smartphone, come accedere alla posta elettronica, o ad elenchi del telefono centralizzati).</li>
</ul>"],
        "Softphone" => ['text' => "<p>Col il termine softphone si intende un<strong> software che pu&ograve; essere installato su vari tipi di dispositivi</strong>&nbsp; (computer, tablet, smartphone..) e permette di effettuare o ricevere chiamate senza il bisogno di avere un apparato telefonico vero e proprio.</p>
<p>I softphone si comportano esattamente come un normale telefono IP, fornendo funzioni telefoniche regolari come effettuare e ricevere chiamate oltre ad alcune funzioni pi&ugrave; avanzate come la videoconferenza o la messaggistica istantanea,</p>
<p>Tutto ci&ograve; che serve sono&nbsp; l&rsquo;altoparlante o il microfono integrato del dispositivo o <strong>un auricolare</strong> connesso alla scheda audio del pc o dello smartphone/tablet.</p>
<p>Una volta installato il software sul tuo computer, ad esempio, il tuo pc diventa <strong>un telefono multimediale</strong>. Un esempio molto popolare di questa tecnologia &egrave; l'applicazione Skype, che consente di effettuare chiamate gratuite da computer a computer.</p>"],
    ],
];