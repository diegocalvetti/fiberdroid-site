<p>Un telefono VoIP permette di effettuare chiamate verso la linea telefonica tradizionale e la rete mobile utilizzando
    la tecnologia VoIP.</p>
<p>Nella telefonia VoIP il messaggio vocale invece di viaggiare tramite il sistema PSTN tradizionale come segnale
    acustico viene convertito in pacchetti di dati digitali e <strong>trasmesso attraverso Internet</strong> utilizzando
    il protocollo di comunicazione IP.</p>
<p>Di conseguenza i telefoni VoIP diversamente dai vecchi telefoni non devono essere connessi alla linea analogica. Per
    funzionare un telefono VoIP deve essere <strong>connesso alla rete dati</strong> (ad esempio una LAN o la rete
    Internet).</p>
<p>Oltre al risparmio di costo nel creare l'infrastruttura in tecnologia VoIP (non &egrave; necessario il cablaggio come
    per le vecchie linee analogiche) &egrave; possibile pagare per le <strong>chiamate internazionali e
        intercontinentali </strong>allo <strong>stesso costo delle chiamate nazionali</strong>, dal momento che la voce
    non viaggi&agrave; pi&ugrave; occupando una linea analogica dedicata ma sfruttando la rete Internet come qualuque
    altro pacchetto di dati.</p>
<p>Un altro beneficio della telefonia VoIP &egrave; che non importa dove tu sia e quale dispositivo usi perch&egrave;
    puoi effettuare chiamate <strong>tramite il tuo account</strong> <strong>da qualsiasi parte del mondo</strong> tu
    riesca a trovare una connessione Internet a banda larga.</p>
<p>Un telefono VoIP pu&ograve; essere</p>
<ul>
    <li>un <strong>dispositivo hardware </strong>vero e proprio molto simile nell'aspetto ai telefoni analogici
        tradizionali (fissi o cordless)
    </li>
    <li>un semplice <strong>softphone</strong> basato su <strong>software o app</strong> installato su vari tipi di
        dispositivi (computer, tablet, smartphone..).
    </li>
</ul>
<p>Grazie ad alcuni <strong>software gratuiti (quali Skype) </strong>&egrave; possibile chiamare ed essere chiamati
    gratuitamente in qualsiasi momento da qualsiasi parte del mondo. In questo modo sono possibili chiamate da <strong>computer
        verso computer</strong> ad esempio. L&rsquo;unico requisito necessario &egrave; che anche dall'altro capo della
    comunicazione <strong>sia installato lo stesso software</strong> e che ci sia una&nbsp;buona connessione ad
    internet.</p>
<p>Per maggiori informazioni sulla tecnologia VoIP visita la nostra pagina
    <br>
    <a href="<?=route('servizio', 'VoIP')?>"><?=route('servizio', 'VoIP')?></a>
</p>
