<?php

return [

    'accordions' => [
        "Internet satellitare come funziona" => ['text' => "<p>A quasi 36.000 Km dalla terra, orbitano centinaia di satelliti&nbsp; usati anche per le trasmissioni dei segnali Internet. Questi segnali vengono intercettati dai NOC (centri terrestri per le operazioni di rete) e da qui trasmessi alle dorsali che fanno da \"snodo\" indirizzandoli ai server di Rete che ritrasmettono alla parabola e al modem utente ad essa collegato e percorso inverso.</p>
<p>Per utilizzare la connessione a Internet via satellite, bisogna installare gli strumenti per intercettare i segnali:</p>
<ul>
<li><strong>una parabola</strong></li>
<li><strong>un modem.</strong></li>
</ul>
<p>&Egrave; una connessione di tipo wireless in quanto non sono necessari n&egrave; cavi telefonici n&egrave; la fibra.</p>
<p>La trasmissione dati &egrave; in modalit&agrave; Wireless, cio&egrave; senza i cavi della rete telefonica.</p>
<p>La connessione internet satellitare mediante i satelliti di nuova generazione KA-SAT (K-above band) permette un accesso a internet ad alta velocit&agrave;, 50Mbs, ben superiore alla classica ADSL che in genere non supera i 20Mbs.</p>"],
        "Internet satellitare opinioni" => ['text' => "<p>In generale <strong>Internet satellitare</strong> riceve delle buone recensioni perch&eacute; <strong>ha innumerevoli vantaggi</strong>:</p>
<ul>
<li>non serve una linea telefonica (risparmio sui costi fissi)</li>
<li>non serve adsl</li>
<li>non serve la fibra</li>
<li><strong>collegamento internet veloce</strong> equiparabile all'adsl se non meglio (ADSL 20Mbs satellitare 50Mbs)</li>
<li>&egrave; collaudato da diversi anni</li>
<li>disponibile anche per brevi periodi (eventi, vacanze)</li>
<li>ha un'installazione semplice e veloce (una parabola da orientare verso il satellite e collegare al modem)</li>
</ul>"],
        "Internet satellitare svantaggi" => ['text' => "<p>L'unico vero svantaggio di questo <strong>collegamento internet veloce</strong> e affidabile &egrave; il <strong>tempo di latenza (lag)</strong> che ne sconsiglia l'utilizzo nel caso di giochi \"sparatutto\" basati sulla velocit&agrave; di azione-reazione. Infatti il segnale dal satellite a terra impiega circa 120ms e per l'utilizzatore il tempo totale &egrave; circa 500ms.</p>
<p><strong>Come si valuta il lag? </strong>Si calcola il ping della connessione. Un ping troppo alto pregiudica la qualit&agrave; di una connessione per il gaming online del tipo \"sparatutto\".</p>
<p>In questo caso &egrave; consigliato ove possibile l'utilizzo della <a href='".route('servizio', 'fibra')."'>fibra</a>. Il lag in genere non comporta alcun problema con tutti gli altri tipi di giochi online.</p>
<p>Una connessione in fibra ottica, infatti, si caratterizza per un ping di pochi millisecondi.</p>"],
        "Curiosità sul satellitare" => ['text' => "<p>Il primo satellite artificiale, lo <strong>Sputnik</strong> 1, fu lanciato nel 1957 dall'Unione Sovietica e rest&ograve; in orbita solo 92 giorni.</p>
<p><strong>In orbita vi sono oltre 4000 satelliti</strong> tra cui:</p>
<ul>
<li><strong>24 satelliti per la trasmissione di dati GPS</strong>, pi&ugrave; 3 di scorta per garantire copertura globale del servizio.</li>
<li><strong>un migliaio di satelliti meteorologici</strong> per monitorare gli eventi atmosferici comprese tempeste di sabbia, incendi, il fluire delle correnti oceaniche e gli effetti dell'inquinamento</li>
</ul>
<p><strong>I satelliti KA SAT</strong></p>
<ul>
<li>permettono l'accesso a Internet banda larga a quasi un milione di utenti.</li>
<li>Il servizio funziona grazie a <strong>10 stazioni terrestri</strong></li>
<li>il servizio &egrave; gestito centralmente dal NOC (network operations center) di Skylogic con sede a Torino (Skylogic &egrave; una filiale di Eutelsat).</li>
<li>sono in orbita dal 2011</li>
<li>offrono una copertura di tutta l'area europea e una parte del Medio Oriente.</li>
<li>Sono di propriet&agrave; di Eutelsat (il terzo operatore al mondo per ricavi) Skylogic &egrave;</li>
<li>infatti una filiale di Eutelsat.</li>
</ul>
<p>Il termine KA si riferisce alla banda di frequenze utilizzata per la comunicazione: la banda Ka &egrave; la porzione superiore delle frequenze a microonde dello spettro elettromagnetico e segue la banda K (e per questo &egrave; chiamata K-above band).</p>
<p><strong>Progetti futuri per il satellitare:</strong></p>
<ul>
<li><strong>Facebook</strong> (progetto Internet.org) e <strong>Google</strong> (Project Look) che sono interessate alla creazione di servizi Internet wireless per garantire una copertura su tutto il pianeta per i loro servizi,</li>
<li>il progetto Starlink della <strong>Spacex (Elon Musk)</strong>per posizionare satelliti a bassa quota per ridurre il lag (tempo di latenza della connessione)</li>
</ul>"]

    ]

];
