<p class="p10"><span class="s1">Il VDSL utilizza <strong>sia la fibra ottica</strong> che dalla centrale arriva fino ai cosiddetti armadi stradali o \"cabinet\" degli operatori (FTTC, Fiber To The Cabinet), <strong>sia il doppino in rame</strong> che dal cabinet arriva fino all'appartamento. Per questo motivo &egrave; anche chiamata soluzione \"<strong>fibra misto rame</strong>"</span>
</p>
<p class="p10"><span class="s1">Queta tecnologia permette di raggiungere velocit&agrave; di connessione superiori rispetto all'ADSL, anche se ancora inferiori rispetto ad una connessione che avviene interamente su fibra ottica (anche chiamata FTTH, Fiber To The Home).</span>
</p>
<p class="p10"><span class="s1">Il VDSL permette di avere un'ottimizzazione della soluzione ADSL senza stravolgere la rete dei cavi in rame dell'\"ultimo miglio\" verso gli appartamenti ed per questo motivo &egrave; la connessione <strong>pi&ugrave; utilizzata in Italia.&nbsp;</strong></span><span
            class="s1">La velocit&agrave; base generalmente &egrave; di 30 Mega e pu&ograve; poi arrivare anche fino a 100 o 200 Mega in download.</span>
</p>
<p class="p10"><span class="s1">Vedi le soluzioni fibra misto rame di Fiberdroid</span></p>
<p class="p10"><span class="s1"><span class="s6"><a
                    href="<?= route('servizio', 'fibra') ?>"><?= route('servizio', 'fibra') ?></a></span></span></p>
