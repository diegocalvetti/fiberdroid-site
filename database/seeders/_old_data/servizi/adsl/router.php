<p class="p2"><span class="s1"><strong>Il router </strong>&egrave; il dispositivo che gestisce lo scambio di informazioni tra i vari dispositivi e la rete internet: in pratica fa da interfaccia tra il modem e i computer/smartphone/tablet e si occupa di <strong>distribuire via cavo o in Wi-Fi il segnale Internet</strong> demodulato dal modem o viceversa di inviare al modem le informazioni provenienti dai vari dispositivi, che verrranno modulate dal modem ed inviate in rete.</span>
</p>
<p class="p2"><span class="s1"><em>Curiosit&agrave;</em>: anche se non ne conosciamo l'esistenza, anche per la rete mobile dei cellulari esiste un router che distribuisce le informazioni ma in questo caso non si trova in casa o in ufficio ma nella centralina dell'operatore telefonico.</span>
</p>
<p class="p4"><span class="s1">Se non hai gi&agrave; un tuo router/modem possiamo <strong>fornirtelo noi a noleggio o in vendita</strong>, vedi le nostre offerte nella pagina <a
                href="<?= route('servizio', 'router') ?>">Router</a> </span></p>
<p class="p4"><span class="s1">In questo caso, cio&egrave; se scegli di noleggiare o acquistare il router presso di noi, riceverai <strong>assistenza anche su tutta la parte hardware.</strong></span>
</p>
<p class="p4"><span class="s1">E nel prossimo paragrafo ti spieghiamo un altro motivo per cui ti sarebbe utile ricevere questa assistenza.</span>
</p>
