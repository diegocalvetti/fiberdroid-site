<p class="p5"><span class="s1">Mediante l'HDSL si pu&ograve; trasportare <strong>solo traffico dati e non voce</strong> e questo &egrave; il motivo per cui ha una banda di frequenze pi&ugrave; ampia dell'HDSL: pu&ograve; sfruttare tutto il range di frequenze che nell'HDSL viene dedicato al traffico voce.</span>
</p>
<p class="p5"><span class="s1">Questo implica per&ograve; che se con l'HDSL potevamo avere con una sola connessione sia la connessione ad Internet sia la nostra linea telefonica, con l'HDSL avremo solo la linea dati.</span>
</p>
<p class="p5"><span class="s1">Ovviamente <strong>il servizio voce VoIP </strong>che viaggia su una linea dati come qualunque altro dato <strong>&egrave; disponibile </strong>con l'HDSL, anzi fornir&agrave; un servizio ancora maggiore grazie ai ridotti tempi di latenza della tecnologia HDSL.</span>
</p>
<p class="p5"><span class="s1">Se sei interessato a mantenere la tua linea telefonica insieme alla connessione Internet visita la nostra pagina </span><span
            class="s7"><a href="<?= route('servizio', 'HDSL') ?>">HDSL</a></span></p>
