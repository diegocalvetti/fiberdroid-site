<p>In passato il centralino telefonico era il dispositivo utilizzato dalle aziende per comunicare con l'esterno e
    collegare gli interni degli uffici. Il mondo della telefonia fissa e di quella mobile erano separati.</p>
<p>Oggi con l'evolvere delle tecnologie oltre alle funzioni tradizionali accennate sopra il centralino implementa una
    <strong>convergenza tra telefonia fissa e telefonia mobile</strong>, oltre ad andare verso un'<strong>integrazione
        sempre maggiore di funzionalit&agrave;</strong> (videoconference, software gestionali, applicativi di
    produttivit&agrave;).</p>
<p>Il Centralino telefonico tradizionale analogico viene sempre pi&ugrave; spesso sotituito dai Centralini VoIP di nuova
    generazione</p>
<ul>
    <li><strong>Centralino VoIP</strong> (on premise, installazione fisica presso sede del cliente)</li>
    <li><strong>Centralino Cloud</strong> (centralino VoIP esternalizzato presso datacenter)</li>
</ul>
<p>Per questa soluzione vedi la nostra offerta alla pagina <a href="<?= route('servizio', 'centralino-cloud') ?>">Centralino
        Cloud </a></p>
<p>Entrambi questi centralini utilizzano nella maggioranza dei casi <strong>il protocollo SIP (Session Initiation
        Protocol)</strong> che &egrave; lo standard di riferimento per la segnalazione telefonica adoperato per
    stabilire, modificare e concludere telefonate VoIP e permette che due dispositivi (telefoni IP, centralini...)
    coinvolti in una telefonata attraverso Internet riescano a dialogare tra di loro.</p>
<p>In realt&agrave; il SIP &egrave; un protocollo utilizzato per gestire lo <strong>scambio generale di contenuti
        multimediali</strong>,quindi oltre che per impostare le chiamate telefoniche via Internet la sessione
    multimediale pu&ograve; contenere qualsiasi multimedia inclusi dati, voce, video, immagini.</p>
