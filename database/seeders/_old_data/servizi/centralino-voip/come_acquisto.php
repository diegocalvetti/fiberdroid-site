<?php

return [

    'accordions' => [

        'Contattaci' => [
            'text' => "<p>Prima cosa contattaci e prendiamo un appuntamento per analizzare la tua attuale piattaforma telefonica.</p>"
        ],

        'Progetto e scelta tra diverse opzioni' => [
            'text' => "<p>In seguito ad un progetto che ti consegneremo avrai la possibilita’ di scegliere su diverse opzioni che ti forniremo.</p>"
        ],

        'Installazione e migrazione' => [
            'text' => "<p>L’installazione e migrazione verso la nuova piattaforma, sara’ pianificata in modo tale da ridurre al minimo i disservizi, in modo tale che il processo sia un'evoluzione e non una rivoluzione.</p>"
        ],

        'Attivazione sistema di monitoraggio' => [
            'text' => "<p>Una volta che tutto sara’ operativo, verra’ attivato il nostro sistema di monitoraggio che consentira’ in caso di problemi di continuare ad operare con l’impianto telefonico (con alcune limitazioni) grazie alla piattaforma cloud.</p>"
        ],
    ]
];
