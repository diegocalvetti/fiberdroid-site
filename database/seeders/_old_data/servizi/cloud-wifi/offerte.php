<?php

return [

    'cards' => [
        ['title' => "Da 1 a 5 antenne <br> connesse", 'icon' => asset('images/home/s-ricettive.svg'), 'features' => ['App su smartphone per il controllo in tempo reale', 'Assistenza tecnica inclusa', 'Aggiornamenti firmware inclusi', 'Supporto specialistisco per progettazione copertura'], 'price' => 14.99],
        ['title' => "Da 6 a 15 antenne <br> connesse", 'icon' => asset('images/home/s-ricettive.svg'), 'features' => ['App su smartphone per il controllo in tempo reale', 'Assistenza tecnica inclusa', 'Aggiornamenti firmware inclusi', 'Supporto specialistisco per progettazione copertura'], 'price' => 19.99],
        ['title' => "Da 16 a 30 antenne <br> connesse", 'icon' => asset('images/home/s-ricettive.svg'), 'features' => ['App su smartphone per il controllo in tempo reale', 'Assistenza tecnica inclusa', 'Aggiornamenti firmware inclusi', 'Supporto specialistisco per progettazione copertura'], 'price' => 24.99],
        [
            'title' => "Oltre 30 antenne <br> connesse",
            'icon' => asset('images/home/s-ricettive.svg'),
            'price_label' => "A PROGETTO",
            'features' => ['App su smartphone per il controllo in tempo reale', 'Assistenza tecnica inclusa', 'Aggiornamenti firmware inclusi', 'Supporto specialistisco per progettazione copertura'],
            'price' => 39.99,
            'sellable' => false,
        ],

    ]
];
