<?php

return [

    'cards' => [
        ['title' => "UAP-AC-LITE", 'icon' => asset('images/home/s-ricettive.svg'), 'features' => ['Antenna: dual band integrata, 3dBi a 2.4GHz e 3dBi a 5GHz', 'Wi-Fi Standards: 802.11 a/b/g/n/ac', 'Power Method: Passive Power over Ethernet (24V), (Pairs 4, 5+; 7, 8 Return), 802.3af/A', 'Network Interface: (1) 10/100/1000 Ethernet Port', 'Maximum TX Power 2.4 GHz: 20 dBm', 'Maximum TX Power 5 GHz: 20 dBm', 'Mounting: Wall/Ceiling (Kits Included)', 'PoE injector incluso in confezione.'], 'price' => 14.99],
        ['title' => "UAP-AC-LR", 'icon' => asset('images/home/s-ricettive.svg'), 'features' => ['Antenna: dual band integrata, 3dBi a 2.4GHz e 3dBi a 5GHz', 'Wi-Fi Standards: 802.11 a/b/g/n/ac', 'Power Method: Passive Power over Ethernet (24V), (Pairs 4, 5+; 7, 8 Return), 802.3af/A', 'Network Interface: (1) 10/100/1000 Ethernet Port', 'Maximum TX Power 2.4 GHz: 24 dBm', 'Maximum TX Power 5 GHz: 22 dBm', 'Mounting: Wall/Ceiling (Kits Included)', 'PoE injector incluso in confezione.'], 'price' => 19.99],
        ['title' => "UAP-AC-PRO", 'icon' => asset('images/home/s-ricettive.svg'), 'features' => ['Antenna: dual band integrata, 3dBi a 2.4GHz e 3dBi a 5GHz', 'Wi-Fi Standards: 802.11 a/b/g/n/ac', 'Power Method: Passive Power over Ethernet (24V), (Pairs 4, 5+; 7, 8 Return), 802.3af/A', 'Network Interface: (1) 10/100/1000 Ethernet Port', 'Maximum TX Power 2.4 GHz: 22 dBm', 'Maximum TX Power 5 GHz: 22 dBm', 'Mounting: Wall/Ceiling (Kits Included)', 'PoE injector incluso in confezione.'], 'price' => 24.99],
        ['title' => "UAP-AC-OUTDOOR", 'icon' => asset('images/home/s-ricettive.svg'), 'features' => ['Antenna: dual band integrata, 3dBi a 2.4GHz e 3dBi a 5GHz', 'Wi-Fi Standards: 802.11 a/b/g/n/ac', 'Power Method: Passive Power over Ethernet (24V), (Pairs 4, 5+; 7, 8 Return), 802.3af/A', 'Network Interface: (1) 10/100/1000 Ethernet Port', 'Maximum TX Power 2.4 GHz: 22 dBm', 'Maximum TX Power 5 GHz: 22 dBm', 'Mounting: Wall/Ceiling (Kits Included)', 'PoE injector incluso in confezione.'], 'price' => "A progetto"],

    ]
];
