<?php

return [
    'cards' => [
        ['title' => "JetFiber 1", 'image' => 'icon-jet', 'icon' => asset('images/home/jet-icon.png'), 'features' => [
            'Connettività simmetrica via cavo',
            'IP Statico Incluso',
            'Banda minima garantita 99%',
        ], 'price' => -1, 'sellable' => false],

        ['title' => "JetFiber 2", 'image' => 'icon-jet', 'icon' => asset('images/home/jet-icon.png'), 'features' => [
            'Connettività simmetrica via radio',
            'IP Statico Incluso',
            'Banda minima garantita 99%',
        ], 'price' => -1, 'sellable' => false],

        ['title' => "JetFiber 3", 'image' => 'icon-jet', 'icon' => asset('images/home/jet-icon.png'), 'features' => [
            '5 Servizi connettività radio o via cavo asimmetrici',
            'Numerazione telefonica GNR',
            'Centralino IP cloud',
        ], 'price' => -1, 'sellable' => false],
    ]
];
