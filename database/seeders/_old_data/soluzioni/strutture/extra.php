<?php

return [

    'cards' => [
        ['title' => "Piccoli alberghi e B&B", 'image' => 'icon-strutture-ricettive', 'features' => ['Connettività in fibra', 'FTTC o FTTH', 'Ip statico pubblico', 'Numerazione telefonica', 'Centralino VoIP', 'WiFi sicuro per Clienti'], 'price' => 69.90],
        ['title' => "Hotel e catene alberghiere", 'image' => 'icon-hotel', 'features' => ['Connettività in fibra', 'FTTC o FTTH', 'Ip statico pubblico', 'Backup con sim 4G', 'Centralino VoIP', 'WiFi sicuro per Clienti'], 'price' => 149.90]
    ]
];
