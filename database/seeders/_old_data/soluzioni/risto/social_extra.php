<?php

return [
    'cards' => [
        [
            'title' => "Offerta ristorazione",
            'image' => 'icon-ristorazione-e-retail',
            'features' => [
                    'Connettività in fibra',
                    'FTTC o FTTH',
                    'Ip statico pubblico',
                    'Centralino VoIP con Tuo numero telefonico',
                    'WiFi sicuro per Clienti'
                ],
            'price' => 69.90],
    ]

];
