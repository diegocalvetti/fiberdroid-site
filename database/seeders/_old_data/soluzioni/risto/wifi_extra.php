<?php

return [
    'accordions' => [
        'Aiuta a raccogliere informazioni utili' => [
            'text' => "<p>puoi dare accesso al WI-FI gratuito rispondendo a una domanda: (il tuo piatto preferito, perch&eacute; hai scelto noi, cosa ti piace del nostro ristorante&hellip;?) oppure con un login che raccolga dati e ti faccia capire se gli utenti tornano. Oltre il 60% degli utenti &egrave; disposto a dare la propria email per ricevere la connessione al WIFI gratuito.</p>",
            'isActive' => true,
        ],
        'Il cliente che si ferma di più consuma di più' => [
            'text' => "<p>in media oltre il 50%</p>",
            'isActive' => true,
        ],
        'il Wi-Fi allevia l\'attesa:' => [
            'text' => "<p>offri la connessione gi&agrave; all'ingresso del tuo ristorante, mentre i clienti attendono che si liberi il tavolo, magari facendo aprire la connessione sul tuo menu del giorno!</p>",
            'isActive' => true,
        ],
        'il 30% dei clienti controlla le tue offerte e recensioni' => [
            'text' => "<p>quando &egrave; collegata al tuo WIFI. un buon canale per le tue promozioni!</p>"
        ],
        'Per ottenere recensioni immediate' => [
            'text' => "<p>davanti a un piatto ben preparato il 65% dei clienti decideranno di condividerne l'immagine sui social taggando i loro amici. Se non c'&egrave; un'ottima connessione l'occasione di farsi conoscere si perder&agrave;.</p>"
        ],
        'Puoi sfruttare i social' => [
            'text' => "<p>usa le stories di Instagram per fare domande sul tuo servizio o come il cliente vorrebbe essere accolto nel ristorante, oppure offri il caff&egrave; a chi condivide i tuoi piatti sui social.</p>"
        ],
        'Puoi fidelizzare i clienti' => [
            'text' => "<p>chi si registra ti dar&agrave; informazioni sui suoi piatti preferiti, su cosa ama di più del tuo locale o su eventuali limiti nell'alimentazione, come celiaci, allergici, diabetici e tu puoi inviargli e-mail con offerte mirate sui loro gusti o con sconti per le serate meno affollate.</p>"
        ],
        'Il cliente ama essere connesso' => [
            'text' => "<p>l'84% degli utenti usa il WIFI in bar e negozi per leggere news e email e il 54% per restare in contatto con gli amici.</li></p>"
        ],
        'Perchè puoi gestire ordini dai tavoli' => [
            'text' => "<p>sia all'interno che all'esterno del locale velocizzando il servizio nelle ore di punta.</p>"
        ],
        'E c\'è chi lavora anche nel tempo libero' => [
            'text' => "<p>la possibilit&agrave; di rispondere a una email o controllare dei dati tra un piatto e l'altro diventa un vantaggio per il tuo cliente business.</li></p>"
        ],

    ]
];
