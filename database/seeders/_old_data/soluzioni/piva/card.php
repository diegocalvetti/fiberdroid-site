<?php

return [
    'cards' => [
        ['title' => "Offerta partite iva", 'image' => 'icon-piva', 'icon' => asset('images/home/s-ricettive.svg'), 'features' => [
            'Connettività in fibra',
            'FTTC o FTTH',
            'Ip statico pubblico',
            'Numero VoIP',
            'Interno telefonico centrale cloud',
        ], 'price' => 65.00],
    ]
];
