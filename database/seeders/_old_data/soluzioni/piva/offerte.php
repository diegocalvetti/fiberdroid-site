<?php

return [

    'accordions' => [
        'connessione veloce' => [
            'text' => "<p>caricare file, scaricare foto, inviare presentazioni, modificare documenti, tutto deve essere immediato per non farti perdere tempo e denaro.</p>",
            'isActive' => true,
        ],
        'libertà di movimento' => [
            'text' => "<p>puoi lavorare in città, in una sala d’attesa o in treno, al mare, mentre ti rilassi e dai un’occhiata allo stato dei lavori, in montagna dopo una bella passeggiata. Ovunque tu vada basta solo una buona connessione per il tuo business.</p>",
            'isActive' => true,
        ],
        'condividere i dati' => [
            'text' => "<p>in qualunque momento e in qualunque posto ti trovi con i tuoi clienti o collaboratori inviando o modificando in modo semplice e sicuro,  senza perdere tempo tra email e chat.</p>",
            'isActive' => true,
        ],
        'sicurezza dei backup' => [
            'text' => "<p>niente rischi di hard disk rotti, dati inaccessibili o cancellati per problemi tecnici, col nostro sistema di backup e la condivisione in cloud ogni documento è al sicuro per sempre.</p>",
            'isActive' => true,
        ],
        'bisogno di certezze' => [
            'text' => "<p><strong>certezza</strong> di essere seguito, di avere un&rsquo;<strong>assistenza veloce ed efficiente</strong> e non trovarsi abbandonato se la connessione cade, se qualcuno ha staccato un cavo, se ha dimenticato la password&hellip; perch&eacute; il tempo va usato per il business o per se stessi, non per impazzire per lentezze burocratiche!</p>",
            'isActive' => true,
        ],
    ]

];
