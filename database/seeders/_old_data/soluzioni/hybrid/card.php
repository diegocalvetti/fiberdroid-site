<?php

return [
    'cards' => [
        ['title' => "Hybrid 5G <br><b>30</b>", 'image' => '5g-icon', 'icon' => asset('images/home/jet-icon.png'), 'features' => [
            '5G Download 300 Mega Upload 30 Mega',
            'FTTC Download 30 Mega Upload 3 Mega',
            '2 IP Statici pubblici',
            'Router firewall in comodato d\'uso',
        ], 'price' => 99.90, 'sellable' => false],

        ['title' => "Hybrid 5G <br><b>50</b>", 'image' => '5g-icon', 'icon' => asset('images/home/jet-icon.png'), 'features' => [
            '5G Download 300 Mega Upload 30 Mega',
            'FTTC Download 50 Mega Upload 10 Mega',
            '2 IP Statici pubblici',
            'Router firewall in comodato d\'uso',
        ], 'price' => 104.90, 'sellable' => false],

        ['title' => "Hybrid 5G <br><b>100</b>", 'image' => '5g-icon', 'icon' => asset('images/home/jet-icon.png'), 'features' => [
            '5G Download 300 Mega Upload 30 Mega',
            'FTTC Download 100 Mega Upload 20 Mega',
            '2 IP Statici pubblici',
            'Router firewall in comodato d\'uso',
        ], 'price' => 109.90, 'sellable' => false],

        ['title' => "Hybrid 5G <br><b>200</b>", 'image' => '5g-icon', 'icon' => asset('images/home/jet-icon.png'), 'features' => [
            '5G Download 300 Mega Upload 30 Mega',
            'FTTC Download 200 Mega Upload 20 Mega',
            '2 IP Statici pubblici',
            'Router firewall in comodato d\'uso',
        ], 'price' => 119.90, 'sellable' => false],
    ]
];
