<?php

namespace Database\Seeders;

use App\Models\Crew;
use App\Models\Image;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CrewSeeder extends Seeder
{

    protected $crews = [
        [
            'name' => 'André Claude',
            'role' => 'Ceo, Sales director, comandante.',
            'photo' => 'andre',
            'order' => 1,
            'url_linkedin' => 'https://www.linkedin.com/in/andre-claude-benin/',
            'content' => '<p>Con Fiberdroid ho realizzato il mio sogno di Libert&agrave;. Libert&agrave; di scelta tecnologica, libert&agrave; di cambiamento. Perch&eacute; in un mondo in costante Evoluzione siamo chiamati, costantemente, ad imparare e ad adattarci a nuove soluzioni.</p><p>E&rsquo; importante vedere il mondo da nuove prospettive e lasciare spazio all&rsquo;ispirazione quindi nel mio tempo libero mi dedico al volo in aliante.</p>'
        ],
        [
            'name' => 'Katia',
            'role' => 'Amministrazione, hr',
            'order' => 2,
            'photo' => 'katia',
            'url_linkedin' => 'https://www.linkedin.com/in/katia-lanzani/',
            'content' => '<p>Ordine, precisione sono le parole chiave per una buona gestione contabile. Mi chiamano catterpillar perche’ non mollo fino ad ottenere il risultato. Amo viaggiare.</p>'
        ],
        [
            'name' => 'Massimo',
            'role' => 'Attivazioni, Supporto tecnico',
            'photo' => 'massimo',
            'order' => 3,
            'url_linkedin' => 'https://www.linkedin.com/in/massimo-guarnera-b9006546/',
            'content' => '<p>Un buon staff necessita di istruzioni precise: l&rsquo;organizzazione &egrave; fondamentale per ridurre al minimo i tempi di intervento e ogni rischio di errore.</p><p>Nei momenti di svago mi rilasso con lunghe passeggiate a cavallo.</p>'
        ],
        [
            'name' => 'Jenny',
            'role' => 'Impiegata amministrativa',
            'photo' => 'jenny',
            'order' => 4,
            'url_linkedin' => 'https://www.linkedin.com/in/jenny-bianchi-01829625a/',
            'content' => '<p>Precisione e Passione sono i miei punti forti. Sono una persona meticolosa e attenta ai dettagli, in grado di individuare anche le sfumature piu sottili. La mia passione per i viaggi riflette il mio spirito avventuroso e la costante ricerca di nuove prospettive, dimostrando un\'apertura mentale verso l\'innovazione e la diversità.</p>',
        ],
        [
            'name' => 'Gabriele',
            'role' => 'Project Manager',
            'order' => 5,
            'photo' => 'gabriele',
            'url_linkedin' => 'https://www.linkedin.com/in/angelo-gabriele-pallotta-6b0334200/',
            'content' => '<p>La disponibilità e la fiducia sono la base per avere un rapporto solido e duraturo con il cliente. Il mio punto di forza è la continua voglia di imparare e aiutare le persone a risolvere i loro problemi.
</p><p>Mi appassiona tutto ciò che riguarda il mondo del cinema e della lettura.</p>'],
        [
            'name' => 'Diego',
            'role' => 'Sviluppo web',
            'photo' => 'diego',
            'order' => 7,
            'url_linkedin' => 'https://www.linkedin.com/in/diegocalvetti/',
            'content' => '<p>Assicuro ogni giorno al team che grafica e funzioni dei nostri sistemi siano sempre corretti e a supporto dell\'utente.</p><p>Mi definiscono un Apple-dipendente. Ma perch&eacute; esiste altro all&rsquo;infuori della mela?</p>'
        ],
        [
            'name' => 'Luka',
            'role' => 'Sviluppo web',
            'photo' => 'person',
            'order' => 8,
            'url_linkedin' => 'https://www.linkedin.com/in/luka-popadi%C4%87-lp36942',
            'content' => '<p>Ogni codice pu&ograve; essere migliorato con nuove funzionalit&agrave;, ed &egrave; questo il mio compito in FiberDroid. Continuo a studiare le novit&agrave; nel campo delle tecnologie e in ambito scientifico.</p>'
        ],
        /*[
            'name' => 'Ubaldo e leon',
            'role' => 'Sviluppo IOT, app e nuove tecnologie',
            'order' => 9,
            'photo' => 'ubaldo',
            'content' => '<p>La mia citazione preferita &egrave; &ldquo;Se lo posso sognare, lo posso fare.&rdquo; Ogni nuova tecnologia &egrave; l&rsquo;unione di un sogno alla caparbiet&agrave; e il coraggio di osare. Adoro fare lunghe passeggiate con il mio cane, allontanandomi dal caos della citta&rsquo;.</p>'
        ],

        [
            'name' => 'Alessandro',
            'role' => 'Sviluppo software operativo',
            'order' => 8,
            'photo' => 'ale',
            'content' => '<p>Ogni codice pu&ograve; essere migliorato con nuove funzionalit&agrave;, ed &egrave; questo il mio compito in FiberDroid. Continuo a studiare le novit&agrave; nel campo delle tecnologie e in ambito scientifico.</p>'
        ],
        [
            'name' => 'Gabriele',
            'role' => 'Supporto tecnico',
            'order' => 6,
            'photo' => 'gabriele',
            'content' => '<p>La disponibilità e la fiducia sono la base per avere un rapporto solido e duraturo con il cliente. Il mio punto di forza è la continua voglia di imparare e aiutare le persone a risolvere i loro problemi.
</p><p>Mi appassiona tutto ciò che riguarda il mondo del cinema e della lettura.</p>'],
        [
            'name' => 'Raffaella',
            'role' => 'Seo specialist',
            'order' => 10,
            'photo' => 'raffaella',
            'content' => '
                    <p>Informatica convertita alla SEO amo l\'analisi e la psicologia.
Mi occupo della visibilità del sito e dei suoi contenuti.</p><p>
Mi piace leggere o scrivere racconti fantasy per evadere in mondi sconosciuti.
Nel tempo libero scopro posti nuovi o cammino in montagna.
Felice di contribuire all\'avventura di Fiberdroid!</p>
                    '
        ],
        [
            'name' => 'Gabriele',
            'role' => 'Grafica e concept',
            'order' => 11,
            'photo' => 'gabriele-grafico',
            'content' => '<p>Dietro ogni progetto che funzioni c’è un design che lo rappresenti e lo caratterizzi con il giusto valore.
</p><p>Creo il bello delle cose orientate al corretto funzionamento per chi le usa e che rispettino le esigenze di chi le propone. Nel tempo libero dipingo ritratti, la creatività è la mia vita</p>'
        ],*/
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->crews as $crewData) {

            $crewData['image_id'] = Image::getId($crewData['photo']);
            unset($crewData['photo']);
            DB::table('crews')->insert($crewData);
        }
    }
}
