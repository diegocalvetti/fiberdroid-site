<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiberdroidServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fiberdroid_services', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->text('description');
            $table->text('short_description');
            $table->string('title')->nullable();
            $table->string('url');
            $table->string('bg');
            $table->string('icon');
            $table->string('quiz_icon')->nullable();
            $table->string('quiz_gradient')->nullable();
            $table->string('title_plans')->nullable();
            $table->string('type');
            $table->string('tag')->nullable();
            $table->string('model')->nullable();
            $table->boolean('active')->default(true);

            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();

            $table->json('json_plans')->nullable();
            $table->json('json_slider')->nullable();
            $table->json('json_views')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fiberdroid_services');
    }
}
