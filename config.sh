#!/usr/bin/env bash
php artisan cache:clear
php artisan config:clear
php artisan config:cache
#php artisan reset
composer install
composer dump-autoload
# chcon -R -t httpd_sys_rw_content_t storage
# chcon -R -t httpd_sys_rw_content_t bootstrap/cache
